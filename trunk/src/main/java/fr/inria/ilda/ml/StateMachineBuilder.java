/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import com.sun.glass.ui.*;
import fr.inria.ilda.ml.StateMachine.Action;
import fr.inria.ilda.ml.StateMachine.*;
import fr.inria.ilda.ml.StateMachine.Event;
import fr.inria.ilda.ml.queryWidgets.ShapeFileTableSummary;
import org.lwjgl.Sys;

import javax.swing.*;
import javax.vecmath.Vector3f;
import java.awt.*;
import java.awt.Cursor;
import java.awt.event.KeyEvent;

/**
 * Created by mjlobo on 07/08/15.
 */
public class StateMachineBuilder {

    static String navigationMode = "navigationMode";
    static String areaSelected = "areaSelected";
    static String drawingMode = "drawingMode";
    static String vectorSelected = "vectorSelected";
    static String movingArea = "movingArea";
    static String drawing = "drawing";
    static String panning = "panning";
    static String movingVector ="movingVector";
    static String selectionMode = "selectionMode";
    static String backgroundSelected = "backgroundSelected";
    static String drawingCircle = "drawingCircle";
    static String circleMode = "circleMode";
    static String overlay = "overlay";
    static String squareMode = "squareMode";
    static String drawingSquare = "drawSquare";
    static String magicWand = "magicWand";
    static String previewingRaster = "previewingRaster";

    boolean mousePressed = false;
    boolean spaceBarPressed = false;
    String previousState = null;

    MapLayerViewer mapLayerViewer;
    public StateMachineBuilder(MapLayerViewer mapLayerViewer) {
        this.mapLayerViewer = mapLayerViewer;
    }

    public StateMachine getExpeStateMachine() {
        StateMachine expeStateMachine = new StateMachine();
        expeStateMachine
                .addState(navigationMode, new State()
                                .addAction(StateChangeEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        //System.out.println("entering to navigation mode");
                                        mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.NAVIGATION_MODE);
                                        mapLayerViewer.deselectAll();
                                        //mapLayerViewer.getInspector().backgroundSelected();
                                        mapLayerViewer.getInspector().notShow();
                                        mapLayerViewer.setCursor(mapLayerViewer.NAVIGATION_MODE);
                                        return null;
                                    }
                                })
                                .addAction(KeyPressedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyPressedEvent keyEvent = (KeyPressedEvent) event;
                                        if (keyEvent.getKeyChar() == 'd') {
                                            return drawingMode;
                                        }
                                        if (keyEvent.getKeyChar() == 's') {
                                            return selectionMode;
                                        }
                                        if (keyEvent.getKeyChar() == 'i') {
                                            mapLayerViewer.getInspectorFrame().setVisible(true);
                                            mapLayerViewer.getInspectorFrame().pack();
                                        }
                                        if (keyEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
                                            return selectionMode;
                                        }
                                        if (keyEvent.getKeyChar() == 'f') {
                                            mapLayerViewer.getCanvas().printFPS();
                                        }
                                        return null;
                                    }
                                })
                                .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                        if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = false;
                                        }
                                        return null;
                                    }
                                })
                                .addAction(ModeButtonPressedEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        //System.out.println("modebuttonpressed!");
                                        ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                        mapLayerViewer.getCanvas().requestFocus();
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.DRAW_MODE) {
                                            return drawingMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.SELECT_MODE) {
                                            return selectionMode;
                                        }
                                        if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                            //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                            if (mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode())) {
                                                return previewingRaster;
                                            }
                                            if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                                return overlay;
                                            }
                                        }
                                        if (modeButtonPressedEvent.getMode() == mapLayerViewer.CREATE_CIRCLE) {
                                            return circleMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.CREATE_SQUARE) {
                                            return squareMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.MAGIC_WAND) {
                                            return magicWand;
                                        }
                                        return null;
                                    }
                                })


                                .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                                        mapLayerViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                                        previousState = navigationMode;
                                        return panning;
                                    }
                                })
                                .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                        if (mouseEvent.getWheelRotation() > 0) {
                                            mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                            //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                        } else {
                                            mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                            //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                        }

                                        return null;
                                    }
                                })
                                .addAction(MouseEnteredEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.getCanvas().requestFocus();
                                        return null;
                                    }
                                })
                                .addAction(CheckBoxEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.toggleLayerVisibility(((CheckBoxEvent) event).getText());
                                        return null;
                                    }
                                })
                )

                .addState(panning, new State()
                        .addAction(MouseMoveEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                MouseMoveEvent mouseEvent = (MouseMoveEvent) event;
                                mapLayerViewer.pan(mouseEvent.getX(), mouseEvent.getY());
                                return panning;
                            }
                        })
                        .addAction(LeftMouseUpEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                //return navigationMode;
                                mousePressed = false;
                                return previousState;
                            }
                        })
                        .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                    spaceBarPressed = false;
                                }
                                return null;
                            }
                        })
                        .addAction(StateChangeEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                //System.out.println("Entered state 'panning'");
                                //mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.NAVIGATION_MODE);
                                mapLayerViewer.setCursor(MapLayerViewer.NAVIGATION_MODE);
                                return null;
                            }
                        }))
                .addState(drawingMode, new State()
                                .addAction(StateChangeEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.DRAW_MODE);
                                        mapLayerViewer.deselectAll();
                                        mapLayerViewer.getInspector().notShow();
                                        mapLayerViewer.setCursor(mapLayerViewer.DRAW_MODE);
                                        return null;
                                    }
                                })
                                .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                                        mousePressed = true;
                                        if (spaceBarPressed) {
                                            //System.out.println("spaceBarPressed!");
                                            previousState = drawingMode;
                                            mapLayerViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                                            return panning;
                                        }
                                        mapLayerViewer.newDrawnGeoElement(mouseEvent.getX(), mouseEvent.getY());
                                        return drawing;
//
                                    }
                                })
                                .addAction(MouseEnteredEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.getCanvas().requestFocus();
                                        return null;
                                    }
                                })
                                .addAction(KeyPressedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyPressedEvent keyPressedEvent = (KeyPressedEvent) event;
                                        //System.out.println("key pressed!");
                                        if (keyPressedEvent.getKeyChar() == 's') {
                                            return selectionMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'n') {
                                            return navigationMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'd') {
                                            return drawingMode;
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            //System.out.println("space bar pressed!");
                                            spaceBarPressed = true;
                                            mapLayerViewer.setCursor(MapLayerViewer.NAVIGATION_MODE);
//                                            if (mousePressed) {
//                                                previousState = drawingMode;
//                                                return panning;
//                                            }
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'i') {
                                            mapLayerViewer.getInspectorFrame().setVisible(true);
                                            mapLayerViewer.getInspectorFrame().pack();
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
                                            return navigationMode;
                                        }

                                        return null;
                                    }
                                })
                                .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                        if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = false;
                                            mapLayerViewer.setCursor(MapLayerViewer.DRAW_MODE);
                                        }
                                        return null;
                                    }
                                })
                                .addAction(ModeButtonPressedEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                        mapLayerViewer.getCanvas().requestFocus();
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.DRAW_MODE) {
                                            return drawingMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.SELECT_MODE) {
                                            return selectionMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.NAVIGATION_MODE) {
                                            return navigationMode;
                                        }
                                        if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                            //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                            mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode());
                                            if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                                return overlay;
                                            }
                                            return navigationMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == mapLayerViewer.CREATE_CIRCLE) {
                                            return circleMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.CREATE_SQUARE) {
                                            return squareMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.MAGIC_WAND) {
                                            return magicWand;
                                        }
                                        return null;
                                    }
                                })
                                .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                        if (mouseEvent.getWheelRotation() > 0) {
                                            mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                            //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                        } else {
                                            mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                            //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                        }

                                        return null;
                                    }
                                })

                )
                .addState(circleMode, new State()
                        .addAction(StateChangeEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.CREATE_CIRCLE);
                                mapLayerViewer.getInspector().notShow();
                                mapLayerViewer.setCursor(MapLayerViewer.CREATE_CIRCLE);
                                return null;
                            }
                        })
                        .addAction(MouseEnteredEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                mapLayerViewer.getCanvas().requestFocus();
                                return null;
                            }
                        })
                        .addAction(ModeButtonPressedEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                mapLayerViewer.getCanvas().requestFocus();
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.DRAW_MODE) {
                                    return drawingMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.SELECT_MODE) {
                                    return selectionMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.NAVIGATION_MODE) {
                                    return navigationMode;
                                }
                                if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                    //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                    if (mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode())){
                                        return previewingRaster;
                                    }
                                    if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                        return overlay;
                                    }
                                    return navigationMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.CREATE_SQUARE) {
                                    return squareMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.MAGIC_WAND) {
                                    return magicWand;
                                }

                                return null;
                            }
                        })
                        .addAction(KeyPressedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                KeyPressedEvent keyPressedEvent = (KeyPressedEvent) event;
                                if (keyPressedEvent.getKeyChar() == 's') {
                                    return selectionMode;
                                }
                                if (keyPressedEvent.getKeyChar() == 'n') {
                                    return navigationMode;
                                }
                                if (keyPressedEvent.getKeyChar() == 'd') {
                                    return drawingMode;
                                }
                                if (keyPressedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                    spaceBarPressed = true;
                                    mapLayerViewer.setCursor(MapLayerViewer.NAVIGATION_MODE);
//                                    if (mousePressed) {
//                                        previousState = circleMode;
//                                        return panning;
//                                    }
                                }
                                if (keyPressedEvent.getKeyChar() == 'i') {
                                    mapLayerViewer.getInspectorFrame().setVisible(true);
                                    mapLayerViewer.getInspectorFrame().pack();
                                }
                                if (keyPressedEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
                                    return navigationMode;
                                }

                                return null;
                            }
                        })
                        .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                    spaceBarPressed = false;
                                    mapLayerViewer.setCursor(MapLayerViewer.CREATE_CIRCLE);
                                }
                                return null;
                            }
                        })
                        .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                if (mouseEvent.getWheelRotation() > 0) {
                                    mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                    //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                } else {
                                    mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                    //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                }

                                return null;
                            }
                        })
                        .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                                mousePressed = true;
                                if (spaceBarPressed) {
                                    previousState = circleMode;
                                    mapLayerViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                                    return panning;
                                }
                                mapLayerViewer.newCircleDrawnGeoElement(mouseEvent.getX(), mouseEvent.getY());
                                return drawingCircle;
                            }
                        }))
                .addState(squareMode, new State()
                        .addAction(StateChangeEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.CREATE_SQUARE);
                                mapLayerViewer.getInspector().notShow();
                                mapLayerViewer.setCursor(MapLayerViewer.CREATE_SQUARE);
                                return null;
                            }
                        })
                        .addAction(MouseEnteredEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                mapLayerViewer.getCanvas().requestFocus();
                                return null;
                            }
                        })
                        .addAction(ModeButtonPressedEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                mapLayerViewer.getCanvas().requestFocus();
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.DRAW_MODE) {
                                    return drawingMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.SELECT_MODE) {
                                    return selectionMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.NAVIGATION_MODE) {
                                    return navigationMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.CREATE_CIRCLE) {
                                    return circleMode;
                                }
                                if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                    //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                    if (mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode())){
                                        return previewingRaster;
                                    }
                                    if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                        return overlay;
                                    }
                                    return navigationMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.CREATE_SQUARE) {
                                    return squareMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.MAGIC_WAND) {
                                    return magicWand;
                                }

                                return null;
                            }
                        })
                        .addAction(KeyPressedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                KeyPressedEvent keyPressedEvent = (KeyPressedEvent) event;
                                if (keyPressedEvent.getKeyChar() == 's') {
                                    return selectionMode;
                                }
                                if (keyPressedEvent.getKeyChar() == 'n') {
                                    return navigationMode;
                                }
                                if (keyPressedEvent.getKeyChar() == 'd') {
                                    return drawingMode;
                                }
                                if (keyPressedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                    spaceBarPressed = true;
                                    mapLayerViewer.setCursor(MapLayerViewer.NAVIGATION_MODE);
//                                    if (mousePressed) {
//                                        previousState = circleMode;
//                                        return panning;
//                                    }
                                }
                                if (keyPressedEvent.getKeyChar() == 'i') {
                                    mapLayerViewer.getInspectorFrame().setVisible(true);
                                    mapLayerViewer.getInspectorFrame().pack();
                                }
                                if (keyPressedEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
                                    return navigationMode;
                                }

                                return null;
                            }
                        })
                        .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                    spaceBarPressed = false;
                                    mapLayerViewer.setCursor(MapLayerViewer.CREATE_SQUARE);
                                }
                                return null;
                            }
                        })
                        .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                if (mouseEvent.getWheelRotation() > 0) {
                                    mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                    //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                } else {
                                    mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                    //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                }

                                return null;
                            }
                        })
                        .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                                mousePressed = true;
                                if (spaceBarPressed) {
                                    previousState = squareMode;
                                    mapLayerViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                                    return panning;
                                }
                                mapLayerViewer.newSquareDrawnGeoElement(mouseEvent.getX(), mouseEvent.getY());
                                return drawingSquare;
                            }
                        }))
                .addState(drawingCircle, new State()
                                .addAction(LeftMouseUpEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.finishDrawnGeoElement();
                                        mousePressed = false;
                                        return selectionMode;
                                    }
                                })
                                .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                        if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = false;
                                            //return previousState;
                                        }
                                        return null;
                                    }
                                })
                                .addAction(MouseMoveEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseMoveEvent mouseEvent = (MouseMoveEvent) event;
                                        mapLayerViewer.drawCircleDrawnGeoElement(mouseEvent.getX(), mouseEvent.getY());
                                        return drawingCircle;
                                    }
                                })
                )
                .addState(drawingSquare, new State()
                                .addAction(LeftMouseUpEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.finishDrawnGeoElement();
                                        mousePressed = false;
                                        return selectionMode;
                                    }
                                })
                                .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                        if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = false;
                                            //return previousState;
                                        }
                                        return null;
                                    }
                                })
                                .addAction(MouseMoveEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseMoveEvent mouseEvent = (MouseMoveEvent) event;
                                        mapLayerViewer.drawSquareDrawnGeoElement(mouseEvent.getX(), mouseEvent.getY());
                                        return drawingSquare;
                                    }
                                })
                )

                .addState(drawing, new State()
                                .addAction(MouseMoveEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseMoveEvent mouseEvent = (MouseMoveEvent) event;
                                        mapLayerViewer.addPointDrawnGeoElement(mouseEvent.getX(), mouseEvent.getY());
                                        return drawing;
                                    }
                                })
                                .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                        if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = false;
                                            //return previousState;
                                        }
                                        return null;
                                    }
                                })
                                .addAction(LeftMouseUpEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mousePressed = false;
                                        mapLayerViewer.finishDrawnGeoElement();
                                        //return navigationMode;
                                        return selectionMode;
                                    }
                                })
                )
                .addState(selectionMode, new State()
                                .addAction(StateChangeEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        //System.out.println("enter selection mode");
                                        mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.SELECT_MODE);
                                        mapLayerViewer.setCursor(Cursor.getDefaultCursor());
                                        return null;
                                    }
                                })
                                .addAction(MouseEnteredEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.getCanvas().requestFocus();
                                        return null;
                                    }
                                })
                                .addAction(MouseEnteredEvent.class, "inspector", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        if (mapLayerViewer.getInElement() != null) ;
                                        {
                                            mapLayerViewer.getInElement().outElement();
                                            mapLayerViewer.setInElement(null);
                                        }
                                        return null;
                                    }
                                })
                                .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        //System.out.println("click!");
                                        LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                                        mousePressed = true;
                                        if (spaceBarPressed) {
                                            previousState = selectionMode;

                                            mapLayerViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                                            return panning;
                                        }
                                        mapLayerViewer.selectElementAt(mouseEvent.getX(), mouseEvent.getY());
                                        if (mapLayerViewer.getSelected() != null) {
                                            if (mapLayerViewer.getSelected() instanceof DrawnGeoElement) {
                                                return movingArea;
                                            } else {
                                                return movingVector;
                                            }

                                        } else {
                                            //return navigationMode;
                                            return backgroundSelected;
                                        }

                                    }

                                })
                                .addAction(MouseMoveEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseMoveEvent mouseMoveEvent = (MouseMoveEvent) event;
                                        mapLayerViewer.suggestSelect(mouseMoveEvent.getX(), mouseMoveEvent.getY());
                                        //mapLayerViewer.suggestSelected(mouseMoveEvent.getX(), mouseMoveEvent.getY());
                                        return null;
                                    }
                                })
                                .addAction(KeyPressedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyPressedEvent keyPressedEvent = (KeyPressedEvent) event;
                                        //System.out.println("Key code "+keyPressedEvent.getKeyCode());
                                        if (keyPressedEvent.getKeyChar() == 'd') {
                                            return drawingMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'n') {
                                            return navigationMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 's') {
                                            return selectionMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'q') {
                                            //System.out.println("q!");
                                            mapLayerViewer.getShapeFileQuery().pack();
                                            mapLayerViewer.getShapeFileQuery().setVisible(true);
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'i') {
                                            mapLayerViewer.getInspectorFrame().setVisible(true);
                                            mapLayerViewer.getInspectorFrame().pack();
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = true;
                                            mapLayerViewer.setCursor(MapLayerViewer.NAVIGATION_MODE);
                                            //System.out.println("space bar pressed!");
//                                            if (mousePressed) {
//                                                previousState = selectionMode;
//                                                return panning;
//                                            }
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
                                            return navigationMode;
                                        }

                                        return selectionMode;
                                    }
                                })
                                .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                        if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            mapLayerViewer.setCursor(Cursor.getDefaultCursor());
                                            spaceBarPressed = false;
                                        }
                                        return null;
                                    }
                                })
                                .addAction(ModeButtonPressedEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                        if (modeButtonPressedEvent.getMode() == mapLayerViewer.DRAW_MODE) {
                                            return drawingMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == mapLayerViewer.SELECT_MODE) {
                                            return selectionMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == mapLayerViewer.NAVIGATION_MODE) {
                                            return navigationMode;
                                        }
                                        if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                            //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                            if (mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode())){
                                                return previewingRaster;
                                            }
                                            if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                                return overlay;
                                            }
                                            return navigationMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.CREATE_SQUARE) {
                                            return squareMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == mapLayerViewer.CREATE_CIRCLE) {
                                            return circleMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.MAGIC_WAND) {
                                            return magicWand;
                                        }
                                        return null;
                                    }
                                })
                                .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                        if (mouseEvent.getWheelRotation() > 0) {
                                            mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                            //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                        } else {
                                            mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                            //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                        }

                                        return null;
                                    }
                                })
                )
                .addState(movingArea, new State()
                        .addAction(StateChangeEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                mapLayerViewer.currentMovingGeoElement = mapLayerViewer.getSelected();
                                mapLayerViewer.setCursor(Cursor.getDefaultCursor());
                                mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.SELECT_MODE);
                                mapLayerViewer.updateInspectorDrawn();
                                return null;
                            }
                        })
                        .addAction(MouseMoveEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                MouseMoveEvent mouseEvent = (MouseMoveEvent) event;
                                mapLayerViewer.moveDrawnObject(mouseEvent.getX(), mouseEvent.getY());
                                return movingArea;
                            }
                        })
                        .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                    spaceBarPressed = false;
                                    //return previousState;
                                }
                                return null;
                            }
                        })
                        .addAction(LeftMouseUpEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                mapLayerViewer.finishMovingDrawnObject();
                                mousePressed = false;
                                return areaSelected;
                            }
                        }))
                .addState(areaSelected, new State()
                                .addAction(StateChangeEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
//                                    if (selectedVectorSelection != null) {
//                                        updateShapeFileTableSummary(selectedVectorSelection);
//                                    }
//                                    else {
//                                        resetShapeFileTableSummaries();
//                                        resetLayerCheckboxs();
//                                    }
                                        //System.out.println("enter area selected");
                                        mapLayerViewer.setCursor(Cursor.getDefaultCursor());
                                        mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.SELECT_MODE);
                                        mapLayerViewer.updateInspectorDrawn();
                                        mapLayerViewer.updateShapeFileTableSummary();
                                        return null;
                                    }
                                })
                                .addAction(MouseEnteredEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.getCanvas().requestFocus();
                                        return null;
                                    }
                                })
                                .addAction(MouseEnteredEvent.class, "inspector", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        if (mapLayerViewer.getInElement() != null) {
                                            mapLayerViewer.getInElement().outElement();
                                            mapLayerViewer.setInElement(null);
                                        }
                                        return null;
                                    }
                                })

                                .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                                        mousePressed = true;
                                        if (spaceBarPressed) {
                                            previousState = areaSelected;
                                            mapLayerViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                                            return panning;
                                        }

                                        mapLayerViewer.selectElementAt(mouseEvent.getX(), mouseEvent.getY());
                                        if (mapLayerViewer.getSelected() != null) {
                                            if (mapLayerViewer.getSelected() instanceof DrawnGeoElement) {
                                                mapLayerViewer.updateInspectorDrawn();

                                                return movingArea;
                                            } else {
                                                return movingVector;
                                            }

                                        } else {
                                            //return navigationMode;
                                            return backgroundSelected;
                                        }
                                    }
                                    //}
//                            else {
//                                selectionManager.selectElementsInVectorSelection(selectedVectorSelection, (DrawnGeoElement)selected, selectionsDrawn);
//                                return areaSelected;
//                            }


                                })

                                .addAction(WindowFocusLostEvent.class, "shapeFileQuery", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
//                                        querySelectedLayer = null;
                                        return null;
                                    }
                                })
                                .addAction(WindowFocusGainedEvent.class, "shapeFileQuery", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {

                                        return null;
                                    }
                                })
                                .addAction(KeyPressedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyPressedEvent keyPressedEvent = (KeyPressedEvent) event;
                                        if (keyPressedEvent.getKeyChar() == 'd') {
                                            return drawingMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'n') {
                                            return navigationMode;
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_UP) {
                                            mapLayerViewer.moveLensUp();
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_DOWN) {
                                            mapLayerViewer.moveLensDown();
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                                            mapLayerViewer.deleteDrawnGeoElement((DrawnGeoElement) mapLayerViewer.getSelected());
                                            return navigationMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'q') {
                                            mapLayerViewer.updateShapeFileTableSummary();
                                            mapLayerViewer.getShapeFileQuery().pack();
                                            mapLayerViewer.getShapeFileQuery().setVisible(true);
                                            return null;
                                        }

                                        if (keyPressedEvent.getKeyChar() == 'i') {
                                            mapLayerViewer.getInspectorFrame().setVisible(true);
                                            mapLayerViewer.getInspectorFrame().pack();
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = true;
                                            mapLayerViewer.setCursor(MapLayerViewer.NAVIGATION_MODE);
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
                                            return navigationMode;
                                        }
                                        return areaSelected;
                                    }
                                })
                                .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                        if (mouseEvent.getWheelRotation() > 0) {
                                            mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                        } else {
                                            mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                        }

                                        return null;
                                    }
                                })
                                .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                        if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = false;
                                            mapLayerViewer.setCursor(Cursor.getDefaultCursor());
                                        }
                                        return null;
                                    }
                                })
                                .addAction(ModeButtonPressedEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                        mapLayerViewer.getCanvas().requestFocus();
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.DRAW_MODE) {
                                            return drawingMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.SELECT_MODE) {
                                            return selectionMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.NAVIGATION_MODE) {
                                            return navigationMode;
                                        }
                                        if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                            //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                            if (mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode())){
                                                return previewingRaster;
                                            }
                                            if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                                return overlay;
                                            }
                                            return navigationMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == mapLayerViewer.CREATE_CIRCLE) {
                                            return circleMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.CREATE_SQUARE) {
                                            return squareMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.MAGIC_WAND) {
                                            return magicWand;
                                        }
                                        return null;
                                    }
                                })
                                .addAction(MouseMoveEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseMoveEvent mouseMoveEvent = (MouseMoveEvent) event;
                                        mapLayerViewer.suggestSelect(mouseMoveEvent.getX(), mouseMoveEvent.getY());
                                        return null;
                                    }
                                })
                                .addAction(CheckBoxEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        JCheckBox checkBox = (JCheckBox) (((CheckBoxEvent) event).getTarget());
                                        mapLayerViewer.fireCheckBoxActionDrawn(checkBox.getActionCommand(), checkBox.isSelected());
                                        return null;
                                    }
                                })
                                .addAction(ColorChangedEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        ColorChangedEvent colorChangedEvent = (ColorChangedEvent) event;
                                        Vector3f newColor = new Vector3f((float) (colorChangedEvent.getColor().getRed() / 255.0), (float) (colorChangedEvent.getColor().getGreen() / 255.0), (float) (colorChangedEvent.getColor().getBlue() / 255.0));
                                        if (colorChangedEvent.getTarget().getName() == MapLayerViewer.FILL_COLOR) {
                                            mapLayerViewer.changeColorInDrawn(newColor);
                                        } else {
                                            mapLayerViewer.getSelected().setOutlineColor(newColor);
                                        }
                                        return null;
                                    }
                                })
                                .addAction(ComboBoxEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        ComboBoxEvent comboBoxEvent = (ComboBoxEvent) event;
                                        if (comboBoxEvent.getSelectedText().equals(MapLayerViewer.FILL_COLOR)) {
                                            mapLayerViewer.changeColorInDrawn();
                                        } else {
                                            mapLayerViewer.changeTextureInDrawn(comboBoxEvent.getSelectedText());
                                        }
                                        return null;
                                    }
                                })
                                .addAction(TextFieldEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        TextFieldEvent textFieldEvent = (TextFieldEvent) event;
                                        if (textFieldEvent.getText().length() > 0) {
                                            Double d = null;
                                            try {
                                                d = Double.valueOf(textFieldEvent.getText());
                                            } catch (NumberFormatException e) {
                                                e.printStackTrace();
                                            }
                                            if (d != null) {
                                                if (textFieldEvent.getTarget().getName().equals(MapLayerViewer.MARGIN)) {
                                                    mapLayerViewer.bufferSelectedDrawn(d);
                                                } else if (textFieldEvent.getTarget().getName().equals(MapLayerViewer.ALPHA)) {
                                                    mapLayerViewer.changeAlphaInDrawn(d);
                                                }
                                            }
                                        }
                                        return null;
                                    }
                                })
                                .addAction(LayerOptionChanged.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        LayerOptionChanged layerOptionChanged = (LayerOptionChanged) event;
                                        mapLayerViewer.updateVectorSelection(layerOptionChanged.getLayerName(), layerOptionChanged.getFillColor(), layerOptionChanged.getRasterName(), layerOptionChanged.getOutlineSelected(), layerOptionChanged.getOutlineColor()
                                                , layerOptionChanged.getMargin(), layerOptionChanged.getAlpha());
                                        return null;
                                    }
                                })
                                .addAction(NumericQueryEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        NumericQueryEvent numericQueryEvent = (NumericQueryEvent) event;
                                        if (mapLayerViewer.getQuerySelectedLayer() != null) {
                                            mapLayerViewer.getSelectionManager().filterByNumericAttribute((VectorLayer) mapLayerViewer.getQuerySelectedLayer(), numericQueryEvent.getTypeName(), numericQueryEvent.getLowerValue(), numericQueryEvent.getUpperValue(), (DrawnGeoElement) mapLayerViewer.getSelected());
                                        }
                                        return null;
                                    }
                                })
                                .addAction(TextQueryEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        TextQueryEvent textQueryEvent = (TextQueryEvent) event;
                                        if (mapLayerViewer.getQuerySelectedLayer() != null) {
                                            mapLayerViewer.getSelectionManager().filterByStringAttribute((VectorLayer) mapLayerViewer.getQuerySelectedLayer(), textQueryEvent.getTypeName(), textQueryEvent.getValues(), (DrawnGeoElement) mapLayerViewer.getSelected());
                                        }
                                        return null;
                                    }
                                })
                                .addAction(FreeQueryEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        FreeQueryEvent freeQueryEvent = (FreeQueryEvent)event;
                                        mapLayerViewer.getSelectionManager().filterByFreeTextQuery((VectorLayer) mapLayerViewer.getQuerySelectedLayer(),freeQueryEvent.getQuery(),(DrawnGeoElement) mapLayerViewer.getSelected());
                                        return null;
                                    }
                                })
                                .addAction(TabbedPaneStateChangedEvent.class, "tabbedPane", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        TabbedPaneStateChangedEvent tabbedEvent = (TabbedPaneStateChangedEvent) event;
                                        mapLayerViewer.setQuerySelectedLayer(mapLayerViewer.getLayerbyName(tabbedEvent.getTarget().getSelectedComponent().getName()));
                                        return null;
                                    }
                                })
                                .addAction(WindowFocusLostEvent.class, "shapeFileQuery", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.setQuerySelectedLayer(null);
                                        return null;
                                    }
                                })
                                .addAction(WindowFocusGainedEvent.class, "shapeFileQuery", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.setQuerySelectedLayer(mapLayerViewer.getLayerbyName(mapLayerViewer.getJtp().getSelectedComponent().getName()));
                                        return null;
                                    }
                                })
                                .addAction(LayerOrderChangedEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        LayerOrderChangedEvent layerOrderChangedEvent = (LayerOrderChangedEvent)event;
                                        mapLayerViewer.layerSwapInArea(layerOrderChangedEvent.getMovedLayerName(), layerOrderChangedEvent.getDestinationLayerName());
                                        return null;
                                    }
                                })
                                .addAction(RightMouseDownEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        RightMouseDownEvent mouseEvent = (RightMouseDownEvent) event;
                                        mapLayerViewer.getPopUpMenu().showPopUpMenu( mouseEvent.getX(), mouseEvent.getY(), mouseEvent.getTarget());
                                        return null;
                                    }
                                })
                                .addAction(MenuActionEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        //System.out.println("menu action!");
                                        MenuActionEvent menuActionEvent = (MenuActionEvent)event;
                                        if (menuActionEvent.getText().equals(MapLayerViewer.COPY_STYLE)) {
                                            mapLayerViewer.copySelectedStyle();
                                        }
                                        else if (menuActionEvent.getText().equals(MapLayerViewer.PASTE_STYLE)) {
                                            mapLayerViewer.pasteSelectedStyle();
                                        }
                                        return null;
                                    }
                                })


                )
//
                .addState(movingVector, new State()
                        .addAction(MouseMoveEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                MouseMoveEvent mouseEvent = (MouseMoveEvent) event;
                                mapLayerViewer.moveVectorGeoElement(mouseEvent.getX(), mouseEvent.getY());
                                return movingVector;
                            }
                        })
                        .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                    spaceBarPressed = false;
                                }
                                return null;
                            }
                        })
                        .addAction(LeftMouseUpEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                mapLayerViewer.finishMovingVectorGeoElement();
                                mousePressed = false;
                                return vectorSelected;
                            }
                        }))
                .addState(vectorSelected, new State()
                                .addAction(StateChangeEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.SELECT_MODE);
                                        mapLayerViewer.getInspector().updateVectorGeoElement((VectorGeoElement) mapLayerViewer.getSelected());
                                        return null;
                                    }
                                })
                                .addAction(MouseEnteredEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.getCanvas().requestFocus();
                                        return null;
                                    }
                                })
                                .addAction(CheckBoxEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        CheckBoxEvent checkBoxEvent = (CheckBoxEvent) event;
                                        //System.out.println("((JCheckBox) checkBoxEvent.getTarget()).getActionCommand()" + ((JCheckBox) checkBoxEvent.getTarget()).getActionCommand());
                                        if (((JCheckBox) checkBoxEvent.getTarget()).getActionCommand().equals(MapLayerViewer.TOGGLE_COMPOSITE)) {
                                            mapLayerViewer.createDrawnFromSelectedVector();
                                            return areaSelected;
                                        } else {
                                            return vectorSelected;
                                        }

                                    }
                                })
                                .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                                        mousePressed = true;
                                        if (spaceBarPressed) {
                                            previousState = vectorSelected;
                                            mapLayerViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                                            return panning;
                                        }
                                        mapLayerViewer.selectElementAt(mouseEvent.getX(), mouseEvent.getY());
                                        if (mapLayerViewer.getSelected() != null) {
                                            if (mapLayerViewer.getSelected() instanceof DrawnGeoElement) {
                                                return movingArea;
                                            } else {
                                                return movingVector;
                                            }

                                        } else {
                                            //return navigationMode;
                                            return backgroundSelected;
                                        }
                                    }
                                })
                                .addAction(LeftMouseDoubleDownEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        LeftMouseDoubleDownEvent mouseEvent = (LeftMouseDoubleDownEvent) event;
                                        mapLayerViewer.setSelectedVectorSelection(mapLayerViewer.getContextVectorSelection());
                                        return null;

                                    }
                                })
                                .addAction(KeyPressedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyPressedEvent keyPressedEvent = (KeyPressedEvent) event;
                                        if (keyPressedEvent.getKeyChar() == 'b') {
                                            mapLayerViewer.bufferSelectedDrawn(4);
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'v') {
                                            mapLayerViewer.bufferSelected(-1);
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'd') {
                                            return drawingMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'n') {
                                            return navigationMode;
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_UP) {
                                            mapLayerViewer.moveLayerUp(mapLayerViewer.getSelectedLayer());
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_DOWN) {
                                            mapLayerViewer.moveLayerDown(mapLayerViewer.getSelectedLayer());
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'i') {
                                            mapLayerViewer.getInspectorFrame().setVisible(true);
                                            mapLayerViewer.getInspectorFrame().pack();
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = true;
                                            mapLayerViewer.setCursor(MapLayerViewer.NAVIGATION_MODE);
                                        }

                                        return vectorSelected;
                                    }
                                })
                                .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                        if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = false;
                                            mapLayerViewer.setCursor(Cursor.getDefaultCursor());
                                        }
                                        return null;
                                    }
                                })
                                .addAction(ModeButtonPressedEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                        mapLayerViewer.getCanvas().requestFocus();
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.DRAW_MODE) {
                                            return drawingMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.SELECT_MODE) {
                                            return selectionMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.NAVIGATION_MODE) {
                                            return navigationMode;
                                        }
                                        if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                            //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                            if (mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode())){
                                                return previewingRaster;
                                            }
                                            if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                                return overlay;
                                            }
                                            return navigationMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.CREATE_SQUARE) {
                                            return squareMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == mapLayerViewer.CREATE_CIRCLE) {
                                            return circleMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.MAGIC_WAND) {
                                            return magicWand;
                                        }
                                        return null;
                                    }
                                })
                                .addAction(MouseMoveEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseMoveEvent mouseMoveEvent = (MouseMoveEvent) event;
                                        mapLayerViewer.suggestSelect(mouseMoveEvent.getX(), mouseMoveEvent.getY());
                                        //mapLayerViewer.suggestSelected(mouseMoveEvent.getX(), mouseMoveEvent.getY());
                                        return null;
                                    }
                                })
                                .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                        if (mouseEvent.getWheelRotation() > 0) {
                                            mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                            //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                        } else {
                                            mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                            //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                        }

                                        return null;
                                    }
                                })
                )
                .addState(backgroundSelected, new State()
                                .addAction(StateChangeEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        //Color fillColor = new Color(mapLayerViewer.getBackgroundRaster().getFillColor().x, mapLayerViewer.getBackgroundRaster().getFillColor().y, mapLayerViewer.getBackgroundRaster().getFillColor().z);
                                        //System.out.println("enter background selected");
                                        mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.SELECT_MODE);
                                        mapLayerViewer.updateInspectorBackground();
                                        mapLayerViewer.updateShapeFileTableSummaryBackground();
                                        return null;
                                    }
                                })
                                .addAction(MouseEnteredEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.getCanvas().requestFocus();
                                        return null;
                                    }
                                })
                                .addAction(KeyPressedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyPressedEvent keyPressedEvent = (KeyPressedEvent) event;
                                        if (keyPressedEvent.getKeyChar() == 'd') {
                                            return drawingMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'n') {
                                            return navigationMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 's') {
                                            return selectionMode;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'q') {
                                            //System.out.println("q!");
                                            mapLayerViewer.getShapeFileQuery().pack();
                                            mapLayerViewer.getShapeFileQuery().setVisible(true);
                                            return null;
                                        }
                                        if (keyPressedEvent.getKeyChar() == 'i') {
                                            mapLayerViewer.getInspectorFrame().setVisible(true);
                                            mapLayerViewer.getInspectorFrame().pack();
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = true;
                                            mapLayerViewer.setCursor(MapLayerViewer.NAVIGATION_MODE);
                                        }
                                        if (keyPressedEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
                                            return navigationMode;
                                        }
                                        return backgroundSelected;
                                    }
                                })
                                .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                        if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                            spaceBarPressed = false;
                                            mapLayerViewer.setCursor(Cursor.getDefaultCursor());
                                        }
                                        return null;
                                    }
                                })
                                .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        //System.out.println("click!");
                                        LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                                        mousePressed = true;
                                        if (spaceBarPressed) {
                                            previousState = backgroundSelected;
                                            mapLayerViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                                            return panning;
                                        }
                                        mapLayerViewer.selectElementAt(mouseEvent.getX(), mouseEvent.getY());
                                        if (mapLayerViewer.getSelected() != null) {
                                            if (mapLayerViewer.getSelected() instanceof DrawnGeoElement) {
                                                return movingArea;
                                            } else {
                                                return movingVector;
                                            }

                                        } else {
                                            //return navigationMode;
                                            return backgroundSelected;
                                        }

                                    }

                                })
                                .addAction(ModeButtonPressedEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.DRAW_MODE) {
                                            return drawingMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.SELECT_MODE) {
                                            return selectionMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.NAVIGATION_MODE) {
                                            return navigationMode;
                                        }
                                        if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                            //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                            if (mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode())){
                                                return previewingRaster;
                                            }
                                            if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                                return overlay;
                                            }
                                            return navigationMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.CREATE_SQUARE) {
                                            return squareMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == mapLayerViewer.CREATE_CIRCLE) {
                                            return circleMode;
                                        }
                                        if (modeButtonPressedEvent.getMode() == MapLayerViewer.MAGIC_WAND) {
                                            return magicWand;
                                        }
                                        return null;
                                    }
                                })
                                .addAction(MouseMoveEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseMoveEvent mouseMoveEvent = (MouseMoveEvent) event;
                                        mapLayerViewer.suggestSelect(mouseMoveEvent.getX(), mouseMoveEvent.getY());
                                        //mapLayerViewer.suggestSelected(mouseMoveEvent.getX(), mouseMoveEvent.getY());
                                        return null;
                                    }
                                })
                                .addAction(ComboBoxEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        ComboBoxEvent comboBoxEvent = (ComboBoxEvent) event;
                                        if (comboBoxEvent.getSelectedText().equals(MapLayerViewer.FILL_COLOR)) {
                                            mapLayerViewer.changeBackgroundColor();
                                        } else {
                                            mapLayerViewer.changeBackgroundTexture(comboBoxEvent.getSelectedText());
                                        }
                                        return null;
                                    }
                                })
                                .addAction(ColorChangedEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        ColorChangedEvent colorChangedEvent = (ColorChangedEvent) event;
                                        Vector3f fillColorVector = new Vector3f(colorChangedEvent.getColor().getRed() / 255.0f, colorChangedEvent.getColor().getGreen() / 255.0f, colorChangedEvent.getColor().getBlue() / 255.0f);
                                        mapLayerViewer.changeBackgroundColor(fillColorVector);
                                        return null;
                                    }
                                })
                                .addAction(CheckBoxEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        CheckBoxEvent checkBoxEvent = (CheckBoxEvent) event;
                                        Layer layer = mapLayerViewer.getLayerbyName(checkBoxEvent.getText());
                                        if (layer != null) {
                                            //System.out.println("check box pressed! "+System.currentTimeMillis());
                                            //System.out.println("toggle elements in layer visibility "+((VectorLayer)layer).getName());
                                            mapLayerViewer.getSelectionManager().toggleElementsInLayerVisibility((VectorLayer) layer);
                                            mapLayerViewer.updateInspectorBackground();
                                        }
                                        return null;
                                    }
                                })
                                .addAction(LayerOptionChanged.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        LayerOptionChanged layerOptionChanged = (LayerOptionChanged) event;
                                        mapLayerViewer.updateVectorSelectionBackground(layerOptionChanged.getLayerName(), layerOptionChanged.getFillColor(), layerOptionChanged.getRasterName(), layerOptionChanged.getOutlineSelected(), layerOptionChanged.getOutlineColor(), layerOptionChanged.getMargin(), layerOptionChanged.getAlpha());
                                        return null;
                                    }
                                })
                                .addAction(WindowFocusLostEvent.class, "shapeFileQuery", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.setQuerySelectedLayer(null);
                                        return null;
                                    }
                                })
                                .addAction(WindowFocusGainedEvent.class, "shapeFileQuery", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        mapLayerViewer.setQuerySelectedLayer(mapLayerViewer.getLayerbyName(mapLayerViewer.getJtp().getSelectedComponent().getName()));
                                        return null;
                                    }
                                })
                                .addAction(NumericQueryEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        NumericQueryEvent numericQueryEvent = (NumericQueryEvent) event;
                                        if (mapLayerViewer.getQuerySelectedLayer() != null) {
                                            mapLayerViewer.getSelectionManager().filterByNumericAttribute((VectorLayer) mapLayerViewer.getQuerySelectedLayer(), numericQueryEvent.getTypeName(), numericQueryEvent.getLowerValue(), numericQueryEvent.getUpperValue());
                                        }
                                        //System.out.println("Numeric Query " + numericQueryEvent.getLowerValue() + "-" + numericQueryEvent.getUpperValue() + "-" + numericQueryEvent.getTypeName());
                                        return null;
                                    }
                                })
                                .addAction(TextQueryEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        TextQueryEvent textQueryEvent = (TextQueryEvent) event;
                                        if (mapLayerViewer.getQuerySelectedLayer() != null) {
                                            mapLayerViewer.getSelectionManager().filterByStringAttribute((VectorLayer) mapLayerViewer.getQuerySelectedLayer(), textQueryEvent.getTypeName(), textQueryEvent.getValues());
                                        }
                                        //System.out.println("Numeric Query " + numericQueryEvent.getLowerValue() + "-" + numericQueryEvent.getUpperValue() + "-" + numericQueryEvent.getTypeName());
                                        return null;
                                    }
                                })
                                .addAction(TabbedPaneStateChangedEvent.class, "tabbedPane", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        TabbedPaneStateChangedEvent tabbedEvent = (TabbedPaneStateChangedEvent) event;
                                        //System.out.println("Tab: " + tabbedEvent.getTarget().getSelectedComponent().getName());
                                        mapLayerViewer.setQuerySelectedLayer(mapLayerViewer.getLayerbyName(tabbedEvent.getTarget().getSelectedComponent().getName()));
                                        return null;
                                    }
                                })
                                .addAction(FreeQueryEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        FreeQueryEvent freeQueryEvent = (FreeQueryEvent)event;
                                        //System.out.println("Free query text "+freeQueryEvent.getQuery());
                                        mapLayerViewer.getSelectionManager().filterByFreeTextQuery((VectorLayer) mapLayerViewer.getQuerySelectedLayer(), freeQueryEvent.getQuery());
                                        return null;
                                    }
                                })
                                .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                        if (mouseEvent.getWheelRotation() > 0) {
                                            mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                            //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                        } else {
                                            mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                            //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                        }

                                        return null;
                                    }
                                })
                                .addAction(LayerOrderChangedEvent.class, new Action() {
                                    @Override
                                    public String perform(StateMachine stateMachine, Event event) {
                                        LayerOrderChangedEvent layerOrderChangedEvent = (LayerOrderChangedEvent) event;
                                        mapLayerViewer.layerSwap(layerOrderChangedEvent.getMovedLayerName(), layerOrderChangedEvent.getDestinationLayerName());
                                        return null;
                                    }
                                })

                ).addState(magicWand, new State()
                        .addAction(StateChangeEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                //mapLayerViewer.getInspector().notShow();
                                mapLayerViewer.getInspector().updateMagicWand(mapLayerViewer.getMagicWandTolerance());
                                mapLayerViewer.setCursor(MapLayerViewer.MAGIC_WAND);
                                mapLayerViewer.getModeToolBar().setButtonSelected(MapLayerViewer.MAGIC_WAND);
                                mapLayerViewer.deselectAll();
                                return null;
                            }
                        })
                        .addAction(ModeButtonPressedEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.DRAW_MODE) {
                                    return drawingMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.SELECT_MODE) {
                                    return selectionMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.NAVIGATION_MODE) {
                                    return navigationMode;
                                }
                                if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                    //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                    if (mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode())){
                                        return previewingRaster;
                                    }
                                    if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                        return overlay;
                                    }
                                    return navigationMode;
                                }
                                if (modeButtonPressedEvent.getMode() == MapLayerViewer.CREATE_SQUARE) {
                                    return squareMode;
                                }
                                if (modeButtonPressedEvent.getMode() == mapLayerViewer.CREATE_CIRCLE) {
                                    return circleMode;
                                }
                                return null;
                            }
                        })
                        .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                MouseDownEvent mouseDownEvent = (MouseDownEvent) event;
                                mousePressed = true;
                                if (spaceBarPressed) {
                                    previousState = magicWand;
                                    mapLayerViewer.startPanning(mouseDownEvent.getX(), mouseDownEvent.getY());
                                    return panning;
                                }
                                //System.out.println("leftmousedownevent!");
                                mapLayerViewer.doMagicWand(mouseDownEvent.getX(), mouseDownEvent.getY());
                                if (mapLayerViewer.getSelected() != null) {
                                    return areaSelected;
                                }
                                return null;
                            }
                        })
                        .addAction(TextFieldEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                TextFieldEvent textFieldEvent = (TextFieldEvent) event;
                                if (textFieldEvent.getText().length() > 0) {
                                    Double d = null;
                                    try {
                                        d = Double.valueOf(textFieldEvent.getText());
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }
                                    if (d != null) {
                                        //System.out.println("textFieldEvent.getTarget().getName()" + textFieldEvent.getTarget().getName());
                                        if (textFieldEvent.getTarget().getName().equals(MapLayerViewer.WAND_TOOLERANCE)) {
                                            mapLayerViewer.setMagicWandTolerance(d.floatValue());
                                        }
                                    }
                                }
                                return null;
                            }
                        })
                        .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                if (mouseEvent.getWheelRotation() > 0) {
                                    mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                    //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                } else {
                                    mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                    //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                }

                                return null;
                            }
                        })
                        .addAction(KeyReleasedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;
                                if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                    spaceBarPressed = false;
                                    mapLayerViewer.setCursor(MapLayerViewer.MAGIC_WAND);
                                }
                                return null;
                            }
                        })
                        .addAction(KeyPressedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                KeyPressedEvent keyPressedEvent = (KeyPressedEvent) event;
                                if (keyPressedEvent.getKeyChar() == 'd') {
                                    return drawingMode;
                                }
                                if (keyPressedEvent.getKeyChar() == 'n') {
                                    return navigationMode;
                                }
                                if (keyPressedEvent.getKeyChar() == 's') {
                                    return selectionMode;
                                }
                                if (keyPressedEvent.getKeyChar() == 'q') {
                                    //System.out.println("q!");
                                    mapLayerViewer.getShapeFileQuery().pack();
                                    mapLayerViewer.getShapeFileQuery().setVisible(true);
                                    return null;
                                }
                                if (keyPressedEvent.getKeyChar() == 'i') {
                                    mapLayerViewer.getInspectorFrame().setVisible(true);
                                    mapLayerViewer.getInspectorFrame().pack();
                                }
                                if (keyPressedEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                                    spaceBarPressed = true;
                                    mapLayerViewer.setCursor(MapLayerViewer.NAVIGATION_MODE);
                                    //System.out.println("space bar pressed");
//                                            if (mousePressed) {
//                                                previousState = backgroundSelected;
//                                                return panning;
//                                            }
                                }
                                if (keyPressedEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
                                    return navigationMode;
                                }
                                return magicWand;
                            }
                        })
        )
                .addState(overlay, new State()
                        .addAction(StateChangeEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                mapLayerViewer.getInspector().updateOverlay(mapLayerViewer.getRasterOverlayAlphas());
                                mapLayerViewer.setCursor(Cursor.getDefaultCursor());
                                return null;
                            }
                        })
                        .addAction(ModeButtonPressedEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                //System.out.println("modebuttonpressed!");
                                ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                    //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                    if (mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode())){
                                        return previewingRaster;
                                    }
                                    if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                        return overlay;
                                    }
                                    return navigationMode;
                                }
                                return null;
                            }
                        })
                        .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                                mapLayerViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                                previousState = overlay;
                                return panning;
                            }
                        })
                        .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                if (mouseEvent.getWheelRotation() > 0) {
                                    mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                    //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                } else {
                                    mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                    //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                }

                                return null;
                            }
                        })
                        .addAction(TextFieldEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                //System.out.println("text field event in overlay!");
                                TextFieldEvent textFieldEvent = (TextFieldEvent) event;
                                if (textFieldEvent.getText().length() > 0) {
                                    Double d = null;
                                    try {
                                        d = Double.valueOf(textFieldEvent.getText());
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }
                                    if (d != null) {
                                        mapLayerViewer.changeAlphaInRasterOverlay(textFieldEvent.getTarget().getName(), d);

                                    }
                                }
                                return null;
                            }
                        }))
                .addState(previewingRaster, new State()
                        .addAction(StateChangeEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                return null;
                            }
                        })
                        .addAction(ColorChangedEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                ColorChangedEvent colorChangedEvent = (ColorChangedEvent) event;
                                Vector3f newColor = new Vector3f((float) (colorChangedEvent.getColor().getRed() / 255.0), (float) (colorChangedEvent.getColor().getGreen() / 255.0), (float) (colorChangedEvent.getColor().getBlue() / 255.0));
                                //System.out.println("Color change event name " + colorChangedEvent.getTarget().getName());
                                if (colorChangedEvent.getTarget().getName() == MapLayerViewer.INTERPOLATION_START) {
                                    ((MNTRasterGeoElement) mapLayerViewer.getRasters().get(mapLayerViewer.getPreviewingTexture())).setMinColor(newColor);
                                    mapLayerViewer.updateMNT();
                                } else {
                                    ((MNTRasterGeoElement) mapLayerViewer.getRasters().get(mapLayerViewer.getPreviewingTexture())).setMaxColor(newColor);
                                    mapLayerViewer.updateMNT();
                                }
                                return null;
                            }
                        })
                        .addAction(ComboBoxEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                ((MNTRasterGeoElement)mapLayerViewer.getRasters().get(mapLayerViewer.getPreviewingTexture())).setInterpolationMethod(((ComboBoxEvent)event).getSelectedText());
                                mapLayerViewer.updateMNT();
                                return null;
                            }
                        })
                        .addAction(LeftMouseDownEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                                mapLayerViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                                previousState = previewingRaster;
                                return panning;
                            }
                        })
                        .addAction(MouseWheelMovedEvent.class, "canvas", new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                                if (mouseEvent.getWheelRotation() > 0) {
                                    mapLayerViewer.getCanvas().getCamera().zoomIn(mouseEvent.getX(), mouseEvent.getY());
                                    //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                } else {
                                    mapLayerViewer.getCanvas().getCamera().zoomOut(mouseEvent.getX(), mouseEvent.getY());
                                    //mapLayerViewer.getSelectionManager().updateAllVectorSelections();
                                }

                                return null;
                            }
                        })
                        .addAction(ModeButtonPressedEvent.class, new Action() {
                            @Override
                            public String perform(StateMachine stateMachine, Event event) {
                                //System.out.println("modebuttonpressed!");
                                ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                                if (modeButtonPressedEvent.getModeToolBar() == mapLayerViewer.getRasterToolBar()) {
                                    //System.out.println("modeButtonPressedEvent.getTarget() == mapLayerViewer.getRasterToolBar()");
                                    if (mapLayerViewer.changePreviewingTexture(modeButtonPressedEvent.getMode())){
                                        return previewingRaster;
                                    }
                                    if (modeButtonPressedEvent.getMode() == MapLayerViewer.OVERLAY) {
                                        return overlay;
                                    }
                                    return navigationMode;
                                }
                                return null;
                            }
                        }))
        ;
        return expeStateMachine;

    }


}
