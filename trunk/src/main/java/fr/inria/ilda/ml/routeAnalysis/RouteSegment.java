/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.routeAnalysis;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineString;

import javax.vecmath.Vector2d;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 17/06/15.
 */
public class RouteSegment {
    Node startNode;
    Node endNode;
    Coordinate secondPoint;
    Coordinate penultimatePoint;
    Vector2d startVector;
    Vector2d endVector;
    LineString line;
    int strokeId;

    public RouteSegment(Node startNode, Node endNode, Coordinate secondPoint, Coordinate penultimatePoint, LineString line) {
        this.startNode = startNode;
        this.endNode = endNode;
        this.secondPoint = secondPoint;
        this.penultimatePoint = penultimatePoint;
        startVector = new Vector2d(secondPoint.x-startNode.getCoordinate().x, secondPoint.y - startNode.getCoordinate().y);
        endVector = new Vector2d(endNode.getCoordinate().x - penultimatePoint.x, endNode.getCoordinate().y - penultimatePoint.y);
        this.line = line;
        this.strokeId = -1;
    }

    public Vector2d getStartVector() {
        return startVector;
    }

    public Vector2d getEndVector() {
        return endVector;
    }

    public Node getStartNode() {
        return startNode;
    }

    public Node getEndNode() {
        return endNode;
    }

    public LineString getLine() {
        return line;
    }

    public int getStrokeId() {
        return strokeId;
    }

    public void setStrokeId(int strokeId) {
        this.strokeId = strokeId;
    }

    public List<Coordinate> getCoordinates() {
        ArrayList <Coordinate> coordinates = new ArrayList<>();
        for (Coordinate coord: line.getCoordinates()) {
            coordinates.add(coord);
        }
        return coordinates;
    }
}
