/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.gl;

import com.jogamp.opengl.GL;
import fr.inria.ilda.ml.GLUtilities;

import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by mjlobo on 27/07/15.
 */
public class LineStripGL extends ShapeGL {

    BufferGL vertexBufferGL;
    BufferGL indexesBufferGL;
    //Vector3f color;
    float lineWidth = 2.0f;
    //float alpha = 1.0f;

    public LineStripGL (List<double[]> coordinates) {
        this (coordinates, 2.0f);
    }

    public LineStripGL (List<double[]> coordinates, float lineWidth) {
        this(coordinates, lineWidth, new Vector3f(1.0f,0.0f,0.0f));
    }

    public LineStripGL(List<double[]> coordinates, float lineWidth, Vector3f color) {
        //System.out.println("Here line strip with coordinates");
        buffers = new Hashtable<>();
//        this.coordinates.add(coordinates.get(0));
//        this.coordinates.addAll(coordinates);
//        this.coordinates.add(coordinates.get(0));
//        this.coordinates.add(coordinates.get(0));

        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});
        for (double[] coord : coordinates) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});
        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});

       // this.coordinates.add(coordinates.get(coordinates.size()-1));
       // this.coordinates.add(coordinates.get(coordinates.size()-1));
        indexes = new ArrayList<>();
        calculateCoordinates();
        updateBuffers();
        this.lineWidth = lineWidth;
        this.color = color;
        //printCoordinates();
    }



    public LineStripGL(LineStripGL lineStripGL) {
        buffers = new Hashtable<>();
        coordinates = new ArrayList<>();
        indexes = new ArrayList<>();
        //coordinates.addAll(lineStripGL.getCoordinates());
        for (double[] coord : lineStripGL.getCoordinates()) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        //indexes.addAll(lineStripGL.getIndexes());
        for (Integer index : lineStripGL.getIndexes()) {
            indexes.add(index);
        }
        //calculateCoordinates();
        color = new Vector3f(lineStripGL.getColor().x, lineStripGL.getColor().y, lineStripGL.getColor().z);
        lineWidth = lineStripGL.getLineWidth();
        updateBuffers();

    }

    public LineStripGL () {
        this(new Vector3f(1.0f,0.0f,0.0f));
    }

    public LineStripGL(Vector3f color) {
        buffers = new Hashtable<>();
        coordinates = new ArrayList<>();
        indexes = new ArrayList<>();
        this.color =  color;

    }

    protected void calculateCoordinates() {
        for (int i=0; i<coordinates.size(); i++) {
            indexes.add(i);
        }
    }

    @Override
    public void updateBuffers() {
        if (buffers.size() == 0) {
            vertexBufferGL = new BufferGL(GL.GL_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.VERTEX, BufferGL.DataType.FLOAT, 3);
            indexesBufferGL = new BufferGL(GL.GL_ELEMENT_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.INDEX, BufferGL.DataType.INT, 1);
        }
        buffers.put(vertexBufferGL, getVertices());
        buffers.put(indexesBufferGL, getIndex());
    }



    public void addPointToList(double[] point) {
        coordinates.add(point);
        indexes.add(coordinates.size()-1);


    }

    public void removeLastPoint() {
        //System.out.println(parentCoordinates.size());
        coordinates.remove(coordinates.size()-1);
        indexes.remove(indexes.size()-1);


    }

    public void addPoint(double[] point) {
//        removeLastPoint();
//        removeLastPoint();
//        addPointToList(point);
//        addPointToList(coordinates.get(0));
//        addPointToList(new double[] {coordinates.get(coordinates.size()-1)[0], coordinates.get(coordinates.size()-1)[1]});
       // addPointToList(coordinates.get(0));
//        addPointToList(coordinates.get(0));
//        addPointToList(point);
        //indexes.add(0);
        coordinates.add(coordinates.size()-2, new double[]{point[0], point[1]});
        indexes.add(coordinates.size()-1);
        updateBuffers();
        //printCoordinates();

    }

    public void addFirstPoint(double[] point) {
//        addPointToList(point);
//        addPointToList(point);
        //System.out.println("add first point ");
        coordinates.add(new double[]{point[0], point[1]});
        indexes.add(coordinates.size()-1);
        //coordinates.add(point);
        coordinates.add(new double[]{point[0], point[1]});
        indexes.add(coordinates.size()-1);
        //coordinates.add(point);
        coordinates.add(new double[]{point[0], point[1]});
        indexes.add(coordinates.size()-1);
        //coordinates.add(point);
        coordinates.add(new double[]{point[0], point[1]});
        indexes.add(coordinates.size()-1);
        //printCoordinates();
        //indexes.add(0);
        updateBuffers();
        //coordinatesSize = coordinates.size();
    }

    public GLUtilities.RenderedElementTypes getType() {
        return GLUtilities.RenderedElementTypes.LINE_STRIP;
    }

    public void offsetCoordinates() {
        super.offsetCoordinates();
        //coordinatesSize = coordinates.size();
        updateBuffers();
    }

    public float getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(float lineWidth) {
        this.lineWidth = lineWidth;
    }


    public void refreshCoordinates(List<double[]> coordinates) {
        super.refreshCoordinates();
        this.coordinates.clear();
        indexes.clear();
//        this.coordinates.add(coordinates.get(0));
//        this.coordinates.addAll(coordinates);
//        this.coordinates.add(coordinates.get(0));
//        this.coordinates.add(coordinates.get(0));
        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});
        for (double[] coord : coordinates) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});
        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});

        calculateCoordinates();
        //coordinatesSize = coordinates.size();
        updateBuffers();
    }

    public Vector3f getColor() {
        return color;
    }

    public void setColor(Vector3f color) {
        this.color = color;
    }


}
