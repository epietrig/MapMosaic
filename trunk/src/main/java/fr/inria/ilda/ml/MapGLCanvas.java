/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;


import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import fr.inria.ilda.ml.Filters.GaussianFilter;
import fr.inria.ilda.ml.Filters.SimpleFilter;
import fr.inria.ilda.ml.Filters.TextureFilter;
import fr.inria.ilda.ml.canvasEvents.*;
import fr.inria.ilda.ml.gl.*;

import javax.vecmath.Vector3f;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.jogamp.opengl.GL.*;

//import javax.media.opengl.*;
//import javax.media.opengl.awt.GLCanvas;
//import javax.media.opengl.glu.GLU;
//import static javax.media.opengl.GL.*;  // GL constants

/**
 * JOGL 2.0 Program Template (GLCanvas)
 * This is a "Component" which can be added into a top-level "Container".
 * It also handles the OpenGL events to render graphics.
 */
@SuppressWarnings("serial")
public class MapGLCanvas extends GLCanvas implements GLEventListener {
    private int[] buffers;
    private int[] buffersDrawn;
    private int[] stencilBuffers = new int[1];
    private int[] frameBuffer = new int[1];
    //shaders ids
    private int rendering_program;
    private int points_program;
    private int lines_program;

    private int tVertexArray[];
    private int cVertexArray[] = new int[1];
    GLU glu = new GLU();

    private GaussianFilter gaussianFilter;
    private SimpleFilter simpleFilter;
    private SimpleFilter vectorSelectionSimpleFilter;
    private SimpleFilter traceFilter;
    private TextureFilter mntSimpleFilter;



    private MapLayerViewer application;

    private int[] samplers = new int[1];

    Camera camera;
    GLUtilities glUtilities;
    CanvasListener canvasListener;

    private ArrayList<Layer> layers;
    private GL3 gl_context;
    private ConcurrentLinkedQueue<CanvasEvent> canvasEvents;
    private HashMap<DrawnGeoElement, Texture> drawnTextures;
    private HashMap<VectorSelection, Texture> selectionTextures;
    private HashMap<VectorLayer, Texture> layerBlurredTexture;
    private HashMap<GeoElement, Texture> blurredDrawnTextures;
    private HashMap<DrawnGeoElement, Texture> traceTextures;
    private HashMap<MNTRasterGeoElement, Texture> mntTextures;
    Texture traceTexture;
    Texture mntTexture;


    int width;
    int height;
    // Setup OpenGL Graphics Renderer
    // for the GL Utility

    CanvasEvent zoomEvent;

    private ArrayList<Float> fps = new ArrayList<>();

    /**
     * Constructor to setup the GUI for this Component
     */
    public MapGLCanvas(MapLayerViewer application, GLCapabilities glCapabilities) {
        super(glCapabilities);
        this.addGLEventListener(this);
        this.application = application;
        this.layers = application.getLayers();
        camera = new Camera(this);
        tVertexArray = new int[layers.size()];
        canvasListener = new CanvasListener(this.application);
        drawnTextures = new HashMap<>();
        selectionTextures = new HashMap<>();
        blurredDrawnTextures = new HashMap<>();
        layerBlurredTexture = new HashMap<>();
        traceTextures = new HashMap<>();
        mntTextures = new HashMap<>();
        //addMouseListener(canvasListener);
        //addKeyListener(canvasListener);
        //addMouseWheelListener(canvasListener);
        //addMouseMotionListener(canvasListener);
        canvasEvents = new ConcurrentLinkedQueue<CanvasEvent>();
    }

    // ------ Implement methods declared in GLEventListener ------

    /**
     * Called back immediately after the OpenGL context is initialized. Can be used
     * to perform one-time initialization. Run only once.
     */
    @Override
    public void init(GLAutoDrawable drawable) {

        //System.out.println("initgl");

        GL3 gl = (GL3) drawable.getGL();
        drawable.getAnimator().setUpdateFPSFrames(3, null);
        gl.setSwapInterval(0);
        gl_context = gl;
        //System.out.println("GL Profile: " + gl.getGLProfile().getName());
        glUtilities = new GLUtilities(gl, application);
        glUtilities.initializeShaders(drawable);
        gl.glEnable(gl.GL_PROGRAM_POINT_SIZE);
        //vboBlackandWhite = glUtilities.createTextureFBO().getVboiD();
//        gl.glEnable(gl.GL_CULL_FACE);
////        gl.glCullFace(gl.GL_FRONT_AND_BACK);
//        gl.glEnable(GL_BLEND);
//        gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//        gl.glEnable(GL_DEPTH_TEST);
//        gl.glDepthFunc(GL_LEQUAL);




        for (Layer layer : layers) {
            for (GeoElement ge : layer.getElements()) {
                if (layer instanceof RasterLayer) {
                    //System.out.println("hereee raster Layer");
                    if (ge instanceof RGBRasterGeoElement) {
                        glUtilities.loadTextureFromRasterTiff(drawable, (RGBRasterGeoElement) ge);

                    }
                    else if (ge instanceof MNTRasterGeoElement) {
                        //System.out.println("MNT raster geo element in mapglcanvas init "+((MNTRasterGeoElement) ge).getName()+" "+((MNTRasterGeoElement) ge).getTexture().getName());
//                        glUtilities.loadTextureFromRasterMNT(drawable, (MNTRasterGeoElement)ge);
//                        mntSimpleFilter =glUtilities.initializeTextureFilter((RasterGeoElement)ge);
                        //glUtilities.initializeTextureFilterQuad(mntSimpleFilter, ((MNTRasterGeoElement) ge).getTexture(), GLUtilities.RenderedElementTypes.ALTITUDE);
                        //ystem.out.println("Quad coordinates!");
                        //mntSimpleFilter.getQuad().printCoordinates();
                        pushEvent(new CanvasMNTEvent(CanvasEvent.NEW_MNT_RASTERGEOELEMENT, (MNTRasterGeoElement) ge, layer));

                    }
                    //System.out.println("Texture id Raster"+ge.getTextureID());
                    //rge.setShaderId(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.TEXTURED).getProgramId());

                }
                if(ge instanceof VectorGeoElement) {

                    //((VectorGeoElement)ge).setTextureID(RasterGeoElement.textures.get(RasterGeoElement.Texture.ORTHO));
                }

                for (ShapeGL shape : ge.getShapes()) {
                    //shape.updateBuffers();
                    if (shape instanceof PolygonGL) {
                        shape.setShader(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.TEXTURED));
                        shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(shape.getShader().getAttribute(GLUtilities.POSITION));
                        shape.getBuffer(BufferGL.Type.TEXTURE).setShaderAttributeId(shape.getShader().getAttribute(GLUtilities.TEXTURE_POSITION));
                    }

                    if (shape instanceof LineGL) {
                        shape.setShader(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.LINE));
                        shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(shape.getShader().getAttribute(GLUtilities.POSITION));
                        shape.getBuffer(BufferGL.Type.NORMALS).setShaderAttributeId(shape.getShader().getAttribute("normal"));
                    }

                    if (shape instanceof LineStripGL) {
                        shape.setShader(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.LINE_STRIP));
                        shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(shape.getShader().getAttribute(GLUtilities.POSITION));
                    }



                }
                if (ge instanceof DrawnGeoElement) {
                    ((DrawnGeoElement) ge).getPolygon().getBuffer(BufferGL.Type.ALPHA).setShaderAttributeId(((DrawnGeoElement) ge).getPolygon().getShader().getAttribute("alphaPos"));
                }
            }
        }



        gl.glUseProgram(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.TEXTURED).getProgramId());
        //System.out.println("Tx location " + glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.TEXTURED).getUniform("s"));
        gl.glUniform1i(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.TEXTURED).getUniform("s"), 0);
        //gl.glUseProgram(points_program);


        layers = application.getLayers();
        //System.out.println("Layers length " + layers.size());
        gl.glGenVertexArrays(tVertexArray.length, tVertexArray, 0);
        int totalElements = 0;
//        for (int i = 0; i < layers.size(); i++) {
////            totalElements += layers.get(i).getElements().size() * 3; //Change to bufferSize
//           for (GeoElement ge : layers.get(i).getElements()) {
//               totalElements += ge.getBuffers().size();
//           }
//        }
        for (int i = 0; i < layers.size(); i++) {
           for (GeoElement ge : layers.get(i).getElements()) {
               for (ShapeGL shape : ge.getShapes())
               totalElements += shape.getBuffers().size();
           }
        }
        buffers = new int[totalElements];
        //System.out.println("totalElements "+totalElements);
        int j = 0;
        gl.glGenBuffers(buffers.length, buffers, 0);
        for (int i = 0; i < layers.size(); i++) {
            gl.glBindVertexArray(tVertexArray[i]);
            layers.get(i).setVao(tVertexArray[i]);


            for (GeoElement ge: layers.get(i).getElements()) {
                for (ShapeGL shape : ge.getShapes()) {
                    if (shape instanceof PolygonGL) {
                        //System.out.println("SHape texture id " + ((PolygonGL) shape).getTexture());
                    }
                    //glUtilities.shapeBufferData(shape,drawable);
                    for (BufferGL bufferGL : shape.getBuffers().keySet()) {
                        gl.glBindBuffer(bufferGL.getTarget(), buffers[j]);
                        if(bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                            FloatBuffer data = (FloatBuffer)shape.getBuffers().get(bufferGL);

                            //System.out.println("data float "+data);
                            //gl.glBufferData(bufferGL.getTarget(), data.limit() * data.array().length, data, bufferGL.getUsage());
                            gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
                            //System.out.println(gl.glGetError());
                        }
                        else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                            IntBuffer data = (IntBuffer)shape.getBuffers().get(bufferGL);

                            //System.out.println("data float "+data);
                            //gl.glBufferData(bufferGL.getTarget(), data.limit()*data.array().length, data,bufferGL.getUsage());
                            gl.glBufferData(bufferGL.getTarget(), data.limit()*Buffers.SIZEOF_INT, data,bufferGL.getUsage());
                            //System.out.println(gl.glGetError());
                        }
                        bufferGL.setBuffer(buffers[j]);

                        j++;
                    }
                }

            }




        }

        //System.out.println("gaussian filter");
        gaussianFilter = glUtilities.initializeGaussianFilter();
        gl.glUseProgram(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.BLUR).getProgramId());
        gl.glUniform1i(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.BLUR).getUniform("s"), 0);

        gl.glUseProgram(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.DRAWN).getProgramId());
        gl.glUniform1i(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.DRAWN).getUniform("s"),0);
        gl.glUniform1i(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.DRAWN).getUniform("blurred"),1);

        gl.glUseProgram(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT).getProgramId());
        gl.glUniform1i(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT).getUniform("blurred"),1);



        simpleFilter = glUtilities.initializeSimpleFilter();
        vectorSelectionSimpleFilter = glUtilities.initializeSimpleFilter();
        traceFilter = glUtilities.initializeSimpleFilter();
        //traceTexture = glUtilities.createScreenTexture();
        traceTexture = traceFilter.getFrameBufferObject().getTexture();

        //mntSimpleFilter = glUtilities.initializeSimpleFilter();

        //System.out.println("end init gl");




    }

    /**
     * Call-back handler for window re-size event. Also called when the drawable is
     * first set to visible.
     */
    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        //System.out.println("Height "+height);
        //System.out.println("width "+width);
        this.width = width;
        this.height = height;
        //Here
        GL3 gl = drawable.getGL().getGL3();  // get the OpenGL 2 graphics context
//
//        if (height == 0) height = 1;   // prevent divide by zero
        float aspect = (float) width / height;
//
//        // Set the view port (display area) to cover the entire window
        gl.glViewport(0, 0, width, height);

        camera.setBottom(0.0);
        camera.setLeft(0.0);

        if(application.isRetina) {

            camera.setTop((float)(height / (application.getCanvas().getCamera().getPixels_per_unit()[0]*2)));
            camera.setRight((float)(width / (application.getCanvas().getCamera().getPixels_per_unit()[1]*2)));


        }
        else {

            camera.setTop((float)(height / (application.getCanvas().getCamera().getPixels_per_unit()[1])));
            camera.setRight((float)(width / (application.getCanvas().getCamera().getPixels_per_unit()[0])));
        }


        gaussianFilter = glUtilities.initializeGaussianFilter();
        gl.glUseProgram(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.BLUR).getProgramId());
        gl.glUniform1i(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.BLUR).getUniform("s"), 0);

        gl.glBindVertexArray(gaussianFilter.getVaoID());
        gaussianFilter.refreshQuadCoordinates(camera.eye, camera.right, camera.left, camera.bottom, camera.top);
        glUtilities.shapeBufferData(gaussianFilter.getQuad(), drawable);

        vectorSelectionSimpleFilter.refreshQuadCoordinates(camera.eye, camera.right, camera.left, camera.bottom, camera.top);
        vectorSelectionSimpleFilter.getFrameBufferObject().refreshTexture(camera.getScreenRealWidth(), camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        glUtilities.shapeBufferData(vectorSelectionSimpleFilter.getQuad(), drawable);

        traceFilter.refreshQuadCoordinates(camera.eye, camera.right, camera.left, camera.bottom, camera.top);
        traceFilter.getFrameBufferObject().refreshTexture(camera.getScreenRealWidth(), camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        glUtilities.shapeBufferData(traceFilter.getQuad(), drawable);

    }


    /**
     * Called back by the animator to perform rendering.
     */
    @Override
    public void display(GLAutoDrawable drawable) {
        //System.out.println("FPS!: " +drawable.getAnimator().getLastFPS());
        long currentTime = System.currentTimeMillis();
        fps.add(drawable.getAnimator().getLastFPS());
        GL3 gl = (GL3) drawable.getGL();
        gl.glEnable(GL_BLEND);
        gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        gl.glClear(GL_COLOR_BUFFER_BIT);
        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);



        popEvents(drawable);
        if (camera.getChanged()) {
            for (DrawnGeoElement ge: application.getSelectionManager().getDrawnSelections().keySet()) {
                updateVectorSelection(application.getSelectionManager().getVectorSelectionsDrawn(ge),ge,drawable, application.getSelectionManager().getBackgroundTextures().get(ge),
                        application.getSelectionManager().getBackgroundColors().get(ge), application.getSelectionManager().getDrawnSelectionType().get(ge), application.getSelectionManager().getBackgroundAlphas().get(ge));

            }
            if (application.getSelectionManager().getBackgroundSelectionDrawn()!=null) {
                application.getSelectionManager().updateBackgroundSelectionDrawn();
                updateVectorSelection(application.getSelectionManager().getBackgroundSelectionsList(), application.getSelectionManager().getBackgroundSelectionDrawn(), drawable, application.getBackgroundTexture(), application.defaultFillColor, GLUtilities.RenderedElementTypes.TEXTURED, 0);
            }
            camera.setChanged(false);
        }

       Layer layer;


        if (application.getCurrentMovingGeoElement()!=null) {
            if (application.getCurrentMovingGeoElement().getLeavingTrace()) {
                updateTraceTexture((DrawnGeoElement) application.getCurrentMovingGeoElement(), drawable, application.getSelectedLayer());
            }
        }




        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, 0);

        gl.glClear(gl.GL_COLOR_BUFFER_BIT);
        gl.glClearColor(0.0f,0.0f,0.0f,1.0f);


        layers = application.getLayers();



        if (application.getPreviewingTexture() == null) {

            if (application.getOverlay()) {
                for (int i = layers.size()-1; i >= 0; i--) {
                    layer = layers.get(i);
                    //Only for background texture
                    if (layer instanceof RasterLayer) {
                        gl.glBindVertexArray(layer.getVao());
                        for (GeoElement ge : layers.get(i).getElements()) {
                            //System.out.println("name in display"+((RasterGeoElement) ge).getName());
                                glUtilities.drawRasterElement((RasterGeoElement) ge,application.getRasterOverlayAlphas().get(((RasterGeoElement) ge).getName()).floatValue());

                            }
                            //}
                        }

                    }
                }

            else {

                for (int i = 0; i < layers.size(); i++) {
                    layer = layers.get(i);
                    //Only for background texture
                    if (layer instanceof RasterLayer) {
                        gl.glBindVertexArray(layer.getVao());
                        for (GeoElement ge : layers.get(i).getElements()) {
                            //if (layer.getVisibility()) {
//                        if (ge instanceof RGBRasterGeoElement) {
//                            glUtilities.drawRasterElement((RasterGeoElement) ge);
//                        }
//                        else {
//                            glUtilities.drawMNTElement((RasterGeoElement) ge);
//                        }
                            if (application.getTextures().get(((RasterGeoElement) ge)) == application.getBackgroundTexture()) {
                                if (ge.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED) {
                                    glUtilities.drawFilledGeoElement(ge);
                                } else {
                                    glUtilities.drawRasterElement((RasterGeoElement) ge);
                                }
                            }
                            //}
                        }
                    }
                    if (layer.getVisibility()) {
                        if (layer instanceof VectorLayer) {
                            if (application.getSelectionManager().getBackgroundSelections().containsKey(layer)) {
                                //System.out.println("drawing background selection");
                                if (application.getSelectionManager().getBackgroundSelections().get(layer).getIsVisible()) {
                                    //System.out.println("drawing background selection");
                                    gl.glBindVertexArray(application.getSelectionManager().getBackgroundSelectionLayer().getVao());
                                    //glUtilities.drawBackgroundVectorSelection(application.getSelectionManager().getBackgroundSelections().get(layer));
                                    glUtilities.drawTexturedElement(application.getSelectionManager().getBackgroundSelectionDrawn());
//                                    for (GeoElement ge : application.getSelectionManager().getBackgroundSelections().get(layer).getSelectedElements()) {
//                                        glUtilities.drawLayerSelectionElement(ge, application.getSelectionManager().getBackgroundSelections().get(layer));
//                                    }
                                }
                            }
                            else {
                                gl.glBindVertexArray(layer.getVao());
                                for (GeoElement ge : layer.getElements()) {
                                    if (camera.getCameraSpace().intersects(ge.getGeometry().getEnvelopeInternal())) {
                                    switch (ge.getRenderingType()) {
                                        case TEXTURED:
                                            glUtilities.drawTexturedElementWithLines(ge);
                                            break;
                                        case DRAWN:
                                            glUtilities.drawTranslucentGeoelement(ge);
                                            break;
                                        case FILLED:
                                            glUtilities.drawFilledGeoElement(ge);
                                            break;
                                        case FILLED_TRANSLUCENT:
                                            glUtilities.drawTranslucentFilledGeoElement(ge);
                                            break;
                                    }

                                    }
//                                    if (ge.getIsIn()) {
//                                        for (PolygonGL inPolygon:((VectorGeoElement) ge).getInPolygons()) {
//                                            glUtilities.renderColoredPolygon(inPolygon,MapLayerViewer.inAreaColor,MapLayerViewer.inAreaAlpha);
//                                        }
//                                    }

                                }
                            }

//                    for (GeoElement ge: layer.getElements()) {
//                        //System.out.println("draw vector geo element");
//                        if (layer.getTranslucentElements().contains(ge)) {
//                            glUtilities.drawTranslucentGeoelement(ge);
//                        }
//                        else {
//                            glUtilities.drawTexturedElementWithLines(ge);
//                        }
//                        //System.out.println("drawVectorGeoElement");
//
//                    }

                        } else if (layer instanceof DrawnLayer) {
                            for (GeoElement ge: layer.getElements()) {
                                //if (application.getCurrentMovingGeoElement() == ge) {
                                    if (ge.getLeavingTrace() && traceTextures.get(ge)!=null && application.getSelected()==ge) {
                                        glUtilities.drawTrace(traceFilter.getQuad(), traceTextures.get(ge));
                                    }
                                //}
                            }
                            gl.glBindVertexArray(layer.getVao());
//                    for (GeoElement ge: layer.getElements()) {
//                        if (layer.getTranslucentElements().contains(ge)) {
//                            glUtilities.drawTranslucentGeoelement(ge);
//                        }
//                        else {
//                            glUtilities.drawTexturedElementWithLines(ge);
//                        }
                            for (GeoElement ge : layer.getElements()) {
                                if (ge.getGeometry()==null || (ge.getGeometry()!=null && camera.getCameraSpace().intersects(ge.getGeometry().getEnvelopeInternal()))) {
                                    switch (ge.getRenderingType()) {
                                        case TEXTURED:
                                            glUtilities.drawTexturedElementWithLines(ge);
                                            break;
                                        case DRAWN:
                                            //System.out.println("drawn!");
                                            glUtilities.drawTranslucentGeoelement(ge);
                                            break;
                                        case FILLED:
                                            glUtilities.drawFilledGeoElement(ge);
                                            break;
                                        case FILLED_TRANSLUCENT:
                                            glUtilities.drawTranslucentFilledGeoElement(ge);
                                            break;

                                    }

                                }
                            }
                        }

                    }
                }
                if (application.getInLayer() != null && application.getInElement()!=null) {
                    gl.glBindVertexArray(application.getInLayer().getVao());
                    //System.out.println("in layer "+application.getInLayer());
                    glUtilities.drawInElement(application.getInElement());
                }
                if (application.getSelectedLayer() != null && application.getSelected()!= null) {
                    gl.glBindVertexArray(application.getSelectedLayer().getVao());
                    glUtilities.drawSelectedElement(application.getSelected());
                }

                for (PointGL pointGL : application.getAlignementManager().getAlignPoints()) {
                    if (pointGL != null) {
                        gl.glBindVertexArray(application.getAlignementManager().getVaoId());
                        //System.out.println("Vao point id in display "+application.getAlignementManager().getVaoId());
                        glUtilities.renderPoint(pointGL);
                    }

                }
            }

        }
        else {
            for (int i = 0; i < layers.size(); i++) {
                layer = layers.get(i);
                //Only for background texture
                if (layer instanceof RasterLayer) {
                    gl.glBindVertexArray(layer.getVao());
                    for (GeoElement ge : layers.get(i).getElements()) {
                        if (application.getTextures().get(((RasterGeoElement) ge)) == application.getPreviewingTexture()) {
                            glUtilities.drawRasterElement((RasterGeoElement) ge);

                        }
                        //}
                    }

                }
            }
        }

        if ((System.currentTimeMillis()-currentTime)>10) {

            //System.out.println("time for drawing " + (System.currentTimeMillis() - currentTime));
            //System.out.println("current time " + System.currentTimeMillis());
        }


    }

    /**
     * Called back before the OpenGL context is destroyed. Release resource such as buffers.
     */
    @Override
    public void dispose(GLAutoDrawable drawable) {
        GL3 gl = (GL3) drawable.getGL();
        gl.glDeleteVertexArrays(1, tVertexArray, 0);
        gl.glDeleteProgram(rendering_program);
    }

    public Camera getCamera() {
        return camera;
    }


    public void newDrawnGeoElement(Layer drawnLayer, DrawnGeoElement ge, GLAutoDrawable drawable) {
        long currentTime = System.currentTimeMillis();
        GL3 gl_context = (GL3) drawable.getGL();
//        ge.setShader(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.POINTS));
//        ge.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(ge.getShader().getAttribute(GLUtilities.POSITION));
//        //ge.getBuffer(BufferGL.Type.TEXTURE).setShaderAttributeId(ge.getShader().getAttribute("texPos"));
        int totalShapes = 0;
        for (ShapeGL shape : ge.getShapes()) {
            shape.setShader(glUtilities.getPrograms().get(shape.getType()));
            totalShapes += shape.getBuffersSize();
            if (shape instanceof LineGL) {
                shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId((shape.getShader().getAttribute(GLUtilities.POSITION)));
                shape.getBuffer(BufferGL.Type.NORMALS).setShaderAttributeId(shape.getShader().getAttribute("normal"));
            }
            if (shape instanceof LineStripGL) {
                shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId((shape.getShader().getAttribute(GLUtilities.POSITION)));
            }
        }


        //System.out.println("total shapes "+totalShapes);
        buffersDrawn = new int[totalShapes];
        gl_context.glGenBuffers(buffersDrawn.length, buffersDrawn, 0);
        int j=0;
        for (ShapeGL shape : ge.getShapes()) {
            for (BufferGL bufferGL : shape.getBuffers().keySet()) {
                bufferGL.setBuffer(buffersDrawn[j]);
                j++;
            }
            glUtilities.shapeBufferData(shape, drawable);
        }
        //System.out.println("Time to create geoElement "+(System.currentTimeMillis()-currentTime));

        //glUtilities.shapeBufferData(ge.getLine(), drawable);
    }

    public void refreshDrawnIncomplete(Layer drawnLayer, DrawnGeoElement ge, GLAutoDrawable drawable) {
        //System.out.println("In draw incomplete "+ge);

        GL3 gl = (GL3) drawable.getGL();
        gl.glBindVertexArray(drawnLayer.getVao());

        glUtilities.shapeBufferData(ge.getLine(), drawable);

    }

    private void finishMoving(Layer layer, GeoElement ge, GLAutoDrawable drawable) {
        GL3 gl = (GL3) drawable.getGL();
        gl.glBindVertexArray(layer.getVao());
        //System.out.println("Shapes in finish moving "+ge.getShapes().size());
        for (ShapeGL shape: ge.getShapes()) {
            //System.out.println("offset in shape "+shape.getOffset()[0]+";"+shape.getOffset()[1]);
            glUtilities.shapeBufferData(shape, drawable);
        }

    }

    private void refreshDrawnComplete(Layer drawnLayer, DrawnGeoElement ge, GLAutoDrawable drawable) {
        //System.out.println("GE: "+ge);
        GL3 gl = (GL3) drawable.getGL();
        gl.glBindVertexArray(drawnLayer.getVao());
        for (ShapeGL shape : ge.getShapes()) {
            //System.out.println("Shapes length "+ge.getShapes().size());
            if (shape instanceof PolygonGL) {
                int[] buffersDrawnComplete = new int [shape.getBuffersSize()];
                gl_context.glGenBuffers(buffersDrawnComplete.length, buffersDrawnComplete, 0);
                int i =0;
                for (BufferGL bufferGL: shape.getBuffers().keySet()) {
                    bufferGL.setBuffer(buffersDrawnComplete[i]);
                    i++;
                }

                glUtilities.shapeBufferData(shape, drawable);
            }
        }
        for (LineStripGL selectionLine: ge.getSelectionLines()) {
            //LineStripGL selectionLine = ge.getSelectionLine();
            int[] buffersDrawnCompleteSelectionLine = new int[selectionLine.getBuffersSize()];
            gl_context.glGenBuffers(buffersDrawnCompleteSelectionLine.length, buffersDrawnCompleteSelectionLine, 0);
            int i = 0;
            for (BufferGL bufferGL : selectionLine.getBuffers().keySet()) {
                bufferGL.setBuffer(buffersDrawnCompleteSelectionLine[i]);
                i++;
            }

            glUtilities.shapeBufferData(selectionLine, drawable);
        }

        for (LineStripGL selectionLine: ge.getSelectionLinesBis()) {
            //LineStripGL selectionLine = ge.getSelectionLine();
            int[] buffersDrawnCompleteSelectionLine = new int[selectionLine.getBuffersSize()];
            gl_context.glGenBuffers(buffersDrawnCompleteSelectionLine.length, buffersDrawnCompleteSelectionLine, 0);
            int i = 0;
            for (BufferGL bufferGL : selectionLine.getBuffers().keySet()) {
                bufferGL.setBuffer(buffersDrawnCompleteSelectionLine[i]);
                i++;
            }

            glUtilities.shapeBufferData(selectionLine, drawable);
        }
//        int[] buffersDrawnComplete = new int [ge.getPolygon().getBuffersSize()];
//        gl_context.glGenBuffers(buffersDrawnComplete.length, buffersDrawnComplete, 0);
//        int i =0;
//        for (BufferGL bufferGL: ge.getPolygon().getBuffers().keySet()) {
//            bufferGL.setBuffer(buffersDrawnComplete[i]);
//            i++;
//        }
//
//        glUtilities.shapeBufferData(ge.getPolygon(), drawable);



        refreshDrawnIncomplete(drawnLayer, ge, drawable);
        //System.out.println("Is Complete!");
        //ge.getPolygon().setTextureID(RasterGeoElement.textures.get(RasterGeoElement.Texture.ORTHO));
        for (ShapeGL shape:ge.getShapes()) {
            if(shape instanceof PolygonGL) {
                PolygonGL polygon = (PolygonGL)shape;
                shape.setShader(glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.DRAWN));
                shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(polygon.getShader().getAttribute(GLUtilities.POSITION));
                shape.getBuffer(BufferGL.Type.TEXTURE).setShaderAttributeId(polygon.getShader().getAttribute(GLUtilities.TEXTURE_POSITION));
                //shape.getBuffer(BufferGL.Type.ALPHA).setShaderAttributeId(ge.getPolygon().getShader().getAttribute("alphaPos"));
            }
        }

        generateBlurredTexture(ge, drawnLayer, drawable);


        application.setCurrentDrawingGeoElement(null);
    }

    private void generateBlurredTexture(GeoElement ge, Layer layer, GLAutoDrawable drawable) {
        //System.out.println("geoelement in generateBlurredTexture "+ge);
        if (ge!=null) {
            Texture blurredTexture = ge.getBlurredTexture();
            if (blurredTexture == null) {
                //System.out.println("blurred texture null");
                blurredTexture = glUtilities.createScreenTexture();
                //blurredDrawnTextures.put(ge,blurredTexture);
                //
                ge.setBlurredTexture(blurredTexture);
                for (ShapeGL shape : ge.getShapes()) {
                    if (shape instanceof PolygonGL) {
                        ((PolygonGL) shape).updateTextureBuffer();
                    }
                }
                gaussianFilter.getFboFirstPass().setTexture(blurredTexture);
                glUtilities.attachTextureToFbo(gaussianFilter.getFboFirstPass(), blurredTexture);
                glUtilities.generateBlurredTexture(ge, gaussianFilter, layer, camera, drawable);

                for (ShapeGL shape : ge.getShapes()) {
                    //System.out.println("shape buffer data");
                    glUtilities.shapeBufferData(shape, drawable);
                }

            }
        }

    }


    private void newGeoElement(Layer layer, GeoElement ge, GLAutoDrawable drawable) {
        if (layer.getVao()== -1) {
            int[] layerVao = new int[1];
            gl_context.glGenVertexArrays(layerVao.length, layerVao, 0);
            layer.setVao(layerVao[0]);

        }
        gl_context.glBindVertexArray(layer.getVao());
        GL3 gl_context = (GL3) drawable.getGL();
        int totalShapes = 0;
        for (ShapeGL shape : ge.getShapes()) {
            totalShapes += shape.getBuffersSize();
        }


        //System.out.println("total shapes "+totalShapes);
        buffersDrawn = new int[totalShapes];
        gl_context.glGenBuffers(buffersDrawn.length, buffersDrawn, 0);
        int j=0;
        for (ShapeGL shape : ge.getShapes()) {
            for (BufferGL bufferGL : shape.getBuffers().keySet()) {
                bufferGL.setBuffer(buffersDrawn[j]);
                j++;
            }
            glUtilities.shapeBufferData(shape, drawable);
        }
    }
    private void refreshGeoElement(Layer layer, GeoElement ge, GLAutoDrawable drawable) {
        GL3 gl = (GL3) drawable.getGL();
        //System.out.println("layer " + layer);
        int [] newBuffer = new int[1];
        gl.glBindVertexArray(layer.getVao());
        for (ShapeGL shapeGL: ge.getShapes()) {
            for (BufferGL bufferGL: shapeGL.getBuffers().keySet()) {
                if (bufferGL.getBuffer()==-1) {
                    gl_context.glGenBuffers(newBuffer.length, newBuffer, 0);
                    bufferGL.setBuffer(newBuffer[0]);
                }
            }
        }
        for (ShapeGL shape : ge.getShapes()) {
            glUtilities.shapeBufferData(shape, drawable);
        }
        if (layer.getTranslucentElements().contains(ge)) {
            //System.out.println("translucent elements contains!");
            //System.out.println("generate blurred texture from refrsehgeoelement");
            generateBlurredTexture(ge, layer, drawable);
        }
        else {
            for (ShapeGL shape : ge.getShapes()) {
                glUtilities.shapeBufferData(shape, drawable);
            }
        }
    }

    private void drawnMovedChanged (GeoElement geoElement, GLAutoDrawable drawable) {
        //System.out.println("here in drawn moved changed!");
//        if (blurredDrawnTextures.keySet().contains(geoElement)) {
        //gaussianFilter.getFboFirstPass().setTexture(blurredDrawnTextures.get(geoElement));

//        }
//        else {
//            System.out.println("new texture");
//            Texture newTexture = glUtilities.createScreenTexture();
//            simpleFilter.getFrameBufferObject().setTexture(newTexture);
//            blurredDrawnTextures.put(geoElement, newTexture);
//        }

        //glUtilities.attachTextureToFbo(gaussianFilter.getFboFirstPass(), gaussianFilter.getFboFirstPass().getTexture());
        //glUtilities.generateBlurredTexture(geoElement,gaussianFilter, application.getSelectedLayer(), camera, drawable);
        //geoElement.setBlurredTexture(gaussianFilter.getCurrentReadBuffer().getTexture());

    }

    private void moveTranslucentElement(GeoElement geoElement, GLAutoDrawable drawable) {
        //glUtilities.generateBlurredTexture(geoElement, gaussianFilter, application.getSelectedLayer(), camera, drawable);

    }

    private void updateTraceTexture(DrawnGeoElement ge,GLAutoDrawable drawable, Layer layer) {
        //System.out.println("updateTraceTexture");
        boolean isFirst = false;
        Texture drawnTraceTexture = traceTextures.get(ge);
        if (drawnTraceTexture == null) {
            drawnTraceTexture = glUtilities.createScreenTexture();
            traceTextures.put(ge, drawnTraceTexture);
            isFirst = true;
        }
//        traceFilter.getFrameBufferObject().setTexture(traceTexture);
//        System.out.println("trace texture in fbo"+traceFilter.getFrameBufferObject().getTexture());
//        System.out.println("trace texture drawn"+traceTextures.get(ge));
//        glUtilities.attachTextureToFbo(traceFilter.getFrameBufferObject(), traceFilter.getFrameBufferObject().getTexture());
        //System.out.println("ge: " + ge);
        //System.out.println("layer "+layer);
        drawTrace(ge, drawable, layer, isFirst);
        traceTexture = drawnTraceTexture;
        traceTextures.put(ge, traceFilter.getFrameBufferObject().getTexture());
        traceFilter.getFrameBufferObject().setTexture(traceTexture);
        //System.out.println("trace texture in fbo"+traceFilter.getFrameBufferObject().getTexture());
//        System.out.println("trace texture drawn"+traceTextures.get(ge));
        glUtilities.attachTextureToFbo(traceFilter.getFrameBufferObject(), traceFilter.getFrameBufferObject().getTexture());


        //System.out.println("trace texture "+traceTexture);

    }

    public void drawTrace(GeoElement ge, GLAutoDrawable drawable, Layer layer, boolean isFirst) {
        GL3 gl = (GL3) drawable.getGL();
        traceFilter.refreshQuadCoordinates(camera.eye, camera.right, camera.left, camera.bottom, camera.top);
        traceFilter.getFrameBufferObject().refreshTexture(camera.getScreenRealWidth(), camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        glUtilities.shapeBufferData(traceFilter.getQuad(), drawable);
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, traceFilter.getFrameBufferObject().getVboiD());
        //gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0);

        gl.glClearColor(0.0f,0.0f,0.0f,1.0f);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        gl.glClearColor(0.0f,0.0f,0.0f,1.0f);

        if (!isFirst) {
            glUtilities.drawTraceQuad(traceFilter, traceTextures.get(ge));
        }
        //glUtilities.drawTraceQuad(traceFilter, application.getTextureByName("Ortho"));
        gl.glBindVertexArray(layer.getVao());
        glUtilities.drawElementTrace(ge);
        //glUtilities.drawFilledGeoElement(ge, new Vector3f(1.0f,0.0f,0.0f));
        gl.glReadBuffer(GL_COLOR_ATTACHMENT0);
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0);
//        gl.glClearColor(0.0f,0.0f,0.0f,1.0f);
//        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    }

    private void updateVectorSelection(List <VectorSelection> vectorSelections, DrawnGeoElement ge, GLAutoDrawable drawable, Texture backgroundTexture,Vector3f color, GLUtilities.RenderedElementTypes type, float backgroundAlpha) {
        long time = System.currentTimeMillis();
        if (drawnTextures.keySet().contains(ge)) {
            vectorSelectionSimpleFilter.getFrameBufferObject().setTexture(drawnTextures.get(ge));
        }
        else {
            Texture newTexture = glUtilities.createScreenTexture();
            //Texture newTexture = glUtilities.createTextureFromScene();
            vectorSelectionSimpleFilter.getFrameBufferObject().setTexture(newTexture);
            drawnTextures.put(ge, newTexture);

        }
        long time2 = System.currentTimeMillis();
        glUtilities.attachTextureToFbo(vectorSelectionSimpleFilter.getFrameBufferObject(), vectorSelectionSimpleFilter.getFrameBufferObject().getTexture());
        //System.out.println("Time to attach texture "+(System.currentTimeMillis()-time2));
        //if (ge.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED || ge.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT) {
        if (type == GLUtilities.RenderedElementTypes.FILLED) {
            //System.out.println("type = filled");
            drawVectorSelection(vectorSelections, drawable, color, backgroundAlpha);
            //ge.setRenderingType(GLUtilities.RenderedElementTypes.DRAWN);
        }
        else {
            //System.out.println("type = not filled");
            time2 = System.currentTimeMillis();
            drawVectorSelection(vectorSelections, drawable, backgroundTexture, backgroundAlpha);
            //System.out.println("Time to draw selection only "+(System.currentTimeMillis()-time2));
        }

        //drawVectorSelection(vectorSelections, drawable);
        //System.out.println("update vector selection in geo element "+ge);
        //System.out.println("vectorSelectionSimpleFilter.getFrameBufferObject() "+vectorSelectionSimpleFilter.getFrameBufferObject());
        ge.setTexture(vectorSelectionSimpleFilter.getFrameBufferObject().getTexture());
        //System.out.println("Time to draw vector selection "+(System.currentTimeMillis()-time));
        //((GeometryQueryFilter)vectorSelection.getGeometricFilter()).getDrawnGeoElement().getPolygon().updateBuffers();
        //refreshGeoElement(application.getDrawnLayer(),((GeometryQueryFilter) vectorSelection.getGeometricFilter()).getDrawnGeoElement(),drawable);


    }


    public void drawVectorSelection(List<VectorSelection> vectorSelections, GLAutoDrawable drawable, Vector3f color, float backgroundAlpha) {
        GL3 gl = (GL3) drawable.getGL();
        vectorSelectionSimpleFilter.refreshQuadCoordinates(camera.eye, camera.right, camera.left, camera.bottom, camera.top);
        vectorSelectionSimpleFilter.getFrameBufferObject().refreshTexture(camera.getScreenRealWidth(), camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        glUtilities.shapeBufferData(vectorSelectionSimpleFilter.getQuad(), drawable);
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, vectorSelectionSimpleFilter.getFrameBufferObject().getVboiD());
        gl.glDisable(gl.GL_BLEND);
        gl.glClear(gl.GL_COLOR_BUFFER_BIT);
        Layer layer;
        for (int i = 0; i < layers.size(); i++) {
            layer = layers.get(i);
            if (layer instanceof RasterLayer) {
                gl.glBindVertexArray(layer.getVao());
                for (GeoElement ge : layers.get(i).getElements()) {
                    if (((RasterGeoElement) ge).getTexture() == MapLayerViewer.baseTexture) {
                        for (ShapeGL shape: ge.getShapes()) {
                            if (shape instanceof PolygonGL) {
                                glUtilities.renderColoredPolygon((PolygonGL)shape, color, backgroundAlpha);
                            }
                        }
                    }
                }
            }
        }

        gl.glEnable(gl.GL_BLEND);
        gl.glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);


        for (VectorSelection vectorSelection: vectorSelections) {
            if (vectorSelection.getIsVisible()) {
                for (GeoElement ge : vectorSelection.getSelectedElements()) {
                    if (ge.getGeometry().getEnvelopeInternal().intersects(camera.getCameraSpace())) {
                        for (ShapeGL shapeGL : ge.getShapes()) {

                            if (shapeGL instanceof LineStripGL && ge instanceof VectorGeoElement && ((VectorGeoElement) ge).getIsPoint() && ((VectorGeoElement) ge).getLines().contains(shapeGL)) {
                                //System.out.println("heree rendering lineStrip");
                                glUtilities.renderLineStrip((LineStripGL) shapeGL);
                            } else if (shapeGL instanceof LineStripGL && ((VectorGeoElement) ge).getSelectionLines().contains(shapeGL)) {
                                if (vectorSelection.getSelectedElement() == ge) {
                                    glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.selectionColorIn);
                                }
                                if (vectorSelection.getInElement() == ge) {
                                    //System.out.println("render Line strip in element!");
                                    glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.inColorVector);
                                }
                                if (vectorSelection.getSelected()) {
                                    glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.selectionColor);
                                }
                                if (vectorSelection.getIn()) {
                                    glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.inColorVector);
                                }
//                            if (vectorSelection.getSelectedElement() == ge) {
//                                glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.selectionColor);
//                            }
                            } else if (shapeGL instanceof LineStripGL && ((VectorGeoElement) ge).getSelectionLinesBis().contains(shapeGL)) {
                                if (vectorSelection.getSelectedElement() == ge) {
                                    glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.selectionColorOut);
                                }
                            } else if (shapeGL instanceof LineStripGL && vectorSelection.getOutlineVisible() && ((VectorGeoElement) ge).getLines().contains(shapeGL)) {
                                glUtilities.renderLineStrip((LineStripGL) shapeGL, vectorSelection.getOutlineColor());
                            }

                        }
                    }
                }

                long time = System.currentTimeMillis();
                for (GeoElement ge : vectorSelection.getSelectedElements()) {
                    if (ge.getGeometry().getEnvelopeInternal().intersects(camera.getCameraSpace())) {
                        for (ShapeGL shapeGL : ge.getShapes()) {
                            if (shapeGL instanceof PolygonGL && !shapeGL.getOfseted()) {
                                if (vectorSelection.getSelectedType() == GLUtilities.RenderedElementTypes.FILLED) {
                                    glUtilities.renderColoredPolygon((PolygonGL) shapeGL, vectorSelection.getColor(), vectorSelection.getAlpha());
                                } else {
                                    glUtilities.renderTexturedPolygon((PolygonGL) shapeGL, vectorSelection.getTexture(), vectorSelection.getAlpha(), ge.getIsDocked());
                                }
                                if (((PolygonGL) shapeGL).getIsHole()) {
                                    //glUtilities.renderTexturedPolygon((PolygonGL) shapeGL, application.getBackgroundTexture());
                                    glUtilities.renderColoredPolygon((PolygonGL) shapeGL, color);
                                }
                            }
                        }
                    }
                }
                //System.out.println("Time to render all elements "+ vectorSelection.getSelectedElements().size()+ " "+(System.currentTimeMillis()-time));

            }
        }


        gl.glReadBuffer(GL_COLOR_ATTACHMENT0);
    }

    public void drawVectorSelection (List<VectorSelection> vectorSelections, GLAutoDrawable drawable, Texture texture, float backgroundAlpha) {
        //System.out.println("Texture name in drawVEcorSelection "+texture);
        Layer layer;
        GL3 gl = (GL3) drawable.getGL();
        vectorSelectionSimpleFilter.refreshQuadCoordinates(camera.eye, camera.right, camera.left, camera.bottom, camera.top);
        vectorSelectionSimpleFilter.getFrameBufferObject().refreshTexture(camera.getScreenRealWidth(), camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        glUtilities.shapeBufferData(vectorSelectionSimpleFilter.getQuad(), drawable);
        //gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, 0);
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, vectorSelectionSimpleFilter.getFrameBufferObject().getVboiD());
        gl.glDisable(gl.GL_BLEND);
        //gl.glClearColor(1.0f,1.0f,0.0f,1.0f);
        gl.glClear(gl.GL_COLOR_BUFFER_BIT);
        for (int i = 0; i < layers.size(); i++) {
            layer = layers.get(i);
            if (layer instanceof RasterLayer) {
                //System.out.println("Layer instance of RasterLayer");
                gl.glBindVertexArray(layer.getVao());
                for (GeoElement ge : layers.get(i).getElements()) {
                    if (((RasterGeoElement) ge).getTexture() == texture) {
                        //System.out.println("hereee drawing rastergeoelement in drawVEctorSelection");
                        //System.out.println("background alpha when drawing raster selection "+ backgroundAlpha);
                        glUtilities.drawRasterElement((RasterGeoElement) ge, backgroundAlpha);
                        //ge.setFillColor(new Vector3f(1.0f,0.0f,0.0f));
                        //glUtilities.drawFilledGeoElement(ge);
                    }
                }
            }
        }


        gl.glEnable(gl.GL_BLEND);
        gl.glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        for (VectorSelection vectorSelection: vectorSelections) {
            if (vectorSelection.getIsVisible()) {
                for (GeoElement ge : vectorSelection.getSelectedElements()) {
                    if (ge.getGeometry().getEnvelopeInternal().intersects(camera.getCameraSpace())) {
                        for (ShapeGL shapeGL : ge.getShapes()) {

                            if (shapeGL instanceof LineStripGL && ge instanceof VectorGeoElement && ((VectorGeoElement) ge).getIsPoint() && ((VectorGeoElement) ge).getLines().contains(shapeGL)) {
                                //System.out.println("heree rendering lineStrip");
                                glUtilities.renderLineStrip((LineStripGL) shapeGL);
                            } else if (shapeGL instanceof LineStripGL && ((VectorGeoElement) ge).getSelectionLines().contains(shapeGL)) {
                                if (vectorSelection.getSelectedElement() == ge) {
                                    glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.selectionColorIn);
                                }
                                if (vectorSelection.getInElement() == ge) {
                                    //System.out.println("render Line strip in element!");
                                    glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.inColorVector);
                                }
                                if (vectorSelection.getSelected()) {
                                    glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.selectionColor);
                                }
                                if (vectorSelection.getIn()) {
                                    glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.inColorVector);
                                }
//                            if (vectorSelection.getSelectedElement() == ge) {
//                                glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.selectionColor);
//                            }
                            } else if (shapeGL instanceof LineStripGL && ((VectorGeoElement) ge).getSelectionLinesBis().contains(shapeGL)) {
                                if (vectorSelection.getSelectedElement() == ge) {
                                    glUtilities.renderLineStrip((LineStripGL) shapeGL, MapLayerViewer.selectionColorOut);
                                }
                            } else if (shapeGL instanceof LineStripGL && vectorSelection.getOutlineVisible() && ((VectorGeoElement) ge).getLines().contains(shapeGL)) {
                                glUtilities.renderLineStrip((LineStripGL) shapeGL, vectorSelection.getOutlineColor());
                            }

                        }
                    }
                }

                long time = System.currentTimeMillis();

                for (GeoElement ge : vectorSelection.getSelectedElements()) {
                    if (ge.getGeometry().getEnvelopeInternal().intersects(camera.getCameraSpace())) {
                        for (ShapeGL shapeGL : ge.getShapes()) {
                            if (shapeGL instanceof PolygonGL && !shapeGL.getOfseted()) {
                                //glUtilities.renderTexturedPolygon((PolygonGL) shapeGL, vectorSelection.getTexture());
                                if (vectorSelection.getSelectedType() == GLUtilities.RenderedElementTypes.FILLED) {
                                    glUtilities.renderColoredPolygon((PolygonGL) shapeGL, vectorSelection.getColor(), vectorSelection.getAlpha());
                                } else {
                                    glUtilities.renderTexturedPolygon((PolygonGL) shapeGL, vectorSelection.getTexture(), vectorSelection.getAlpha(), ge.getIsDocked());
                                    //glUtilities.renderColoredPolygon((PolygonGL) shapeGL, vectorSelection.getColor(), vectorSelection.getAlpha());
                                }
                                if (((PolygonGL) shapeGL).getIsHole()) {
                                    //glUtilities.renderTexturedPolygon((PolygonGL) shapeGL, application.getBackgroundTexture());
                                    glUtilities.renderTexturedPolygon((PolygonGL) shapeGL, texture, ge.getIsDocked());
                                }
                            }
                        }
                    }
                }
                //System.out.println("Time to render all elements "+vectorSelection.getSelectedElements().size()+ " time "+(System.currentTimeMillis()-time));

            }
        }


        gl.glReadBuffer(GL_COLOR_ATTACHMENT0);

    }

    public void loadMntTexture (MNTRasterGeoElement mntRasterGeoElement, Layer layer,GLAutoDrawable drawable) {
        //Texture newTexture = glUtilities.createScreenTexture();
        glUtilities.loadTextureFromRasterMNT(drawable, (MNTRasterGeoElement)mntRasterGeoElement);
        mntSimpleFilter =glUtilities.initializeTextureFilter((RasterGeoElement)mntRasterGeoElement);
//        if (mntTexture == null) {
//            mntTexture = glUtilities.createTextureFromTexture(mntRasterGeoElement);
//        }
        mntTextures.put(mntRasterGeoElement, glUtilities.createTextureFromTexture(mntRasterGeoElement));
        //System.out.println("mnt texture id "+mntTexture.getID());
        //System.out.println(" mntSimpleFilter.getFrameBufferObject().getTexture()" + mntSimpleFilter.getFrameBufferObject().getTexture());
        glUtilities.attachTextureToFbo(mntSimpleFilter.getFrameBufferObject(), mntSimpleFilter.getFrameBufferObject().getTexture());
        drawRasterMNT(mntRasterGeoElement, drawable, layer);
        //Texture oldTexture= mntRasterGeoElement.getTexture();
        mntRasterGeoElement.setTexture(mntSimpleFilter.getFrameBufferObject().getTexture());
       // mntSimpleFilter.getFrameBufferObject().setTexture(oldTexture);
        //glUtilities.attachTextureToFbo(mntSimpleFilter.getFrameBufferObject(), mntSimpleFilter.getFrameBufferObject().getTexture());
        application.getTextures().put(mntRasterGeoElement, mntRasterGeoElement.getTexture());
        application.getRasters().put(mntRasterGeoElement.getTexture(), mntRasterGeoElement);

        //System.out.println("mntRasterGeoElement.getName()"+mntRasterGeoElement.getName());
        mntRasterGeoElement.getTexture().setName(mntRasterGeoElement.getName());

    }

    public void drawRasterMNT (MNTRasterGeoElement mntRasterGeoElement, GLAutoDrawable drawable, Layer layer) {
        GL3 gl = (GL3) drawable.getGL();
        mntSimpleFilter.getFrameBufferObject().refreshTexture(mntRasterGeoElement.getRealWidth(), mntRasterGeoElement.getRealHeight(), mntRasterGeoElement.getLowerCorner(), mntRasterGeoElement.getOriginalTexture().getPixels_per_unit(), mntRasterGeoElement.getOriginalTexture().getPixels_per_unit_array());

        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, mntSimpleFilter.getFrameBufferObject().getVboiD());
        System.out.println("VBO id "+mntSimpleFilter.getFrameBufferObject().getVboiD());
        gl.glClear(gl.GL_COLOR_BUFFER_BIT);
        gl.glBindVertexArray(layer.getVao());
        gl.glViewport(0, 0, mntRasterGeoElement.getImageWidth(), mntRasterGeoElement.getImageHeight());
        glUtilities.drawMNTElement(mntRasterGeoElement);
        gl.glViewport(0, 0, width, height);
    }

    public void updateRasterMNT(MNTRasterGeoElement mntRasterGeoElement, Layer layer, GLAutoDrawable drawable) {
        mntSimpleFilter.getFrameBufferObject().setTexture(mntTextures.get(mntRasterGeoElement));
        glUtilities.attachTextureToFbo(mntSimpleFilter.getFrameBufferObject(), mntSimpleFilter.getFrameBufferObject().getTexture());
        drawRasterMNT(mntRasterGeoElement, drawable, layer);
        Texture oldTexture= mntRasterGeoElement.getTexture();
        //System.out.println("Mnt simple filter texture id "+mntSimpleFilter.getFrameBufferObject().getTexture().getID());
        mntRasterGeoElement.setTexture(mntSimpleFilter.getFrameBufferObject().getTexture());
        application.getTextures().put(mntRasterGeoElement, mntRasterGeoElement.getTexture());
        application.getRasters().put(mntRasterGeoElement.getTexture(), mntRasterGeoElement);
        application.setPreviewingTexture(mntRasterGeoElement.getTexture());
        //mntTexture = oldTexture;
        mntTextures.put(mntRasterGeoElement, oldTexture);
        mntRasterGeoElement.getTexture().setName(mntRasterGeoElement.getName());

    }

    public void addPoint(PointGL pointGL, GLAutoDrawable drawable) {
        GL3 gl = (GL3) drawable.getGL();

        if (application.getAlignementManager().getVaoId()==-1) {
            int[] pointVertextArray = new int[1];
            gl.glGenVertexArrays(pointVertextArray.length, pointVertextArray, 0);
            application.getAlignementManager().setVaoId(pointVertextArray[0]);
        }

        gl.glBindVertexArray(application.getAlignementManager().getVaoId());
        //System.out.println("Vao point id in addPoint "+application.getAlignementManager().getVaoId());
        int buffersPoint[] = new int[pointGL.getBuffers().size()];
        //System.out.println("buffersPoint length "+buffersPoint.length);
        gl.glGenBuffers(buffersPoint.length, buffersPoint, 0);
        int j=0;
        //System.out.println("point buffers length "+pointGL.getBuffers().size());
        for (BufferGL bufferGL : pointGL.getBuffers().keySet()) {
            bufferGL.setBuffer(buffersPoint[j]);
            j++;
        }
        glUtilities.shapeBufferData(pointGL, drawable);
        //System.out.println("Vertex buffer in addPoint" + pointGL.getBuffer(BufferGL.Type.VERTEX).getBuffer());
    }


    public void pushEvent(CanvasEvent event) {
        canvasEvents.add(event);
    }

    private void popEvents(GLAutoDrawable drawable) {
        //System.out.println("In pop events");
        CanvasEvent current;
        Iterator<CanvasEvent> iterator;
        while((current=canvasEvents.poll())!=null) {
            //System.out.println("Canvas event " + current.event);
            switch (current.getEvent()) {
                case CanvasEvent.DRAWN_FINISHED:
//                    System.out.println("application.getDrawnLayer()"+application.getDrawnLayer());
//                    System.out.println("current.getDrawnGeoElement()"+current.getDrawnGeoElement());
                    refreshDrawnComplete(((CanvasDrawnEvent)current).getLayer(), (DrawnGeoElement)(((CanvasDrawnEvent)current).getGeoElement()), drawable);
                    break;
                case CanvasEvent.POINT_ADDED:
                    refreshDrawnIncomplete(((CanvasDrawnEvent)current).getLayer(),(DrawnGeoElement)((CanvasDrawnEvent)current).getGeoElement(), drawable);
                    break;
                case CanvasEvent.DRAWN_CREATED: newDrawnGeoElement(((CanvasDrawnEvent)current).getLayer(), (DrawnGeoElement)((CanvasDrawnEvent)current).getGeoElement(), drawable);
                    break;
                case CanvasEvent.DRAWN_MOVED:
                    finishMoving(((CanvasDrawnEvent)current).getLayer(),((CanvasDrawnEvent)current).getGeoElement(), drawable);
                    break;
                case CanvasEvent.ELEMENT_MODIFIED:
                    refreshGeoElement(((CanvasDrawnEvent)current).getLayer(), ((CanvasDrawnEvent)current).getGeoElement(), drawable);
                    break;
                case CanvasEvent.DRAWN_MOVED_CHANGE:
                    drawnMovedChanged(((CanvasDrawnEvent)current).getGeoElement(),drawable);
                    break;
                case CanvasEvent.VECTOR_SELECTION_UPDATED:
                    updateVectorSelection(((CanvasVectorSelectionsEvent)current).getVectorSelections(),(DrawnGeoElement)((CanvasVectorSelectionsEvent)current).getGeoElement(), drawable, ((CanvasVectorSelectionsEvent)current).getTexture(),((CanvasVectorSelectionsEvent)current).getColor(), ((CanvasVectorSelectionsEvent)current).getType(), ((CanvasVectorSelectionsEvent)current).getAlpha());
                    break;
                case CanvasEvent.POINT_ALIGN_ADDED:
                    addPoint(((CanvasPointEvent)current).getPointGL(), drawable);
                    break;
                case CanvasEvent.NEW_MNT_RASTERGEOELEMENT:
                    loadMntTexture(((CanvasMNTEvent)current).getMntRasterGeoElement(),((CanvasMNTEvent)current).getLayer(), drawable);
                    break;
                case CanvasEvent.TRANSLUCENT_MOVED:
                    moveTranslucentElement(((CanvasDrawnEvent)current).getGeoElement(), drawable);
                    break;
                case CanvasEvent.TRANSLUCENT_BORDER_ENABLED:
                    //System.out.println("Translucent border enabled"+((CanvasDrawnEvent)current).getGeoElement());
                    generateBlurredTexture(((CanvasDrawnEvent) current).getGeoElement(), application.getSelectedLayer(), drawable);
                    break;
                case CanvasEvent.GEOELEMENT_CREATED:
                    newGeoElement(((CanvasDrawnEvent)current).getLayer(),((CanvasDrawnEvent)current).getGeoElement(), drawable) ;
                    break;
                case CanvasEvent.MNT_UPDATED:
                    updateRasterMNT(((CanvasMNTEvent)current).getMntRasterGeoElement(), ((CanvasMNTEvent)current).getLayer(), drawable);
                    break;
//                case CanvasEvent.TRANSLUCENT_VECTOR_SELECTION_CHANGED:
//                    generateBlurredTexture(((CanvasVectorSelectionsEvent)current).getVectorSelections(),



            }



        }

    }

    public void setZoomEvent(CanvasEvent zoomEvent) {
        this.zoomEvent = zoomEvent;
        if (zoomEvent != null && !canvasEvents.contains(zoomEvent)) {
            canvasEvents.add(zoomEvent);
        }
    }

    public CanvasEvent getZoomEvent() {
        return zoomEvent;
    }

    public boolean isRetina() {
        return application.isRetina;
    }

    public Texture getSimpleFilterTexture() {
        return simpleFilter.getFrameBufferObject().getTexture();
    }

    public GaussianFilter getGaussianFilter() {
        return gaussianFilter;
    }

    public HashMap<GeoElement, Texture> getBlurredDrawnTextures () {return blurredDrawnTextures;}

    public SimpleFilter getTraceFilter() {
        return traceFilter;
    }

    public void printFPS() {
        System.out.println("Max fps "+Collections.max(fps));
        System.out.println("Min fps "+Collections.min(fps));
        float sum = 0;
        for (Float f : fps) {
            sum+=f;
        }
        System.out.println("Average fps "+sum/fps.size());
        fps.clear();
    }
}