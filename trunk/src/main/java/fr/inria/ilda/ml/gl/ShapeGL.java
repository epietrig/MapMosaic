/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.gl;

import fr.inria.ilda.ml.GLUtilities;

import javax.vecmath.Vector3f;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by mjlobo on 05/03/15.
 */
public abstract class ShapeGL {


    protected Hashtable<BufferGL, Buffer> buffers;
    protected List<double[]> coordinates = Collections.synchronizedList(new ArrayList<double[]>());
    protected ArrayList<Integer> indexes;
    protected int [] indexesA = new int[0];
    protected float [] vertices = new float[0];

    protected ShaderProgram shader;
    protected float z=0.0f;

    protected boolean visible = true;

    double[] offset;

    GLUtilities.RenderedElementTypes type;

    protected boolean offseted = false;
    protected boolean onlyInFirst = false;

    protected int coordinatesSize = -1;

    protected Vector3f color = new Vector3f(0.0f,0.0f,0.0f);
    float alpha = 1.0f;

    public abstract void updateBuffers();

    public FloatBuffer getVertices() {
        //System.out.println("get Vertices !");
        //System.out.println("coordinates!!!"+coordinates.size());
        offset = new double[] {0.0,0.0};
        if (vertices.length != coordinates.toArray().length*3) {
            //coordinatesSize = coordinates.size();
            vertices = new float[coordinates.size()*3];
            //System.out.println("vertices.length != coordinates.size()*3");
            int vertexCount = 0;
            for(int i=0; i<coordinates.size(); i++){
                vertices[vertexCount] = (float)coordinates.get(i)[0];
                vertices[vertexCount+1] = (float) coordinates.get(i)[1];
                vertices[vertexCount+2] = z;
                //System.out.println("x: "+vertices[vertexCount]+"y: "+vertices[vertexCount+1]+"z: "+vertices[vertexCount+2]);
                vertexCount +=3;
            }
        }
        FloatBuffer vertBuffer = FloatBuffer.wrap(vertices);
        return vertBuffer;
    }

    public IntBuffer getIndex(){
        if(indexesA.length!=indexes.size()) {
            indexesA = new int[indexes.size()];
            for (int i=0; i<indexesA.length; i++) {
                indexesA[i] = indexes.get(i);
            }
        }
        IntBuffer indexBuffer = IntBuffer.wrap(indexesA);
        return indexBuffer;
    }

    public List<double[]> getCoordinates() {return coordinates;}

    public boolean isCCW() {
        if(!((coordinates.get(0)[0]==coordinates.get(coordinates.size()-1)[0]) &&(coordinates.get(0)[1]==coordinates.get(coordinates.size()-1)[1]))){
            coordinates.add(coordinates.get(0));
        }
        float sum = 0;
        double[] current, next;
        for (int i=0; i<coordinates.size()-1; i++){
            current = coordinates.get(i);
            next = coordinates.get(i+1);
            sum+=(next[0]-current[0])*(current[1]+next[1]);
        }
        //System.out.println("SUM "+sum);
        if (sum<0) {
            return true;
        }
        else {
            return false;
        }

    }

    protected boolean coordinatesContains(float[] point) {
        for(double [] coord:coordinates) {
            if(coord[0] == point[0] && coord[1] == point[1]) {

                return true;
            }
        }
        return false;
    }

    public Hashtable<BufferGL,Buffer> getBuffers () {
        return buffers;
    }

    public void setCoordinates (List<double[]> coordinates) {
        this.coordinates = coordinates;
    }

    public void setIndexes (ArrayList<Integer> indexes) {
        this.indexes = indexes;
    }


    public ShaderProgram getShader() {
        return shader;
    }

    public void setShader(ShaderProgram shader) {
        this.shader = shader;
    }

    public BufferGL getBuffer(BufferGL.Type type) {
        for (BufferGL bufferGL : buffers.keySet()) {
            if (bufferGL.getType() == type) {
                return bufferGL;
            }

        }
        return null;
    }

    public abstract GLUtilities.RenderedElementTypes getType();

    public void setType(GLUtilities.RenderedElementTypes type) {
        this.type = type;
    }

    public int getBuffersSize() {
        return buffers.size();
    }

    public boolean getVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public double[] getOffset() {
        return offset;
    }

    public void setOffset(double[] offset) {

        this.offset = offset;
    }

    public void offsetCoordinates() {
        //System.out.println("offset in offsetCoordinates "+offset[0]+" "+offset[1]);
        for (double[] coord : coordinates) {
            coord[0]= coord[0]+(float)offset[0];
            coord[1] = coord[1] + (float)offset[1];
        }
        //System.out.println("After coords[0]" +coordinates.get(0)[0] + "coords[1]" +coordinates.get(0)[1]);

        vertices =new float[0];
        indexesA=new int[0];
    }

    public void refreshCoordinates() {
        vertices = new float[0];
        indexesA = new int[0];
    }

    public boolean getOfseted() {
        return offseted;
    }

    public void setOffseted(boolean offseted) {
        this.offseted = offseted;
    }

    public boolean getOnlyInFirst() {
        return onlyInFirst;
    }

    public void setOnlyInFirst(boolean onlyInFirst){
        this.onlyInFirst = onlyInFirst;
    }

    public void printCoordinates() {
        System.out.println("------------------------Coordinates-----------------------");
        for (double[] coord: coordinates) {
            System.out.println("X " + coord[0] + " Y " +coord[1]);
        }
    }

    public void setColor(Vector3f color) {
        this.color = color;
    }

    public Vector3f getColor() {
        return color;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public float getAlpha() {
        return alpha;
    }

    public ArrayList<Integer> getIndexes() {
        return indexes;
    }




}
