/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.canvasEvents;

import fr.inria.ilda.ml.GeoElement;
import fr.inria.ilda.ml.Layer;

/**
 * Created by mjlobo on 23/02/15.
 */
public class CanvasDrawnEvent extends CanvasEvent{
    String event;
    GeoElement geoElement;
    Layer layer;


    public CanvasDrawnEvent(String event, GeoElement geoElement, Layer layer) {
        super (event);
        this.event = event;
        this.geoElement = geoElement;
        this.layer = layer;
    }



    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public GeoElement getGeoElement() {
        return geoElement;
    }

    public void setGeoElement(GeoElement geoElement) {
        this.geoElement = geoElement;
    }

    public Layer getLayer() {
        return layer;
    }

}
