/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.canvasEvents;

import fr.inria.ilda.ml.GLUtilities;
import fr.inria.ilda.ml.GeoElement;
import fr.inria.ilda.ml.Texture;
import fr.inria.ilda.ml.VectorSelection;

import javax.vecmath.Vector3f;
import java.util.List;

/**
 * Created by mjlobo on 01/07/15.
 */
public class CanvasVectorSelectionsEvent extends CanvasEvent {


    GeoElement geoElement;
    List<VectorSelection> vectorSelections;
    Texture texture;
    Vector3f color;
    GLUtilities.RenderedElementTypes type;
    float alpha=1.0f;

    public CanvasVectorSelectionsEvent(String event, List<VectorSelection> vectorSelections, GeoElement ge, Texture texture, GLUtilities.RenderedElementTypes type) {
        super(event);
        this.vectorSelections = vectorSelections;
        this.geoElement = ge;
        this.texture = texture;
        this.type = type;
    }

    public CanvasVectorSelectionsEvent(String event, List<VectorSelection> vectorSelections, GeoElement ge, Texture texture, Vector3f color, GLUtilities.RenderedElementTypes type) {
        super(event);
        this.vectorSelections = vectorSelections;
        this.geoElement = ge;
        this.texture = texture;
        this.color = color;
        this.type = type;
    }

    public CanvasVectorSelectionsEvent(String event, List<VectorSelection> vectorSelections, GeoElement ge, Vector3f color, GLUtilities.RenderedElementTypes type) {
        super(event);
        this.vectorSelections = vectorSelections;
        this.geoElement = ge;
        this.color = color;
        this.type = type;
    }

    public CanvasVectorSelectionsEvent(String event, List<VectorSelection> vectorSelections, GeoElement ge, Texture texture, Vector3f color, GLUtilities.RenderedElementTypes type, float alpha) {
        super(event);
        this.vectorSelections = vectorSelections;
        this.geoElement = ge;
        this.texture = texture;
        this.color = color;
        this.type = type;
        this.alpha = alpha;
    }

    public GeoElement getGeoElement() {
        return geoElement;
    }

    public void setGeoElement(GeoElement geoElement) {
        this.geoElement = geoElement;
    }

    public List<VectorSelection> getVectorSelections() {
        return vectorSelections;
    }

    public Texture getTexture() {
        return texture;
    }

    public Vector3f getColor() {return color;}

    public GLUtilities.RenderedElementTypes getType () {return type;}

    public float getAlpha() {return alpha;}
}
