/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.util.AffineTransformation;
import com.vividsolutions.jts.geom.util.AffineTransformationBuilder;
import fr.inria.ilda.ml.canvasEvents.CanvasDrawnEvent;
import fr.inria.ilda.ml.canvasEvents.CanvasEvent;
import fr.inria.ilda.ml.canvasEvents.CanvasPointEvent;
import fr.inria.ilda.ml.gl.PointGL;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3f;

/**
 * Created by mjlobo on 01/07/15.
 */
public class AlignementManager {
    PointGL[] alignPoints;
    int alignIndex = 0;
    int vaoId=-1;
    MapLayerViewer mapLayerViewer;
    Vector3f color1;
    Vector3f color2;
    Vector3f color3;

    public AlignementManager (MapLayerViewer mapLayerViewer) {
        this.mapLayerViewer = mapLayerViewer;
        alignPoints = new PointGL[6];
        color1 = new Vector3f(0.57f,0.17f,0.8f);
        color2 = new Vector3f(0.7f,0.3f,0.31f);
        color3 = new Vector3f(0.23f,0.8f,0.17f);
    }

    public void addPointToAlign(int x, int y) {
        double[] newPoint = mapLayerViewer.getCanvas().getCamera().screenToWorld(x, y);
        System.out.println("new Point with old method "+newPoint[0]+":"+newPoint[1]);
        double[] newPoint2 = mapLayerViewer.getCanvas().getCamera().screenToWorldMatrixes(x,y);
        System.out.println("new Point with new method "+newPoint2[0]+":"+newPoint2[1]);
        //Vector2d point = new Vector2d(newPoint[0], newPoint[1]);
        Vector2d point = new Vector2d(newPoint2[0], newPoint2[1]);
        Vector3f color;
        if (alignIndex<=1) {
            color = color1;
        }
        else if (alignIndex<=3) {
            color = color2;
        }
        else if (alignIndex<=5) {
            color = color3;
        }
        else {
            color = null;
        }
        PointGL pointGL = new PointGL(color, point);
        mapLayerViewer.getCanvas().pushEvent(new CanvasPointEvent(pointGL, CanvasEvent.POINT_ALIGN_ADDED));
        System.out.println("Point to align: " +point.x +":" + point.y);
        if (alignIndex < alignPoints.length) {
            alignPoints[alignIndex] = pointGL;
        }
    }

    public void alignLayer (Layer layer) {
        System.out.println("aligning layer " + layer);
        mapLayerViewer.setSelectedLayer(layer);
        Coordinate[] alignCoords = new Coordinate[alignPoints.length];
        for (int i=0; i<alignPoints.length; i++)
        {
            alignCoords[i] = new Coordinate(alignPoints[i].getCoordinate().x, alignPoints[i].getCoordinate().y);
        }
        //AffineTransformationBuilder affineTransformationBuilder = new AffineTransformationBuilder(alignCoords[0], alignCoords[1], alignCoords[2], alignCoords[3], alignCoords[4], alignCoords[5]);
        AffineTransformationBuilder affineTransformationBuilder = new AffineTransformationBuilder(alignCoords[0], alignCoords[2], alignCoords[4], alignCoords[1], alignCoords[3], alignCoords[5]);
        //AffineTransformation affineTransformation = new AffineTransformation(alignCoords[0], alignCoords[1], alignCoords[2], alignCoords[3], alignCoords[4], alignCoords[5]);
        AffineTransformation affineTransformation = affineTransformationBuilder.getTransformation();
        //AffineTransform affineTransform = new AffineTransform(alignPoints[0], alignPoints[1], alignPoints[2], alignPoints[3], alignPoints[4], alignPoints[5]);
        for (GeoElement ge: layer.getElements()) {
            //ge.transformCoordinates(affineTransform);
            ge.transformCoordinates(affineTransformation);
            mapLayerViewer.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.ELEMENT_MODIFIED, ge, layer));
        }
        alignPoints = new PointGL[6];
    }

    public void reverseAlignement(Layer layer) {
        for (GeoElement ge: layer.getElements()) {
            ge.revertTransform();
            mapLayerViewer.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.ELEMENT_MODIFIED, ge, layer));
        }
    }

    public void setAlignIndex(int alignIndex) {
        this.alignIndex = alignIndex;
    }

    public void incrementAlignIndex() {
        alignIndex +=1;
    }

    public int getVaoId() {
        return vaoId;
    }

    public void setVaoId(int vaoId) {
        this.vaoId = vaoId;
    }

    public PointGL[] getAlignPoints() {
        return alignPoints;
    }
}
