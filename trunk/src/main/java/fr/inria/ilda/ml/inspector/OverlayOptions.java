/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import fr.inria.ilda.ml.StateMachine.State;
import fr.inria.ilda.ml.StateMachine.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 17/08/15.
 */
public class OverlayOptions extends JPanel {
    HashMap<String, MarginTextField> rasterLayersFields;

    public OverlayOptions(List<String> rasterLayers) {
        rasterLayersFields = new HashMap<>();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.gridheight = 1;
        gbc.weightx = 0.5;
        gbc.weighty =0;
        gbc.anchor = GridBagConstraints.LINE_START;


        for (String layer: rasterLayers) {
            MarginTextField opacityLayer = new MarginTextField(layer+" opacity: ");
            opacityLayer.setName(layer);
            rasterLayersFields.put(layer, opacityLayer);
            add(opacityLayer, gbc);
            gbc.gridy +=1;

        }
    }

    public void addListeners(StateMachine stateMachine) {
        for (MarginTextField opacityTextField : rasterLayersFields.values()) {
            opacityTextField.addListeners(stateMachine);
        }
    }

    public void update (HashMap<String, Double> alphasOverlay, JFrame parentJFrame) {
        for (String layer: rasterLayersFields.keySet()) {
            rasterLayersFields.get(layer).updateText(""+alphasOverlay.get(layer));
        }
        //parentJFrame.pack();
    }
}
