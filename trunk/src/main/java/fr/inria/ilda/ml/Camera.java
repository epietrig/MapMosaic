/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import com.jogamp.opengl.util.PMVMatrix;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3d;
import java.nio.FloatBuffer;

/**
 * Created by mjlobo on 06/02/15. Creates a camera with orthographic projection. For now, it manages both ModelView matrix and ProjectionMatrix.
 * TODO: add pan and zoom
 */
public class Camera {

    double top = 3.5;
    double bottom = -3.5;
    double right = 5.0;
    double left = -5.f;
    float zNear = 2.5f;
    float zFar = 100.0f;
    Vector3d eye = new Vector3d(0.0,0.0,5.0);
    Vector3d center=new Vector3d(0.0,0.0,0.0);
    Vector3d up = new Vector3d(0.0,1.0,0.0);
    double zoomFactor = 1.01;
    double camera_pixels_per_unit;
    double [] pixels_per_unit;

    PMVMatrix pmvMatrix;
    MapGLCanvas canvas;
    boolean changed;

    Envelope cameraSpace;


    public Camera (MapGLCanvas canvas) {

        pmvMatrix = new PMVMatrix();
        this.canvas = canvas;
        camera_pixels_per_unit = 1.0;
        cameraSpace = new Envelope(center.x+left, center.x+right, center.y + bottom, center.y + top);

    }
    public Camera (MapGLCanvas canvas,double right, double left, double top, double bottom, float zNear, float zFar, Vector3d eye, Vector3d center, Vector3d up) {
        this.canvas = canvas;
        this.top = top;
        this.bottom = bottom;
        this.right = right;
        this.left = left;
        this.zNear = zNear;
        this.zFar = zFar;
        this.eye = eye;
        this.center = center;
        this.up = up;
        pmvMatrix = new PMVMatrix();
        cameraSpace = new Envelope(center.x+left, center.x+right, center.y + bottom, center.y + top);
    }

    public float[] lookAt(Vector3d eye, Vector3d center, Vector3d up) {
        this.eye = eye;
        this.center = center;
        this.up = up;
        return lookAt();
    }

    public float[] lookAt() {
        pmvMatrix.glMatrixMode(PMVMatrix.GL_MODELVIEW);
        pmvMatrix.glLoadIdentity();
        pmvMatrix.gluLookAt((float)(eye.x), (float)(eye.y), (float)(eye.z), (float)(center.x), (float)(center.y), (float)(center.z), (float)(up.x), (float)(up.y), (float)(up.z));
        pmvMatrix.update();
        FloatBuffer v_matrixBuffer = pmvMatrix.glGetMvMatrixf();
        float [] viewMatrix = new float[16];
        int index = 0;
        for (int i=v_matrixBuffer.position(); i<v_matrixBuffer.limit();i++){
            viewMatrix[index]=v_matrixBuffer.get(i);
            index++;
        }
        return viewMatrix;
    }

    public float[] lookAt(Vector3d eye, Vector3d center) {
        PMVMatrix temporaryPmvMatrix = new PMVMatrix();
        temporaryPmvMatrix.glMatrixMode(PMVMatrix.GL_MODELVIEW);
        temporaryPmvMatrix.glLoadIdentity();
        temporaryPmvMatrix.gluLookAt((float)(eye.x), (float)(eye.y), (float)(eye.z), (float)(center.x), (float)(center.y), (float)(center.z), (float)(up.x), (float)(up.y), (float)(up.z));
        temporaryPmvMatrix.update();
        FloatBuffer v_matrixBuffer = temporaryPmvMatrix.glGetMvMatrixf();
        float [] viewMatrix = new float[16];
        int index = 0;
        for (int i=v_matrixBuffer.position(); i<v_matrixBuffer.limit();i++){
            viewMatrix[index]=v_matrixBuffer.get(i);
            index++;
        }
        return viewMatrix;
    }

    public float[] getProjectionMatrix () {
        pmvMatrix.glMatrixMode(PMVMatrix.GL_PROJECTION);
        pmvMatrix.glLoadIdentity();
        pmvMatrix.glOrthof((float)left,(float)right,(float)bottom,(float)top,zNear,zFar);
        pmvMatrix.update();
        FloatBuffer proj_matrixBuffer = pmvMatrix.glGetPMatrixf();
        float [] projMatrix = new float[16];
        int index = 0;
        for (int i=proj_matrixBuffer.position(); i<proj_matrixBuffer.limit();i++){
            projMatrix[index]=proj_matrixBuffer.get(i);
            index++;
        }
        return projMatrix;
    }

    public float[] getProjectionMatrix(double left, double right, double bottom, double top) {
        PMVMatrix temporaryPmvMatrix = new PMVMatrix();
        temporaryPmvMatrix.glMatrixMode(PMVMatrix.GL_PROJECTION);
        temporaryPmvMatrix.glLoadIdentity();
        temporaryPmvMatrix.glOrthof((float) left, (float) right, (float) bottom, (float) top, zNear, zFar);
        temporaryPmvMatrix.update();
        FloatBuffer proj_matrixBuffer = temporaryPmvMatrix.glGetPMatrixf();
        float [] projMatrix = new float[16];
        int index = 0;
        for (int i=proj_matrixBuffer.position(); i<proj_matrixBuffer.limit();i++){
            projMatrix[index]=proj_matrixBuffer.get(i);
            index++;
        }
        return projMatrix;
    }



    public void setEye (Vector3d eye) {
        this.eye = eye;

    }

    public void setCenter (Vector3d center) {
        this.center = center;
    }

    public void setUp(Vector3d up) {
        this.up = up;
        cameraSpace = new Envelope(center.x+left, center.x+right, center.y + bottom, center.y + top);
    }

    public Vector3d getUp() {
        return up;
    }

    public void setTop(double top) {
        this.top = top;
        cameraSpace = new Envelope(center.x+left, center.x+right, center.y + bottom, center.y + top);
    }

    public void setBottom (double bottom) {
        this.bottom = bottom;
        cameraSpace = new Envelope(center.x+left, center.x+right, center.y + bottom, center.y + top);
    }

    public void setRight (double right) {
        //System.out.println("Setting right "+right);
        this.right = right;
        cameraSpace = new Envelope(center.x+left, center.x+right, center.y + bottom, center.y + top);
    }

    public void setLeft(double left) {
        this.left = left;
        cameraSpace = new Envelope(center.x+left, center.x+right, center.y + bottom, center.y + top);
    }

    public double[] screenToWorld(int x, int y) {
       return screenToWorldMatrixes(x,y);

    }

    public double[] screenToWorldMatrixes(int x, int y) {


        Matrix4f projectionMat = new Matrix4f(getProjectionMatrix());
        projectionMat.transpose();
        Matrix4f mvMat = new Matrix4f(lookAt());
        mvMat.transpose();

        projectionMat.invert();
        mvMat.invert();



        double xworld = (2.0 * x/canvas.getWidth()- 1)*projectionMat.getElement(0,0)+projectionMat.getElement(0,3)+mvMat.getElement(0,3);

        double yworld = (-2.0 * y/canvas.getHeight() + 1)*projectionMat.getElement(1,1)+projectionMat.getElement(1,3)+mvMat.getElement(1,3);

        return new double[] {xworld, yworld};

    }

    public void zoomIn(int x, int y) {
        double[] zoomCenter = screenToWorld(x, y);


        eye.set(zoomCenter[0], zoomCenter[1], eye.z);
        center.set(eye.x, eye.y, center.z);

        camera_pixels_per_unit = camera_pixels_per_unit*zoomFactor;

        pixels_per_unit [0] *= zoomFactor;
        pixels_per_unit [1] *= zoomFactor;


        left = -x/pixels_per_unit[0];
        right = (canvas.getWidth()-x)/pixels_per_unit[0];
        top = y/pixels_per_unit[1];
        bottom = -(canvas.getHeight()-y)/pixels_per_unit[1];
        changed = true;
        cameraSpace = new Envelope(center.x+left, center.x+right, center.y + bottom, center.y + top);


    }

    public void zoomOut(int x, int y) {
        double[] zoomCenter = screenToWorld(x, y);
        camera_pixels_per_unit = camera_pixels_per_unit/zoomFactor;

        pixels_per_unit [0] /= zoomFactor;
        pixels_per_unit [1] /= zoomFactor;

        eye.set(zoomCenter[0], zoomCenter[1], eye.z);
        center.set(eye.x, eye.y, center.z);

        left = -x/pixels_per_unit[0];
        right = (canvas.getWidth()-x)/pixels_per_unit[0];
        top = y/pixels_per_unit[1];
        bottom = -(canvas.getHeight()-y)/pixels_per_unit[1];
        changed = true;
        cameraSpace = new Envelope(center.x+left, center.x+right, center.y + bottom, center.y + top);

    }

    public double getCamera_pixels_per_unit() {
        return camera_pixels_per_unit;
    }

    public void setCamera_pixels_per_unit(double camera_pixels_per_unit) {
        this.camera_pixels_per_unit = camera_pixels_per_unit;
        //this.camera_pixels_per_unit = 1.0;
    }

    public void moveCamera (int dx, int dy) {
        double dxWorld = dx/pixels_per_unit[0];
        double dyWorld = dy/pixels_per_unit[1];
        eye.set((float)(eye.x + dxWorld), (float)(eye.y + dyWorld), eye.z);
        center.set (eye.x, eye.y, center.z);
        cameraSpace = new Envelope(center.x+left, center.x+right, center.y + bottom, center.y + top);
        changed = true;
    }


    public double[] getLowerCorner() {
        double lower_corner_x = eye.x+left;
        double lower_corner_y = eye.y + bottom;
        return new double[] {lower_corner_x, lower_corner_y};
    }

    public Vector3d getEye() {
        return eye;
    }

    public Vector3d getCenter() {return center;}

    public double getRight() {
        return right;
    }

    public double getLeft() {
        return left;
    }

    public double getTop() {
        return top;
    }

    public double getBottom() {
        return bottom;
    }

    public void setPixels_per_unit(double[] pixels_per_unit) {
        this.pixels_per_unit = pixels_per_unit;
    }

    public double[] getPixels_per_unit(){
        return pixels_per_unit;
    }

    public double getScreenRealWidth() {
        //System.out.println("screenRealWidth "+(right-left));
        return right - left;
    }

    public double getScreenRealHeight() {
        return top - bottom;
    }

    public boolean getChanged() {
        return changed;
    }

    public void setChanged (boolean changed) {
        this.changed = changed;
    }

    public Envelope getCameraSpace() {
        return cameraSpace;
    }


}
