/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.processing.Operations;

import javax.media.jai.Interpolation;
import javax.media.jai.RenderedOp;
import javax.vecmath.Vector3f;
import java.awt.image.*;

/**
 * Created by mjlobo on 21/07/15.
 */
public class MNTRasterGeoElement extends RasterGeoElement {

    float[] mntData;

    DataBuffer buffer;
    double noValue;
    Vector3f minColor = new Vector3f(0.0f,1.0f,0.0f);
    Vector3f maxColor = new Vector3f(1.0f,0.0f,0.0f);
    String interpolationMethod;
    Texture originalTexture;

    public MNTRasterGeoElement(GridCoverage2D coverage) {
        super(coverage);
        javax.media.jai.Interpolation interp = Interpolation.getInstance(Interpolation.INTERP_BICUBIC);
        GridCoverage2D covinterpol =  (GridCoverage2D) Operations.DEFAULT.interpolate(coverage, interp);
        RenderedImage ri = covinterpol.getRenderedImage();
        //System.out.println("interpolated image buffer type "+ri.getData().getDataBuffer().getClass());
        getTerrainPixelData(ri);
        calculateColorValues();
        interpolationMethod = MapLayerViewer.LINEAR_INTERPOLATION;


    }

    public MNTRasterGeoElement(GridCoverage2D coverage, double noValue, Vector3f minColor, Vector3f maxColor) {
        super(coverage);
        this.noValue = noValue;
        javax.media.jai.Interpolation interp = Interpolation.getInstance(Interpolation.INTERP_BICUBIC);
        GridCoverage2D covinterpol =  (GridCoverage2D) Operations.DEFAULT.interpolate(coverage, interp);
        RenderedImage ri = covinterpol.getRenderedImage();
        //System.out.println("interpolated image buffer type "+ri.getData().getDataBuffer().getClass());
        getTerrainPixelData(ri);
        calculateColorValues(noValue);
        this.maxColor = maxColor;
        this.minColor = minColor;


    }

    public MNTRasterGeoElement(GridCoverage2D coverage, double noValue) {
        super(coverage);
        this.noValue = noValue;
        javax.media.jai.Interpolation interp = Interpolation.getInstance(Interpolation.INTERP_BICUBIC);
        GridCoverage2D covinterpol =  (GridCoverage2D) Operations.DEFAULT.interpolate(coverage, interp);
        RenderedImage ri = covinterpol.getRenderedImage();
        //System.out.println("interpolated image buffer type "+ri.getData().getDataBuffer().getClass());
        getTerrainPixelData(ri);
        calculateColorValues(noValue);
        interpolationMethod = MapLayerViewer.LINEAR_INTERPOLATION;



    }

    public DataBuffer getTerrainPixelData(RenderedImage img) {
        //System.out.println("rendered image color model "+img.getClass());
        int height = img.getHeight();
        int width = img.getWidth();


        //WritableRaster raster  = img.getColorModel().createCompatibleWritableRaster(width, height);
        //BufferedImage newImage = new BufferedImage(img.getColorModel(), raster, false, null);



        DataBufferDouble dataBuf = null;
        RenderedOp ri = (RenderedOp)img;
        //System.out.println("ri.getData() " + ri.getData().getDataBuffer());
        BufferedImage newImage = ri.getAsBufferedImage();

//        java.awt.geom.AffineTransform gt = new java.awt.geom.AffineTransform();
//        gt.translate(0, height);
//        gt.scale(1,-1d);
//        Graphics2D g = newImage.createGraphics();
//        //g.transform(gt);
//        g.drawRenderedImage(img,gt);
//        g.dispose();


//        this.printCoordinates();
//        Coordinate lowerCornerCoord = new Coordinate(x-realWidth/2, y-realHeight/2, z);
//        Coordinate upperCornerCoord = new Coordinate(x + realWidth/2, y + realHeight/2, z);
//        Coordinate upperLeftCoord = new Coordinate(lowerCornerCoord.x, upperCornerCoord.y);
//        Coordinate lowerRightCoord = new Coordinate(upperCornerCoord.x, lowerCornerCoord.y);
//        Coordinate center = new Coordinate(x,y,z);
//        Coordinate midleleft = new Coordinate(lowerCorner[0], lowerCorner[1]+realHeight/2,z);
//        AffineTransformationBuilder affineTransformationBuilder = new AffineTransformationBuilder(upperCornerCoord, upperLeftCoord, center, lowerRightCoord, lowerCornerCoord, center);
//        AffineTransformation affineTransformation = affineTransformationBuilder.getTransformation();
//        //affineTransformation
//
////        AffineTransformation affineTransformation = new AffineTransformation();
////        affineTransformation.reflect(coordinates.get(0)[0], (coordinates.get(0)[1]-coordinates.get(3)[1])/2, coordinates.get(1)[0], (coordinates.get(1)[1]-coordinates.get(2)[1])/2);
//        //affineTransformation.scale(1, -1d);
//
//        //transformCoordinates(affineTransformation);
        //this.printCoordinates();

        //System.out.println("new image "+newImage);
        //dataBuf = ((DataBufferDouble) newImage.getRaster().getDataBuffer());
        buffer = ri.getData().getDataBuffer();
        //System.out.println("RGB: "+newImage.getRGB(100,100));


        return buffer;
    }




    public void calculateColorValues () {
        if (buffer instanceof DataBufferDouble) {
            double max= Double.MIN_VALUE;
            double min = Double.MAX_VALUE;
            double[] imgRGBA = ((DataBufferDouble)buffer).getData();
            for (int i=0; i<imgRGBA.length; i++) {
                if (imgRGBA[i] > max) {
                    max = imgRGBA[i];
                }
                if (imgRGBA[i]<min) {
                    min = imgRGBA[i];
                }
            }

            float[] floatRGBA = new float[imgRGBA.length];
            for (int i=0; i<imgRGBA.length; i++) {
                floatRGBA[i] = ((float)(imgRGBA[i]/(max-min)+min));
            }
            mntData =floatRGBA;
        }
        else if (buffer instanceof DataBufferFloat) {
            float[] imgRGBA = ((DataBufferFloat) buffer).getData();
            float[] newImgRGBA = imgRGBA.clone();
            float max = -Float.MAX_VALUE;
            float min = Float.MAX_VALUE;
//            System.out.println("max before "+ max);
//            System.out.println("min before "+ min);
//            System.out.println("imgRGBA[0]"+imgRGBA[0]);
            for (int i=0; i<imgRGBA.length; i++) {
                if (imgRGBA[i]>max) {
//                    System.out.println("imgRGBA[i]"+imgRGBA[i]);
                    max = imgRGBA[i];
                }
                if (imgRGBA[i]<min) {
                    min = imgRGBA[i];
                }
                //System.out.println("image RGBA "+imgRGBA[i]);
            }
//            System.out.println("Min value "+min);
//            System.out.println("Max value "+max + " "+Float.MIN_VALUE);
            for (int i=0; i<imgRGBA.length; i++) {
                //imgRGBA[i] = (imgRGBA[i]-min)/(max-min);
                imgRGBA[i] = (imgRGBA[i])/(max);
                //newImgRGBA[imgRGBA.length-i-1] = imgRGBA[i]/max;
                //System.out.println("image RGBA "+imgRGBA[i]);
            }

            //mntData = imgRGBA;
            //mntData = newImgRGBA;
            mntData = flipImageVertically(imgRGBA);
        }

    }

    public void calculateColorValues (double noValue) {
        if (buffer instanceof DataBufferDouble) {
            double max= Double.MIN_VALUE;
            double min = Double.MAX_VALUE;
            double[] imgRGBA = ((DataBufferDouble)buffer).getData();
            for (int i=0; i<imgRGBA.length; i++) {
                if (imgRGBA[i] > max && imgRGBA[i]!=noValue) {
                    max = imgRGBA[i];
                }
                if (imgRGBA[i]<min && imgRGBA[i]!= noValue) {
                    min = imgRGBA[i];
                }
            }

            float[] floatRGBA = new float[imgRGBA.length];
            for (int i=0; i<imgRGBA.length; i++) {
                floatRGBA[i] = ((float)(imgRGBA[i]/(max-min)+min));
            }
            mntData =floatRGBA;
        }
        else if (buffer instanceof DataBufferFloat) {
            float[] imgRGBA = ((DataBufferFloat) buffer).getData();
            float[] newImgRGBA = imgRGBA.clone();
            float max = -Float.MAX_VALUE;
            float min = Float.MAX_VALUE;
            //System.out.println("max before "+ max);
            //System.out.println("min before "+ min);
            //System.out.println("imgRGBA[0]"+imgRGBA[0]);
            for (int i=0; i<imgRGBA.length; i++) {
                if (imgRGBA[i]>max && imgRGBA[i]!= (float)noValue) {
                    //System.out.println("imgRGBA[i]"+imgRGBA[i]);
                    max = imgRGBA[i];
                }
                if (imgRGBA[i]<min && imgRGBA[i]!= (float)noValue) {
                    min = imgRGBA[i];
                }
                //System.out.println("image RGBA "+imgRGBA[i]);
            }
            //System.out.println("Min value "+min);
            //System.out.println("Max value "+max + " "+Float.MIN_VALUE);
            for (int i=0; i<imgRGBA.length; i++) {
                imgRGBA[i] = (imgRGBA[i]-min)/(max-min);
                //imgRGBA[i] = (imgRGBA[i])/(max);
                //newImgRGBA[imgRGBA.length-i-1] = imgRGBA[i]/max;
                //System.out.println("image RGBA "+imgRGBA[i]);
            }

            //mntData = imgRGBA;
            //mntData = newImgRGBA;
            mntData = flipImageVertically(imgRGBA);
        }

    }

    public float[] flipImageVertically (float[] array) {
        int rows = imageHeight;
        int columns = imageWidth;
        float[] flipped = new float [array.length];
        int countrows = 0;
        int countColumns = 0;
        for (int i=0; i<rows; i++) {
            for (int j=0; j<columns ; j++) {
                flipped[i * columns + j] = array[(rows-i-1)*columns+j];
            }
        }
        return flipped;
    }

    public float[] getMntData() {
        return mntData;
    }

    public Vector3f getMinColor() {
        return minColor;
    }

    public Vector3f getMaxColor() {
        return maxColor;
    }

    public void setMinColor(Vector3f minColor) {
        this.minColor = minColor;
    }

    public void setMaxColor (Vector3f maxColor) {
        this.maxColor = maxColor;
    }

    public String getInterpolationMethod() {return interpolationMethod;}

    public void setOriginalTexture(Texture originalTexture) {
        this.originalTexture = originalTexture;
    }

    public Texture getOriginalTexture() {
        return originalTexture;
    }

    public void setInterpolationMethod(String interpolationMethod) {
        this.interpolationMethod = interpolationMethod;
    }



}
