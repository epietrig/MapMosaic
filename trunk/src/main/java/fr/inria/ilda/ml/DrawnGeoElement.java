/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;


import com.vividsolutions.jts.algorithm.MinimumBoundingCircle;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.operation.buffer.BufferOp;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;
import fr.inria.ilda.ml.gl.LineStripGL;
import fr.inria.ilda.ml.gl.PolygonGL;
import fr.inria.ilda.ml.gl.ShapeGL;

import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 16/02/15.
 */
public class DrawnGeoElement extends GeoElement{


    float z = 2.0f;


    List<double[]> coordinatesOffseted;
    Geometry offsetedGeometry;
    double borderOffset = 0.000002;
    double alphaBorder = 0.000002;
    float alpha = 1.0f;
    protected Texture blurredTexture;
    LineStripGL lineStripGL;
    protected boolean isComposite = false;
    Vector3f previousLineColor;
    float previousLineWidth = -1;
    List<PolygonGL> polygons;
    List<PolygonGL> inPolygons;
    List<LineStripGL> selectionLines;
    List<LineStripGL> selectionLinesBis;
    List<LineStripGL> lines;
    List<PolygonGL> offsetedPolygons;
    boolean outlineVisible;
    double[] offsetFromOriginal;
    boolean selected = false;



    public DrawnGeoElement() {
        lines = new ArrayList<>();
        selectionLines = new ArrayList<>();
        selectionLinesBis = new ArrayList<>();
        polygons = new ArrayList<>();
        offsetedPolygons = new ArrayList<>();
        shapes = new ArrayList<>();
        isComplete = false;
        coordinates = new ArrayList<double[]>();
        coordinatesOffseted = new ArrayList<double[]>();
        inPolygons = new ArrayList<>();
        LineStripGL line= new LineStripGL();
        line.setColor(MapLayerViewer.inColorDrawn);
        lines.add(line);
        shapes.add(line);
        renderingType = GLUtilities.RenderedElementTypes.TEXTURED;
        fillColor = new Vector3f(MapLayerViewer.defaultFillColor.x, MapLayerViewer.defaultFillColor.y, MapLayerViewer.defaultFillColor.z);
        outlineColor = new Vector3f(MapLayerViewer.defaultOutlineLine.x, MapLayerViewer.defaultOutlineLine.y, MapLayerViewer.defaultOutlineLine.z);
        offsetFromOriginal = new double[]{0,0};
        offset = new double[] {0,0};
        isDocked = false;

    }

    public DrawnGeoElement(Geometry geometry) {
        shapes = new ArrayList<>();
        lines = new ArrayList<>();
        selectionLines = new ArrayList<>();
        selectionLinesBis = new ArrayList<>();
        polygons = new ArrayList<>();
        offsetedPolygons = new ArrayList<>();
        coordinates = new ArrayList<>();
        coordinatesOffseted = new ArrayList<double[]>();
        inPolygons = new ArrayList<>();
        for (Coordinate coord: geometry.getCoordinates()) {
            coordinates.add(new double[] {coord.x, coord.y});
        }
        coordinates.remove(coordinates.size()-1);
        LineStripGL line = new LineStripGL(coordinates, MapLayerViewer.areaBaseLineWidth,  MapLayerViewer.inColorDrawn);
        //System.out.println("line coordinates");
        //line.printCoordinates();
        lines.add(line);
        shapes.add(line);
        renderingType = GLUtilities.RenderedElementTypes.TEXTURED;
        fillColor = new Vector3f(MapLayerViewer.defaultFillColor.x, MapLayerViewer.defaultFillColor.y, MapLayerViewer.defaultFillColor.z);
        outlineColor = new Vector3f(MapLayerViewer.defaultOutlineLine.x, MapLayerViewer.defaultOutlineLine.y, MapLayerViewer.defaultOutlineLine.z);
        offsetFromOriginal = new double[]{0,0};
        offset = new double[] {0,0};
        isDocked = false;

    }


    public DrawnGeoElement(VectorGeoElement vectorGeoElement, double alphaRadius) {
        renderingType = GLUtilities.RenderedElementTypes.TEXTURED;
        polygons = new ArrayList<>();
        lines = new ArrayList<>();
        selectionLines = new ArrayList<>();
        selectionLinesBis = new ArrayList<>();
        shapes = new ArrayList<>();
        coordinates = new ArrayList<double[]>();
        coordinatesOffseted = new ArrayList<double[]>();
        inPolygons = new ArrayList<>();
        offsetedPolygons = new ArrayList<>();



        fillColor = new Vector3f(MapLayerViewer.defaultFillColor.x, MapLayerViewer.defaultFillColor.y, MapLayerViewer.defaultFillColor.z);
        outlineColor = new Vector3f(MapLayerViewer.defaultOutlineLine.x, MapLayerViewer.defaultOutlineLine.y, MapLayerViewer.defaultOutlineLine.z);
        coordinates.addAll(vectorGeoElement.getCoordinates());
        coordinatesOffseted.addAll(vectorGeoElement.getCoordinatesOffseted());

        for (LineStripGL lineStrip : vectorGeoElement.getLines()) {
            LineStripGL newLine = new LineStripGL(lineStrip);
            lines.add(newLine);
            shapes.add(newLine);
        }

        for (PolygonGL shape: vectorGeoElement.getOriginalPolygons()) {
            PolygonGL newPolygon = new PolygonGL(shape);
            polygons.add(newPolygon);
            shapes.add(newPolygon);

        }

        for (LineStripGL lineStrip: vectorGeoElement.getSelectionLinesBis()) {
            LineStripGL newLine = new LineStripGL(lineStrip);
            selectionLinesBis.add(newLine);
            shapes.add(newLine);
        }
        for (LineStripGL lineStrip: vectorGeoElement.getSelectionLines()) {
            LineStripGL newLine = new LineStripGL(lineStrip);
            selectionLines.add(newLine);
            shapes.add(newLine);
        }
        calculateGeometry();
        if(coordinatesOffseted.size()>0) {
            for (PolygonGL offsetedPolygon : vectorGeoElement.getPolygonsOffseted()) {
                PolygonGL newPolygon = new PolygonGL(offsetedPolygon);
                newPolygon.setVisible(false);
                offsetedPolygons.add(newPolygon);
                shapes.add(newPolygon);
            }
            offsetedGeometry = calculateGeometry(coordinatesOffseted);
        }
        else {
            calculatePolygonOffseted(alphaRadius);
        }

        setTranslucentBorder(false);
        offsetFromOriginal = new double[] {0,0};
        offset = new double[] {0,0};
        isDocked = true;

    }



    public void addPoint(double[] point) {
        if (coordinates.size()<1) {
            coordinates.add(point);
            lines.get(0).addFirstPoint(point);
        }
        else if(coordinatesContains(point)==-1) {
            coordinates.add(point);
            lines.get(0).addPoint(point);
        }


    }

    public void drawCompletePolygon(double alphaRadius, Texture texture){
          calculateGeometry();

          PolygonGL polygon = new PolygonGL(coordinates, texture);
          PolygonGL inPolygon = new PolygonGL(polygon);
          polygons.add(polygon);
          inPolygons.add(inPolygon);

          lines.get(0).refreshCoordinates(polygon.getCoordinates());
          lines.get(0).setColor(MapLayerViewer.areaBaseBoderColor);
          lines.get(0).setLineWidth(MapLayerViewer.areaBaseLineWidth);
          LineStripGL selectionLine = new LineStripGL(polygon.getCoordinates(),MapLayerViewer.selectionLineWidth);
          LineStripGL selectionLineBis = new LineStripGL(polygon.getCoordinates(), MapLayerViewer.selectionOutLineWidth, MapLayerViewer.selectionColorOut);
          selectionLine.setVisible(false);
          selectionLine.setColor(MapLayerViewer.inColorDrawn);
          selectionLineBis.setVisible(false);

          selectionLinesBis.add(selectionLineBis);
          shapes.add(polygon);
          shapes.add(selectionLineBis);
          shapes.add(selectionLine);
          shapes.add(inPolygon);
          selectionLines.add(selectionLine);
          polygon.updateBuffers();
          inPolygon.updateBuffers();
          polygon.setOnlyInFirst(true);
          polygon.setVisible(false);
          inPolygon.setVisible(false);
          calculatePolygonOffseted(alphaRadius);
          setTranslucentBorder(false);



    }

    public void refreshCoordinatesContour(Geometry geometry) {
        coordinates.clear();
        for (Coordinate coord: geometry.getCoordinates()) {
            coordinates.add(new double[] {coord.x, coord.y});
        }
        coordinates.remove(coordinates.size()-1);
        lines.get(0).refreshCoordinates(coordinates);
    }

    public void refreshCoordinates(Geometry geometry) {
        coordinates.clear();
        for (Coordinate coord: geometry.getCoordinates()) {
            coordinates.add(new double[] {coord.x, coord.y});
        }
        coordinates.remove(coordinates.size()-1);
        for (ShapeGL shapeGL : shapes) {
            if (shapeGL instanceof LineStripGL) {
                ((LineStripGL) shapeGL).refreshCoordinates(coordinates);
            }
            if (shapeGL instanceof PolygonGL) {
                ((PolygonGL) shapeGL).refreshCoordinates(coordinates);
            }
        }
    }


    public void setIsComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public boolean isDrawable () {
        if(coordinates.size()<3) {
            return false;
        }
        else {
            return true;
        }

    }


    public PolygonGL getPolygon() {
        return polygons.get(0);
    }

//    public LineGL getLine() {
//        return line;
//    }

    public LineStripGL getLine() {
        return lines.get(0);
    }

//    public void setTextureID(int textureID) {
//        if (polygon!=null) {
//            polygon.setTextureID(textureID);
//        }
//    }

    public void setTexture(Texture texture) {

        for (ShapeGL shape : shapes) {
            if(shape instanceof PolygonGL) {
                ((PolygonGL)shape).setTexture(texture);
            }
        }

    }

    public void select(){
        selected = true;
        isIn=false;
//        if (selectionLine != null) {
//            selectionLine.setVisible(true);
//            selectionLine.setColor(MapLayerViewer.selectionColor);
//        }
        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setColor(MapLayerViewer.selectionColorIn);
            selectionLine.setVisible(true);
        }
        for (LineStripGL selectionLineBis : selectionLinesBis) {
            selectionLineBis.setColor(MapLayerViewer.selectionColorOut);
            selectionLineBis.setVisible(true);
        }
    }

    public void deselect() {
        selected = false;
        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setColor(MapLayerViewer.inColorDrawn);
            selectionLine.setVisible(false);
        }
        for (LineStripGL selectionLineBis : selectionLinesBis) {
            //selectionLineBis.setColor(MapLayerViewer.selectionColorIn);
            selectionLineBis.setVisible(false);
        }
        //System.out.println("deselect!");
    }

    public void inElement() {
        isIn = true;
        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setVisible(true);
        }
    }

    public void outElement() {
        isIn = false;
        if (!selected) {
            for (LineStripGL selectionLine : selectionLines) {
                selectionLine.setVisible(false);
            }
        }
    }





    public Coordinate getLocation () {
        return geometry.getCentroid().getCoordinate();
       // return geometry.getEnvelope().getCentroid().getCoordinate();
    }

    public float getRadius() {
        MinimumBoundingCircle minimumBoundingCircle = new MinimumBoundingCircle(geometry);
        return (float)minimumBoundingCircle.getRadius();
    }

    public float[] getEnvelopeBounds() {
        float[] bounds = new float[4];
        bounds[0] = (float)geometry.getCoordinates()[0].x;
        bounds[1] = (float)geometry.getCoordinates()[0].y;
        bounds[2] = (float)geometry.getCoordinates()[2].x;
        bounds[3] = (float)geometry.getCoordinates()[2].y;
        return bounds;
    }


    public void bufferPolygon(double bufferOffset) {
        super.bufferCoordinates(bufferOffset);
        Texture texture = polygons.get(0).getTexture();
        updatePolygons(geometry.getCoordinates(), texture);

        offsetedGeometry=bufferCoordinates(bufferOffset, offsetedGeometry, coordinatesOffseted);
        Pair<List<PolygonGL>, List<double[]>> pair = updatePolygons(offsetedGeometry.getCoordinates(), texture, offsetedPolygons);
        offsetedPolygons = pair.getValue1();
        coordinatesOffseted = pair.getValue2();
        for (PolygonGL polygonOffseted: offsetedPolygons) {
            polygonOffseted.setOffseted(true);
            if (!shapes.contains(polygonOffseted)) {
                shapes.add(polygonOffseted);
            }
        }


    }

    public void calculatePolygonOffseted(double bufferOffset) {
        coordinatesOffseted.clear();
        BufferOp bufferOp = new BufferOp(geometry);
        Geometry newGeometry = bufferOp.getResultGeometry(bufferOffset);
        newGeometry = DouglasPeuckerSimplifier.simplify(newGeometry, bufferOffset / 100);
        offsetedGeometry = newGeometry;
        Pair<List<PolygonGL>, List<double[]>> pair =  generatePolygons(newGeometry.getCoordinates(), getTexture(), false);
        offsetedPolygons= pair.getValue1();
        coordinatesOffseted = pair.getValue2();
        for (PolygonGL polygonOffseted: offsetedPolygons) {
            polygonOffseted.setOffseted(true);
            shapes.add(polygonOffseted);
        }




    }

    public void offsetCoordinates() {
        offsetFromOriginal[0]+=offset[0];
        offsetFromOriginal[1]+=offset[1];
        if (offset != null) {
            for (double[] coord : coordinatesOffseted) {
                coord[0] = coord[0] + offset[0];
                coord[1] = coord[1] + offset[1];
            }
            //System.out.println("coordinatesOffseted size "+coordinatesOffseted.size());
            offsetedGeometry=calculateGeometry(coordinatesOffseted);
        }

        super.offsetCoordinates();

    }


    public void setTranslucentBorder(boolean translucent) {
        super.setTranslucentBorder(translucent);
        for (PolygonGL polygon: offsetedPolygons) {
            polygon.setVisible(translucent);
        }
        //polygonOffseted.setVisible(translucent);
        for (PolygonGL polygon: polygons) {
            polygon.setOnlyInFirst(translucent);
        }
    }

    public LineStripGL getSelectionLine() {
        return selectionLines.get(0);
    }

    public Texture getTexture() {
        return polygons.get(0).getTexture();
    }

    public void setColor(Vector3f color) {
        fillColor = color;
    }

    public void setOutlineColor(Vector3f outlineColor) {
        this.outlineColor = outlineColor;
        for (LineStripGL line: lines) {
            line.setColor(outlineColor);
        }

    }

    public void toggleOutlineVisible() {
        lines.get(0).setVisible(!lines.get(0).getVisible());
    }

    public void setLineVisible(boolean visible) {
        outlineVisible = visible;
        for (LineStripGL line: lines) {
            line.setVisible(visible);
            if (previousLineWidth != -1 && previousLineColor!=null) {
                line.setColor(previousLineColor);
                line.setLineWidth(previousLineWidth);
            }
            else {
                line.setColor(MapLayerViewer.defaultOutlineLine);
                line.setLineWidth(MapLayerViewer.outlineLineWidth);
            }
        }

    }

    public void setComposite(boolean isComposite) {
        this.isComposite = isComposite;
        if (isComposite) {
            //System.out.println("isComposite!");
            if (translucentBorder) {
                for (PolygonGL polygonOffseted: offsetedPolygons) {
                    polygonOffseted.setVisible(true);
                }
            }
            for (PolygonGL polygon: polygons) {
                polygon.setVisible(true);
            }

            if (previousLineWidth != -1 && previousLineColor != null) {
                for (LineStripGL line: lines) {
                    line.setColor(previousLineColor);
                    line.setLineWidth(previousLineWidth);
                }
            }
            else {
                for (LineStripGL line: lines) {
                    line.setVisible(false);
                }
            }
        }
        else {
            for (PolygonGL polygon: polygons) {
                polygon.setVisible(false);
            }
            for (PolygonGL polygonOffseted: offsetedPolygons) {
                polygonOffseted.setVisible(false);
            }
            previousLineColor = lines.get(0).getColor();
            previousLineWidth = lines.get(0).getLineWidth();
            for (LineStripGL line: lines) {
                line.setVisible(true);
                line.setColor(MapLayerViewer.areaBaseBoderColor);
                line.setLineWidth(MapLayerViewer.areaBaseLineWidth);
            }

        }
    }

    Pair<List<PolygonGL>, List<double[]>> generatePolygons (Coordinate[] coords, Texture texture, boolean addLines) {
        //System.out.println("------------------------generate polygons vector geo element -------------------------");
        List<double[]> coordinates = new ArrayList<>();
        List<PolygonGL> polygons = new ArrayList<>();


        CoordinateList newCoordinates = new CoordinateList(coords,false);
        int indexContains;
        int startPolygon = 0;
        for (int k=0; k<newCoordinates.size();k++) {
            indexContains =
                    coordinatesContains(new double[]{newCoordinates.getCoordinate(k).x,newCoordinates.getCoordinate(k).y}, coordinates);
            if(indexContains==-1) {

                coordinates.add(new double[]{newCoordinates.getCoordinate(k).x, newCoordinates.getCoordinate(k).y});
            }
            else if (indexContains==startPolygon) {
                if (addLines) {
                    LineStripGL line = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), MapLayerViewer.outlineLineWidth, outlineColor);
                    LineStripGL selectionLine = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()),MapLayerViewer.selectionLineWidth);
                    LineStripGL selectionLineBis = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), MapLayerViewer.selectionOutLineWidth, MapLayerViewer.selectionColorOut);
                    lines.add(line);
                    selectionLines.add(selectionLine);
                    selectionLinesBis.add(selectionLineBis);
                }
                PolygonGL polygon = new PolygonGL(coordinates.subList(startPolygon,coordinates.size()), texture,z);
                polygon.calculateGeometry();
                //polygon.printCoordinates();
                polygons.add(polygon);
                //startPolygon = coordinates.size()-1;
                startPolygon = coordinates.size();
            }

        }
        //System.out.println("generatePolygons size "+polygons.size());
        return new Pair(polygons, coordinates);
    }

    Pair<List<PolygonGL>, List<double[]>> updatePolygons (Coordinate[] coords, Texture texture, List<PolygonGL> polygons) {

        List<double[]> coordinates = new ArrayList<>();
        List<PolygonGL> newPolygons = new ArrayList<>();
        CoordinateList newCoordinates = new CoordinateList(coords,false);
        int indexContains;
        int startPolygon = 0;
        int polygonCount = 0;
        for (int k=0; k<newCoordinates.size();k++) {
            indexContains =
                    coordinatesContains(new double[]{newCoordinates.getCoordinate(k).x,newCoordinates.getCoordinate(k).y}, coordinates);
            if(indexContains==-1) {
                coordinates.add(new double[]{newCoordinates.getCoordinate(k).x, newCoordinates.getCoordinate(k).y});
            }
            else if (indexContains==startPolygon) {
                if (polygonCount < polygons.size()) {
                    polygons.get(polygonCount).refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    newPolygons.add(polygons.get(polygonCount));
                    polygonCount++;
                }
                else {
                    PolygonGL polygon = new PolygonGL(coordinates.subList(startPolygon, coordinates.size()), texture, z);
                    polygons.add(polygon);

                }
                startPolygon = coordinates.size();
            }

        }
        //System.out.println("generatePolygons size "+polygons.size());
        return new Pair(newPolygons, coordinates);
    }

    void updatePolygons(Coordinate[] coords, Texture texture) {
        List<double[]> coordinates = new ArrayList<>();
        List<PolygonGL> newPolygons = new ArrayList<>();
        List<LineStripGL> newLines = new ArrayList<>();
        List<LineStripGL> newSelectionLines = new ArrayList<>();
        List<LineStripGL> newSelectionLinesBis = new ArrayList<>();
        CoordinateList newCoordinates = new CoordinateList(coords,false);
        int indexContains;
        int startPolygon = 0;
        int polygonCount = 0;

        for (int k=0; k<newCoordinates.size();k++) {
            indexContains =
                    coordinatesContains(new double[]{newCoordinates.getCoordinate(k).x,newCoordinates.getCoordinate(k).y}, coordinates);
            if(indexContains==-1) {
                coordinates.add(new double[]{newCoordinates.getCoordinate(k).x, newCoordinates.getCoordinate(k).y});
            }
            else if (indexContains==startPolygon) {
                //System.out.println("indexContains == startPolygon");
                if (polygonCount < polygons.size() && !polygons.get(polygonCount).getIsHole()) {
                    PolygonGL current = polygons.get(polygonCount);
                    LineStripGL currentLine = lines.get(polygonCount);
                    LineStripGL currentSelectinLine = selectionLines.get(polygonCount);
                    LineStripGL currentSelectionLineBis = selectionLinesBis.get(polygonCount);
                    //System.out.println("Polygon count " + polygonCount);
                    //current.printCoordinates();
                    current.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    currentLine.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    currentSelectinLine.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    currentSelectionLineBis.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    //System.out.println("Polygon after ");
                    //current.printCoordinates();
                    newPolygons.add(current);
                    newLines.add(currentLine);
                    newSelectionLines.add(currentSelectinLine);
                    polygonCount++;
                }
                else {
                    //System.out.println("new polygon");
                    PolygonGL polygon = new PolygonGL(coordinates.subList(startPolygon, coordinates.size()), texture, z);
                    LineStripGL line = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), MapLayerViewer.outlineLineWidth, outlineColor);
                    LineStripGL selectionLine = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()),MapLayerViewer.selectionLineWidth);
                    LineStripGL selectionLineBis = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), MapLayerViewer.selectionOutLineWidth, MapLayerViewer.selectionColorOut);
                    lines.add(line);
                    selectionLines.add(selectionLine);
                    selectionLinesBis.add(selectionLineBis);
                    polygons.add(polygon);
                    newPolygons.add(polygon);
                    newLines.add(line);
                    lines.add(line);
                    selectionLines.add(selectionLine);
                    newSelectionLines.add(selectionLine);
                    newSelectionLinesBis.add(selectionLineBis);
                    selectionLinesBis.add(selectionLineBis);
                    polygonCount++;

                }
                startPolygon = coordinates.size();
            }

        }
        this.coordinates = coordinates;
        polygons = newPolygons;
        selectionLines = newSelectionLines;
        lines = newLines;
    }



    public boolean getIsComposite() {
        return isComposite;
    }

    public boolean getOutlineVisible() {
        return outlineVisible;
    }



    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
        for (PolygonGL polygon: polygons) {
            polygon.setAlpha(alpha);
        }
        for (PolygonGL polygon: offsetedPolygons) {
            polygon.setAlpha(alpha);
        }
    }

    public List<LineStripGL> getSelectionLines() {
        return selectionLines;
    }

    public List<LineStripGL> getSelectionLinesBis() {
        return selectionLinesBis;
    }

    public List<PolygonGL> getInPolygons() {return  inPolygons;}




















}
