/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.canvasEvents;

import fr.inria.ilda.ml.VectorLayer;

/**
 * Created by mjlobo on 03/08/15.
 */
public class CanvasLayerTranslucentEvent extends CanvasEvent {

    VectorLayer layer;

    public CanvasLayerTranslucentEvent(String event, VectorLayer layer) {
        super(event);
        this.layer = layer;
    }

    public VectorLayer getLayer() {
        return layer;
    }
}
