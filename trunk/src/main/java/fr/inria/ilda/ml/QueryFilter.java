/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
/**
 * Created by mjlobo on 11/06/15.
 */
package fr.inria.ilda.ml;

import java.util.HashSet;
import java.util.List;

public abstract class QueryFilter {

    protected Layer layer;

    public void setLayer (Layer layer) {
        this.layer = layer;
    }

    public Layer getLayer() {
        return layer;
    }

    public abstract HashSet<GeoElement> getElements();

    public abstract HashSet<GeoElement> getElements(List<GeoElement> elementsToConsider);

    public abstract HashSet<ShapeFileFeature> getFeatures();

    public abstract HashSet<ShapeFileFeature> getFeatures(HashSet<ShapeFileFeature> featuresToConsider);

    public abstract HashSet<GeoElement> update();

    public abstract HashSet<ShapeFileFeature> updateFeatures(HashSet<ShapeFileFeature> featuresToConsider);

    public abstract HashSet<ShapeFileFeature> updateFeatures();

}
