/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import fr.inria.ilda.ml.StateMachine.MenuActionEvent;
import fr.inria.ilda.ml.StateMachine.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 07/07/15.
 */
public class PopUpMenu {
    JPopupMenu popup;
    Collection<Texture> textures;
    Collection<String> actions;
    HashMap <String, JMenuItem> menuItems;

    public PopUpMenu(Collection<Texture> textures) {
        this.textures = textures;
        menuItems = new HashMap<>();
        createPopupMenu();
    }

    public PopUpMenu(Collection<Texture> textures, List<String> actions) {
        this.textures = textures;
        this.actions = actions;
        menuItems = new HashMap<>();
        createPopupMenu();
    }

    public PopUpMenu(List<String> actions) {
        this.actions = actions;
        menuItems = new HashMap<>();
        createPopUpMenuActions();
    }

    public void createPopupMenu() {
        //final JMenuItem menuItem;

        //Create the popup menu.
        popup = new JPopupMenu();
        for (Texture texture : textures) {
            final JMenuItem menuItem = new JMenuItem(texture.getName());
            menuItems.put(texture.getName(), menuItem);
            popup.add(menuItem);
        }

        //popup.addSeparator();

        for (String action: actions) {
            final JMenuItem menuItem = new JMenuItem(action);
            menuItems.put(action, menuItem);
            popup.add(menuItem);
            //menuItem.setBackground(new Color(1.0f,0.0f,1.0f));
        }
    }

    public void createPopUpMenuActions() {
        popup = new JPopupMenu();
        for (String action: actions) {
            final JMenuItem menuItem = new JMenuItem(action);
            menuItems.put(action, menuItem);
            popup.add(menuItem);
            //menuItem.setBackground(new Color(1.0f,0.0f,1.0f));
        }
    }

    public void addListeners(final StateMachine stateMachine) {
        for (final JMenuItem item: menuItems.values()) {
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    stateMachine.handleEvent(new MenuActionEvent(item, item.getText()));
                    //System.out.println("Action in PopUpMEnu "+((JMenuItem)e.getSource()).getText());
                }
            });
        }
    }





    public void showPopUpMenu(int x, int y, Component component) {
        //PopUpMenu demo = new PopUpMenu();
//        frame.setJMenuBar(demo.createMenuBar());
//        frame.setContentPane(demo.createContentPane());

        //Create and set up the popup menu.
        //demo.createPopupMenu();

        //Display the window.
//        frame.setSize(450, 260);
        popup.setVisible(true);
        popup.show(component,x,y);
    }

    public void showPopUpMenu(int x, int y, Component component, boolean showActions) {
        if (showActions) {
            for (String action:actions) {
                menuItems.get(action).setEnabled(true);
            }
        }
        else {
            for (String action:actions) {
                menuItems.get(action).setEnabled(false);
            }
        }
        popup.setVisible(true);
        popup.show(component,x,y);
    }






}

class PopupListener extends MouseAdapter {
    JPopupMenu popup;

    PopupListener(JPopupMenu popupMenu) {
        popup = popupMenu;
    }

    public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
            popup.show(e.getComponent(),
                    e.getX(), e.getY());
        }
    }
}

