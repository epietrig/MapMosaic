/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
/**
 * Created by mjlobo on 22/03/15.
 */
package fr.inria.ilda.ml.StateMachine;

public abstract class Action {
    public abstract String perform(StateMachine stateMachine, Event event);
}
