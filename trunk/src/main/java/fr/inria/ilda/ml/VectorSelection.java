/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by mjlobo on 05/06/15.
 */
public class VectorSelection {
    private GeometryQueryFilter geometricFilter;
    private List<QueryFilter> filters;
    private HashSet<GeoElement> selectedElements;
    private HashSet<GeoElement> selectedElementsBuffered;
    protected HashSet<ShapeFileFeature> features;
    protected HashSet<GeoElement> removedElements;
    protected HashSet<ShapeFileFeature> featuresInGeometry;
    private boolean isVisible = false;
    private boolean isEmpty = false;
    private Texture texture;
    private Texture backgroundTexture;
    protected VectorLayer layer;
    protected VectorLayer bufferedLayer;
    protected boolean selected;
    protected boolean in;
    protected Vector3f color;
    GLUtilities.RenderedElementTypes selectedType;
    protected Vector3f outlineColor;
    boolean outlineVisible;
    Texture blurredTexture;
    double buffer=0.0;
    float alpha=1.0f;
    float alphaBackground = 1.0f;
    protected Layer selectionLayer;
    GeoElement inElement;
    GeoElement selectedElement;





      public VectorSelection(GeometryQueryFilter geometricFilter, Texture texture, VectorLayer layer) {
        this.geometricFilter = geometricFilter;
        this.layer = layer;
        filters = new ArrayList<>();
        this.texture = texture;
        this.backgroundTexture = texture;
        selectedElements = new HashSet<>();
        for (GeoElement ge : layer.getElements()) {
            selectedElements.add(ge);
        }
        setTexture(texture);
        if (selectedElements.size()==0) {
            isEmpty = true;
        }
          selectedType = GLUtilities.RenderedElementTypes.TEXTURED;
          color = MapLayerViewer.defaultFillColor;
          outlineColor = MapLayerViewer.defaultOutlineLine;
    }

    public VectorSelection(GeometryQueryFilter geometricFilter, VectorSelection otherVectorSelection) {
        this.geometricFilter = geometricFilter;
        this.layer = otherVectorSelection.getLayer();
        filters = new ArrayList<>();
        this.texture = otherVectorSelection.getTexture();
        this.backgroundTexture = otherVectorSelection.getBackgroundTexture();
        selectedElements = new HashSet<>();
        for (GeoElement ge : otherVectorSelection.getSelectedElements()) {
            selectedElements.add(ge);
        }
        setTexture(texture);
        if (selectedElements.size()==0) {
            isEmpty = true;
        }
        for (QueryFilter filter : filters) {
            if (filter instanceof NumericQueryFilter) {
                filters.add(new NumericQueryFilter((NumericQueryFilter)filter));
            }
            if (filter instanceof StringQueryFilter) {
                filters.add(new StringQueryFilter((StringQueryFilter)filter));
            }
            if (filter instanceof FreeQueryFilter) {
                filters.add(new FreeQueryFilter((FreeQueryFilter)filter));
            }
        }
        selectedType = otherVectorSelection.getSelectedType();
        color = otherVectorSelection.getColor();
        outlineColor = otherVectorSelection.getOutlineColor();
        outlineVisible = otherVectorSelection.getOutlineVisible();
        alpha = otherVectorSelection.getAlpha();
        alphaBackground = otherVectorSelection.getAlphaBackground();


    }

    public VectorSelection(List<QueryFilter> filters, Texture texture, VectorLayer layer) {
        this.layer = layer;
        this.texture = texture;
        this.filters = filters;
        List <ShapeFileFeature> features = null;
        removedElements = new HashSet<>();
        featuresInGeometry = new HashSet<>();

        selectedType = GLUtilities.RenderedElementTypes.TEXTURED;
        color = MapLayerViewer.defaultFillColor;
        outlineColor = MapLayerViewer.defaultOutlineLine;

    }

    public VectorSelection(List<QueryFilter> filters, Vector3f color, VectorLayer layer) {
        this.layer = layer;
        this.filters = filters;
        List <ShapeFileFeature> features = null;
        removedElements = new HashSet<>();
        featuresInGeometry = new HashSet<>();
        this.color = color;

        selectedType = GLUtilities.RenderedElementTypes.FILLED;

    }

    public VectorSelection(GeometryQueryFilter geometricFilter, Vector3f color, VectorLayer layer) {


        this.geometricFilter = geometricFilter;
        this.layer = layer;
        filters = new ArrayList<>();

        //filters.add(geometricFilter);
        this.color = color;
        this.backgroundTexture = texture;
        selectedElements = new HashSet<>();
        //selectedElements = geometricFilter.getElements();

        for (GeoElement ge : layer.getElements()) {
            selectedElements.add(ge);
        }

        setTexture(texture);

        if (selectedElements.size()==0) {
            isEmpty = true;
        }

        removedElements = new HashSet<>();
        featuresInGeometry = new HashSet<>();
        selectedType = GLUtilities.RenderedElementTypes.FILLED;
        //color = MapLayerViewer.defaultFillColor;
        outlineColor = MapLayerViewer.defaultOutlineLine;
    }


    public VectorSelection(List<QueryFilter> filters, Texture texture, VectorLayer layer, HashSet<ShapeFileFeature> features) {
        this.filters = filters;
        this.texture = texture;
        this.layer = layer;
        this.features = features;
        selectedElements = new HashSet<>();
        removedElements = new HashSet<>();
        featuresInGeometry = new HashSet<>();
        for (ShapeFileFeature feature : features) {
            for (VectorGeoElement ge : feature.getVectorGeoElements()) {
                selectedElements.add(ge);

            }
        }

    }

    public void toggleVisibility() {
        isVisible = !isVisible;
        //setElementsVisibility(isVisible);
    }



   public void update() {

      features = null;

       features = (HashSet<ShapeFileFeature>)layer.getFeatures().clone();
        for (QueryFilter filter: filters) {
           if (features == null) {
               features = filter.updateFeatures();
           }
           else {
               features.retainAll(filter.updateFeatures());
           }
       }
       HashSet<GeoElement> newSelectedElements = new HashSet<>();
       for (ShapeFileFeature feature : features) {
           for (VectorGeoElement ge : feature.getVectorGeoElements()) {
               newSelectedElements.add(ge);
           }
       }

       selectedElements = newSelectedElements;
       setTexture(texture);

   }


    public GeometryQueryFilter getGeometricFilter() {
        return geometricFilter;
    }

    public HashSet<GeoElement> getSelectedElements() {
        return selectedElements;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
        if (geometricFilter==null) {
            for (GeoElement ge : selectedElements) {
                ge.setTexture(texture);
                ge.setRenderingType(GLUtilities.RenderedElementTypes.TEXTURED);
            }
        }
    }

    public List<QueryFilter> getFilters() {return filters;}

    public QueryFilter getNumericFilterByLayerAndAttribute (VectorLayer layer, ShapeFileAttribute attribute) {
        for (QueryFilter filter : filters) {
            if (filter instanceof  NumericQueryFilter) {
                if (filter.getLayer() == layer && ((NumericQueryFilter) filter).getAttribute()==attribute) {
                    return filter;
                }
            }
        }
        return null;
    }

    public QueryFilter getStringFilterByLayerAndAttribute(VectorLayer layer, ShapeFileAttribute attribute) {
        for (QueryFilter filter : filters) {
            if (filter instanceof  StringQueryFilter) {
                if (filter.getLayer() == layer && ((StringQueryFilter) filter).getAttribute()==attribute) {
                    return filter;
                }
            }
        }
        return null;
    }

    public QueryFilter getFreeQueryFilter (VectorLayer layer) {
        for (QueryFilter filter: filters) {
            if (filter instanceof FreeQueryFilter) {
                if (filter.getLayer() == layer) {
                    return filter;
                }
            }
        }
        return null;
    }

    public VectorLayer getLayer() {

        return layer;
    }

    public List<GeometryQueryFilter> getGeometryQueryFilters() {
        List<GeometryQueryFilter> geometryQueryFilters = new ArrayList<>();
        for (QueryFilter filter: filters) {
            if (filter instanceof GeometryQueryFilter) {
                geometryQueryFilters.add((GeometryQueryFilter)filter);

            }
        }
        return geometryQueryFilters;
    }

    public void addFilter (QueryFilter filter) {
        if (filter instanceof GeometryQueryFilter) {
            geometricFilter = (GeometryQueryFilter)filter;
        }

        filters.add(filter);
    }

    public void removeElements (HashSet <GeoElement> elementsToRemove) {
        selectedElements.removeAll(elementsToRemove);
        removedElements.addAll(elementsToRemove);
        //System.out.println("removed elements length "+removedElements.size());
    }

    public HashSet<GeoElement> getRemovedElements () {
        return removedElements;
    }

    public boolean getIsVisible() {
        return  isVisible;
    }

    public void setBackgroundTexture(Texture backgroundTexture) {
        this.backgroundTexture = backgroundTexture;
    }

    public Texture getBackgroundTexture() {
        return backgroundTexture;
    }

    public Texture getTexture() {
        return texture;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        in = false;
    }

    public boolean containsPoint (double x, double y) {
        //System.out.println("selected elements size "+selectedElements.size());
        for (GeoElement element: selectedElements) {
            if (element.containsPoint(x,y)) {
                return true;
            }
        }
        return false;
    }

    public GeoElement geoElementContainsPoint(double x, double y) {
        for (GeoElement element: selectedElements) {
            if (element.containsPoint(x,y)) {
                return element;
            }
        }
        return null;
    }


    public void inSelection() {
        in = true;
    }

    public void outSelected() {
        in = false;
    }

    public boolean getIn() {
        return in;
    }

    public Vector3f getColor() {
        return color;
    }

    public void setColor(Vector3f color) {
        if (geometricFilter == null) {
            for (GeoElement ge : selectedElements) {
                ge.setFillColor(color);
                ge.setRenderingType(GLUtilities.RenderedElementTypes.FILLED);
            }
        }
        this.color = color;
    }





    public GLUtilities.RenderedElementTypes getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(GLUtilities.RenderedElementTypes selectedType) {
        this.selectedType = selectedType;
    }

    public Vector3f getOutlineColor() {
        return outlineColor;
    }

    public void setOutlineColor(Vector3f outlineColor) {
        this.outlineColor = outlineColor;
    }


    public void toggleOutlineVisible() {
        outlineVisible = !outlineVisible;
    }

    public boolean getOutlineVisible() {
        return outlineVisible;
    }

    public void setBlurredTexture(Texture blurredTexture) {
        this.blurredTexture = blurredTexture;
    }

    public Texture getBlurredTexture() {
        return blurredTexture;
    }

    public double getBuffer() {return buffer;}

    public void setBuffer(double buffer) {
        this.buffer = buffer;

    }

    public boolean bufferSelectedElements(double buffer) {
        boolean result = false;
        if (bufferedLayer == null) {
            bufferedLayer = new VectorLayer(false, null);
            selectedElementsBuffered = new HashSet<>();
            for (GeoElement ge: selectedElements) {
                VectorGeoElement bufferedVectorGeoElement = new VectorGeoElement((VectorGeoElement)ge);
                bufferedVectorGeoElement.bufferOnlyBasePolygon(buffer);
                selectedElementsBuffered.add(bufferedVectorGeoElement);
                bufferedLayer.addElement(bufferedVectorGeoElement);
            }
            result = true;

        }
        else {
            for (GeoElement ge: selectedElements) {
                ((VectorGeoElement)ge).bufferOnlyBasePolygon(buffer);
            }
        }
        return result;

    }

    public void setOutlineVisible(boolean outlineVisible) {
        this.outlineVisible = outlineVisible;
    }

    public HashSet<GeoElement> getSelectedElementsBuffered() {
        return selectedElementsBuffered;
    }

    public void setSelectedElements(HashSet<GeoElement> selectedElements) {
        this.selectedElements = selectedElements;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha){
        this.alpha = alpha;
    }

    public float getAlphaBackground() {
        return alphaBackground;
    }

    public void setAlphaBackground(float alphaBackground) {
        this.alphaBackground = alphaBackground;
    }


    public GeoElement getInElement() {
        return  inElement;
    }

    public GeoElement getSelectedElement() {
        return  selectedElement;
    }

    public void setInElement(GeoElement inElement) {
        this.inElement = inElement;
    }

    public void setSelectedElement(GeoElement selectedElement) {
        this.selectedElement = selectedElement;
    }

    public VectorLayer getBufferedLayer() {
        return bufferedLayer;
    }

    public void setBufferedLayer(VectorLayer bufferedLayer) {
        this.bufferedLayer = bufferedLayer;
        for (GeoElement ge: selectedElements) {
            selectedElementsBuffered.add(ge);
        }
    }




}
