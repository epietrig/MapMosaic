/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.routeAnalysis;

import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import org.geotools.graph.structure.GraphVisitor;
import org.geotools.graph.structure.Graphable;
import org.geotools.graph.structure.basic.BasicEdge;
import org.geotools.graph.traverse.GraphTraversal;
import org.opengis.feature.simple.SimpleFeature;

import javax.vecmath.Vector2d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mjlobo on 19/06/15.
 */
public class AngleVisitor implements GraphVisitor {
    String attribute;
    List<Stroke> strokes;
    Stroke currentStroke = null;


    public AngleVisitor(String attribute) {
        this.attribute = attribute;
        strokes = new ArrayList<>();

    }

    public int visit(Graphable graphable) {
        Iterator related = graphable.getRelated();
        SimpleFeature currentFeature = ((SimpleFeature)graphable.getObject());
        MultiLineString currentLine=(MultiLineString)currentFeature.getDefaultGeometry();
        int nextCount =0;
        //System.out.println("visiting before if-------------------------------------------------->"+graphable.getCount());
        //if (related.hasNext()) System.out.println("related has next");
        //if (graphable.isVisited()) System.out.println("visited");
        if(related.hasNext() && !graphable.isVisited()){
            //System.out.println("visiting-------------------------------------------------->");
            graphable.setVisited(true);
            SimpleFeature nextFeature = (SimpleFeature)((BasicEdge)related.next()).getObject();
            MultiLineString nextLine=(MultiLineString)nextFeature.getDefaultGeometry();

            if (currentStroke == null) {
                currentStroke = new Stroke();
                currentStroke.addMultiLineString(currentLine);
                strokes.add(currentStroke);
            }
            if (currentFeature.getAttribute(attribute).equals(nextFeature.getAttribute(attribute))) {
                //System.out.println("Same attribute "+attribute);
                double angle = Math.toDegrees(calculateAngle(currentLine, nextLine));
                if (!(angle<90 && angle >40)) {
                    //System.out.println("Same stroke yuju");
                    currentStroke.addMultiLineString(nextLine);
//                    System.out.println("Current first point " + ((LineString) currentLine.getGeometryN(0)).getStartPoint().getCoordinate().x + ":" + ((LineString) currentLine.getGeometryN(0)).getStartPoint().getCoordinate().y);
//                    System.out.println("Current last point "+ ((LineString)currentLine.getGeometryN(currentLine.getNumGeometries()-1)).getEndPoint().getCoordinate().x+":"+((LineString)currentLine.getGeometryN(currentLine.getNumGeometries()-1)).getEndPoint().getCoordinate().y);
//                    System.out.println("Next first point " + ((LineString) nextLine.getGeometryN(0)).getStartPoint().getCoordinate().x + ":" + ((LineString) nextLine.getGeometryN(0)).getStartPoint().getCoordinate().y);
//                    System.out.println("Next last point "+ ((LineString)nextLine.getGeometryN(nextLine.getNumGeometries()-1)).getEndPoint().getCoordinate().x+":"+((LineString)nextLine.getGeometryN(nextLine.getNumGeometries()-1)).getEndPoint().getCoordinate().y);

                }
                else {
                    currentStroke = new Stroke();
                    currentStroke.addMultiLineString(nextLine);
                    strokes.add(currentStroke);
                }

            }
            else {
                currentStroke = new Stroke();
                currentStroke.addMultiLineString(nextLine);
                strokes.add(currentStroke);
            }
            //System.out.println("Numero: "+((SimpleFeature) graphable.getObject()).getAttribute("NUMERO"));
            //System.out.println("Angle " + Math.toDegrees(calculateAngle(currentLine, nextLine)));
            //System.out.println("next geometry "+next.getGeometryType());
            nextCount++;
            // no related components makes this an orphan
            //System.out.println("((BasicEdge)related.next()).getObject()"+((BasicEdge)related.next()).getObject());
           // System.out.println("related.next().getClass()"+related.next().getClass());
            //System.out.println("graphable.getObject()"+graphable.getObject());
            //System.out.println("has next");
        }
        //System.out.println("Stroke length "+strokes.size());
        return GraphTraversal.CONTINUE;
    }

    double calculateAngle(MultiLineString l1, MultiLineString l2){
        Vector2d vectorL1 = new Vector2d(((LineString)l1.getGeometryN(0)).getStartPoint().getCoordinate().x-((LineString)l1.getGeometryN(l1.getNumGeometries()-1)).getEndPoint().getCoordinate().x,((LineString)l1.getGeometryN(0)).getStartPoint().getCoordinate().y -((LineString)l1.getGeometryN(l1.getNumGeometries()-1)).getEndPoint().getCoordinate().y);
        Vector2d vectorL2 = new Vector2d(((LineString)l2.getGeometryN(0)).getStartPoint().getCoordinate().x-((LineString)l2.getGeometryN(l2.getNumGeometries()-1)).getEndPoint().getCoordinate().x,((LineString)l2.getGeometryN(0)).getStartPoint().getCoordinate().y -((LineString)l2.getGeometryN(l2.getNumGeometries()-1)).getEndPoint().getCoordinate().y);
        double angle = vectorL1.angle(vectorL2);
        return angle;
    }

    public List<Stroke> getStrokes() {
        return strokes;
    }
}
