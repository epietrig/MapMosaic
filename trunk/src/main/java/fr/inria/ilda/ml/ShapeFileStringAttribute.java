/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

/**
 * Created by mjlobo on 22/06/15.
 */

import fr.inria.ilda.ml.queryWidgets.TextQuery;
import org.opengis.feature.type.Name;

import java.util.LinkedList;

public class ShapeFileStringAttribute extends ShapeFileAttribute{

    protected LinkedList<String> values;

    public ShapeFileStringAttribute(Name typeName, String localName) {
        super(typeName, localName);
        this.values = new LinkedList<String>();
    }


    @Override
    public void update(Object o) {
        String s = o.toString();
        if(!values.contains(s)) {
            values.add(s);
        }
    }


    public LinkedList<String> getValues() {
        return values;
    }


    @Override
    public void buildQueryComponent(int sizeLabel) {
        this.queryWidget = new TextQuery(localName, values, sizeLabel);
    }

}
