/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.routeAnalysis;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineString;

import javax.vecmath.Vector2d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 17/06/15.
 */
public class StrokeNetwork {

    List<Stroke> strokes;
    List<RouteSegment> routeSegments;
    HashMap<Node, List<Stroke>> startNodes;
    HashMap<Node, List<Stroke>> endNodes;
    HashMap<Integer, List<RouteSegment>> strokeIds;
    List <Stroke> currentStrokes;
    double minAngle = Math.PI/2;
    List<Node> nodes;
    int currentStrokeId=0;

    public StrokeNetwork(List<LineString> lines) {
        strokes = new ArrayList<>();
        routeSegments = new ArrayList<>();
        startNodes = new HashMap<>();
        endNodes = new HashMap<>();
        currentStrokes = new ArrayList<>();
        nodes = new ArrayList<>();
        strokeIds = new HashMap<>();
        //buildNetwork(lines);
        buildNetwork2(lines);

    }

    void buildNetwork(List<LineString> lines) {
        Node startNode;
        Node endNode;
        Coordinate secondPoint;
        Coordinate penultimatePoint;
        RouteSegment segment;

        for (LineString line : lines) {
            startNode = getNodeByCoordinate(line.getStartPoint().getCoordinate());
            //System.out.println("Start Point "+line.getStartPoint().getCoordinate().x+":"+line.getStartPoint().getCoordinate().y);
            if (startNode == null) {

                startNode = new Node(line.getStartPoint().getCoordinate());
                startNodes.put(startNode, new ArrayList<Stroke>());
            }
            endNode = getNodeByCoordinate(line.getEndPoint().getCoordinate());
            //System.out.println("End Point "+line.getEndPoint().getCoordinate().x+":"+line.getEndPoint().getCoordinate().y);
            if (endNode == null) {
                endNode = new Node(line.getEndPoint().getCoordinate());
                endNodes.put(endNode, new ArrayList<Stroke>());
            }
            secondPoint = line.getCoordinateN(1);
            penultimatePoint = line.getCoordinateN(line.getCoordinates().length-2);
            segment = new RouteSegment(startNode, endNode, secondPoint, penultimatePoint, line);
            routeSegments.add(segment);

            if (endNodes.keySet().contains(startNode)) {
                double minimumAngle = Double.MAX_VALUE;
                double maximumAngle = Double.MIN_VALUE;
                Stroke closerStroke = null;
                for (Stroke stroke: endNodes.get(startNode)) {
                    if (stroke.getLastRouteSegment().getEndVector().angle(segment.getStartVector())>maximumAngle) {
                        maximumAngle = stroke.getLastRouteSegment().getEndVector().angle(segment.getStartVector());
                        closerStroke = stroke;
                    }
                }
                if (maximumAngle > minAngle && closerStroke !=null) {
                    endNodes.get(startNode).remove(closerStroke);
                    if (!endNodes.keySet().contains(endNode)) {
                        endNodes.put(endNode, new ArrayList<Stroke>());
                    }
                    endNodes.get(endNode).add(closerStroke);
                    closerStroke.addRouteSegment(segment);

                }
                else {
                    Stroke stroke = new Stroke();
                    stroke.addRouteSegment(segment);
                    strokes.add(stroke);
                    if (!startNodes.keySet().contains(startNode)) {
                        startNodes.put(startNode, new ArrayList<Stroke>());
                    }
                    startNodes.get(startNode).add(stroke);
                    if (!endNodes.keySet().contains(endNode)) {
                        endNodes.put(endNode, new ArrayList<Stroke>());
                    }
                    endNodes.get(endNode).add(stroke);
                }

            }
            else {
                Stroke stroke = new Stroke();
                stroke.addRouteSegment(segment);
                strokes.add(stroke);
                if (!startNodes.keySet().contains(startNode)) {
                    startNodes.put(startNode, new ArrayList<Stroke>());
                }
                startNodes.get(startNode).add(stroke);
                if (!endNodes.keySet().contains(endNode)) {
                    endNodes.put(endNode, new ArrayList<Stroke>());
                }
                endNodes.get(endNode).add(stroke);
            }


        }
    }

    void buildNetwork2 (List<LineString> lines) {
        Node startNode;
        Node endNode;
        Coordinate secondPoint;
        Coordinate penultimatePoint;
        RouteSegment segment;

        for (LineString line : lines) {
            startNode = getNodeByCoordinate2(line.getStartPoint().getCoordinate());
            if (startNode == null) {

                startNode = new Node(line.getStartPoint().getCoordinate());
                nodes.add(startNode);
            }

            endNode = getNodeByCoordinate2(line.getEndPoint().getCoordinate());
            //System.out.println("End Point "+line.getEndPoint().getCoordinate().x+":"+line.getEndPoint().getCoordinate().y);
            if (endNode == null) {
                endNode = new Node(line.getEndPoint().getCoordinate());
                nodes.add(endNode);
            }
            secondPoint = line.getCoordinateN(1);
            penultimatePoint = line.getCoordinateN(line.getCoordinates().length-2);
            segment = new RouteSegment(startNode, endNode, secondPoint, penultimatePoint, line);
            routeSegments.add(segment);
        }


        double thresholdAngle = 5*Math.PI/6;
        double minThresholdAngle = Math.PI/6;
        //thresholdAngle = Math.PI;

        for (RouteSegment routeSegment: routeSegments) {
            double maxAngle = Double.MIN_VALUE;
            double minAngle = Double.MAX_VALUE;
            double angle = Double.MIN_VALUE;
            int selectedStrokeid = -1;
            //Road segment doesn't have neighbours we set its stroke id to current strokeid
            if (getEndNeighbours(routeSegment).size()==0 && getStartNeighbours(routeSegment).size()==0) {
                strokeIds.put(currentStrokeId, new ArrayList<RouteSegment>());
                routeSegment.setStrokeId(currentStrokeId);
                strokeIds.get(currentStrokeId).add(routeSegment);
                currentStrokeId++;
            }
            else{
                for(RouteSegment startNeighbour: getStartNeighbours(routeSegment)) {
                    if (startNeighbour.getStrokeId()!=-1) {
                        if(startNeighbour.getStartNode() == routeSegment.getStartNode()) {
                            angle = startNeighbour.getStartVector().angle(routeSegment.getStartVector());

                        }
                        else if(startNeighbour.getEndNode() == routeSegment.getStartNode()) {
                            Vector2d oppEndVector = new Vector2d(-startNeighbour.getEndVector().x, -startNeighbour.getEndVector().y);
                            //angle = startNeighbour.getEndVector().angle(routeSegment.getStartVector());
                            angle = oppEndVector.angle(routeSegment.getStartVector());
                        }

                        if (angle > maxAngle) {
                            maxAngle = angle;
                            selectedStrokeid = startNeighbour.getStrokeId();
                        }
                    }
                }

                if (selectedStrokeid != -1 && maxAngle>thresholdAngle) {
                    routeSegment.setStrokeId(selectedStrokeid);
                    strokeIds.get(selectedStrokeid).add(routeSegment);
                }

                selectedStrokeid = -1;
                maxAngle = Double.MIN_VALUE;
                angle = Double.MIN_VALUE;
                for (RouteSegment endNeighbour : getEndNeighbours(routeSegment)) {
                    if (endNeighbour.getStrokeId()!=-1) {
                        if(endNeighbour.getStartNode() == routeSegment.getEndNode()) {
                            Vector2d oppEndVector = new Vector2d(-routeSegment.getEndVector().x, -routeSegment.getEndVector().y);
                            //Vector2d oppStartVector = new Vector2d(-endNeighbour.getStartVector().x, -endNeighbour.getStartVector().y);
                            //angle = endNeighbour.getStartVector().angle(routeSegment.getEndVector());
                            angle = oppEndVector.angle(endNeighbour.getStartVector());
                            System.out.println("angle "+angle);
                        }
                        else if (endNeighbour.getEndNode() == routeSegment.getEndNode()) {
                            Vector2d oppEndVector = new Vector2d(-routeSegment.getEndVector().x, -routeSegment.getEndVector().y);
                            Vector2d oppEndVectorNeighbour = new Vector2d(-endNeighbour.getEndVector().x, -endNeighbour.getEndVector().y);
                            //angle = endNeighbour.getEndVector().angle(routeSegment.getEndVector());
                            angle = oppEndVector.angle(oppEndVectorNeighbour);
                            System.out.println("angle "+angle);
                        }
                        if (angle > maxAngle && maxAngle>thresholdAngle) {
                            maxAngle = angle;
                            selectedStrokeid = endNeighbour.getStrokeId();
                        }
                    }
                }

                if (selectedStrokeid != -1 && selectedStrokeid != routeSegment.getStrokeId() && routeSegment.getStrokeId()!=-1) {
                    for (RouteSegment idPropagated : getRouteSegmentsbyStrokeId(selectedStrokeid)) {
                        idPropagated.setStrokeId(routeSegment.getStrokeId());
                        strokeIds.get(routeSegment.getStrokeId()).add(idPropagated);
                        System.out.println("propagating");
                    }
                    strokeIds.get(selectedStrokeid).clear();
                }
                else if (selectedStrokeid!=-1 && routeSegment.getStrokeId()==-1) {
                    routeSegment.setStrokeId(selectedStrokeid);
                    strokeIds.get(selectedStrokeid).add(routeSegment);
                }

            }
            if (routeSegment.getStrokeId() == -1) {
                routeSegment.setStrokeId(currentStrokeId);
                strokeIds.put(currentStrokeId, new ArrayList<RouteSegment>());
                strokeIds.get(currentStrokeId).add(routeSegment);
                currentStrokeId++;
            }


        }

        System.out.println("Number of nodes "+nodes.size());
        printRouteSegments();
        createStrokes();


    }

    public void addStroke(Stroke s) {
        strokes.add(s);
    }

    public List<Stroke> getStrokes() {
        return strokes;
    }

    public Node getNodeByCoordinate(Coordinate coordinate) {
        for (Node startNode : startNodes.keySet()) {
            if (startNode.getCoordinate().x == coordinate.x && startNode.getCoordinate().y==coordinate.y) {
                return startNode;
            }
        }
        for (Node endNode: endNodes.keySet()) {
            if (endNode.getCoordinate().x == coordinate.x && endNode.getCoordinate().y == coordinate.y) {
                return endNode;
            }
        }
        return null;
    }

    public Node getNodeByCoordinate2(Coordinate coordinate) {
        for (Node node: nodes) {
            if (node.getCoordinate().x == coordinate.x && node.getCoordinate().y==coordinate.y) {
                return node;
            }
        }
        return null;
    }

    public void printNetwork() {
        for (Stroke s: strokes) {
            System.out.println("-------------------- Strokes -----------------------");
            for (RouteSegment routeSegment: s.getRouteSegmentList()) {
                System.out.println("Route segment start node" + routeSegment.getStartNode().getCoordinate().x +":" + routeSegment.getStartNode().getCoordinate().y);
                System.out.println("Route segment end node" + routeSegment.getEndNode().getCoordinate().x +":" + routeSegment.getEndNode().getCoordinate().y);
            }
        }
    }

    public void printNodes() {
        HashMap <Node, List<RouteSegment>> nodeRouteSegmentHashMap = new HashMap<>();
        for (RouteSegment routeSegment: routeSegments) {
            if(nodeRouteSegmentHashMap.containsKey(routeSegment.getStartNode())) {
                nodeRouteSegmentHashMap.get(routeSegment.getStartNode()).add(routeSegment);
            }
            else {
                nodeRouteSegmentHashMap.put(routeSegment.getStartNode(), new ArrayList<RouteSegment>());
                nodeRouteSegmentHashMap.get(routeSegment.getStartNode()).add(routeSegment);
            }
            if (nodeRouteSegmentHashMap.containsKey(routeSegment.getEndNode())) {
                nodeRouteSegmentHashMap.get(routeSegment.getEndNode()).add(routeSegment);
            }
            else {
                nodeRouteSegmentHashMap.put(routeSegment.getEndNode(), new ArrayList<RouteSegment>());
                nodeRouteSegmentHashMap.get(routeSegment.getEndNode()).add(routeSegment);
            }


        }

        for (Node node: nodeRouteSegmentHashMap.keySet()) {
            System.out.println("Node contains "+nodeRouteSegmentHashMap.get(node).size());
        }
    }

    public List<RouteSegment> getStartNeighbours(RouteSegment current) {
        ArrayList <RouteSegment> result = new ArrayList<>();
        for (RouteSegment routeSegment: routeSegments) {
            if (routeSegment.getStartNode() == current.getStartNode()) {
                result.add(routeSegment);
            }

            if(routeSegment.getEndNode() == current.getStartNode()) {
                result.add(routeSegment);
            }
        }
        return result;
    }

    public List<RouteSegment> getEndNeighbours(RouteSegment current) {
        ArrayList <RouteSegment> result = new ArrayList<>();
        for (RouteSegment routeSegment: routeSegments) {
            if (routeSegment.getStartNode() == current.getEndNode()) {
                result.add(routeSegment);
            }

            if(routeSegment.getEndNode() == current.getEndNode()) {
                result.add(routeSegment);
            }
        }
        return result;
    }

    public Stroke getStrokeContainingSegment (RouteSegment routeSegment) {
        for (Stroke stroke : strokes) {
            if (stroke.getRouteSegmentList().contains(routeSegment)) {
                return stroke;
            }

        }
        return null;
    }

    public List<RouteSegment> getRouteSegmentsbyStrokeId (int id) {
        ArrayList <RouteSegment> result = new ArrayList<>();
        for (RouteSegment routeSegment: routeSegments) {
            if(routeSegment.getStrokeId() == id) {
                result.add(routeSegment);
            }
        }
        return result;
    }

    public void printRouteSegments() {
        for(RouteSegment routeSegment: routeSegments) {
            System.out.println("Route segment stroke id " +routeSegment.getStrokeId());
        }
    }

    void createStrokes ()
    {
        for (int strokeId : strokeIds.keySet()) {
            if (strokeIds.get(strokeId).size()>0) {
                strokes.add(new Stroke(strokeIds.get(strokeId)));
            }
        }
    }

}
