/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * Created by mjlobo on 05/10/15.
 */
public class TransferableLayerOptions implements Transferable {

    //protected static DataFlavor layerOptionsDataFlavor = new DataFlavor(LayerOptions.class, "A Layer Options Panel");
    protected static DataFlavor[] supportedFlavors = {DataFlavor.stringFlavor};
    LayerOptions layerOptions;

    public TransferableLayerOptions (LayerOptions layerOptions) {
        this.layerOptions = layerOptions;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return supportedFlavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        if (flavor.equals(DataFlavor.stringFlavor)) {
            return true;
        }
        return false;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            if (flavor.equals(DataFlavor.stringFlavor))
                return layerOptions.getName();
            else
                throw new UnsupportedFlavorException(flavor);
        }



}
