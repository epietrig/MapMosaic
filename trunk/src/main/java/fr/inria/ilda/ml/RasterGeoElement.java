/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import com.vividsolutions.jts.geom.util.AffineTransformation;
import fr.inria.ilda.ml.gl.PolygonGL;
import fr.inria.ilda.ml.gl.ShapeGL;
import org.geotools.coverage.grid.GridCoverage2D;

import java.util.ArrayList;

//import javax.media.opengl.GL;

/**
 * Created by mjlobo on 06/02/15.
 */
public class RasterGeoElement extends GeoElement {



    int imageWidth;
    int imageHeight;
    double realWidth;
    double realHeight;
    double [] lowerCorner;
    double pixels_per_unit;
    ArrayList<float[]> texCoords;
    String name;
    float alpha=1.0f;
    //int numBands;
    //public enum RasterType {TIFF, MNT};
    //protected RasterType type;


    PolygonGL polygon;

    //static enum Texture {ORTHO, SCAN};
    //static Hashtable<Texture, Integer> textures = new Hashtable<Texture, Integer>();
    //Texture texture;

    public RasterGeoElement (GridCoverage2D coverage) {
        //this.type = type;
        renderingType = GLUtilities.RenderedElementTypes.TEXTURED;
        fillColor = MapLayerViewer.defaultFillColor;
        shapes = new ArrayList<>();
        texCoords = new ArrayList<>();
        imageWidth = coverage.getRenderedImage().getWidth();
        imageHeight = coverage.getRenderedImage().getHeight();
        double [] upperCorner = coverage.getEnvelope2D().getUpperCorner().getCoordinate();
//        upperCorner[0] =Math.round(upperCorner[0]*100000);
//        upperCorner[1] =Math.round(upperCorner[1]*100000);
        upperCorner[0] =Math.round(upperCorner[0]);
        upperCorner[1] =Math.round(upperCorner[1]);

        lowerCorner = coverage.getEnvelope2D().getLowerCorner().getCoordinate();
//        lowerCorner[0] = Math.round(lowerCorner[0]*100000);
//        lowerCorner[1] = Math.round(lowerCorner[1]*100000);
        lowerCorner[0] = Math.round(lowerCorner[0]);
        lowerCorner[1] = Math.round(lowerCorner[1]);

//        realWidth = (upperCorner[0]-lowerCorner[0])/1000;
//        realHeight = (upperCorner[1]-lowerCorner[1])/1000;
//        x = lowerCorner[0]/1000+realWidth/2;
//        y = lowerCorner[1]/1000+realHeight/2;
//        System.out.println("upper corner " + upperCorner[0]);
//        System.out.println("upper corner " + upperCorner[1]);
//        System.out.println("lower corner " + lowerCorner[0]);
//        System.out.println("lower corner " + lowerCorner[1]);
        realWidth = (upperCorner[0]-lowerCorner[0]);
        realHeight = (upperCorner[1]-lowerCorner[1]);

//        double scale_x = imageWidth/realWidth;
//        double scale_y = imageHeight/realHeight;
//        realHeight = Math.round(realHeight *scale_y);
//        realWidth = Math.round(realWidth * scale_x);
//        lowerCorner[0] = Math.round(lowerCorner[0] *scale_x);
//        lowerCorner[1] = Math.round(lowerCorner[1] * scale_y);
//
//        System.out.println("scale x "+scale_x);
//        System.out.println("scale y "+scale_y);

        x = (lowerCorner[0]+realWidth/2);
        y = (lowerCorner[1]+realHeight/2);
//        System.out.println("x "+x);
//        System.out.println("y "+ y);
//        System.out.println("lowerCorner: "+lowerCorner[0]+" "+lowerCorner[1]);
//        System.out.println("upperCorner: "+upperCorner[0]+" "+upperCorner[1]);
//        System.out.println("imageWidth "+imageWidth);
//        System.out.println("imageHeight "+ imageHeight);

        calculateArrays();
//        pixels_per_unit = Math.round(imageWidth/realWidth*100000)/100000;
        //pixels_per_unit = Math.round(imageWidth/realWidth);
//        pixels_per_unit = Math.round(imageWidth/realWidth);
        pixels_per_unit = imageWidth/realWidth;
//
//        System.out.println("---------------------------");
//        System.out.println("pixels_per_unit " + pixels_per_unit);
        polygon = new PolygonGL(coordinates, indexesList, new Texture(this));
        polygon.setTexCoords(texCoords);
        shapes.add(polygon);
        calculateGeometry();


        //this.texture = texture;


//        buffers = new Hashtable<>();
//        updateBuffers();
    }

//    public byte[] getRGBAPixelData(RenderedImage img) {
//        int [] pixel = new int[3];
//        System.out.println("rendered image color model "+img.getClass());
//        byte[] imgRGBA;
//        int height = img.getHeight();
//        int width = img.getWidth();
//
//        //WritableRaster raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, width, height, 4, null);
//
//
//        ComponentColorModel colorModel = null;
//        WritableRaster raster = null;
//        BufferedImage newImage = null;
//        DataBufferByte dataBuf = null;
//        if (img.getColorModel().getNumComponents()>1) {
//           colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[]{8, 8, 8, 8}, true, false, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);
//            raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, width, height, 4, null);
//             newImage = new BufferedImage(colorModel, raster, false, null);
//            dataBuf = (DataBufferByte) raster.getDataBuffer();
//        }
//        else if (img.getColorModel().getNumComponents()==1) {
////            colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[]{8, 8, 8, 8}, true, false, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);
////            raster = Raster.create
////             newImage = new BufferedImage(img.getColorModel(), raster, false, null);
//            RenderedOp ri = (RenderedOp)img;
//            newImage = ri.getAsBufferedImage();
//            System.out.println("new image "+newImage);
//            raster = newImage.getRaster();
//            dataBuf = ((DataBufferByte) newImage.getRaster().getDataBuffer());
//            System.out.println("RGB: "+newImage.getRGB(100,100));
//            //raster = Raster.;
//        }
//        //numBands = colorModel.getNumComponents();
//        //WritableRaster raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, width, height, img.getColorModel().getNumComponents(), null);
//
//        //BufferedImage newImage = new BufferedImage(colorModel, raster, false, null);
//        //BufferedImage newImage = new BufferedImage(img.getColorModel(), raster, false, null);
//        AffineTransform gt = new AffineTransform();
//        gt.translate(0, height);
//        gt.scale(1,-1d);
//        Graphics2D g = newImage.createGraphics();
//        //g.transform(gt);
//        g.drawRenderedImage(img,gt);
//        g.dispose();
//
//       // DataBufferByte dataBuf = (DataBufferByte) raster.getDataBuffer();
//        imgRGBA = dataBuf.getData();
//        System.out.println("imgRGBA" + dataBuf.getData());
//        return imgRGBA;
//    }



    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }



    private void calculateArrays() {
        coordinates = new ArrayList<>();
        coordinates.add(new double[] {x+realWidth/2,y+realHeight/2});
        texCoords.add(new float[] {1.0f,1.0f});
        coordinates.add(new double[] {x-realWidth/2,y+realHeight/2});
        texCoords.add(new float[] {0.0f,1.0f});
        coordinates.add(new double[]{x-realWidth/2,y-realHeight/2});
        texCoords.add(new float[] {0.0f,0.0f});
        coordinates.add(new double[] {x+realWidth/2,y-realHeight/2});
        texCoords.add(new float[] {1.0f,0.0f});

        indexesList = new ArrayList<>();
        indexesList.add(0);
        indexesList.add(1);
        indexesList.add(2);
        indexesList.add(0);
        indexesList.add(2);
        indexesList.add(3);
    }

    public void setTextureID(int textureId) {
        polygon.setTextureID(textureId);
        //System.out.println("raster texture id "+polygon.getTextureID());
    }

    public void setTexture(Texture texture) {
        polygon.setTexture(texture);
        texture.print();
    }


//    public Texture getTexture() {
//        return texture;
//    }

    public double getRealWidth() {
        //System.out.println("real width " + realWidth);
        return realWidth;
    }

    public double getRealHeight() {
        return realHeight;
    }

    public double[] getLowerCorner() {
        return  lowerCorner;
    }

    public double getPixels_per_unit() {
        return pixels_per_unit;
    }

    public Texture getTexture() {
        return polygon.getTexture();
    }


    @Override
    public void bufferPolygon(double bufferOffset) {

    }

//    public void transformCoordinates(fr.inria.ilda.ml.AffineTransform affineTransform) {
//        System.out.println("before");
//        polygon.printCoordinates();
//        super.transformCoordinates(affineTransform);
//        polygon.refreshCoordinatesContour(coordinates);
//        System.out.println("realWidth before"+realWidth);
//        System.out.println("realHeight before"+realHeight);
//        realWidth = coordinates.get(0)[0]-coordinates.get(1)[0];
//        realHeight = coordinates.get(1)[1]-coordinates.get(2)[1];
//        lowerCorner = coordinates.get(2);
//        pixels_per_unit = (float)(imageWidth/realWidth);
//        x = lowerCorner[0]+realWidth/2;
//        y = lowerCorner[1]+realHeight/2;
//        System.out.println("realWidth "+realWidth);
//        System.out.println("realHeight "+realHeight);
//        System.out.println("pixels_per_unit "+pixels_per_unit);
//        System.out.println("lowerCorner "+lowerCorner[0]+":"+lowerCorner[1]);
//
//        polygon.getTexture().setHeight((float)realHeight);
//        polygon.getTexture().setWidth((float) realWidth);
//        polygon.getTexture().setLowerCorner(lowerCorner);
//        polygon.getTexture().setPixels_per_unit(pixels_per_unit);
//        System.out.println("after");
//        polygon.printCoordinates();
//    }

    public void transformCoordinates(AffineTransformation affineTransformation) {
        //originalCoordinates = (ArrayList<double[]>)(coordinates.clone());
        super.copyInOriginalCoordinates();
        System.out.println("before");
        polygon.printCoordinates();
        super.transformCoordinates(affineTransformation);
        polygon.refreshCoordinates(coordinates);
        System.out.println("realWidth before"+realWidth);
        System.out.println("realHeight before"+realHeight);
        updateAttributes();
        System.out.println("realWidth "+realWidth);
        System.out.println("realHeight "+realHeight);
        System.out.println("pixels_per_unit "+pixels_per_unit);
        System.out.println("lowerCorner "+lowerCorner[0]+":"+lowerCorner[1]);
        updateTexture();

        System.out.println("after");
        polygon.printCoordinates();
    }

    void updateTexture () {
        polygon.getTexture().setHeight(realHeight);
        polygon.getTexture().setWidth(realWidth);
        polygon.getTexture().setLowerCorner(lowerCorner);
        polygon.getTexture().setPixels_per_unit(pixels_per_unit);
    }

    void updateAttributes () {
        realWidth = coordinates.get(0)[0]-coordinates.get(1)[0];
        realHeight = coordinates.get(1)[1]-coordinates.get(2)[1];
        lowerCorner = coordinates.get(2);
        pixels_per_unit = imageWidth/realWidth;
        x = lowerCorner[0]+realWidth/2;
        y = lowerCorner[1]+realHeight/2;
    }

    public void revertTransform() {
//        System.out.println("-------------------------------------------------------------------");
//        for (double[] coord : originalCoordinates) {
//            System.out.println("coord originalCoordinates" + coord[0]+":"+coord[1]);
//        }
        System.out.println("revert transform in rastergeoelement");
        polygon.printCoordinates();
        polygon.refreshCoordinates(originalCoordinates);
        System.out.println ("after in revert transform rastergeoelement");
        polygon.printCoordinates();
        updateAttributes();
        updateTexture();
        calculateGeometry();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
        for (ShapeGL shape: shapes) {
            if (shape instanceof PolygonGL) {
                shape.setAlpha(alpha);
            }
        }
    }

    public float getAlpha() {
        return alpha;
    }

    public int[] getCoordinateInPx(double[] coord) {
        int x = (int) Math.round((coord[0] - lowerCorner[0])*pixels_per_unit);
        int y = (int) Math.round((realHeight-coord[1] + lowerCorner[1])*pixels_per_unit);

        return new int[]{x,y};
    }

    public double[] pxToCoordinate(int [] pixelCoords) {
        double x = pixelCoords[0]/pixels_per_unit+lowerCorner[0];
        double y = realHeight+lowerCorner[1]-pixelCoords[1]/pixels_per_unit;
        return new double[] {x,y};
    }








}

