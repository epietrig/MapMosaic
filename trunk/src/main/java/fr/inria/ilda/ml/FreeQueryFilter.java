/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import fr.inria.ilda.ml.StateMachine.FreeQueryEvent;

import java.util.HashSet;
import java.util.List;

/**
 * Created by mjlobo on 20/05/16.
 */
public class FreeQueryFilter extends QueryFilter {
    String query;
    HashSet<GeoElement> elements;
    HashSet<ShapeFileFeature> features;

    public FreeQueryFilter(String query, VectorLayer layer) {
        this.query = query;
        this.layer = layer;
    }

    public FreeQueryFilter (FreeQueryFilter otherFreeQueryFilter) {
        this.query = otherFreeQueryFilter.getQuery();
        this.layer = otherFreeQueryFilter.getLayer();
    }

    @Override
    public HashSet<GeoElement> getElements() {
        elements = new HashSet<>();
        getFeatures();
        for (ShapeFileFeature feature: features) {
            for (GeoElement ge : feature.getVectorGeoElements()) {
                elements.add(ge);
            }
        }
        //elements = ((VectorLayer)layer).getInNumericAttributeRange(attribute, minValue, maxValue);
        return elements;
    }

    @Override
    public HashSet<GeoElement> getElements(List<GeoElement> elementsToConsider) {
        return null;
    }

    @Override
    public HashSet<ShapeFileFeature> getFeatures() {
        features = ((VectorLayer)layer).getFeaturesByQuery(query);
        System.out.println("Features size in free query filter "+features.size());
        return features;
    }

    @Override
    public HashSet<ShapeFileFeature> getFeatures(HashSet<ShapeFileFeature> featuresToConsider) {
        features = ((VectorLayer)layer).getFeaturesByQuery(query, featuresToConsider);
        System.out.println("Features size in free query filter "+features.size());
        return features;
    }

    @Override
    public HashSet<GeoElement> update() {
        getFeatures();
        elements.clear();
        for (ShapeFileFeature feature: features) {
            for (GeoElement ge : feature.getVectorGeoElements()) {
                elements.add(ge);
            }
        }
        return elements;
    }

    @Override
    public HashSet<ShapeFileFeature> updateFeatures(HashSet<ShapeFileFeature> featuresToConsider) {
        System.out.println("updateFeatures with parameters");
        getFeatures(featuresToConsider);
        return features;
    }

    @Override
    public HashSet<ShapeFileFeature> updateFeatures() {
        return getFeatures();
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }
}
