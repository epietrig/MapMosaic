/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.vividsolutions.jts.geom.Geometry;
import fr.inria.ilda.ml.Filters.GaussianFilter;
import fr.inria.ilda.ml.Filters.SimpleFilter;
import fr.inria.ilda.ml.Filters.TextureFilter;
import fr.inria.ilda.ml.Filters.TwoPassFilter;
import fr.inria.ilda.ml.gl.*;

import javax.imageio.ImageIO;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.*;

import static com.jogamp.opengl.GL.*;


/**
 * Created by mjlobo on 06/02/15.
 */
public class GLUtilities {

    GL3 gl;
    MapLayerViewer application;
    //GLU glu;
    public enum RenderedElementTypes {TEXTURED, LINE, POINTS, DRAWN, SIMPLE, BLUR, RASTER, ALTITUDE, LINE_STRIP, FILLED, FILLED_TRANSLUCENT, TRACE, SIGMOID};
    Hashtable<RenderedElementTypes,ShaderProgram> programs;

    //Shaders variables
    static String MV_MATRIX = "mv_matrix";
    static String PROJ_MATRIX = "proj_matrix";
    static String POSITION = "position";
    static String TEXTURE_POSITION = "texPos";
    static String SAMPLER = "s";

    public GLUtilities (GL3 gl, MapLayerViewer application) {
        this.gl = gl;
        this.application = application;
        //glu = new GLU();

    }

    private String[] readShaderSource(String filename) {
        Vector<String> lines = new Vector<String>();
        Scanner sc;
        try {
            sc = new Scanner(new File(filename));
        } catch (IOException e) {
            System.err.println("IOException reading shader file: " + e);
            return null;
        }
        while (sc.hasNext()) {
            lines.addElement(sc.nextLine());
        }
        String[] program = new String[lines.size()];
        for (int i = 0; i < lines.size(); i++) {
            program[i] = (String) lines.elementAt(i) + "\n";
        }

        return program;
    }

    public int createShaderPrograms(GLAutoDrawable drawable, String vs, String fs) {
        //GL3 gl = (GL3) drawable.getGL();

        int vertexShader = createShaderProgram(drawable, vs, gl.GL_VERTEX_SHADER);
        int fragmentShader = createShaderProgram(drawable, fs, gl.GL_FRAGMENT_SHADER);

        int[] linked = new int[1];

        int vfprogram = gl.glCreateProgram();
        gl.glAttachShader(vfprogram, vertexShader);
        gl.glAttachShader(vfprogram, fragmentShader);
        gl.glLinkProgram(vfprogram);
        checkOpenGLError(drawable);
        gl.glGetProgramiv(vfprogram, GL3.GL_LINK_STATUS, linked, 0);
        if (linked[0] == 1)
        {
            //System.out.println("... linking succeeded.");
        } else
        {
            //System.out.println("... linking failed.");
            printProgramLog(drawable, vfprogram);
        }
        gl.glDeleteShader(vertexShader);
        gl.glDeleteShader(fragmentShader);
        return vfprogram;
    }

    public int createShaderProgram(GLAutoDrawable drawable, String shaderSource, int shaderType) {
        //GL3 gl = (GL3) drawable.getGL();
        int shaderInt = gl.glCreateShader(shaderType);

        int[] compiled = new int[1];


        String[] shaderSourceArray = readShaderSource(shaderSource);

        int[] lengths = new int[shaderSourceArray.length];
        for (int i = 0; i < lengths.length; i++) {
            lengths[i] = shaderSourceArray[i].length();
        }
        gl.glShaderSource(shaderInt, shaderSourceArray.length, shaderSourceArray, lengths, 0);

        gl.glCompileShader(shaderInt);
        checkOpenGLError(drawable);
        gl.glGetShaderiv(shaderInt, GL3.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 1)
        {
            //System.out.println("... compilation succeeded.");
        } else {
            //System.out.println("... compilation failed.");
            printProgramLog(drawable, shaderInt);
            printShaderLog(drawable, shaderInt);
        }

        return shaderInt;
    }

    public int createShaderPrograms(GLAutoDrawable drawable, String vs, String fs, String gs) {

        int vertexShader = createShaderProgram(drawable, vs, gl.GL_VERTEX_SHADER);
        int fragmentShader = createShaderProgram(drawable, fs, gl.GL_FRAGMENT_SHADER);
        int geometryShader = createShaderProgram(drawable, gs, gl.GL_GEOMETRY_SHADER);

        int[] linked = new int[1];

        int vfprogram = gl.glCreateProgram();
        //System.out.println("create shader program with geometric!");
        gl.glAttachShader(vfprogram, vertexShader);
        gl.glAttachShader(vfprogram, fragmentShader);
        gl.glAttachShader(vfprogram, geometryShader);
        //System.out.println("create shader program with geometric!");
        gl.glLinkProgram(vfprogram);
        //System.out.println("create shader program with geometric!");
        checkOpenGLError(drawable);
        //System.out.println("create shader program with geometric!");
        gl.glGetProgramiv(vfprogram, GL3.GL_LINK_STATUS, linked, 0);

        if (linked[0] == 1)
        {
            //System.out.println("... linking succeeded.");
        } else
        {
            //System.out.println("... linking failed.");
            printProgramLog(drawable, vfprogram);
        }
        gl.glDeleteShader(vertexShader);
        gl.glDeleteShader(fragmentShader);
        gl.glDeleteShader(geometryShader);
        return vfprogram;
    }

    private boolean checkOpenGLError(GLAutoDrawable drawable) {
        GL3 gl = (GL3) drawable.getGL();
        // examples of places to check for OpenGL and GLSL errors:
        boolean foundError = false;
        int glErr = gl.glGetError();
        while (glErr != GL.GL_NO_ERROR) {
            //System.err.println("glError: " + glu.gluErrorString(glErr));
            foundError = true;
            glErr = gl.glGetError();
        }
        return foundError;
    }

    private byte[] infoLog;
    private void printShaderLog(GLAutoDrawable d, int shader) {
        //GL3 gl = (GL3) d.getGL();
        int[] len = new int[1];
        int[] charsWritten = new int[1];
        byte[] log = null;
        IntBuffer intBuffer = IntBuffer.allocate(1);
        // determine the length of the shader compilation log
        gl.glGetShaderiv(shader, GL3.GL_INFO_LOG_LENGTH, len, 0);
        gl.glGetProgramiv(shader, GL3.GL_INFO_LOG_LENGTH, intBuffer);
        if (len[0] > 0) {
            System.out.println("HEREEE");
            infoLog = new byte[len[0]];
            gl.glGetShaderInfoLog(shader, len[0], charsWritten, 0, log, 0);
            System.out.println("Shader Info Log: ");
            for (int i = 0; i < log.length; i++) {
                System.out.print((char) log[i]);
            }
        }

        if (intBuffer.get(0)!=1) {
            System.out.println("heeeeere");
            infoLog = new byte[len[0]];
            gl.glGetProgramInfoLog(shader, len[0], charsWritten, 0, log, 0);
            //System.out.println(log.length);
            System.out.println("Program Info Log: ");
            for (int i = 0; i < log.length; i++) {
                System.out.print((char) log[i]);
            }
            gl.glGetProgramiv(shader, gl.GL_INFO_LOG_LENGTH,intBuffer);
            int size = intBuffer.get(0);
            System.err.println("Program link error: ");
            if (size>0){
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetProgramInfoLog(shader, size, intBuffer, byteBuffer);
                for (byte b:byteBuffer.array()){
                    System.err.print((char)b);
                }
            }

        }
    }

    void printProgramLog(GLAutoDrawable d, int prog) {
       // GL3 gl = (GL3) d.getGL();
        int[] len = new int[1];
        int[] charsWritten = new int[1];
        byte[] log = null;
        IntBuffer intBuffer = IntBuffer.allocate(1);
        // determine the length of the shader compilation log
        gl.glGetProgramiv(prog, GL3.GL_INFO_LOG_LENGTH, intBuffer);
        if (intBuffer.get(0)!=1) {
            //System.out.println("heeeeere");
            infoLog = new byte[len[0]];
            gl.glGetProgramInfoLog(prog, len[0], charsWritten, 0, log, 0);
            //System.out.println(log.length);
            System.out.println("Program Info Log: ");
            for (int i = 0; i < log.length; i++) {
                System.out.print((char) log[i]);
            }
            gl.glGetProgramiv(prog, gl.GL_INFO_LOG_LENGTH,intBuffer);
            int size = intBuffer.get(0);
            System.err.println("Program link error: ");
            if (size>0){
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetProgramInfoLog(prog, size, intBuffer, byteBuffer);
                gl.glGetProgramInfoLog(prog, size, intBuffer, byteBuffer);
                for (byte b:byteBuffer.array()){
                    System.err.print((char)b);
                }
            }

        }
    }




    public int loadTextureFromRasterTiff(GLAutoDrawable drawable, RGBRasterGeoElement rasterGeoElement) {
         GL3 gl = (GL3)drawable.getGL();

        //RasterGeoElement rasterGeoElement = application.geoToolsAdapter.getRasterPixelData(texFile);
        byte[] imgRGBA = rasterGeoElement.getImageData();

        ByteBuffer wrappedRGBA = ByteBuffer.wrap(imgRGBA);
        //System.out.println("wrapped buffer "+wrappedRGBA.limit());

        int[] textureIDs = new int [1];
        gl.glGenTextures(1,textureIDs,0);
        int textureID=textureIDs[0];

        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);

        gl.glTexStorage2D(GL_TEXTURE_2D, 4, GL_RGBA8, rasterGeoElement.getImageWidth(), rasterGeoElement.getImageHeight());
        gl.glTexSubImage2D(GL_TEXTURE_2D,0,0,0,rasterGeoElement.getImageWidth(), rasterGeoElement.getImageHeight(), GL_RGBA, GL_UNSIGNED_BYTE, wrappedRGBA);
        //gl.glEnable(GL_TEXTURE_2D);
        gl.glGenerateMipmap(GL_TEXTURE_2D);

        gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

        application.getTextures().get(rasterGeoElement).setID(textureID);
        //MapLayerViewer.RasterTextures.put(textureID, rasterGeoElement);
        rasterGeoElement.setTextureID(textureID);

        //System.out.println("Texture ID "+textureID);
        return textureID;

    }

    public int loadTextureFromRasterMNT (GLAutoDrawable drawable, MNTRasterGeoElement rasterGeoElement) {
        GL3 gl = (GL3)drawable.getGL();

        //RasterGeoElement rasterGeoElement = application.geoToolsAdapter.getRasterPixelData(texFile);
//
        Buffer buffer = FloatBuffer.wrap(rasterGeoElement.getMntData());
//        if (rasterGeoElement.getMntData() instanceof DataBufferDouble) {
//            double[] imgRGBA = ((DataBufferDouble) rasterGeoElement.getMntData()).getData();
//            buffer = DoubleBuffer.wrap(imgRGBA);
//        }
//        else if (rasterGeoElement.getMntData() instanceof DataBufferFloat) {
//            System.out.println("max "+max +" min "+ min);
//            buffer = FloatBuffer.wrap(imgRGBA);
//        }
//
//        DoubleBuffer wrappedRGBA = DoubleBuffer.wrap(imgRGBA);
        //System.out.println("wrapped buffer "+wrappedRGBA.limit());

        int[] textureIDs = new int [1];
        gl.glGenTextures(1,textureIDs,0);
        int textureID=textureIDs[0];

        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);

        // if (rasterGeoElement.getNumBands()>1) {
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, gl.GL_RED, rasterGeoElement.getImageWidth(), rasterGeoElement.getImageHeight(), 0, gl.GL_RED, GL.GL_FLOAT, buffer);
//        }
        //else {
//            System.out.println("hereeeee <1");
//            gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, gl.GL_LUMINANCE, rasterGeoElement.getImageWidth(), rasterGeoElement.getImageHeight(), 0,gl.GL_LUMINANCE, GL.GL_UNSIGNED_BYTE, wrappedRGBA);
//        }

        //System.out.println("hereeeee after tex image 2d");
        //gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        application.getTextures().get(rasterGeoElement).setID(textureID);
        //MapLayerViewer.RasterTextures.put(textureID, rasterGeoElement);
        rasterGeoElement.setTextureID(textureID);
        rasterGeoElement.setOriginalTexture(application.getTextures().get(rasterGeoElement));

        //System.out.println("Texture ID "+textureID);
        return textureID;
    }




    private byte[] getRGBAPixelDataFromBI (BufferedImage img) {
        byte[] imgRGBA;
        int height = img.getHeight(null); int width = img.getWidth(null);
        WritableRaster raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE,
                width, height, 4, null); ComponentColorModel colorModel = new ComponentColorModel( ColorSpace.getInstance(ColorSpace.CS_sRGB),
                new int[] { 8, 8, 8, 8 },
                true,
                false, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);
        BufferedImage newImage = new BufferedImage(colorModel, raster, false,
                null); // properties

        AffineTransform gt = new AffineTransform(); gt.translate(0, height);
        gt.scale(1, -1d);
        Graphics2D g = newImage.createGraphics(); g.transform(gt);
        g.drawImage(img, null, null); // draw into new image g.dispose();
// retrieve the underlying byte array from the raster data buffer
        DataBufferByte dataBuf = (DataBufferByte) raster.getDataBuffer(); imgRGBA = dataBuf.getData();
        return imgRGBA;

    }

    private int loadTextureBI(GLAutoDrawable drawable, String texFile) {
        BufferedImage img;
        try
        {
            img = ImageIO.read(new File(texFile));
        } catch (IOException e)
        {
            System.err.println("Error reading '" + texFile + '"');
            throw new RuntimeException(e);
        }

        GL gl = drawable.getGL();
        String fullName = texFile;
        byte[] imgRGBA = getRGBAPixelDataFromBI(img);
        ByteBuffer wrappedRGBA = ByteBuffer.wrap(imgRGBA);

        // reserve an integer texture ID (name)
        int[] textureIDs = new int[1];
        gl.glGenTextures(1, textureIDs, 0);
        int textureID = textureIDs[0];

        // make the textureID the "current texture"
        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0,GL.GL_RGBA, img.getWidth(), img.getHeight(), 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, wrappedRGBA );
        gl.glTexParameteri(GL.GL_TEXTURE_2D,
                GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        return textureID;
    }

    public void initializeShaders (GLAutoDrawable drawable) {
        GL3 gl = (GL3) drawable.getGL();

        programs = new Hashtable<>();
        ShaderProgram textured = new ShaderProgram(createShaderPrograms(drawable,"resources/shaders/basic_vertex.glsl", "resources/shaders/basic_fragment.glsl"));
        textured.addUniform(gl.glGetUniformLocation(textured.getProgramId(), SAMPLER),SAMPLER);
        textured.addUniform(gl.glGetUniformLocation(textured.getProgramId(), MV_MATRIX), MV_MATRIX);
        textured.addUniform(gl.glGetUniformLocation(textured.getProgramId(), PROJ_MATRIX), PROJ_MATRIX);
        textured.addAttribute(gl.glGetAttribLocation(textured.getProgramId(), POSITION), POSITION);
        textured.addAttribute(gl.glGetAttribLocation(textured.getProgramId(), TEXTURE_POSITION), TEXTURE_POSITION);
        textured.addUniform(gl.glGetUniformLocation(textured.getProgramId(),"offset"),"offset");
        addUniformToProgram(textured, "textureDimension");
        addUniformToProgram(textured, "textureLowerCorner");
        addUniformToProgram(textured, "alpha");
        addUniformToProgram(textured, "offsetTexture");
        System.out.println("Program id in initialize shaders "+textured.getProgramId());
        programs.put(RenderedElementTypes.TEXTURED, textured);



        ShaderProgram lines = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/line_vertex.glsl", "resources/shaders/line_fragment.glsl"));
        lines.addUniform(gl.glGetUniformLocation(lines.getProgramId(), MV_MATRIX), MV_MATRIX);
        lines.addUniform(gl.glGetUniformLocation(lines.getProgramId(), PROJ_MATRIX), PROJ_MATRIX);
        lines.addAttribute(gl.glGetAttribLocation(lines.getProgramId(), POSITION), POSITION);
        lines.addUniform(gl.glGetUniformLocation(lines.getProgramId(), "u_linewidth"), "u_linewidth");
        lines.addAttribute(gl.glGetAttribLocation(lines.getProgramId(), "normal"), "normal");
        addUniformToProgram(lines,"offset");
        programs.put(RenderedElementTypes.LINE, lines);

        ShaderProgram points = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/points_vertex.glsl", "resources/shaders/points_fragment.glsl"));
        points.addUniform(gl.glGetUniformLocation(points.getProgramId(), MV_MATRIX), MV_MATRIX);
        points.addUniform(gl.glGetUniformLocation(points.getProgramId(), PROJ_MATRIX), PROJ_MATRIX);
        points.addAttribute(gl.glGetAttribLocation(points.getProgramId(), POSITION), POSITION);
        addUniformToProgram(points, "color");
        System.out.println("points id " + points.getProgramId());
        programs.put(RenderedElementTypes.POINTS, points);

        //ShaderProgram drawn = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/drawn_vertex.glsl", "resources/shaders/drawn_fragment.glsl", "resources/shaders/alpha_geometry.glsl"));
        ShaderProgram drawn = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/drawn_vertex.glsl", "resources/shaders/drawn_fragment.glsl"));
        drawn.addUniform(gl.glGetUniformLocation(drawn.getProgramId(), SAMPLER),SAMPLER);
        drawn.addUniform(gl.glGetUniformLocation(drawn.getProgramId(), MV_MATRIX), MV_MATRIX);
        drawn.addUniform(gl.glGetUniformLocation(drawn.getProgramId(), PROJ_MATRIX), PROJ_MATRIX);
        drawn.addAttribute(gl.glGetAttribLocation(drawn.getProgramId(), POSITION), POSITION);
        drawn.addAttribute(gl.glGetAttribLocation(drawn.getProgramId(), TEXTURE_POSITION), TEXTURE_POSITION);
        drawn.addUniform(gl.glGetUniformLocation(drawn.getProgramId(), "offset"), "offset");
        addUniformToProgram(drawn, "offsetTexture");
        addUniformToProgram(drawn, "textureDimension");
        addUniformToProgram(drawn, "blurred");
        addUniformToProgram(drawn, "alpha");
        addUniformToProgram(drawn, "bgLowerCorner");
        programs.put(RenderedElementTypes.DRAWN, drawn);


        System.out.println("Initializing simple program..");
        ShaderProgram simpleTwoColor = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/simple_vertex.glsl", "resources/shaders/simple_fragment.glsl"));
        addUniformToProgram(simpleTwoColor, MV_MATRIX);
        addUniformToProgram(simpleTwoColor, PROJ_MATRIX);
        addUniformToProgram(simpleTwoColor, "offset");
        simpleTwoColor.addAttribute(gl.glGetAttribLocation(simpleTwoColor.getProgramId(), POSITION), POSITION);
        programs.put(RenderedElementTypes.SIMPLE, simpleTwoColor);

        System.out.println("Initializing blur shader");
        ShaderProgram blurShader = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/blur_vertex.glsl", "resources/shaders/blur_fragment.glsl"));
        addUniformToProgram(blurShader, MV_MATRIX);
        addUniformToProgram(blurShader, PROJ_MATRIX);
        addUniformToProgram(blurShader, SAMPLER);
        addAttributeToProgram(blurShader,POSITION);
        addAttributeToProgram(blurShader, TEXTURE_POSITION);
        addUniformToProgram(blurShader, "blurMultiplyVec");
        addUniformToProgram(blurShader, "pixel_size");
        addUniformToProgram(blurShader, "radius");
        programs.put(RenderedElementTypes.BLUR, blurShader);

        System.out.println("Initializing raster shader..");
        ShaderProgram rasterShader = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/raster_vertex.glsl", "resources/shaders/raster_fragment.glsl"));
        addUniformToProgram(rasterShader, MV_MATRIX);
        addUniformToProgram(rasterShader, PROJ_MATRIX);
        addUniformToProgram(rasterShader, SAMPLER);
        addAttributeToProgram(rasterShader,POSITION);
        addAttributeToProgram(rasterShader, TEXTURE_POSITION);
        addUniformToProgram(rasterShader, "alpha");
        programs.put(RenderedElementTypes.RASTER, rasterShader);

        System.out.println("Initializing altitude shader...");
        ShaderProgram altitudeShader = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/altitude_vertex.glsl","resources/shaders/altitude_fragment.glsl"));
        addUniformToProgram(altitudeShader, MV_MATRIX);
        addUniformToProgram(altitudeShader, PROJ_MATRIX);
        addUniformToProgram(altitudeShader, SAMPLER);
        addUniformToProgram(altitudeShader, "startColorRGB");
        addUniformToProgram(altitudeShader, "endColorRGB");
        addAttributeToProgram(altitudeShader,POSITION);
        addAttributeToProgram(altitudeShader, TEXTURE_POSITION);
        programs.put(RenderedElementTypes.ALTITUDE, altitudeShader);


        ShaderProgram linesStripShader = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/line_strip_vertex.glsl", "resources/shaders/line_strip_fragment.glsl", "resources/shaders/line_strip_geometry.glsl"));
        addUniformToProgram(linesStripShader, MV_MATRIX);
        addUniformToProgram(linesStripShader, PROJ_MATRIX);
        addUniformToProgram(linesStripShader, "THICKNESS");
        addUniformToProgram(linesStripShader, "WIN_SCALE");
        addAttributeToProgram(linesStripShader, POSITION);
        //lines_strip.addAttribute(gl.glGetAttribLocation(lines.getProgramId(), "normal"), "normal");
        addUniformToProgram(linesStripShader,"offset");
        addUniformToProgram(linesStripShader, "color");
        addUniformToProgram(linesStripShader, "alpha");
        programs.put(RenderedElementTypes.LINE_STRIP, linesStripShader);

        ShaderProgram fillColorShader = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/color_fill_vertex.glsl","resources/shaders/color_fill_fragment.glsl"));
        addUniformToProgram(fillColorShader, MV_MATRIX);
        addUniformToProgram(fillColorShader, PROJ_MATRIX);
        addAttributeToProgram(fillColorShader, POSITION);
        addUniformToProgram(fillColorShader,"offset");
        addUniformToProgram(fillColorShader, "color");
        addUniformToProgram(fillColorShader, "alpha");
        programs.put(RenderedElementTypes.FILLED, fillColorShader);

        ShaderProgram fillTranslucentColorShader = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/translucent_color_vertex.glsl","resources/shaders/translucent_color_fragment.glsl"));
        addUniformToProgram(fillTranslucentColorShader, MV_MATRIX);
        addUniformToProgram(fillTranslucentColorShader, PROJ_MATRIX);
        addAttributeToProgram(fillTranslucentColorShader, POSITION);
        addAttributeToProgram(fillTranslucentColorShader, TEXTURE_POSITION);
        addUniformToProgram(fillTranslucentColorShader,"offset");
        addUniformToProgram(fillTranslucentColorShader, "color");
        addUniformToProgram(fillTranslucentColorShader, "alpha");;
        addUniformToProgram(fillTranslucentColorShader, "blurred");
        programs.put(RenderedElementTypes.FILLED_TRANSLUCENT, fillTranslucentColorShader);


        System.out.println("initializing trace shader...");
        ShaderProgram traceProgram = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/trace_vertex.glsl", "resources/shaders/trace_fragment.glsl"));
        addUniformToProgram(traceProgram, MV_MATRIX);
        addUniformToProgram(traceProgram, PROJ_MATRIX);
        addAttributeToProgram(traceProgram, POSITION);
        addUniformToProgram(traceProgram,"offset");
        addUniformToProgram(traceProgram, "textureDimension");
        addUniformToProgram(traceProgram, "textureLowerCorner");
        addUniformToProgram(traceProgram, SAMPLER);
        addUniformToProgram(traceProgram, "color");
        addUniformToProgram(traceProgram, "alpha");
        programs.put(RenderedElementTypes.TRACE, traceProgram);

        System.out.println("initializing sigmoid interpolation shader..");
        ShaderProgram sigmoidProgram = new ShaderProgram(createShaderPrograms(drawable, "resources/shaders/altitude_vertex.glsl", "resources/shaders/sigmoid_fragment.glsl"));
        addUniformToProgram(sigmoidProgram, MV_MATRIX);
        addUniformToProgram(sigmoidProgram, PROJ_MATRIX);
        addUniformToProgram(sigmoidProgram, SAMPLER);
        addUniformToProgram(sigmoidProgram, "startColorRGB");
        addUniformToProgram(sigmoidProgram, "endColorRGB");
        addAttributeToProgram(sigmoidProgram, POSITION);
        addAttributeToProgram(sigmoidProgram, TEXTURE_POSITION);
        programs.put(RenderedElementTypes.SIGMOID, sigmoidProgram);
    }

    public Hashtable<RenderedElementTypes, ShaderProgram> getPrograms() {
        return programs;
    }

    public void shapeBufferData(ShapeGL shape, GLAutoDrawable drawable) {
        GL3 gl = (GL3) drawable.getGL();
        for (BufferGL bufferGL : shape.getBuffers().keySet()) {
                gl.glBindBuffer(bufferGL.getTarget(), bufferGL.getBuffer());
                //System.out.println("Get buffer "+bufferGL.getBuffer());
                if(bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                    FloatBuffer data = (FloatBuffer)shape.getBuffers().get(bufferGL);
                    gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
                }
                else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                    IntBuffer data = (IntBuffer)shape.getBuffers().get(bufferGL);
                    gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_INT, data, bufferGL.getUsage());
                }
            }
    }

    public void addUniformToProgram(ShaderProgram shaderProgram, String uniformName) {
        shaderProgram.addUniform(gl.glGetUniformLocation(shaderProgram.getProgramId(), uniformName), uniformName);
    }

    public void addAttributeToProgram (ShaderProgram shaderProgram, String attributeName) {
        shaderProgram.addAttribute(gl.glGetAttribLocation(shaderProgram.getProgramId(), attributeName), attributeName);
    }

    public Texture createScreenTexture() {
        int [] texID = new int[1];
        gl.glGenTextures(texID.length, texID, 0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, texID[0]);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_BASE_LEVEL, 0);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_MAX_LEVEL, 0);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        //gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        if (application.isRetina) {
            gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, application.getCanvas().getWidth() * 2, application.getCanvas().getHeight() * 2, 0, GL_BGRA, GL_UNSIGNED_BYTE, null);
        }
        else {
            gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, application.getCanvas().getWidth(), application.getCanvas().getHeight(), 0, GL_BGRA, GL_UNSIGNED_BYTE, null);
        }
        double pixels_per_unit = application.getCanvas().getCamera().getCamera_pixels_per_unit();
//        Texture texture = new Texture(pixels_per_unit,application.canvas.getWidth()/(pixels_per_unit), application.canvas.getHeight()/(pixels_per_unit), application.getCanvas().getCamera().getLowerCorner());
        //System.out.println("application.canvas.getCamera().getRight()-application.canvas.getCamera().getLeft()"+(application.canvas.getCamera().getRight()-application.canvas.getCamera().getLeft()));
        //System.out.println("application.canvas.getCamera().getTop()-application.canvas.getCamera().getBottom()"+(application.canvas.getCamera().getTop()-application.canvas.getCamera().getBottom()));

        Texture texture = new Texture(pixels_per_unit,application.canvas.getCamera().getScreenRealWidth(),application.getCanvas().getCamera().getScreenRealHeight(), application.getCanvas().getCamera().getLowerCorner());
        texture.setID(texID[0]);
        return texture;
    }

    public Texture createTextureFromTexture( RasterGeoElement rasterGeoElement) {
        int [] texID = new int[1];
        gl.glGenTextures(texID.length, texID, 0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, texID[0]);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_BASE_LEVEL, 0);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_MAX_LEVEL, 0);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        //gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, rasterGeoElement.getImageWidth(), rasterGeoElement.getImageHeight(), 0, GL_BGRA, GL_UNSIGNED_BYTE, null);
//        if (application.isRetina) {
//            gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, application.getCanvas().getWidth() * 2, application.getCanvas().getHeight() * 2, 0, GL_BGRA, GL_UNSIGNED_BYTE, null);
//        }
//        else {
//            gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, application.getCanvas().getWidth(), application.getCanvas().getHeight(), 0, GL_BGRA, GL_UNSIGNED_BYTE, null);
//        }
        double pixels_per_unit =rasterGeoElement.getPixels_per_unit();
//        Texture texture = new Texture(pixels_per_unit,application.canvas.getWidth()/(pixels_per_unit), application.canvas.getHeight()/(pixels_per_unit), application.getCanvas().getCamera().getLowerCorner());
        //System.out.println("application.canvas.getCamera().getRight()-application.canvas.getCamera().getLeft()"+(application.canvas.getCamera().getRight()-application.canvas.getCamera().getLeft()));
        //System.out.println("application.canvas.getCamera().getTop()-application.canvas.getCamera().getBottom()"+(application.canvas.getCamera().getTop()-application.canvas.getCamera().getBottom()));

        Texture texture = new Texture(pixels_per_unit,rasterGeoElement.getRealWidth(),rasterGeoElement.getRealHeight(), rasterGeoElement.getLowerCorner());
        texture.setID(texID[0]);
        return texture;
    }

    public Texture createTextureFromGeometry(GeoElement geoElement) {
        Geometry boundingBox = geoElement.getGeometry().getEnvelope();
        int [] texID = new int[1];
        gl.glGenTextures(texID.length, texID, 0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, texID[0]);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_BASE_LEVEL, 0);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_MAX_LEVEL, 0);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        double width = boundingBox.getCoordinates()[1].x-boundingBox.getCoordinates()[0].x;
        double height = boundingBox.getCoordinates()[2].y - boundingBox.getCoordinates()[0].y;
        gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, (int)(width*application.getBackgroundTexture().getPixels_per_unit()), (int)(height*application.getBackgroundTexture().getPixels_per_unit()), 0, GL_BGRA, GL_UNSIGNED_BYTE, null);
        double pixels_per_unit = application.getBackgroundTexture().getPixels_per_unit();
        double [] lowerCorner = new double[]{boundingBox.getCoordinates()[0].x,boundingBox.getCoordinates()[0].y};
        Texture texture = new Texture(pixels_per_unit,width,height, lowerCorner);
        texture.setID(texID[0]);
        return texture;

    }




    public FrameBufferObject createTextureFBO() {
        Texture texture = createScreenTexture();
        int[] fbo = new int[1];

        gl.glGenFramebuffers(fbo.length, fbo, 0);
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, fbo[0]);

        gl.glFramebufferTexture(gl.GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture.getID(), 0);
        //gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        //gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);

        gl.glDrawBuffer(gl.GL_COLOR_ATTACHMENT0);
        if (gl.glCheckFramebufferStatus(gl.GL_FRAMEBUFFER) != gl.GL_FRAMEBUFFER_COMPLETE)
        {
            System.out.println("Error in creating frame buffer object...");
        }
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0);
        FrameBufferObject frameBufferObject = new FrameBufferObject(fbo[0], texture);

        return frameBufferObject;


    }

    public FrameBufferObject createTextureRasterFBO(RasterGeoElement rasterGeoElement) {
        Texture texture = createTextureFromTexture(rasterGeoElement);
        int[] fbo = new int[1];

        gl.glGenFramebuffers(fbo.length, fbo, 0);
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, fbo[0]);

        gl.glFramebufferTexture(gl.GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture.getID(), 0);
        //gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        //gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);

        gl.glDrawBuffer(gl.GL_COLOR_ATTACHMENT0);
        if (gl.glCheckFramebufferStatus(gl.GL_FRAMEBUFFER) != gl.GL_FRAMEBUFFER_COMPLETE)
        {
            System.out.println("Error in creating frame buffer object...");
        }
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0);

        FrameBufferObject frameBufferObject = new FrameBufferObject(fbo[0], texture);

        return frameBufferObject;
    }

    public void attachTextureToFbo(FrameBufferObject fbo, Texture texture) {
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, fbo.getVboiD());
        gl.glFramebufferTexture(gl.GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture.getID(), 0);
        //gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glDrawBuffer(gl.GL_COLOR_ATTACHMENT0);
        if (gl.glCheckFramebufferStatus(gl.GL_FRAMEBUFFER) != gl.GL_FRAMEBUFFER_COMPLETE)
        {
            //System.out.println("Error in creating frame buffer object...");
        }
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0);

    }

    public GaussianFilter initializeGaussianFilter() {
        FrameBufferObject frameBufferObject1 = createTextureFBO();
        FrameBufferObject frameBufferObject2= createTextureFBO();
        FrameBufferObject frameBufferObject3 = createTextureFBO();

        int[] vertexArray = new int[1];
        gl.glGenVertexArrays(vertexArray.length, vertexArray, 0);
        gl.glBindVertexArray(vertexArray[0]);
        GaussianFilter gaussianFilter = new GaussianFilter(frameBufferObject1, frameBufferObject2, frameBufferObject3, programs.get(RenderedElementTypes.SIMPLE), programs.get(RenderedElementTypes.BLUR), vertexArray[0]);
        gaussianFilter.calculateQuad(application.getCanvas().getCamera().getEye(),application.getCanvas().getCamera().getRight(), application.getCanvas().getCamera().getLeft(), application.getCanvas().getCamera().getBottom(), application.getCanvas().getCamera().getTop());
        //gaussianFilter.calculateQuad(application.getSceneBounds());
        gaussianFilter.getQuad().setShader(getPrograms().get(GLUtilities.RenderedElementTypes.BLUR));
        gaussianFilter.getQuad().getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(gaussianFilter.getQuad().getShader().getAttribute(GLUtilities.POSITION));
        gaussianFilter.getQuad().getBuffer(BufferGL.Type.TEXTURE).setShaderAttributeId(gaussianFilter.getQuad().getShader().getAttribute(GLUtilities.TEXTURE_POSITION));

        int [] buffers = new int[gaussianFilter.getQuad().getBuffers().size()];
        int j=0;
        gl.glGenBuffers(buffers.length, buffers,0);
        for (BufferGL bufferGL: gaussianFilter.getQuad().getBuffers().keySet()) {
            gl.glBindBuffer(bufferGL.getTarget(), buffers[j]);
            if(bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                FloatBuffer data = (FloatBuffer)gaussianFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
            }
            else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                IntBuffer data = (IntBuffer)gaussianFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_INT, data, bufferGL.getUsage());
            }
            bufferGL.setBuffer(buffers[j]);

            j++;
        }


        return gaussianFilter;
    }

    public TwoPassFilter createTwoPassFilter() {
        FrameBufferObject fbo1 = createTextureFBO();
        FrameBufferObject fbo2 = createTextureFBO();

        int[] vertexArray = new int[1];
        gl.glGenVertexArrays(vertexArray.length, vertexArray, 0);
        gl.glBindVertexArray(vertexArray[0]);

        TwoPassFilter twoPassFilter = new TwoPassFilter(fbo1, fbo2, vertexArray[0]);
        twoPassFilter.calculateQuad(application.getCanvas().getCamera().getEye(),application.getCanvas().getCamera().getRight(), application.getCanvas().getCamera().getLeft(), application.getCanvas().getCamera().getBottom(), application.getCanvas().getCamera().getTop());
        int [] buffers = new int[twoPassFilter.getQuad().getBuffers().size()];
        int j=0;
        gl.glGenBuffers(buffers.length, buffers,0);
        for (BufferGL bufferGL: twoPassFilter.getQuad().getBuffers().keySet()) {
            gl.glBindBuffer(bufferGL.getTarget(), buffers[j]);
            if(bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                FloatBuffer data = (FloatBuffer)twoPassFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
            }
            else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                IntBuffer data = (IntBuffer)twoPassFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_INT, data, bufferGL.getUsage());
            }
            bufferGL.setBuffer(buffers[j]);

            j++;
        }


        return twoPassFilter;

    }

    public void initializeFilterQuad(SimpleFilter simpleFilter, RenderedElementTypes type) {
        simpleFilter.calculateQuad(application.getCanvas().getCamera().getEye(), application.getCanvas().getCamera().getRight(), application.getCanvas().getCamera().getLeft(), application.getCanvas().getCamera().getBottom(), application.getCanvas().getCamera().getTop());
        simpleFilter.getQuad().setShader(getPrograms().get(type));
        simpleFilter.getQuad().getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(simpleFilter.getQuad().getShader().getAttribute(GLUtilities.POSITION));
        simpleFilter.getQuad().getBuffer(BufferGL.Type.TEXTURE).setShaderAttributeId(simpleFilter.getQuad().getShader().getAttribute(GLUtilities.TEXTURE_POSITION));

        int [] buffers = new int[simpleFilter.getQuad().getBuffers().size()];
        int j=0;
        gl.glGenBuffers(buffers.length, buffers,0);
        gl.glBindVertexArray(simpleFilter.getVaoID());
        for (BufferGL bufferGL: simpleFilter.getQuad().getBuffers().keySet()) {
            gl.glBindBuffer(bufferGL.getTarget(), buffers[j]);
            if(bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                FloatBuffer data = (FloatBuffer)simpleFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
            }
            else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                IntBuffer data = (IntBuffer)simpleFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_INT, data, bufferGL.getUsage());
            }
            bufferGL.setBuffer(buffers[j]);

            j++;
        }
    }

    public void initializeTextureFilterQuad (TextureFilter textureFilter, Texture texture, RenderedElementTypes type) {
        //System.out.println("Texture width in initialize Filter quad "+texture.getWidth()+" "+ texture.getHeight());
        gl.glBindVertexArray(textureFilter.getVaoID());
        textureFilter.calculateQuad(texture.getLowerCorner(), texture.getWidth(), texture.getHeight());
        textureFilter.getQuad().setShader(getPrograms().get(type));
        textureFilter.getQuad().getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(textureFilter.getQuad().getShader().getAttribute(GLUtilities.POSITION));
        textureFilter.getQuad().getBuffer(BufferGL.Type.TEXTURE).setShaderAttributeId(textureFilter.getQuad().getShader().getAttribute(GLUtilities.TEXTURE_POSITION));

        int [] buffers = new int[textureFilter.getQuad().getBuffers().size()];
        int j=0;
        gl.glGenBuffers(buffers.length, buffers,0);
        for (BufferGL bufferGL: textureFilter.getQuad().getBuffers().keySet()) {
            gl.glBindBuffer(bufferGL.getTarget(), buffers[j]);
            if(bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                FloatBuffer data = (FloatBuffer)textureFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
            }
            else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                IntBuffer data = (IntBuffer)textureFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_INT, data, bufferGL.getUsage());
            }
            bufferGL.setBuffer(buffers[j]);

            j++;
        }

    }

    public SimpleFilter initializeSimpleFilter() {
        FrameBufferObject inFrameBuffer = createTextureFBO();
        int[] vertexArray = new int[1];
        gl.glGenVertexArrays(vertexArray.length, vertexArray, 0);
        gl.glBindVertexArray(vertexArray[0]);
        SimpleFilter simpleFilter = new SimpleFilter(vertexArray[0],inFrameBuffer, programs.get(RenderedElementTypes.TEXTURED));
        initializeFilterQuad(simpleFilter, RenderedElementTypes.TEXTURED);
        return simpleFilter;

    }

    public TextureFilter initializeTextureFilter(RasterGeoElement rasterGeoElement) {
        FrameBufferObject inFrameBuffer = createTextureRasterFBO(rasterGeoElement);
        //System.out.println("inFrameBuffer texture in initializeTextureFilter " +inFrameBuffer.getTexture());
        int[] vertexArray = new int[1];
        gl.glGenVertexArrays(vertexArray.length, vertexArray, 0);
        gl.glBindVertexArray(vertexArray[0]);
        TextureFilter textureFilter= new TextureFilter(vertexArray[0],inFrameBuffer, programs.get(RenderedElementTypes.ALTITUDE));
        initializeTextureFilterQuad(textureFilter, rasterGeoElement.getTexture(), RenderedElementTypes.TEXTURED);
        return textureFilter;
    }

    public void writeBufferGaussianFilter (GaussianFilter gaussianFilter,FrameBufferObject readBuffer, FrameBufferObject writeBuffer, float[] direction, float dimension) {
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, writeBuffer.getVboiD());
        gl.glBindFramebuffer(gl.GL_READ_FRAMEBUFFER, readBuffer.getVboiD());
        //gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, 0);
        gl.glClear(gl.GL_COLOR_BUFFER_BIT);
        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//        if (application.isRetina) {
//            gl.glViewport(0, 0, application.getCanvas().getWidth() * 2, application.getCanvas().getHeight() * 2);
//        }
        gl.glBindVertexArray(gaussianFilter.getVaoID());
        gl.glUseProgram(gaussianFilter.getFilterShader().getProgramId());
        gl.glUniformMatrix4fv(gaussianFilter.getFilterShader().getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
        gl.glUniformMatrix4fv(gaussianFilter.getFilterShader().getUniform(GLUtilities.PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(), 0);
        gl.glUniform2f(gaussianFilter.getFilterShader().getUniform("blurMultiplyVec"),direction[0], direction[1]);
        gl.glUniform1f(gaussianFilter.getFilterShader().getUniform("pixel_size"),1/dimension);
        //System.out.println("pixel size " + 1/dimension);
        gl.glUniform1f(gaussianFilter.getFilterShader().getUniform("radius"),gaussianFilter.getRadius());

        //System.out.println("Tx location " + glUtilities.getPrograms().get(GLUtilities.RenderedElementTypes.TEXTURED).getUniform("s"));


        for (BufferGL currentBuffer : gaussianFilter.getQuad().getBuffers().keySet()) {
            if (currentBuffer != null) {
                if (currentBuffer.getShaderAttributeId() != -1) {
                    gl.glEnableVertexAttribArray(currentBuffer.getShaderAttributeId());
                    gl.glBindBuffer(currentBuffer.getTarget(), currentBuffer.getBuffer());
                    gl.glVertexAttribPointer(currentBuffer.getShaderAttributeId(), currentBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);


                }
            }
        }

        BufferGL indexBuffer = gaussianFilter.getQuad().getBuffer(BufferGL.Type.INDEX);
        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
        gl.glActiveTexture(gl.GL_TEXTURE0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, readBuffer.getTexture().getID());
        gl.glDrawElements(GL_TRIANGLES, gaussianFilter.getQuad().getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
        gl.glReadBuffer(GL_COLOR_ATTACHMENT0);
        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER,0);
    }

    public void drawTexturedElement (GeoElement ge) {
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOfseted()) {
                //System.out.println("shape class "+shape.getClass());
                if (shape instanceof PolygonGL) {
                    //System.out.println("rendertexturedpolygon");
                    renderTexturedPolygon((PolygonGL) shape, ge.getIsDocked());
                }
            }
        }
    }

    public void drawTexturedElementWithLines(GeoElement ge) {
//        if (ge instanceof VectorGeoElement) {
//            System.out.println("draw textured element with lines " + ge);
//        }
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOfseted()) {
                if (shape instanceof PolygonGL) {
                      if (((PolygonGL)shape).getIsHole()) {
//                          if ((!(ge instanceof DrawnGeoElement && (application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge) == null  ||application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge).size()>0)))){
                              if (application.getRasters().get(application.getBackgroundTexture()).getRenderingType()==GLUtilities.RenderedElementTypes.FILLED) {
                                  renderColoredPolygon((PolygonGL)shape,application.getRasters().get(application.getBackgroundTexture()).getFillColor());
                              }
                              else {
                                  renderTexturedPolygon((PolygonGL) shape, application.getBackgroundTexture(), ge.getIsDocked());
                              }
//                          }
//                          else {
//                              if (application.getSelectionManager().getDrawnSelectionType().get((DrawnGeoElement)ge) == RenderedElementTypes.FILLED || application.getSelectionManager().getDrawnSelectionType().get((DrawnGeoElement)ge) == RenderedElementTypes.FILLED_TRANSLUCENT) {
//                                  renderColoredPolygon((PolygonGL) shape, application.getSelectionManager().getBackgroundColors().get((DrawnGeoElement) ge));
//                              }
//                              else {
//                                  renderTexturedPolygon((PolygonGL) shape, application.getSelectionManager().getBackgroundTextures().get(ge));
//                              }
//                          }
//                    ||application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge).size()>0))))
//                    if (((PolygonGL) shape).getIsHole() && (!(ge instanceof DrawnGeoElement && (application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge) == null
//                    ||application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge).size()>0)))) {

                    }
                    else {
                        renderTexturedPolygon((PolygonGL) shape, ge.getIsDocked());
                    }
                }
                else if (shape instanceof LineGL) {
                    renderLine((LineGL)shape);

                }
                else if (shape instanceof LineStripGL) {
                    renderLineStrip((LineStripGL)shape);
                }
            }
        }
    }

    public void drawInElement(GeoElement ge) {
        java.util.List <PolygonGL> inPolygons = null;
        boolean isComposite = false;
        if (ge instanceof VectorGeoElement) {
            inPolygons = ((VectorGeoElement) ge).getInPolygons();
        }
        else if (ge instanceof DrawnGeoElement) {
            inPolygons = ((DrawnGeoElement) ge).getInPolygons();
            isComposite = ((DrawnGeoElement)ge).getIsComposite();
        }
        if (ge.getIsIn() && !isComposite) {
            for (PolygonGL inPolygon:inPolygons) {
                renderColoredPolygon(inPolygon, MapLayerViewer.inAreaColor, MapLayerViewer.inAreaAlpha);
            }
        }
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOfseted()) {
                if (shape instanceof LineStripGL) {
                    renderLineStrip(((LineStripGL) shape));
                }
            }
        }
    }

    public void drawSelectedElement(GeoElement ge) {
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOfseted()) {
                if (shape instanceof LineStripGL) {
                    renderLineStrip(((LineStripGL) shape));
                }
            }
        }
    }




    public void drawRasterElement(RasterGeoElement ge) {
        drawRasterElement(ge, ge.getAlpha());
    }

    public void drawRasterElement(RasterGeoElement ge, float alpha) {
        ShaderProgram raster = getPrograms().get(RenderedElementTypes.RASTER);
        int vertexAttributeId = raster.getAttribute(POSITION);
        int textureAttributeId = raster.getAttribute(TEXTURE_POSITION);
        for (ShapeGL shape : ge.getShapes()) {
            //here is the problem!
            //if (shape.getVisible() && !shape.getOfseted() && shape instanceof PolygonGL) {
            if (!shape.getOfseted() && shape instanceof PolygonGL) {
                gl.glUseProgram(raster.getProgramId());
                gl.glUniformMatrix4fv(raster.getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
                gl.glUniformMatrix4fv(raster.getUniform(GLUtilities.PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(), 0);
                gl.glUniform1f(raster.getUniform("alpha"), alpha);

                BufferGL vertexBuffer = shape.getBuffer(BufferGL.Type.VERTEX);
                BufferGL textureBuffer = shape.getBuffer(BufferGL.Type.TEXTURE);
                if (vertexBuffer != null) {
                    gl.glEnableVertexAttribArray(vertexAttributeId);
                    gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
                    gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                    gl.glEnableVertexAttribArray(textureAttributeId);
                    gl.glBindBuffer(textureBuffer.getTarget(), textureBuffer.getBuffer());
                    gl.glVertexAttribPointer(textureAttributeId, textureBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                    BufferGL indexBuffer = shape.getBuffer(BufferGL.Type.INDEX);
                    gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
                    gl.glActiveTexture(gl.GL_TEXTURE0);
                    gl.glBindTexture(gl.GL_TEXTURE_2D, ((PolygonGL) shape).getTexture().getID());
                    //System.out.println("raster texture id "+((PolygonGL) shape).getTexture().getID());
                    gl.glDrawElements(GL_TRIANGLES, shape.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                }


            }
        }
    }

    public void drawMNTElement(RasterGeoElement ge) {
        MNTRasterGeoElement mntRasterGeoElement = (MNTRasterGeoElement)ge;
        ShaderProgram altitude;
        if (mntRasterGeoElement.getInterpolationMethod()==MapLayerViewer.SIGMOID_INTERPOLATION) {
            altitude = getPrograms().get(RenderedElementTypes.SIGMOID);
        }
        else {
            altitude = getPrograms().get(RenderedElementTypes.ALTITUDE);
        }
        int vertexAttributeId = altitude.getAttribute(POSITION);
        int textureAttributeId = altitude.getAttribute(TEXTURE_POSITION);
        Vector3d eye = new Vector3d(ge.getLowerCorner()[0], ge.getLowerCorner()[1], application.getCanvas().getCamera().getEye().z);
        Vector3d center = new Vector3d(ge.getLowerCorner()[0], ge.getLowerCorner()[1], application.getCanvas().getCamera().getCenter().z);
        double right = ge.getRealWidth();
        double top = ge.getRealHeight();
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOfseted() && shape instanceof PolygonGL) {
                gl.glUseProgram(altitude.getProgramId());
//                gl.glUniformMatrix4fv(altitude.getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
                gl.glUniformMatrix4fv(altitude.getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(eye, center), 0);
                gl.glUniformMatrix4fv(altitude.getUniform(GLUtilities.PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(0,right,0, top), 0);
                gl.glUniform3f(altitude.getUniform("startColorRGB"),mntRasterGeoElement.getMinColor().x, mntRasterGeoElement.getMinColor().y, mntRasterGeoElement.getMinColor().z);
                gl.glUniform3f(altitude.getUniform("endColorRGB"), mntRasterGeoElement.getMaxColor().x, mntRasterGeoElement.getMaxColor().y, mntRasterGeoElement.getMaxColor().z);


                BufferGL vertexBuffer = shape.getBuffer(BufferGL.Type.VERTEX);
                BufferGL textureBuffer = shape.getBuffer(BufferGL.Type.TEXTURE);
                if (vertexBuffer != null) {
                    gl.glEnableVertexAttribArray(vertexAttributeId);
                    gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
                    gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                    gl.glEnableVertexAttribArray(textureAttributeId);
                    gl.glBindBuffer(textureBuffer.getTarget(), textureBuffer.getBuffer());
                    gl.glVertexAttribPointer(textureAttributeId, textureBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                    BufferGL indexBuffer = shape.getBuffer(BufferGL.Type.INDEX);
                    gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
                    gl.glActiveTexture(gl.GL_TEXTURE0);
                    //gl.glBindTexture(gl.GL_TEXTURE_2D, ((PolygonGL) shape).getTexture().getID());
                    gl.glBindTexture(gl.GL_TEXTURE_2D, mntRasterGeoElement.getOriginalTexture().getID());
                    gl.glDrawElements(GL_TRIANGLES, shape.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                }


            }
        }
    }

    public void drawTrace(PolygonGL polygon, Texture texture) {
                ShaderProgram traceProgram = getPrograms().get(RenderedElementTypes.TRACE);
                int vertexAttributeId = traceProgram.getAttribute(POSITION);
                gl.glUseProgram(traceProgram.getProgramId());
                gl.glUniformMatrix4fv(traceProgram.getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
                gl.glUniformMatrix4fv(traceProgram.getUniform(GLUtilities.PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(), 0);
                gl.glUniform2f(traceProgram.getUniform("textureDimension"), (float) (texture.getWidth()), (float) (texture.getHeight()));
                gl.glUniform2f(traceProgram.getUniform("offset"), (float) polygon.getOffset()[0], (float) polygon.getOffset()[1]);
                gl.glUniform2f(traceProgram.getUniform("textureLowerCorner"), (float) (texture.getLowerCorner()[0]), (float) (texture.getLowerCorner()[1]));
                gl.glUniform1f(traceProgram.getUniform("alpha"), application.getTraceAlpha());
                gl.glUniform3f(traceProgram.getUniform("color"), MapLayerViewer.traceColor.x, MapLayerViewer.traceColor.y, MapLayerViewer.traceColor.z);

                BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
                if (vertexBuffer != null) {
                    gl.glEnableVertexAttribArray(vertexAttributeId);
                    gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
                    gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                    BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
                    gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
                    gl.glActiveTexture(gl.GL_TEXTURE0);
                    //gl.glBindTexture(gl.GL_TEXTURE_2D, ((PolygonGL) polygon).getTexture().getID());
                    gl.glBindTexture(gl.GL_TEXTURE_2D, texture.getID());
                    //System.out.println("polygon.getIndex().array "+polygon.getIndex().array().length);
                    gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                }
            }





    public void drawElementTrace(GeoElement ge) {
        for (ShapeGL shapeGL: ge.getShapes()) {
            if (shapeGL instanceof PolygonGL && !shapeGL.getOfseted()) {
                renderColoredPolygon((PolygonGL)shapeGL, new Vector3f(1.0f,0.0f,0.0f));
            }
        }
    }

    public void drawTraceQuad(SimpleFilter traceFilter, Texture texture) {
        //System.out.println("drawtracequad! "+traceFilter.getVaoID());
        //System.out.println("texture "+texture);
        gl.glBindVertexArray(traceFilter.getVaoID());
        renderTexturedPolygon(traceFilter.getQuad(), texture, 1f, true);
        //traceFilter.getQuad().printCoordinates();
        //System.out.println("vertex buffer trace quad "+traceFilter.getQuad().getBuffer(BufferGL.Type.VERTEX).getBuffer());
        //renderColoredPolygon(traceFilter.getQuad(), new Vector3f(1.0f, 0.0f, 0.0f), 1.0f);
    }

    public void drawDrawnGeoElement(DrawnGeoElement ge) {
        ShaderProgram drawn = getPrograms().get(RenderedElementTypes.DRAWN);
        int vertexAttributeId = drawn.getAttribute(POSITION);
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOnlyInFirst()) {
                if (shape instanceof PolygonGL) {
                    renderTranslucentPolygon((PolygonGL) shape, application.getCanvas().getBlurredDrawnTextures().get(ge), ge.getIsDocked());
                }
                else if (shape instanceof LineGL) {
                    renderLine((LineGL)shape);
                }
            }
        }
    }

    public void drawTranslucentGeoelement (GeoElement ge) {
        //System.out.println("draw translucent geo element! "+ge);
        ShaderProgram drawn = getPrograms().get(RenderedElementTypes.DRAWN);
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOnlyInFirst()) {
                if (shape instanceof PolygonGL) {
                    if (((PolygonGL) shape).getIsHole() && (!(ge instanceof DrawnGeoElement && (application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge) == null
                            ||application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge).size()>0)))) {
                        if (application.getRasters().get(application.getBackgroundTexture()).getRenderingType()==GLUtilities.RenderedElementTypes.FILLED) {
                            renderColoredPolygon((PolygonGL)shape,application.getRasters().get(application.getBackgroundTexture()).getFillColor());
                        }
                        else {
                            renderTexturedPolygon((PolygonGL) shape, application.getBackgroundTexture(), ge.getIsDocked());
                        }
                    }
                   // System.out.println("shape is visible "+shape.getVisible());
                    //System.out.println("PolygonGL in drawTranslucentFilledGeoElement");
                    //System.out.println("render translucent polygon !");
                    //renderTranslucentPolygon((PolygonGL) shape, application.getCanvas().getBlurredDrawnTextures().get(ge));
                    else{
                        renderTranslucentPolygon((PolygonGL) shape, ge.getBlurredTexture(), ge.getIsDocked());
                    }
                }
                else if (shape instanceof LineGL) {

                    renderLine((LineGL)shape);
                }
                else if (shape instanceof LineStripGL) {
                    //System.out.println("render lineee");
                    renderLineStrip((LineStripGL)shape);
                }
            }
        }
    }

    public void drawFilledGeoElement(GeoElement ge) {
        drawFilledGeoElement(ge, ge.getFillColor());
    }

    public void drawFilledGeoElement(GeoElement ge, Vector3f color) {
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOfseted()) {
                if (shape instanceof PolygonGL) {
                    if (((PolygonGL) shape).getIsHole()  && (!(ge instanceof DrawnGeoElement && (application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge) == null
                            ||application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge).size()>0)))) {
                        if (application.getRasters().get(application.getBackgroundTexture()).getRenderingType()==GLUtilities.RenderedElementTypes.FILLED) {
                            renderColoredPolygon((PolygonGL)shape,application.getRasters().get(application.getBackgroundTexture()).getFillColor());
                        }
                        else {
                            renderTexturedPolygon((PolygonGL) shape, application.getBackgroundTexture(), ge.getIsDocked());
                        }
                    }
                    else {
                        renderColoredPolygon((PolygonGL) shape, color);
                    }
                }
                else if (shape instanceof LineGL) {
                    renderLine((LineGL)shape);
                }
                else if (shape instanceof LineStripGL) {
                    renderLineStrip((LineStripGL)shape);
                }
            }
        }
    }


    public void drawTranslucentFilledGeoElement(GeoElement ge) {
        //System.out.println("drawTranslucentFilledGeoElement");
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOnlyInFirst()) {
                if (shape instanceof PolygonGL) {
                    if (((PolygonGL) shape).getIsHole()  && (!(ge instanceof DrawnGeoElement && (application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge) == null
                            ||application.getSelectionManager().getVectorSelectionsDrawn((DrawnGeoElement)ge).size()>0)))) {
                        if (application.getRasters().get(application.getBackgroundTexture()).getRenderingType()==GLUtilities.RenderedElementTypes.FILLED) {
                            renderColoredPolygon((PolygonGL)shape,application.getRasters().get(application.getBackgroundTexture()).getFillColor());
                        }
                        else {
                            renderTexturedPolygon((PolygonGL) shape, application.getBackgroundTexture(), ge.getIsDocked());
                        }
                    }
                    else {
                        renderTranslucentPolygonFilled((PolygonGL) shape, ge.getBlurredTexture(), ge.getFillColor());
                    }
                }
                else if (shape instanceof LineGL) {
                    renderLine((LineGL)shape);
                }
                else if (shape instanceof LineStripGL) {
                    renderLineStrip((LineStripGL)shape);
                }
            }
        }
    }



    public void renderLine (LineGL line) {
        ShaderProgram lines = getPrograms().get(RenderedElementTypes.LINE);
        int vertexAttributeId = lines.getAttribute(POSITION);
        int normalAttributeId = lines.getAttribute("normal");
        gl.glUseProgram(lines.getProgramId());
        gl.glUniformMatrix4fv(lines.getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
        gl.glUniformMatrix4fv(lines.getUniform(GLUtilities.PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(), 0);

        BufferGL vertexBuffer = line.getBuffer(BufferGL.Type.VERTEX);
        gl.glEnableVertexAttribArray(vertexAttributeId);
        gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);

        BufferGL normalBuffer = line.getBuffer(BufferGL.Type.NORMALS);
        gl.glEnableVertexAttribArray(normalAttributeId);
        gl.glBindBuffer(normalBuffer.getTarget(), normalBuffer.getBuffer());
        gl.glVertexAttribPointer(normalAttributeId, normalBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);

        gl.glUniform1f(lines.getUniform("u_linewidth"), (float)(((LineGL)line).getLineWidth()/application.getCanvas().getCamera().getCamera_pixels_per_unit()));
        gl.glUniform2f(lines.getUniform("offset"), (float)line.getOffset()[0], (float)line.getOffset()[1]);
        BufferGL indexBuffer = line.getBuffer(BufferGL.Type.INDEX);
        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
        gl.glDrawElements(GL_TRIANGLES, line.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);

    }

    public void renderLineStrip (LineStripGL lineStrip) {
        renderLineStrip(lineStrip, lineStrip.getColor());
    }

    public void renderLineStrip(LineStripGL lineStrip, Vector3f color) {
        ShaderProgram linesStrip = getPrograms().get(RenderedElementTypes.LINE_STRIP);
        int vertexAttributeId = linesStrip.getAttribute(POSITION);
        gl.glUseProgram(linesStrip.getProgramId());
        gl.glUniformMatrix4fv(linesStrip.getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
        gl.glUniformMatrix4fv(linesStrip.getUniform(GLUtilities.PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(), 0);

        BufferGL vertexBuffer = lineStrip.getBuffer(BufferGL.Type.VERTEX);
        gl.glEnableVertexAttribArray(vertexAttributeId);
        gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);


//        gl.glUniform1f(linesStrip.getUniform("u_linewidth"), (float)(((LineStripGL)lineStrip).getLineWidth()/application.getCanvas().getCamera().getCamera_pixels_per_unit()));
        gl.glUniform2f(linesStrip.getUniform("offset"), (float)lineStrip.getOffset()[0], (float)lineStrip.getOffset()[1]);
        gl.glUniform1f(linesStrip.getUniform("THICKNESS"), lineStrip.getLineWidth());
        gl.glUniform2f(linesStrip.getUniform("WIN_SCALE"), application.getCanvas().getWidth(), application.getCanvas().getHeight());
        //System.out.println("Line strip color "+lineStrip.getColor().x +" "+lineStrip.getColor().y + " "+lineStrip.getColor().z);
        gl.glUniform3f(linesStrip.getUniform("color"), color.x, color.y, color.z);
        gl.glUniform1f(linesStrip.getUniform("alpha"), lineStrip.getAlpha());
        BufferGL indexBuffer = lineStrip.getBuffer(BufferGL.Type.INDEX);
        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
        gl.glDrawElements(gl.GL_LINE_STRIP_ADJACENCY, lineStrip.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
        //gl.glDrawElements(gl.GL_LINE_STRIP, lineStrip.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
    }




    public void renderTexturedPolygon(PolygonGL polygon, Texture texture, float alpha, boolean docked) {
        long nanoTime = System.nanoTime();
        ShaderProgram textureProgram = getPrograms().get(GLUtilities.RenderedElementTypes.TEXTURED);
        int vertexAttributeId = textureProgram.getAttribute(POSITION);

        gl.glUseProgram(textureProgram.getProgramId());
        gl.glUniformMatrix4fv(textureProgram.getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
        gl.glUniformMatrix4fv(textureProgram.getUniform(GLUtilities.PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(), 0);
        //gl.glUniform2f(textureProgram.getUniform("textureDimension"), (float)(polygon.getTexture().getWidth()), (float)(polygon.getTexture().getHeight()));
        gl.glUniform2f(textureProgram.getUniform("textureDimension"), (float)(texture.getWidth()), (float)(texture.getHeight()));
        gl.glUniform2f(textureProgram.getUniform("offset"), (float)polygon.getOffset()[0], (float)polygon.getOffset()[1]);
        if (docked) {
            gl.glUniform2f(textureProgram.getUniform("offsetTexture"),0.0f,0.0f);
        }
        else {
            gl.glUniform2f(textureProgram.getUniform("offsetTexture"), (float)polygon.getOffset()[0], (float)polygon.getOffset()[1]);
        }
        //gl.glUniform2f(textureProgram.getUniform("textureLowerCorner"), (float)(polygon.getTexture().getLowerCorner()[0]), (float)(polygon.getTexture().getLowerCorner()[1]));
        gl.glUniform2f(textureProgram.getUniform("textureLowerCorner"), (float)(texture.getLowerCorner()[0]), (float)(texture.getLowerCorner()[1]));
        gl.glUniform1f(textureProgram.getUniform("alpha"), alpha);
        //System.out.println("Time to pass all uniforms "+(System.nanoTime()-nanoTime));

        BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
        if (vertexBuffer != null) {
            gl.glEnableVertexAttribArray(vertexAttributeId);
            nanoTime = System.nanoTime();
            gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
            //System.out.println("Time to bind buffer "+(System.nanoTime()-nanoTime));
            gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
            BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
            gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
            gl.glActiveTexture(gl.GL_TEXTURE0);
            //gl.glBindTexture(gl.GL_TEXTURE_2D, ((PolygonGL) polygon).getTexture().getID());
            gl.glBindTexture(gl.GL_TEXTURE_2D, texture.getID());
            //System.out.println("polygon.getIndex().array "+polygon.getIndex().array().length);
            nanoTime = System.nanoTime();
            gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
            //System.out.println("Time to draw "+(System.nanoTime()-nanoTime));
        }
    }

    public void renderTexturedPolygon(PolygonGL polygon, Texture texture, boolean docked) {
        renderTexturedPolygon(polygon, texture, polygon.getAlpha(), docked);
    }

    public void renderTexturedPolygon(PolygonGL polygon, boolean docked) {
        renderTexturedPolygon(polygon, polygon.getTexture(), docked);


    }


    public void renderTranslucentPolygon(PolygonGL polygon, Texture blurredTexture, boolean docked) {
       // System.out.println("rendering translucent polygon ");
        ShaderProgram drawn = getPrograms().get(RenderedElementTypes.DRAWN);
        int vertexAttributeId = drawn.getAttribute(POSITION);
        int textureAttributeId = drawn.getAttribute(TEXTURE_POSITION);
        gl.glUseProgram(drawn.getProgramId());
        gl.glUniformMatrix4fv(drawn.getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
        gl.glUniformMatrix4fv(drawn.getUniform(GLUtilities.PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(), 0);
        BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
        BufferGL texBuffer = polygon.getBuffer(BufferGL.Type.TEXTURE);
        gl.glEnableVertexAttribArray(vertexAttributeId);
        gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(textureAttributeId);
        gl.glBindBuffer(texBuffer.getTarget(), texBuffer.getBuffer());
        gl.glVertexAttribPointer(textureAttributeId, texBuffer.getSize(), GL.GL_FLOAT, false, 0,0);
        gl.glUniform2f(drawn.getUniform("offset"), (float)polygon.getOffset()[0], (float)polygon.getOffset()[1]);
        if (docked) {
            gl.glUniform2f(drawn.getUniform("offsetTexture"),0.0f,0.0f);
        }
        else {
            gl.glUniform2f(drawn.getUniform("offsetTexture"), (float)polygon.getOffset()[0], (float)polygon.getOffset()[1]);
        }
        gl.glUniform2f(drawn.getUniform("textureDimension"), (float)(polygon.getTexture().getWidth()), (float)(polygon.getTexture().getHeight()));
//        gl.glUniform2f(drawn.getUniform("fboDimension"), (float)(application.getCanvas().getGaussianFilter().getFboFirstPass().getTexture().getWidth()), (float)(application.getCanvas().getGaussianFilter().getFboFirstPass().getTexture().getHeight()));
//        gl.glUniform2f(drawn.getUniform("lowerCorner"), (float)(application.getCanvas().getGaussianFilter().getFboFirstPass().getTexture().getLowerCorner()[0]), (float)(application.getCanvas().getGaussianFilter().getFboFirstPass().getTexture().getLowerCorner()[1]));

        //gl.glUniform2f(drawn.getUniform("fboDimension"), (float)(blurredTexture.getWidth()), (float)(blurredTexture.getHeight()));
       // gl.glUniform2f(drawn.getUniform("lowerCorner"), (float)(blurredTexture.getLowerCorner()[0]), (float)(blurredTexture.getLowerCorner()[1]));
        gl.glUniform1f(drawn.getUniform("alpha"), polygon.getAlpha());
        gl.glUniform2f(drawn.getUniform("bgLowerCorner"), (float)polygon.getTexture().getLowerCorner()[0], (float)polygon.getTexture().getLowerCorner()[1]);
        BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
        gl.glActiveTexture(gl.GL_TEXTURE0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, ((PolygonGL) polygon).getTexture().getID());
        //System.out.println("polygon texture" +((PolygonGL) polygon).getTexture().getID());
        gl.glActiveTexture(gl.GL_TEXTURE1);
        gl.glBindTexture(gl.GL_TEXTURE_2D, blurredTexture.getID());
        gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);

    }

    public void renderTranslucentPolygonFilled(PolygonGL polygon, Texture blurredTexture, Vector3f color) {
        //System.out.println("rendering translucent polygon ");
        ShaderProgram translucentColorFill = getPrograms().get(RenderedElementTypes.FILLED_TRANSLUCENT);
        int vertexAttributeId = translucentColorFill.getAttribute(POSITION);
        int textureAttributeId = translucentColorFill.getAttribute(TEXTURE_POSITION);
        gl.glUseProgram(translucentColorFill.getProgramId());
        gl.glUniformMatrix4fv(translucentColorFill.getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
        gl.glUniformMatrix4fv(translucentColorFill.getUniform(GLUtilities.PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(), 0);
        BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
        BufferGL texBuffer = polygon.getBuffer(BufferGL.Type.TEXTURE);
        gl.glEnableVertexAttribArray(vertexAttributeId);
        gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(textureAttributeId);
        gl.glBindBuffer(texBuffer.getTarget(), texBuffer.getBuffer());
        gl.glVertexAttribPointer(textureAttributeId, texBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
        gl.glUniform2f(translucentColorFill.getUniform("offset"), (float) polygon.getOffset()[0], (float) polygon.getOffset()[1]);
        gl.glUniform1f(translucentColorFill.getUniform("alpha"), polygon.getAlpha());
        gl.glUniform3f(translucentColorFill.getUniform("color"), color.x, color.y, color.z);
        BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
        gl.glActiveTexture(gl.GL_TEXTURE0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, 0);
        gl.glActiveTexture(gl.GL_TEXTURE1);
        gl.glBindTexture(gl.GL_TEXTURE_2D, blurredTexture.getID());
        gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);

    }

    public void renderPoint(PointGL pointGL) {
        ShaderProgram points = getPrograms().get(RenderedElementTypes.POINTS);
        int vertexAttributeId = points.getAttribute(POSITION);
        gl.glUseProgram(points.getProgramId());
        gl.glUniformMatrix4fv(points.getUniform(MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
        gl.glUniformMatrix4fv(points.getUniform(PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(), 0);
        BufferGL vertexBuffer = pointGL.getBuffer(BufferGL.Type.VERTEX);
        gl.glEnableVertexAttribArray(vertexAttributeId);
        gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
        //System.out.println("Vertex buffer in renderPoint"+vertexBuffer.getBuffer());
        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
        gl.glUniform3f(points.getUniform("color"), (float)pointGL.getColor().x, (float)pointGL.getColor().y, (float)pointGL.getColor().z);
        BufferGL indexBuffer = pointGL.getBuffer(BufferGL.Type.INDEX);
        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
        //gl.glPointSize(50.0f);
        //System.out.println("Index length "+pointGL.getIndex().array().length);
        //gl.glEnable(gl.GL_POINT_SMOOTH);
//        gl.glHint(gl.GL_POINT_SMOOTH_HINT, GL_NICEST);
        gl.glDrawElements(GL_POINTS, pointGL.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);

    }

    public void renderColoredPolygon(PolygonGL polygon, Vector3f color) {
        //System.out.println("renderColoredPoygon");
        renderColoredPolygon(polygon, color, polygon.getAlpha());
    }

    public void renderColoredPolygon(PolygonGL polygon, Vector3f color, float alpha) {
        ShaderProgram colorFill = getPrograms().get(RenderedElementTypes.FILLED);
        int vertexAttributeId = colorFill.getAttribute(POSITION);

        gl.glUseProgram(colorFill.getProgramId());
        gl.glUniformMatrix4fv(colorFill.getUniform(GLUtilities.MV_MATRIX), 1, false, application.getCanvas().getCamera().lookAt(), 0);
        gl.glUniformMatrix4fv(colorFill.getUniform(GLUtilities.PROJ_MATRIX), 1, false, application.getCanvas().getCamera().getProjectionMatrix(), 0);
        gl.glUniform2f(colorFill.getUniform("offset"), (float)polygon.getOffset()[0], (float)polygon.getOffset()[1]);
        gl.glUniform1f(colorFill.getUniform("alpha"), alpha);
        gl.glUniform3f(colorFill.getUniform("color"),color.x, color.y, color.z);

        BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
        if (vertexBuffer != null) {
            gl.glEnableVertexAttribArray(vertexAttributeId);
            gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
            gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
            BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
            gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
            gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
        }
    }

    public void generateBlurredTexture (GeoElement ge, GaussianFilter gaussianFilter, Layer layer, Camera camera, GLAutoDrawable drawable) {
        ShaderProgram simpleColor = getPrograms().get(GLUtilities.RenderedElementTypes.SIMPLE);
        //System.out.println("generateBlurredTexture");

        int vertexAttributeId = simpleColor.getAttribute(POSITION);
        gaussianFilter.getFboBlackAndWhite().refreshTexture(camera.getScreenRealWidth(),
                camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, gaussianFilter.getfboBlackAndWhiteId());
        //gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0);
        gl.glClear(gl.GL_COLOR_BUFFER_BIT);
        gl.glBindVertexArray(layer.getVao());
        for (ShapeGL shape:ge.getShapes()) {

            //if(shape.getVisible() && !shape.getOfseted() && shape instanceof PolygonGL) {
            if(!shape.getOfseted() && shape instanceof PolygonGL) {
                //System.out.println("shape "+shape);
                gl.glUseProgram(simpleColor.getProgramId());
                gl.glUniformMatrix4fv(simpleColor.getUniform(GLUtilities.MV_MATRIX), 1, false, camera.lookAt(), 0);
                gl.glUniformMatrix4fv(simpleColor.getUniform(GLUtilities.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(), 0);
                gl.glUniform2f(simpleColor.getUniform("offset"),(float)shape.getOffset()[0],(float)shape.getOffset()[1]);
                BufferGL vertexBuffer = shape.getBuffer(BufferGL.Type.VERTEX);
                if (vertexBuffer != null) {
                    gl.glEnableVertexAttribArray(vertexAttributeId);
                    gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
                    gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                    BufferGL indexBuffer = shape.getBuffer(BufferGL.Type.INDEX);
                    gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
                    //System.out.println("shape.getIndex().array().length"+shape.getIndex().array().length);
                    gl.glDrawElements(GL_TRIANGLES, shape.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                }
            }
        }

        //System.out.println("end simple color rendering");

        gl.glReadBuffer(GL_COLOR_ATTACHMENT0);

        gaussianFilter.getFboFirstPass().refreshTexture(camera.getScreenRealWidth(), camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        gaussianFilter.getFboSecondPass().refreshTexture(camera.getScreenRealWidth(), camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        gaussianFilter.refreshQuadCoordinates(camera.eye, camera.right, camera.left, camera.bottom, camera.top);
        shapeBufferData(gaussianFilter.getQuad(), drawable);
        //writeBufferGaussianFilter(gaussianFilter, gaussianFilter.getFboBlackAndWhite(), gaussianFilter.getCurrentWriteBuffer(), new float[]{1.0f, 0.0f}, (float) (gaussianFilter.getFboBlackAndWhite().getTexture().getWidth() * gaussianFilter.getFboBlackAndWhite().getTexture().getPixels_per_unit_array()[0]));
        //writeBufferGaussianFilter(gaussianFilter, gaussianFilter.getFboBlackAndWhite(), gaussianFilter.getCurrentWriteBuffer(), new float[]{1.0f, 0.0f}, (float) (gaussianFilter.getFboBlackAndWhite().getTexture().getWidth()/gaussianFilter.getRadius()));
        writeBufferGaussianFilter(gaussianFilter, gaussianFilter.getFboBlackAndWhite(), gaussianFilter.getCurrentWriteBuffer(), new float[]{1.0f, 0.0f}, (float) (gaussianFilter.getFboBlackAndWhite().getTexture().getWidth()*gaussianFilter.getFboBlackAndWhite().getTexture().getPixels_per_unit_array()[0]));
        //glUtilities.writeBufferGaussianFilter(gaussianFilter, gaussianFilter.getFboBlackAndWhite(), gaussianFilter.getCurrentWriteBuffer(), new float[] {1.0f,0.0f}, getWidth());
        gaussianFilter.setRadius(2.0f);
        int nIterations = 2;
        FrameBufferObject currentWriteFbo = null;
        FrameBufferObject currentReadFbo = null;
        for (int i=0; i<nIterations;i++) {
            //gaussianFilter.setRadius(gaussianFilter.getRadius()+1.0f);
            currentWriteFbo = gaussianFilter.getCurrentWriteBuffer();
            currentReadFbo = gaussianFilter.getCurrentReadBuffer();


            //writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{0.0f, 1.0f}, (float) (currentReadFbo.getTexture().getHeight() * currentReadFbo.getTexture().getPixels_per_unit_array()[1]));
             //writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{0.0f, 1.0f}, (float) (currentReadFbo.getTexture().getHeight()/gaussianFilter.getRadius()));
            writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{0.0f, 1.0f}, (float) (currentReadFbo.getTexture().getHeight()*currentReadFbo.getTexture().getPixels_per_unit_array()[1]));

            currentWriteFbo = gaussianFilter.getCurrentWriteBuffer();
            currentReadFbo = gaussianFilter.getCurrentReadBuffer();
            //writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{1.0f, 0.0f}, (float) (currentReadFbo.getTexture().getWidth()*currentReadFbo.getTexture().getPixels_per_unit_array()[0]));
            //writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{1.0f, 0.0f}, (float) (application.getCanvas().getWidth() * MapLayerViewer.baseTexture.getPixels_per_unit_array()[0]));
            writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{1.0f, 0.0f}, (float) (currentReadFbo.getTexture().getWidth()*currentReadFbo.getTexture().getPixels_per_unit_array()[0]));

        }
        gl.glReadBuffer(GL_COLOR_ATTACHMENT0);
        //gl.glBindFramebuffer(gl.GL_READ_FRAMEBUFFER, currentWriteFbo.getVboiD());
        //return currentWriteFbo;

    }

    public void generateBlurredTexture ( GaussianFilter gaussianFilter, VectorSelection vectorSelection, GLAutoDrawable drawable, Camera camera) {
        ShaderProgram simpleColor = getPrograms().get(GLUtilities.RenderedElementTypes.SIMPLE);
        //System.out.println("generateBlurredTexture");

        int vertexAttributeId = simpleColor.getAttribute(POSITION);
//        gaussianFilter.getFboBlackAndWhite().refreshTexture(application.DIMENSION_X,
//                application.DIMENSION_Y, new double[] {application.LOWER_CORNER_X, application.LOWER_CORNER_Y}, application.reference.getPixels_per_unit(), new double[] {application.SCENE_PIXELS_WIDTH/application.DIMENSION_X, application.SCENE_PIXELS_HEIGHT/application.DIMENSION_Y});
        gaussianFilter.getFboBlackAndWhite().refreshTexture(camera.getScreenRealWidth(),
                camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, gaussianFilter.getfboBlackAndWhiteId());
        //gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0);
        gl.glClear(gl.GL_COLOR_BUFFER_BIT);
        gl.glBindVertexArray(vectorSelection.getLayer().getVao());
        //gl.glViewport(0,0,application.SCENE_PIXELS_HEIGHT, application.SCENE_PIXELS_HEIGHT);
        for (GeoElement ge: vectorSelection.getSelectedElements()) {
            for (ShapeGL shape : ge.getShapes()) {

                if (!shape.getOfseted() && shape instanceof PolygonGL) {
                     //System.out.println("shape " + shape);
                    gl.glUseProgram(simpleColor.getProgramId());
                    gl.glUniformMatrix4fv(simpleColor.getUniform(GLUtilities.MV_MATRIX), 1, false, camera.lookAt(), 0);
                    gl.glUniformMatrix4fv(simpleColor.getUniform(GLUtilities.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(), 0);
                    gl.glUniform2f(simpleColor.getUniform("offset"), (float) shape.getOffset()[0], (float) shape.getOffset()[1]);
                    BufferGL vertexBuffer = shape.getBuffer(BufferGL.Type.VERTEX);
                    if (vertexBuffer != null) {
                        gl.glEnableVertexAttribArray(vertexAttributeId);
                        gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
                        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        BufferGL indexBuffer = shape.getBuffer(BufferGL.Type.INDEX);
                        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
                        //System.out.println("shape.getIndex().array().length" + shape.getIndex().array().length);
                        gl.glDrawElements(GL_TRIANGLES, shape.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                    }
                }
            }
        }

        //System.out.println("end simple color rendering");

        gl.glReadBuffer(GL_COLOR_ATTACHMENT0);

//        gaussianFilter.getFboFirstPass().refreshTexture(application.DIMENSION_X,
//                application.DIMENSION_Y, new double[] {application.LOWER_CORNER_X, application.LOWER_CORNER_Y},  application.reference.getPixels_per_unit(), new double[] {application.SCENE_PIXELS_WIDTH/application.DIMENSION_X, application.SCENE_PIXELS_HEIGHT/application.DIMENSION_Y});
//        gaussianFilter.getFboSecondPass().refreshTexture(application.DIMENSION_X,
//                application.DIMENSION_Y, new double[] {application.LOWER_CORNER_X, application.LOWER_CORNER_Y},  application.reference.getPixels_per_unit(), new double[] {application.SCENE_PIXELS_WIDTH/application.DIMENSION_X, application.SCENE_PIXELS_HEIGHT/application.DIMENSION_Y});
//        gaussianFilter.refreshQuadCoordinates(new double[] {application.LOWER_CORNER_X, application.LOWER_CORNER_Y}, application.DIMENSION_X, application.DIMENSION_Y);
        gaussianFilter.getFboFirstPass().refreshTexture(camera.getScreenRealWidth(), camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        gaussianFilter.getFboSecondPass().refreshTexture(camera.getScreenRealWidth(), camera.getScreenRealHeight(), camera.getLowerCorner(), camera.getCamera_pixels_per_unit(), camera.getPixels_per_unit());
        gaussianFilter.refreshQuadCoordinates(camera.eye, camera.right, camera.left, camera.bottom, camera.top);
        shapeBufferData(gaussianFilter.getQuad(), drawable);
        //writeBufferGaussianFilter(gaussianFilter, gaussianFilter.getFboBlackAndWhite(), gaussianFilter.getCurrentWriteBuffer(), new float[]{1.0f, 0.0f}, (float) (gaussianFilter.getFboBlackAndWhite().getTexture().getWidth() * gaussianFilter.getFboBlackAndWhite().getTexture().getPixels_per_unit_array()[0]));
        //writeBufferGaussianFilter(gaussianFilter, gaussianFilter.getFboBlackAndWhite(), gaussianFilter.getCurrentWriteBuffer(), new float[]{1.0f, 0.0f}, (float) (gaussianFilter.getFboBlackAndWhite().getTexture().getWidth()/gaussianFilter.getRadius()));
        writeBufferGaussianFilter(gaussianFilter, gaussianFilter.getFboBlackAndWhite(), gaussianFilter.getCurrentWriteBuffer(), new float[]{1.0f, 0.0f}, (float) (gaussianFilter.getFboBlackAndWhite().getTexture().getWidth()*gaussianFilter.getFboBlackAndWhite().getTexture().getPixels_per_unit_array()[0]));
        //glUtilities.writeBufferGaussianFilter(gaussianFilter, gaussianFilter.getFboBlackAndWhite(), gaussianFilter.getCurrentWriteBuffer(), new float[] {1.0f,0.0f}, getWidth());
        gaussianFilter.setRadius(2.0f);
        int nIterations = 2;
        FrameBufferObject currentWriteFbo = null;
        FrameBufferObject currentReadFbo = null;
        for (int i=0; i<nIterations;i++) {
            //gaussianFilter.setRadius(gaussianFilter.getRadius()+1.0f);
            currentWriteFbo = gaussianFilter.getCurrentWriteBuffer();
            currentReadFbo = gaussianFilter.getCurrentReadBuffer();


            //writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{0.0f, 1.0f}, (float) (currentReadFbo.getTexture().getHeight() * currentReadFbo.getTexture().getPixels_per_unit_array()[1]));
            //writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{0.0f, 1.0f}, (float) (currentReadFbo.getTexture().getHeight()/gaussianFilter.getRadius()));
            writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{0.0f, 1.0f}, (float) (currentReadFbo.getTexture().getHeight()*currentReadFbo.getTexture().getPixels_per_unit_array()[1]));

            currentWriteFbo = gaussianFilter.getCurrentWriteBuffer();
            currentReadFbo = gaussianFilter.getCurrentReadBuffer();
            //writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{1.0f, 0.0f}, (float) (currentReadFbo.getTexture().getWidth()*currentReadFbo.getTexture().getPixels_per_unit_array()[0]));
            //writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{1.0f, 0.0f}, (float) (application.getCanvas().getWidth() * MapLayerViewer.baseTexture.getPixels_per_unit_array()[0]));
            writeBufferGaussianFilter(gaussianFilter, currentReadFbo, currentWriteFbo, new float[]{1.0f, 0.0f}, (float) (currentReadFbo.getTexture().getWidth()*currentReadFbo.getTexture().getPixels_per_unit_array()[0]));

        }
        gl.glReadBuffer(GL_COLOR_ATTACHMENT0);
        //gl.glBindFramebuffer(gl.GL_READ_FRAMEBUFFER, currentWriteFbo.getVboiD());
        //return currentWriteFbo;

    }

















}
