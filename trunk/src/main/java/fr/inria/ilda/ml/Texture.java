/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

/**
 * Created by mjlobo on 20/05/15.
 */
public class Texture {
    private String name;
    private double pixels_per_unit;
    protected double[] pixels_per_unit_array;
    private double width;
    private double height;
    double[] lowerCorner;
    int ID;
    int privateID;
    static int count = 0;

    public Texture (double pixels_per_unit, double width, double height, double[] lowerCorner) {
        this(null, pixels_per_unit, width, height, lowerCorner);
    }

    public Texture (String name, double pixels_per_unit, double width, double height, double[] lowerCorner) {
        this.name = name;
        this.pixels_per_unit = pixels_per_unit;
        this.width = width;
        this.height = height;
        this.lowerCorner = lowerCorner;
        privateID = count;
        count++;
    }

    public Texture (RasterGeoElement rasterGeoElement) {
        pixels_per_unit = (rasterGeoElement.getImageWidth()/rasterGeoElement.getRealWidth());
        //pixels_per_unit_array = new double [] {Math.round(pixels_per_unit), Math.round(rasterGeoElement.getImageHeight()/rasterGeoElement.getRealHeight())};
        pixels_per_unit_array = new double [] {pixels_per_unit, rasterGeoElement.getImageHeight()/rasterGeoElement.getRealHeight()};
        //System.out.println("double pixels_per_unit" + rasterGeoElement.getImageWidth()/rasterGeoElement.getRealWidth());
        //System.out.println("float pixels_per_unit" + pixels_per_unit);

        width = rasterGeoElement.getRealWidth();
        height = rasterGeoElement.getRealHeight();
        lowerCorner = rasterGeoElement.getLowerCorner();
        name=null;
        privateID = count;
        count++;
    }

    public void setID (int ID) {
        this.ID = ID;
    }

    public int getID () {
        return ID;
    }

    public double getPixels_per_unit() {
        return pixels_per_unit;
    }

    public double getWidth () {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double[] getLowerCorner() {
        return lowerCorner;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setLowerCorner(double[] lowerCorner) {
        this.lowerCorner = lowerCorner;
    }

    public void setPixels_per_unit(double pixels_per_unit) {
        this.pixels_per_unit = pixels_per_unit;
    }

    public void setPixels_per_unit_array(double[] pixels_per_unit_array) {this.pixels_per_unit_array = pixels_per_unit_array; }

    public double[] getPixels_per_unit_array() {return pixels_per_unit_array;}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void print() {
        System.out.println("Texture Lower Corner "+lowerCorner[0]+" "+lowerCorner[1]);
        System.out.println("Height "+height);
        System.out.println("Width "+width);
    }

}
