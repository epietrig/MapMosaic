/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by mjlobo on 05/08/15.
 */
public class Inspector extends JPanel {

    java.util.List<RasterGeoElement> layers;

    public Inspector(java.util.List<RasterGeoElement> layers) {
        this.layers = layers;
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        setPreferredSize(new Dimension(300, 500));
        addTitle();
        addCheckbox("Composite");
        addCheckbox("Translucent Border");
        addCheckbox("Outline");
        addComboBox();

    }

    void addTitle() {
        JLabel title = new JLabel("Composite settings");
        title.setAlignmentX(Component.CENTER_ALIGNMENT);
        title.setBorder(new EmptyBorder(10,10,10,10));
        add(title);
    }

    JCheckBox addCheckbox (String text) {
        JCheckBox checkBox = new JCheckBox(text);
        checkBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        //checkBox.setBorder(new EmptyBorder(2,0,2,0));
        add(checkBox);
        return checkBox;
    }

    JComboBox addComboBox() {
        String[] layerNames = new String[layers.size()];
        for (int i=0; i<layerNames.length; i++) {
            layerNames[i] = layers.get(i).getName();
        }
        JComboBox rasterComboBox = new JComboBox(layerNames);
        rasterComboBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(rasterComboBox);
        return rasterComboBox;
    }



}
