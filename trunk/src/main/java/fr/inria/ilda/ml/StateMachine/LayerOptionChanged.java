/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.StateMachine;

import fr.inria.ilda.ml.MapLayerViewer;

import java.awt.*;

/**
 * Created by mjlobo on 09/08/15.
 */
public class LayerOptionChanged extends Event {
    String layerName;
    Double margin;
    Color outlineColor;
    Color fillColor;
    String rasterName;
    Double alpha;
    boolean outlineSelected;


    public LayerOptionChanged(String rasterName, String layerName) {
        this.rasterName = rasterName;
        this.layerName = layerName;
    }
    public LayerOptionChanged(Color fillColor, String rasterName, String layerName) {
        this.fillColor = fillColor;
        this.rasterName = rasterName;
        this.layerName = layerName;
    }

    public LayerOptionChanged(double value, String layerName, String textFieldName) {
        if (textFieldName.equals(MapLayerViewer.MARGIN)) {
            this.margin = value;
        }
        else if (textFieldName.equals(MapLayerViewer.ALPHA)) {
            this.alpha = value;
        }
        this.layerName = layerName;
    }

    public LayerOptionChanged(boolean outlineSelected, Color outlineColor, String layerName) {
        this.outlineSelected = outlineSelected;
        this.outlineColor = outlineColor;
        this.layerName = layerName;
    }

    public String getLayerName() {
        return layerName;
    }

    public String getRasterName() {
        return rasterName;
    }

    public Double getMargin() {
        return margin;
    }

    public Color getOutlineColor() {
        return outlineColor;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public boolean getOutlineSelected() {
        return outlineSelected;
    }

    public Double getAlpha() {return alpha;}
}
