/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import fr.inria.ilda.ml.DrawnGeoElement;
import fr.inria.ilda.ml.GLUtilities;
import fr.inria.ilda.ml.MapLayerViewer;
import fr.inria.ilda.ml.StateMachine.CheckBoxEvent;
import fr.inria.ilda.ml.StateMachine.StateMachine;
import fr.inria.ilda.ml.VectorSelection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 06/08/15.
 */
public class LayerOptions extends JPanel{
    JCheckBox titleCheckbox;
    StringColorComboPanel stringComboPanel;
    MarginTextField marginTextField;
    ColorCheckbox outlineCheckbox;
    MarginTextField alphaTextField;
    int indexInParent;

    String layerName;
    List<String> rasterLayers;

    public LayerOptions(String layerName, List<String> rasterLayers) {
        this.layerName = layerName;
        this.rasterLayers = new ArrayList<>();
        this.rasterLayers.addAll(rasterLayers);
        this.indexInParent = indexInParent;
        initUi();
    }

    void initUi() {
        //setPreferredSize(new Dimension(300,300));
        setBorder(BorderFactory.createRaisedBevelBorder());
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());

        titleCheckbox = new JCheckBox(layerName);
        titleCheckbox.setActionCommand(layerName);

        titleCheckbox.setFont(new Font(titleCheckbox.getFont().getName(),Font.BOLD,titleCheckbox.getFont().getSize()));
        titleCheckbox.setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 2));
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.gridheight = 1;
        gbc.weightx = 0.5;
        gbc.weighty =0;
        gbc.anchor = GridBagConstraints.LINE_START;
        add(titleCheckbox, gbc);

        Color defaultOutlineColor = new Color (MapLayerViewer.defaultOutlineLine.x, MapLayerViewer.defaultOutlineLine.y, MapLayerViewer.defaultOutlineLine.z);
        outlineCheckbox = new ColorCheckbox("Outline: ", defaultOutlineColor);
        gbc.gridy =1;
        outlineCheckbox.setEnabled(false);
        add(outlineCheckbox, gbc);


        marginTextField = new MarginTextField("Margin: ");
        marginTextField.setEnabled(false);
        marginTextField.setName(MapLayerViewer.MARGIN);
        gbc.gridy = 2;
        add(marginTextField, gbc);

        rasterLayers.add(MapLayerViewer.FILL_COLOR);
        Color defaultFillColor = new Color (MapLayerViewer.defaultFillColor.x, MapLayerViewer.defaultFillColor.y, MapLayerViewer.defaultFillColor.z);
        stringComboPanel = new StringColorComboPanel("Target Layer: ", rasterLayers, defaultFillColor);
        stringComboPanel.setEnabled(false);
        gbc.gridy = 3;
        add(stringComboPanel, gbc);

        gbc.gridy = 4;
        alphaTextField = new MarginTextField("Opacity: ");
        alphaTextField.setEnabled(false);
        alphaTextField.setName(MapLayerViewer.ALPHA);
        add(alphaTextField, gbc);




    }

    public void addListeners(final StateMachine stateMachine, JFrame parentJFrame) {
        titleCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (titleCheckbox.isSelected()) {
                    stringComboPanel.setEnabled(true);
                    marginTextField.setEnabled(true);
                    outlineCheckbox.setEnabled(true);
                    alphaTextField.setEnabled(true);
                }
                else {
                    stringComboPanel.setEnabled(false);
                    marginTextField.setEnabled(false);
                    outlineCheckbox.setEnabled(false);
                    alphaTextField.setEnabled(false);
                }
                stateMachine.handleEvent(new CheckBoxEvent(titleCheckbox.getText(), titleCheckbox));
            }
        });
        stringComboPanel.addListenersSelection(layerName, stateMachine, parentJFrame);
        marginTextField.addListenersSelection(layerName, stateMachine);
        outlineCheckbox.addListenersSelection(layerName, stateMachine);
        alphaTextField.addListenersSelection(layerName, stateMachine);


    }

    public void update(VectorSelection vectorSelection) {
        if (vectorSelection.getIsVisible()){
            titleCheckbox.setEnabled(true);
            titleCheckbox.setSelected(true);
            stringComboPanel.setEnabled(true);
            marginTextField.setEnabled(true);
            outlineCheckbox.setEnabled(true);
            alphaTextField.setEnabled(true);
        }
        else {
            titleCheckbox.setSelected(false);
            stringComboPanel.setEnabled(false);
            marginTextField.setEnabled(false);
            outlineCheckbox.setEnabled(false);
            alphaTextField.setEnabled(false);
        }
        Color fillColor = new Color(vectorSelection.getColor().x, vectorSelection.getColor().y, vectorSelection.getColor().z);
        Color outlineColor = new Color(vectorSelection.getOutlineColor().x, vectorSelection.getOutlineColor().y, vectorSelection.getOutlineColor().z);
        if (vectorSelection.getSelectedType() == GLUtilities.RenderedElementTypes.FILLED || vectorSelection.getSelectedType() == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT) {
            stringComboPanel.update(MapLayerViewer.FILL_COLOR, fillColor);
        }
        else {
            stringComboPanel.update(vectorSelection.getTexture().getName(), fillColor);
        }

        marginTextField.updateText(""+vectorSelection.getBuffer());
        outlineCheckbox.update(vectorSelection.getOutlineVisible(), outlineColor);
        alphaTextField.updateText(""+vectorSelection.getAlpha());
    }



    public String getLayerName() {
        return layerName;
    }

    public void reset(String fill, Color color) {
        titleCheckbox.setSelected(false);
        stringComboPanel.setEnabled(false);
        stringComboPanel.update(fill, color);
        //stringComboPanel.update(fill, new Color(MapLayerViewer.defaultFillColor.x, MapLayerViewer.defaultFillColor.y, MapLayerViewer.defaultFillColor.z));
        marginTextField.setEnabled(false);
        marginTextField.updateText("" + 0.0);
        outlineCheckbox.setEnabled(false);
        outlineCheckbox.update(false, new Color(MapLayerViewer.defaultOutlineLine.x, MapLayerViewer.defaultOutlineLine.y, MapLayerViewer.defaultOutlineLine.z));
        alphaTextField.setEnabled(false);
        alphaTextField.updateText(""+1.0);
    }

    public void setEnabledTitle(boolean enabledTitle) {
        titleCheckbox.setEnabled(enabledTitle);
    }

    public void reset(DrawnGeoElement drawnGeoElement) {
        if (drawnGeoElement.getRenderingType()== GLUtilities.RenderedElementTypes.FILLED || drawnGeoElement.getRenderingType()== GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT) {
            reset(MapLayerViewer.FILL_COLOR, new Color(drawnGeoElement.getFillColor().x, drawnGeoElement.getFillColor().y, drawnGeoElement.getFillColor().z));
        }
        else {
            reset(drawnGeoElement.getTexture().getName(), new Color(drawnGeoElement.getFillColor().x, drawnGeoElement.getFillColor().y, drawnGeoElement.getFillColor().z));
        }
    }

    public void setTitleCheckboxEnabled(boolean enabled) {
        titleCheckbox.setEnabled(enabled);
    }

    public void setIndexInParent (int indexInParent) {
        this.indexInParent = indexInParent;
    }

    public int getIndexInParent() {
        return indexInParent;
    }

}
