/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mjlobo on 25/06/15.
 */
public class StringQueryFilter extends QueryFilter{

    ShapeFileStringAttribute attribute;
    LinkedList<String> strings;
    HashSet<GeoElement> elements;
    HashSet<ShapeFileFeature> features;

    public StringQueryFilter(ShapeFileStringAttribute attribute, LinkedList<String> strings, VectorLayer layer) {
        this.attribute = attribute;
        this.strings = strings;
        this.layer = layer;
    }

    public StringQueryFilter (StringQueryFilter otherQueryFilter) {
        this.attribute = otherQueryFilter.getAttribute();
        strings = new LinkedList<>();
        for (String s : otherQueryFilter.getStrings()) {
            strings.add(new String(s));
        }
        this.layer = otherQueryFilter.getLayer();
    }
    @Override
    public HashSet<GeoElement> getElements() {
        elements = new HashSet<>();
        getFeatures();
        for (ShapeFileFeature feature: features) {
            for (GeoElement ge : feature.getVectorGeoElements()) {
                elements.add(ge);
            }
        }
        return elements;
    }

    @Override
    public HashSet<GeoElement> getElements(List<GeoElement> elementsToConsider) {
        return null;
    }

    @Override
    public HashSet<ShapeFileFeature> getFeatures() {
        features = ((VectorLayer)layer).getFeaturesByStringAttributeValue(attribute, strings);
        return features;
    }

    @Override
    public HashSet<ShapeFileFeature> getFeatures(HashSet<ShapeFileFeature> featuresToConsider) {
        features = ((VectorLayer)layer).getFeaturesByStringAttributeValue(attribute, strings, featuresToConsider);
        return features;
    }

    @Override
    public HashSet<GeoElement> update() {
        getFeatures();
        elements.clear();
        for (ShapeFileFeature feature: features) {
            for (GeoElement ge : feature.getVectorGeoElements()) {
                elements.add(ge);
            }
        }
        return elements;
    }

    @Override
    public HashSet<ShapeFileFeature> updateFeatures(HashSet<ShapeFileFeature> featuresToConsider) {
        getFeatures(featuresToConsider);
        return features;
    }

    @Override
    public HashSet<ShapeFileFeature> updateFeatures() {
        return getFeatures();
    }

    public void setStrings(LinkedList<String> strings) {
        this.strings = strings;
    }

    public LinkedList<String> getStrings() { return  strings;}

    public ShapeFileStringAttribute getAttribute() {
        return attribute;
    }
}
