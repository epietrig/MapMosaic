/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import com.vividsolutions.jts.geom.Geometry;
import fr.inria.ilda.ml.GeoElement;
import fr.inria.ilda.ml.gl.PointGL;

import javax.vecmath.Vector3f;
import java.util.List;

/**
 * Created by mjlobo on 03/08/15.
 */
public class PointGeoElement extends GeoElement {

    List<PointGL> points;
    double[] coordinate;
    Vector3f color;
    PointGL pointGL;

    public PointGeoElement (Geometry geometry,Vector3f color) {
        this.coordinate = new double[] {};
        this.geometry = geometry;
        this.coordinate = new double[] {geometry.getCoordinate().x, geometry.getCoordinate().y};
        pointGL = new PointGL(color, coordinate);
    }

    public PointGeoElement (Geometry geometry) {
       this(geometry, new Vector3f (1.0f,1.0f,0.0f));
    }
    @Override
    public void bufferPolygon(double bufferOffset) {

    }
}
