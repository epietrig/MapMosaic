/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import fr.inria.ilda.ml.MapLayerViewer;
import fr.inria.ilda.ml.StateMachine.ColorChangedEvent;
import fr.inria.ilda.ml.StateMachine.ComboBoxEvent;
import fr.inria.ilda.ml.StateMachine.LayerOptionChanged;
import fr.inria.ilda.ml.StateMachine.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by mjlobo on 06/08/15.
 */
public class StringColorComboPanel extends JPanel {
    JLabel label;
    JComboBox comboBox;
    String text;
    java.util.List<String> comboValues;
    JLabel colorLabel;
    Color color;
    boolean fireActionEventComboBox = true;


    public StringColorComboPanel(String text, java.util.List<String> comboValues, Color color) {
        this.text = text;
        this.comboValues = comboValues;
        this.color = color;
        initUI();
    }

    void initUI() {
        setLayout(new FlowLayout(FlowLayout.LEFT));
        this.setBorder(BorderFactory.createEmptyBorder(2,5,2,2));
        label = new JLabel(text);
        add(label);
        String[] comboArray = new String[comboValues.size()];
        for (int i=0; i<comboArray.length; i++) {
            comboArray[i] = comboValues.get(i);
        }
        comboBox = new JComboBox(comboArray);
        comboBox.setAlignmentX(Component.LEFT_ALIGNMENT);

        add(comboBox);
        colorLabel = new JLabel("");
        colorLabel.setName(MapLayerViewer.FILL_COLOR);
        colorLabel.setPreferredSize(new Dimension(50,15));
        //colorLabel.setMinimumSize(new Dimension(50, 15));
        colorLabel.setBackground(color);
        colorLabel.setOpaque(true);

        colorLabel.setVisible(false);
        add(colorLabel);

    }

    public void setEnabled(boolean enabled) {
        comboBox.setEnabled(enabled);
        label.setEnabled(enabled);
    }

    public void addListeners(final StateMachine stateMachine, final JFrame parentJFrame) {
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (comboBox.getSelectedItem().equals(MapLayerViewer.FILL_COLOR)) {
                    colorLabel.setVisible(true);
                    //parentJFrame.pack();
                }
                else {
                    colorLabel.setVisible(false);
                    //parentJFrame.pack();
                }
                if (fireActionEventComboBox) {
                    stateMachine.handleEvent(new ComboBoxEvent((String) comboBox.getSelectedItem(), comboBox));
                }
            }
        });
        colorLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                //System.out.println("click on label!");
                Color newColor = JColorChooser.showDialog(StringColorComboPanel.this,"Color picker",color);
                if (newColor!=null) {
                    StringColorComboPanel.this.color = newColor;
                    StringColorComboPanel.this.colorLabel.setBackground(newColor);
                    stateMachine.handleEvent(new ColorChangedEvent(colorLabel, color));
                }
            }
        });

    }

    public void addListenersSelection(final String layer, final StateMachine stateMachine, final JFrame parentJFrame) {
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (comboBox.getSelectedItem().equals(MapLayerViewer.FILL_COLOR)) {
                    colorLabel.setVisible(true);
                    //parentJFrame.pack();
                }
                else {
                    colorLabel.setVisible(false);
                }
                if (fireActionEventComboBox) {
                    stateMachine.handleEvent(new LayerOptionChanged((String) comboBox.getSelectedItem(), layer));
                }
            }
        });
        colorLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                //System.out.println("click on label!");
                Color newColor = JColorChooser.showDialog(StringColorComboPanel.this,"Color picker",color);
                if (newColor!=null) {
                    StringColorComboPanel.this.color = newColor;
                    StringColorComboPanel.this.colorLabel.setBackground(newColor);
                    stateMachine.handleEvent(new LayerOptionChanged(color, (String)comboBox.getSelectedItem(), layer));
                }
            }
        });

    }

    public void update(String text, Color color) {
        //comboBox.setSelectedIndex(comboValues.indexOf(text));
        fireActionEventComboBox = false;
        comboBox.setSelectedItem(text);
        fireActionEventComboBox = true;
        if (text == MapLayerViewer.FILL_COLOR) {
            colorLabel.setBackground(color);
            this.color = color;
        }
    }

    public String getStringSelectedItem() {
        return (String)comboBox.getSelectedItem();
    }

    public Color getColor() {
        return color;
    }


}
