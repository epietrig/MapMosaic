/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import fr.inria.ilda.ml.MapLayerViewer;
import fr.inria.ilda.ml.StateMachine.ColorChangedEvent;
import fr.inria.ilda.ml.StateMachine.ComboBoxEvent;
import fr.inria.ilda.ml.StateMachine.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by mjlobo on 30/09/15.
 */
public class StringComboPanel extends JPanel {
    JLabel label;
    JComboBox comboBox;
    String text;
    java.util.List<String> comboValues;
    boolean fireActionEventComboBox = true;

    public StringComboPanel(String text, java.util.List<String> comboValues) {
        this.text = text;
        this.comboValues = comboValues;
        initUI();
    }

    void initUI() {
        setLayout(new FlowLayout(FlowLayout.LEFT));
        this.setBorder(BorderFactory.createEmptyBorder(2,5,2,2));
        label = new JLabel(text);
        add(label);
        String[] comboArray = new String[comboValues.size()];
        for (int i=0; i<comboArray.length; i++) {
            comboArray[i] = comboValues.get(i);
        }
        comboBox = new JComboBox(comboArray);
        comboBox.setAlignmentX(Component.LEFT_ALIGNMENT);

        add(comboBox);

    }

    public void addListeners(final StateMachine stateMachine) {
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (fireActionEventComboBox) {
                    stateMachine.handleEvent(new ComboBoxEvent((String) comboBox.getSelectedItem(), comboBox));
                }
            }
        });

    }

    public void update(String text) {
        //comboBox.setSelectedIndex(comboValues.indexOf(text));
        fireActionEventComboBox = false;
        comboBox.setSelectedItem(text);
        fireActionEventComboBox = true;
    }

    public String getStringSelectedItem() {
        return (String)comboBox.getSelectedItem();
    }

}
