/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

/**
 * Created by mjlobo on 14/07/15.
 */
public class Pair<T1, T2> {
    private final T1 m_Value1 ;
    private final T2 m_Value2 ;

    public Pair(T1 value1, T2 value2) {
        m_Value1 = value1 ;
        m_Value2 = value2 ;
    }

    public T1 getValue1() { return m_Value1 ; }
    public T2 getValue2() { return m_Value2 ; }
}
