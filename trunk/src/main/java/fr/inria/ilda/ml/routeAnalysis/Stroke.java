/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.routeAnalysis;

import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by mjlobo on 17/06/15.
 */
public class Stroke {
    List<RouteSegment> routeSegmentList;
    List<MultiLineString> multiLineStrings;
    Node startNode;
    Node endNode;
    List<Coordinate> coordinates;

    public Stroke() {
        //System.out.println("new stroke!");
        routeSegmentList = new ArrayList<>();
        multiLineStrings = new ArrayList<>();
        coordinates = new ArrayList<>();
    }

    public Stroke (List <RouteSegment> routeSegmentList) {
        this.routeSegmentList = routeSegmentList;
        multiLineStrings = new ArrayList<>();
        coordinates = new ArrayList<>();
    }

    public void addRouteSegment(RouteSegment routeSegment) {
        routeSegmentList.add(routeSegment);
        if (startNode == null) {
            startNode = routeSegment.getStartNode();
        }
        endNode = routeSegment.getEndNode();
    }

    public List<RouteSegment> getRouteSegmentList() {
        return routeSegmentList;
    }

    public void setStartNode(Node startNode) {
        this.startNode = startNode;
    }

    public void setEndNode(Node endNode) {
        this.endNode = endNode;
    }

    public RouteSegment getLastRouteSegment() {
        if (routeSegmentList.size()>0) {
            return routeSegmentList.get(routeSegmentList.size()-1);
        }
        else {
            return null;
        }
    }

    public Geometry getGeometry () {
        Geometry result=null;
        GeometryFactory geometryFactory = new GeometryFactory();
        List<Coordinate> coordinates = new ArrayList<>();
        for (RouteSegment routeSegment: routeSegmentList) {
            for (Coordinate coord : routeSegment.getLine().getCoordinates()) {
                if (!coordinateContains(coordinates, coord)) {
                    coordinates.add(coord);
                }
            }
//            if (result == null) {
//                result = routeSegment.getLine();
//            }
//            else {
//                result = result.union(routeSegment.getLine()).difference(result.intersection(routeSegment.getLine()));
//            }

        }
        Coordinate[] coordinateArray = new Coordinate[coordinates.size()];
        for (int i=0; i<coordinateArray.length; i++) {
            coordinateArray[i] = coordinates.get(i);
        }
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(coordinateArray);
        LineString lineString = new LineString(coordinateArraySequence, geometryFactory);
        return lineString;
    }

    public Geometry getGeometryLine() {
        Geometry result=null;
        GeometryFactory geometryFactory = new GeometryFactory();
        List<Coordinate> coordinates = new ArrayList<>();
        for (MultiLineString multiLineString: multiLineStrings) {

//            for (int i=0; i<multiLineString.getNumGeometries(); i++){
//                LineString lineString = (LineString)multiLineString.getGeometryN(i);
//                for (Coordinate coord:lineString.getCoordinates()) {
//                    if (!coordinateContains(coordinates, coord)) {
//                        coordinates.add(coord);
//                    }
//                }
//            }

        }
        Coordinate[] coordinateArray = new Coordinate[coordinates.size()];
        for (int i=0; i<coordinateArray.length; i++) {
            coordinateArray[i] = coordinates.get(i);
        }
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(coordinateArray);
        LineString lineString = new LineString(coordinateArraySequence, geometryFactory);
        return lineString;
    }

    public boolean coordinateContains(List<Coordinate> coordinates, Coordinate coordinate) {
        for (Coordinate coord : coordinates) {
            if (coord.x == coordinate.x && coord.y == coordinate.y) {
                return true;
            }
        }
        return false;
    }

    public void addMultiLineString(MultiLineString multiLineString) {
//        if (coordinates.size()==0) {
//
//        }
        LineString firstLine = (LineString)multiLineString.getGeometryN(0);
        LineString lastLine = (LineString)multiLineString.getGeometryN(multiLineString.getNumGeometries()-1);
        ArrayList <Coordinate> newCoordinates = new ArrayList<>();
        //System.out.println("coordinates---------------------------->");
//        for (Coordinate coord: coordinates) {
//            System.out.println("Coordinate "+coord.x+":"+coord.y);
//        }
//        for (Coordinate coord: getCoordinatesMultiLineString(multiLineString)) {
//            System.out.println("MultiLineString Coordinate "+coord.x+":"+coord.y);
//        }
        if (coordinates.size()==0) {
            newCoordinates.addAll(getCoordinatesMultiLineString(multiLineString));
        }
        else if (coordinates.get(0)==firstLine.getStartPoint().getCoordinate() || (coordinates.get(0)==lastLine.getEndPoint().getCoordinate())) {
            newCoordinates.addAll(getCoordinatesMultiLineString(multiLineString));
            newCoordinates.remove(newCoordinates.size()-1);
            newCoordinates.addAll(coordinates);
            //System.out.println("adding beforeee");
        }
        else if (coordinates.get(coordinates.size()-1)==firstLine.getStartPoint().getCoordinate() || coordinates.get(coordinates.size()-1)==lastLine.getEndPoint().getCoordinate()) {
            newCoordinates.addAll(coordinates);
            newCoordinates.remove(newCoordinates.size()-1);
            newCoordinates.addAll(getCoordinatesMultiLineString(multiLineString));
            System.out.println("adding after");
        }
        coordinates = newCoordinates;
//        getCoordinatesMultiLineString(multiLineString);
//        System.out.println("geometry centroid "+multiLineString.getCentroid().getCoordinate().x+":"+multiLineString.getCentroid().getCoordinate().y);
        multiLineStrings.add(multiLineString);
    }

    public List<MultiLineString> getMultiLineStrings() {
        return multiLineStrings;
    }

    List <Coordinate> getCoordinatesMultiLineString(MultiLineString multiLineString) {
        ArrayList<Coordinate> result = new ArrayList<>();
        for (int i=0; i<multiLineString.getNumGeometries(); i++)
        {
//            System.out.println("--------Line-----------------");
            for (Coordinate coord: multiLineString.getGeometryN(i).getCoordinates()){
//                System.out.println("Coordinate : "+coord.toString());
                result.add(coord);
            }
        }
        return result;
    }

    public List <Coordinate> calculateCoordinates () {
        ArrayList<RouteSegment> routeSegmentListCopy = new ArrayList<>();
        HashSet <Node> nodes = new HashSet<>();
        HashSet<Node> repeatedNodes = new HashSet<>();
        List<Coordinate> result = new ArrayList<>();
        Node startNode=null;
        routeSegmentListCopy.addAll(routeSegmentList);
        for (RouteSegment routeSegment: routeSegmentList) {
            if (!nodes.add(routeSegment.getStartNode())) {
                repeatedNodes.add(routeSegment.getStartNode());
            }
            if (!nodes.add(routeSegment.getEndNode())) {
                repeatedNodes.add(routeSegment.getEndNode());
            }
        }

        System.out.println("Nodes length "+nodes.size());
        System.out.println("repeated nodes length "+repeatedNodes.size());

        for (Node node : nodes) {
            if (!repeatedNodes.contains(node)) {
                startNode = node;
            }
        }

//        for (RouteSegment routeSegment: routeSegmentList) {
//            Node nextNode;
//            if (routeSegment.getStartNode()==startNode) {
//                nextNode = routeSegment.getEndNode();
//            }
//            else if (routeSegment.getEndNode() == startNode) {
//                nextNode = routeSegment.getStartNode();
//            }
//            result.addAll(routeSegment.getCoordinates());
//        }

        return addSegmentCoordinates(startNode);


    }

    List<Coordinate> addSegmentCoordinates (Node nextNode) {
        RouteSegment current = findSegmentByNode(nextNode);
        if (current !=null) {
            Node nextNextNode = null;
            if (nextNode == current.getStartNode()) {
                coordinates.addAll(current.getCoordinates());
                printCoordinates();
                nextNextNode = current.getEndNode();
            } else if (nextNode == current.getEndNode()) {
                List<Coordinate> reversedCoordinates = current.getCoordinates();
                Collections.reverse(reversedCoordinates);
                coordinates.addAll(reversedCoordinates);
                printCoordinates();
                nextNextNode = current.getStartNode();
            }
            coordinates.remove(coordinates.size()-1);
            routeSegmentList.remove(current);
            return addSegmentCoordinates(nextNextNode);

        }
        else {
            System.out.println("current null!");
            return coordinates;
        }

    }

    RouteSegment findSegmentByNode (Node node) {
        for (RouteSegment routeSegment: routeSegmentList) {
            if (routeSegment.getStartNode() == node || routeSegment.getEndNode() == node) {
                return routeSegment;
            }
        }
        return null;
    }

    public Coordinate[] getCoordinatesArray() {
        Coordinate [] coordinatesArray = new Coordinate[coordinates.size()];
        for (int i=0; i<coordinatesArray.length; i++){
            coordinatesArray[i] = coordinates.get(i);
        }
        return coordinatesArray;
    }

    public void printCoordinates() {
        System.out.println("-----------Coordinates------------");
        for (Coordinate coord : coordinates) {
            System.out.println("Coordinate "+coord.x +":"+coord.y);
        }
    }


}
