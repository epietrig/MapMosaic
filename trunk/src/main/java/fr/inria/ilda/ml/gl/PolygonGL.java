/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.gl;

import com.jogamp.opengl.GL;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.operation.buffer.BufferOp;
import fr.inria.ilda.ml.GLUtilities;
import fr.inria.ilda.ml.MapLayerViewer;
import fr.inria.ilda.ml.Texture;
import poly2Tri.Triangulation;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by mjlobo on 08/03/15.
 */
public class PolygonGL extends ShapeGL {
    BufferGL vertexBufferGL;
    BufferGL textureBufferGL;
    BufferGL indexesBufferGL;
    float[] texCoords;
    protected Texture texture;
    protected boolean isComplete;
    protected Geometry parentGeometry;
    protected int textureID;
    FloatBuffer texBuffer;
    protected FloatBuffer blurredTexCoords;
    protected Texture blurredTexture;
    Geometry geometry;
    boolean isHole = false;


    //float z=0.0f;


    public PolygonGL(List<double[]> coordinates, ArrayList<Integer> indexes) {
       this(coordinates, indexes, null , 0.0f);
    }

    public PolygonGL(List<double[]> coordinates) {
       this(coordinates, 0.0f);
    }

    public PolygonGL(List<double[]> coordinates, float z) {
        this(coordinates, null ,z);

    }

    public PolygonGL(Geometry parentGeometry) {
        this.parentGeometry = parentGeometry;
        coordinates.clear();


    }

    public PolygonGL(List<double[]> coordinates, ArrayList<Integer> indexes, Texture texture) {
        this(coordinates, indexes, texture, 0.0f);
    }

    public PolygonGL(List<double[]> coordinates, ArrayList<Integer> indexes, Texture texture, float z) {
        this.z = z;
        this.texture = texture;
        this.coordinates = (List<double[]>)((ArrayList<double[]>)coordinates).clone();
        this.indexes = indexes;
        buffers = new Hashtable<>();
        updateBuffers();
        //coordinatesSize = coordinates.size();
    }

    public PolygonGL(List<double[]> coordinates, Texture texture, float z) {
        this.z = z;
        this.texture = texture;
        //this.coordinates = coordinates;
        this.coordinates = new ArrayList<>();
        for (double[] coord : coordinates) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        //this.coordinates.addAll(coordinates);
        buffers = new Hashtable<>();
        indexes = new ArrayList<>();
        drawCompletePolygon();
        //coordinatesSize = coordinates.size();
        updateBuffers();

    }

    public PolygonGL(List<double[]> coordinates, Texture texture) {
        this(coordinates, texture, 0.0f);
    }

    public PolygonGL(PolygonGL polygonGL) {
        coordinates = new ArrayList<>();
        indexes = new ArrayList<>();
        for (double[] coord : polygonGL.getCoordinates()) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        //System.out.println("polygonGL.getIndexes() "+polygonGL.getIndexes().size());
        for (Integer index : polygonGL.getIndexes()) {
            indexes.add(index);
        }
        //System.out.println("indexes in new polygon "+indexes.size());
        //coordinates.addAll(polygonGL.getCoordinates());
        buffers = new Hashtable<>();
        //drawCompletePolygon();
        //indexes = new ArrayList<>();
        //indexes.addAll(polygonGL.getIndexes());
        offseted = polygonGL.getOfseted();
        onlyInFirst = polygonGL.getOnlyInFirst();
        this.texture = polygonGL.getTexture();
        this.isHole = polygonGL.getIsHole();
        updateBuffers();
    }




    public void updateBuffers() {
        if (buffers.size() == 0) {
            vertexBufferGL = new BufferGL(GL.GL_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.VERTEX, BufferGL.DataType.FLOAT, 3);
            textureBufferGL = new BufferGL(GL.GL_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.TEXTURE, BufferGL.DataType.FLOAT, 2);
            indexesBufferGL = new BufferGL(GL.GL_ELEMENT_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.INDEX, BufferGL.DataType.INT, 1);
        }
        buffers.put(vertexBufferGL, getVertices());
        updateTextureBuffer();
        buffers.put(indexesBufferGL, getIndex());
    }

    public void updateTextureBuffer() {
        if (blurredTexture != null) {
            //buffers.put(textureBufferGL, getTextureCoords());
            //System.out.println("blurredTexture != null!");
            if (blurredTexCoords == null) {
                calculateTextureCoords(blurredTexture);
                buffers.put(textureBufferGL, getBlurredTexCoords());
            }

        } else {
            if (texBuffer == null) {
                buffers.put(textureBufferGL, getTextureCoords());
            }
            else {
                buffers.put(textureBufferGL, texBuffer);
            }
        }
    }

    public FloatBuffer getTextureCoords() {
//        float X=MapLayerViewer.LOWER_CORNER_X;
//        float Y=MapLayerViewer.LOWER_CORNER_Y;

        float X=(float)texture.getLowerCorner()[0];
        float Y=(float)texture.getLowerCorner()[1];
        //System.out.println("X in getTextureCoords "+ X);
        //System.out.println("Y in getTextureCoords "+ Y);
        //System.out.println("Texture width "+ texture.getWidth());
        //System.out.println("Texture Height "+ texture.getHeight());
        texCoords = new float[2*vertices.length/3];
        int j=0;
        //System.out.println("dimension en gettexturecoords "+texture.getWidth());
        for(int i=0; i<texCoords.length; i++){
            //System.out.println("vertices[j] "+vertices[j]);
            //System.out.println("vertices[j+1] "+vertices[j+1]);
            texCoords[i] = (float)((vertices[j]-X)/texture.getWidth());
            texCoords[i+1]=(float)((vertices[j+1]-Y)/texture.getHeight());
            i++;
            j+=3;
        }
        //float[] texCoords = {(720-vertices[0])/10,(1625-vertices[1])/10,(720-vertices[2])/10,(1625-vertices[3])/10,(720-vertices[4])/10,(1625-vertices[5])/10};
        FloatBuffer texBuffer = FloatBuffer.wrap(texCoords);
        return texBuffer;

    }

    public FloatBuffer getBlurredTexCoords() {
        return blurredTexCoords;
    }

    public void calculateTextureCoords(Texture texture) {
        float X=(float)texture.getLowerCorner()[0];
        float Y=(float)texture.getLowerCorner()[1];
        texCoords = new float[2*vertices.length/3];
        int j=0;
        //System.out.println("dimension en gettexturecoords "+texture.getWidth());
        for(int i=0; i<texCoords.length; i++){
            texCoords[i] = (float)((vertices[j]-X)/texture.getWidth());
            texCoords[i+1]=(float)((vertices[j+1]-Y)/texture.getHeight());
            i++;
            j+=3;
        }
        blurredTexCoords= FloatBuffer.wrap(texCoords);
    }

    public void setTextureID(int textureID) {
        this.textureID = textureID;
    }

    public int getTextureID() {
        return textureID;
    }

    public void calculatePolygon(){

        if (!isCCW()) {
            //System.out.println("reverse order!");
            Collections.reverse(coordinates);
        }
        //System.out.println("coordinates.size()-1 " + coordinates.get(coordinates.size()-1));
        coordinates.remove(coordinates.size()-1);
        double [][] verticesArray = new double[coordinates.size()][2];
        for (int i=0; i<coordinates.size(); i++){
            verticesArray[i][0] = coordinates.get(i)[0];
            verticesArray[i][1] = coordinates.get(i)[1];
            //System.out.println("Coordinate in Geo element x: "+verticesArray[i][0]+" y:"+verticesArray[i][1]);
        }
        ArrayList triangles = new ArrayList();
        int [] numVerticesinContures = new int []{coordinates.size()};
        Triangulation.debug =true;
        //System.out.println("Coordinates size in GeoElement "+coordinates.size());
        try {
            triangles = Triangulation.triangulate(1, numVerticesinContures, verticesArray);
        }
        catch(Exception e) {
            e.printStackTrace();
            //System.out.println("problem! ");
            printCoordinates();
        }
        ArrayList t;
        indexes = new ArrayList<Integer>();
        for (int i=0; i<triangles.size(); i++){
            t = (ArrayList)triangles.get(i);
            for (int j=0; j<3; j++) {
                indexes.add((int)((ArrayList)t).get(j));
            }
        }
        //System.out.println("indexes length "+indexes.size());



    }

    public void drawCompletePolygon(){
        //System.out.println("coordinates " + coordinates);
        //System.out.println("coordinates size in draw complete polygon" + coordinates.size());

        Coordinate[] coordsJTS = new Coordinate[coordinates.size()+1];
        ArrayList <Coordinate> intersections = new ArrayList<>();
        for(int i=0; i<coordinates.size();i++) {
            coordsJTS[i] = new Coordinate(coordinates.get(i)[0], coordinates.get(i)[1]);
        }
        coordsJTS[coordsJTS.length-1] = new Coordinate(coordsJTS[0].x, coordsJTS[0].y);
        GeometryFactory geometryFactory = new GeometryFactory();
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(coordsJTS);
        Polygon polygon = new Polygon(new LinearRing(coordinateArraySequence,geometryFactory),null,geometryFactory);
        if (!polygon.isSimple()) {
            BufferOp bufferOp = new BufferOp(polygon);
            Geometry simplified = bufferOp.getResultGeometry(0);
           //Geometry simplified = polygon.convexHull();
            coordinates.clear();
            for (Coordinate coord : simplified.getCoordinates()) {
                coordinates.add(new double[]{coord.x, coord.y});
            }
            coordinates.remove(coordinates.size()-1);


        }
        calculatePolygon();
        isComplete = true;
    }

    public void calculateGeometry () {
        GeometryFactory geometryFactory = new GeometryFactory();
        Coordinate[] coordinatesArray = new Coordinate[coordinates.size()+1];
        for (int i=0; i<coordinatesArray.length-1; i++) {
            coordinatesArray[i] = new Coordinate(coordinates.get(i)[0], coordinates.get(i)[1]);
        }
        coordinatesArray[coordinatesArray.length-1]=coordinatesArray[0];
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(coordinatesArray);
        LinearRing linearRing = new LinearRing(coordinateArraySequence, geometryFactory);
        geometry = new Polygon(linearRing, null, geometryFactory);
        geometry.buffer(0);
        //return geometry;
    }



    public boolean getIsComplete(){
        return isComplete;
    }

    public void setIsComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public boolean isDrawable () {
        if(coordinates.size()<3) {
            return false;
        }
        else {
            return true;
        }

    }

    public void offsetCoordinates() {
        super.offsetCoordinates();
        //coordinatesSize = coordinates.size();
        texCoords = new float[0];
        updateBuffers();
        //getTextureCoords();
    }



    public GLUtilities.RenderedElementTypes getType() {
        return GLUtilities.RenderedElementTypes.TEXTURED;
    }


    public void addBuffer (int target, int usage, BufferGL.Type type, BufferGL.DataType datatype, int size, Buffer buffer) {
        BufferGL bufferGL = new BufferGL(target, usage, type, datatype, size);
        buffers.put(bufferGL,buffer);
    }

    public void updateBuffer(BufferGL bufferGL, Buffer buffer) {
        buffers.remove(bufferGL);
        buffers.put(bufferGL, buffer);
    }

//    public void printCoordinates() {
//        System.out.println("------------------------Coordinates-----------------------");
//        for (double[] coord: coordinates) {
//            System.out.println("X " + coord[0] + " Y " +coord[1]);
//        }
//    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public Texture getTexture() {
        return texture;
    }

    public void refreshCoordinates(List<double[]> coordinates) {
        //printCoordinates();
        super.refreshCoordinates();
        //this.coordinates = coordinates;
        this.coordinates.clear();
        for (double[] coord : coordinates) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }

        //buffers = new Hashtable<>();
        //indexes = new ArrayList<>();
        drawCompletePolygon();
        //coordinatesSize = coordinates.size();
        //blurredTexture = null;
        updateBuffers();
        //printCoordinates();
    }



    public void setTexCoords(ArrayList<float[]> texList) {
        texCoords = new float[texList.size()*2];
        int j=0;
        for (int i=0; i<texCoords.length; i++) {
            texCoords[i] = texList.get(j)[0];
            texCoords[i+1]=texList.get(j)[1];
            i++;
            j++;
        }
        texBuffer = FloatBuffer.wrap(texCoords);

    }

    public Texture getBlurredTexture() {
        return blurredTexture;
    }

    public void setBlurredTexture(Texture blurredTexture) {
        this.blurredTexture = blurredTexture;
        blurredTexCoords = null;
        //System.out.println("update bufefrs from setBlurredTExture");
        //updateBuffers();
       // System.out.println("update bufefrs from setBlurredTExture end");
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setIsHole(boolean isHole) {
        this.isHole = isHole;
    }

    public boolean getIsHole() {
        return isHole;
    }


}
