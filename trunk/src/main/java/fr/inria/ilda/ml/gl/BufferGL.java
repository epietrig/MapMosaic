/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.gl;

/**
 * Created by mjlobo on 27/02/15.
 */
public class BufferGL {
    public static enum Type  {VERTEX,TEXTURE,INDEX,NORMALS,ALPHA};
    public static enum DataType {FLOAT, INT}
    int target;
    int buffer=-1;
    int usage;
    int size;
    Type type;
    DataType dataType;
    int shaderAttributeId=-1;

    public BufferGL (int target, int usage, Type type, DataType dataType, int size) {
        this.target = target;
        this.usage = usage;
        this.type = type;
        this.dataType = dataType;
        this.size = size;
    }

    public int getTarget() {
        return target;
    }

    public int getBuffer() {
        return buffer;
    }

    public int getUsage() {
        return usage;
    }

    public Type getType() {
        return type;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setBuffer(int buffer) {
        this.buffer = buffer;
    }

    public int getShaderAttributeId() {
        return shaderAttributeId;
    }

    public void setShaderAttributeId(int shaderAttributeId) {
        this.shaderAttributeId = shaderAttributeId;
    }

    public int getSize() {
        return size;
    }

}
