/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import fr.inria.ilda.ml.canvasEvents.CanvasDrawnEvent;
import fr.inria.ilda.ml.canvasEvents.CanvasEvent;

import javax.swing.*;

/**
 * Created by mjlobo on 09/08/15.
 */
public class BufferSelectionWorker {
    double bufferOffset;
    Layer selectedLayer;
    MapGLCanvas mapGLCanvas;
    SwingWorker bufferSelectionCalculator;
    VectorSelection vectorSelection;
    boolean isFirst;
    SelectionManager selectionManager;
    double bufferOffsetPx;
    boolean isBackground;

    public BufferSelectionWorker(VectorSelection vectorSelection, double bufferOffset, MapGLCanvas mapGLCanvas, Layer selectedLayer, double bufferOffsetPx, SelectionManager selectionManager, boolean isBackground) {
        this.vectorSelection = vectorSelection;
        this.bufferOffset = bufferOffset;
        this.mapGLCanvas = mapGLCanvas;
        this.selectedLayer = selectedLayer;
        bufferSelectionCalculator = new BufferSelectionCalculator();
        this.selectionManager = selectionManager;
        this.bufferOffsetPx = bufferOffsetPx;
        this.isBackground = isBackground;
    }

    public void startWorker() {
        bufferSelectionCalculator.execute();
    }

    class BufferSelectionCalculator extends SwingWorker<VectorSelection, String> {

        @Override
        protected VectorSelection doInBackground() throws Exception {
            isFirst=vectorSelection.bufferSelectedElements(bufferOffset);
            return vectorSelection;
        }

        protected void done() {
            if (isFirst) {
                for (GeoElement ge: vectorSelection.getSelectedElementsBuffered()) {
                    mapGLCanvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.GEOELEMENT_CREATED, ge,selectedLayer));
                }
            }
            else {
                for (GeoElement ge: vectorSelection.getSelectedElementsBuffered()) {
                    mapGLCanvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.ELEMENT_MODIFIED, ge,selectedLayer));
                }
            }
            vectorSelection.setSelectedElements(vectorSelection.getSelectedElementsBuffered());
            selectionManager.vectorSelectionBuffered(vectorSelection, bufferOffsetPx, isBackground);
        }

    }

}


