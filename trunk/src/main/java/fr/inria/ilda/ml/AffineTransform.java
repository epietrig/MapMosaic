/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/



package fr.inria.ilda.ml;

import com.vividsolutions.jts.geom.Coordinate;

import javax.vecmath.Matrix3d;
import javax.vecmath.Vector2d;


/**
 * Created by mjlobo on 15/06/15.
 */
public class AffineTransform {

    Vector2d p1;
    Vector2d p2;
    Vector2d p3;
    Vector2d v1;
    Vector2d v2;
    Vector2d v3;
    double[] matrixTransform;


    public AffineTransform (Vector2d p1, Vector2d p2, Vector2d p3, Vector2d v1, Vector2d v2, Vector2d v3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        calculateTransformMatrix();
    }


    public void calculateTransformMatrix () {
        Matrix3d originalPoints = new Matrix3d(p1.x, p2.x, p3.x,p1.y,p2.y,p3.y,1,1,1);
        System.out.println("Original Points before invert" + originalPoints);
        originalPoints.invert();
        System.out.println("Original Points " + originalPoints);
        double m00 = originalPoints.m00*v1.x+originalPoints.m10*v2.x+originalPoints.m20*v3.x;
        double m01 = originalPoints.m01*v1.x+originalPoints.m11*v2.x+originalPoints.m21*v3.x;
        double m02 = originalPoints.m02*v1.x+originalPoints.m12*v2.x+originalPoints.m22*v3.x;
        double m10 = originalPoints.m00*v1.y+originalPoints.m10*v2.y+originalPoints.m20*v3.y;
        double m11 = originalPoints.m01*v1.y+originalPoints.m11*v2.y+originalPoints.m21*v3.y;
        double m12 = originalPoints.m02*v1.y+originalPoints.m12*v2.y+originalPoints.m22*v3.y;

        matrixTransform = new double [] {m00, m01, m02, m10, m11, m12};
    }

    public double[] calculatePoint(double[] point) {
        double[] result = new double[2];
        result[0] = point[0] *matrixTransform[0] + point[1]*matrixTransform[1] + matrixTransform[2];
        result[1] = point[0] * matrixTransform[3] + point[1] * matrixTransform[4] + matrixTransform[5];
        return result;

    }

    public Coordinate calculateCoordinate(Coordinate coordinate) {
        double x = coordinate.x *matrixTransform[0] + coordinate.y*matrixTransform[1] + matrixTransform[2];
        double y = coordinate.y * matrixTransform[3] + coordinate.y * matrixTransform[4] + matrixTransform[5];
        return new Coordinate(x, y, coordinate.z);
    }


}
