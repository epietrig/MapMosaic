/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.queryWidgets;

import fr.inria.ilda.ml.*;
import fr.inria.ilda.ml.StateMachine.StateMachine;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 22/06/15.
 */
public class ShapeFileTableSummary extends JPanel {
    List<QueryWidget> widgets;
    VectorLayer layer;

    public ShapeFileTableSummary(VectorLayer layer) {
        super();
        widgets = new ArrayList<>();
        this.layer = layer;
        setName(layer.getName());
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        for (ShapeFileAttribute attribute:layer.getAttributes()) {

            if (attribute instanceof ShapeFileNumericAttribute) {
                attribute.buildQueryComponent(layer.getLongestLocalName().length());
                if (((ShapeFileNumericAttribute)attribute).getValues().size()>1) {
                    //System.out.println("query widget "+attribute.getQueryWidget());
                    QueryWidget queryWidget = attribute.getQueryWidget();
                    add(queryWidget);
                    widgets.add(queryWidget);
                    //queryWidget.attachToStateMachine(stateMachine);
                    //queryWidget.addListeners(stateMachine);
                }
            }
        }
        for (ShapeFileAttribute attribute:layer.getAttributes()) {
            if (attribute instanceof ShapeFileStringAttribute) {
                attribute.buildQueryComponent(layer.getLongestLocalName().length());
                if (((ShapeFileStringAttribute)attribute).getValues().size()>1) {
                    QueryWidget queryWidget = attribute.getQueryWidget();
                    add(queryWidget);
                    widgets.add(queryWidget);
                    //queryWidget.attachToStateMachine(stateMachine);
                    //queryWidget.addListeners(stateMachine);
                }
            }
        }

        //this.add(layer.getName(), new JScrollPane(panel));

    }

    public void addListeners(StateMachine stateMachine) {
        for (QueryWidget widget: widgets) {
            widget.addListeners(stateMachine);
        }
    }

    public void update(VectorSelection vectorSelection) {
        //System.out.println("update shape file table summary!");
        for (QueryFilter filter:vectorSelection.getFilters()) {
            if (filter instanceof NumericQueryFilter) {
                NumericQueryFilter numericQueryFilter = (NumericQueryFilter)filter;
                ((NumericQuery)numericQueryFilter.getAttribute().getQueryWidget()).setValues(numericQueryFilter.getMinValue(), numericQueryFilter.getMaxValue());
            }
            else if (filter instanceof StringQueryFilter) {
                //System.out.println("update string query filter ");
                StringQueryFilter stringQueryFilter = (StringQueryFilter)filter;
                //((TextQuery)stringQueryFilter.getAttribute().getQueryWidget()).resetValues();
                ((TextQuery)stringQueryFilter.getAttribute().getQueryWidget()).setValues(stringQueryFilter.getStrings());
            }
        }

        if (vectorSelection.getFilters().size()==0) {
            reset();
        }
    }

    public void update(ShapeFileFeature shapeFileFeature) {
        for (ShapeFileNumericAttribute shapeFileNumericAttribute: shapeFileFeature.getNumericAttributeHashMap().keySet()) {
            ((NumericQuery)shapeFileNumericAttribute.getQueryWidget()).setValues(shapeFileFeature.getNumericAttributeHashMap().get(shapeFileNumericAttribute));
        }

        for (ShapeFileStringAttribute shapeFileStringAttribute: shapeFileFeature.getStringAttributeHashMap().keySet()) {
            ((TextQuery) shapeFileStringAttribute.getQueryWidget()).setValues(shapeFileFeature.getShapeFileStringAttributeValue(shapeFileStringAttribute));
        }
    }

    public void reset() {
        //System.out.println("layer instance of " + layer.getClass());
        for (ShapeFileAttribute attribute:layer.getAttributes()) {
            attribute.getQueryWidget().resetValues();
            attribute.getQueryWidget().setEnabled(true);
        }
    }

    public Layer getLayer() {
        return layer;
    }

    public void setEnabled (boolean enabled) {
        for (ShapeFileAttribute attribute: layer.getAttributes()) {
            attribute.getQueryWidget().setEnabled(true);
        }
    }



}
