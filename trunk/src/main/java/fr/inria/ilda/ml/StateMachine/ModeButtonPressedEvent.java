/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.StateMachine;

import fr.inria.ilda.ml.ModeToolBar;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by mjlobo on 30/07/15.
 */
public class ModeButtonPressedEvent extends Event {
    JButton target;
    String mode;
    ModeToolBar modeToolBar;

    public ModeButtonPressedEvent(JButton target, String mode, ModeToolBar modeToolBar) {
        this.target = target;
        this.mode = mode;
        this.modeToolBar = modeToolBar;
    }

    public ModeButtonPressedEvent(JButton target, String mode) {
        this.target = target;
        this.mode = mode;
    }

    private static ModeButtonPressedEventListener listener = new ModeButtonPressedEventListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof JButton ) {
            listener.attachTo( stateMachine, (JButton)target );
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof JButton) {
            listener.detachFrom( stateMachine, (JButton)target );
        }
    }



    public JComponent getTarget() {
        return target;
    }

    public String getMode() {return mode;}

    public ModeToolBar getModeToolBar() {return modeToolBar;}
}

class ModeButtonPressedEventListener implements ActionListener {
    private HashMap<Component, HashSet<StateMachine>> listeners;
    public ModeButtonPressedEventListener() {
        listeners = new HashMap<Component, HashSet<StateMachine>>();
    }

    public void attachTo( StateMachine stateMachine, JButton target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            // Target was not found, start listening to it
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            target.addActionListener(this);
            //System.out.println("Added MouseMoveListener");
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, JButton target ) {
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                target.removeActionListener(this);
                //System.out.println("Removed MouseMoveListener");
            }
        }
    }


    @Override
    public void actionPerformed(ActionEvent event) {
        JButton target = (JButton)event.getSource();
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            // Create custom event
            ModeButtonPressedEvent customEvent = new ModeButtonPressedEvent(target, event.getActionCommand());
            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
        }
    }
}


