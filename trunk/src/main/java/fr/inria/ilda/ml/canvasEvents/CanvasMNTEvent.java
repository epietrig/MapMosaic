/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.canvasEvents;

import fr.inria.ilda.ml.Filters.TextureFilter;
import fr.inria.ilda.ml.Layer;
import fr.inria.ilda.ml.MNTRasterGeoElement;

/**
 * Created by mjlobo on 22/07/15.
 */
public class CanvasMNTEvent extends CanvasEvent{

    MNTRasterGeoElement mntRasterGeoElement;
    Layer layer;
    TextureFilter textureFilter;
    public CanvasMNTEvent(String event, MNTRasterGeoElement mntRasterGeoElement, Layer layer) {
        super(event);
        this.mntRasterGeoElement = mntRasterGeoElement;
        this.layer = layer;
        this.textureFilter = textureFilter;
    }

    public MNTRasterGeoElement getMntRasterGeoElement() {
        return mntRasterGeoElement;
    }

    public Layer getLayer() {
        return layer;
    }

    public TextureFilter getTextureFilter() {
        return textureFilter;
    }
}
