/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.canvasEvents;

/**
 * Created by mjlobo on 01/07/15.
 */
public abstract class CanvasEvent {

    public static final String POINT_ADDED = "pointAdded";
    public static final  String DRAWN_FINISHED = "drawnFinished";
    public static final  String DRAWN_CREATED = "drawnCreated";
    public static final  String DRAWN_MOVED = "drawnMoved";
    public static final String ELEMENT_MODIFIED = "elementModified";
    public static final String DRAWN_MOVED_CHANGE = "drawnMovedChange";
    public static final String VECTOR_SELECTION_UPDATED = "vectorSelectionUpdated";
    public static final String POINT_ALIGN_ADDED = "pointAlignAdded";
    public static final String NEW_MNT_RASTERGEOELEMENT = "newMNTRasterGeoELement";
    public static final String TRANSLUCENT_VECTOR_SELECTION_CHANGED = "textureVectorSelectionChanged";
    public static final String TRANSLUCENT_MOVED = "translucentMoved";
    public static final String TRANSLUCENT_BORDER_ENABLED = "translucentBorderEnabled";
    public static final String TRANSLUCENT_LAYER_ENABLED = "translucentLayerEnabled";
    public static final String GEOELEMENT_CREATED = "geoElementCreated";
    public static final String MNT_UPDATED = "mntUpdated";

    String event;

    public  String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public CanvasEvent (String event) {
        this.event = event;
    }

}
