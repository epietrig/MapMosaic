/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.gl;

//import javax.media.opengl.GL;

import com.jogamp.opengl.GL;
import fr.inria.ilda.ml.GLUtilities;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by mjlobo on 05/03/15.
 */
public class LineGL extends ShapeGL {

    protected ArrayList<float[]> normals;
    protected float[] normalsA = new float[0];
    List<double[]> parentCoordinates;

    BufferGL vertexBufferGL;
    BufferGL normalsBufferGL;
    BufferGL indexesBufferGL;

    float lineWidth = 3.0f;


    public LineGL (List<double[]> parentCoordinates) {
       this(parentCoordinates, 3.0f);
    }

    public LineGL (List<double[]> parentCoordinates, float lineWidth) {
        buffers = new Hashtable<>();
        coordinates = new ArrayList<>();
        normals = new ArrayList<>();
        indexes = new ArrayList<>();
        this.parentCoordinates = parentCoordinates;
        calculateCoordinates();
        //coordinatesSize = coordinates.size();
        //System.out.println("Coordinates "+coordinates);
        updateBuffers();
        this.lineWidth = lineWidth;
    }


    public LineGL() {
        parentCoordinates = new ArrayList<>();
        buffers = new Hashtable<>();
        coordinates = new ArrayList<>();
        normals = new ArrayList<>();
        indexes = new ArrayList<>();
    }

    public FloatBuffer getNormals() {
        if (normals.size() != normalsA.length) {
            normalsA = new float[normals.size()*2];
            int normCount = 0;
            for (int i=0; i<normals.size();i++){
                normalsA[normCount] = normals.get(i)[0];
                normalsA[normCount+1] = normals.get(i)[1];
                normCount +=2;
            }
        }

        FloatBuffer normBuffer = FloatBuffer.wrap(normalsA);
        return normBuffer;
    }

    public void updateBuffers() {
        if (buffers.size() == 0) {
            vertexBufferGL = new BufferGL(GL.GL_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.VERTEX, BufferGL.DataType.FLOAT, 3);
            normalsBufferGL = new BufferGL(GL.GL_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.NORMALS, BufferGL.DataType.FLOAT, 2);
            indexesBufferGL = new BufferGL(GL.GL_ELEMENT_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.INDEX, BufferGL.DataType.INT, 1);
        }
        buffers.put(vertexBufferGL, getVertices());
        buffers.put(normalsBufferGL, getNormals());
        buffers.put(indexesBufferGL, getIndex());
    }


    protected void calculateCoordinates() {
        //System.out.println("Here in calculate coordinates, bordergeoelement");
        int n = 0;
//        double dx = parentCoordinates.get(parentCoordinates.size()-1)[0]-parentCoordinates.get()
        if(parentCoordinates.get(parentCoordinates.size()-1)!=parentCoordinates.get(0)) {
            parentCoordinates.add(parentCoordinates.get(0));
        }
        for (int i = 0; i < parentCoordinates.size()-1; i++) {
            double dx = parentCoordinates.get(i + 1)[0] - parentCoordinates.get(i)[0];
            double dy = parentCoordinates.get(i + 1)[1] - parentCoordinates.get(i)[1];
            for (int k = 0; k < 2; k++) {
                for (int j = -1; j <= 1; j += 2) {
                    coordinates.add(parentCoordinates.get(i + k));
                    normals.add(new float[]{(float)(-j * dy), (float)(j * dx)});
                }
            }
            indexes.add(i + n * 3);
            indexes.add(i + 1 + n * 3);
            indexes.add(i + 2 + n * 3);
            indexes.add(i + 1 + n * 3);
            indexes.add(i + 3 + n * 3);
            indexes.add(i + 2 + n * 3);
            n++;
        }
        parentCoordinates.remove(parentCoordinates.get(parentCoordinates.size()-1));

    }

    public void addPointToList(double[] point) {
        parentCoordinates.add(point);
        int n = (parentCoordinates.size()-1)*4;
        if (parentCoordinates.size()>1) {
            double dx = parentCoordinates.get(parentCoordinates.size()-1)[0]-parentCoordinates.get(parentCoordinates.size()-2)[0];
            double dy = parentCoordinates.get(parentCoordinates.size()-1)[1]-parentCoordinates.get(parentCoordinates.size()-2)[1];
            int i = parentCoordinates.size()-2;
            for (int k = 0; k < 2; k++) {
                for (int j = -1; j <= 1; j += 2) {
                    coordinates.add(parentCoordinates.get(i + k));
                    normals.add(new float[]{(float)(-j * dy), (float)(j * dx)});
                }
            }

            indexes.add(n);
            indexes.add(n+1);
            indexes.add(n+2);
            indexes.add(n+1);
            indexes.add(n+3);
            indexes.add(n+2);
        }
        updateBuffers();

    }

    public void removeLastPoint() {
        //System.out.println(parentCoordinates.size());
        parentCoordinates.remove(parentCoordinates.size() - 1);
        for (int i = 0; i<4; i++) {
            coordinates.remove(coordinates.size()-1);
            normals.remove(normals.size()-1);
        }

        for (int i = 0; i<6; i++) {
            indexes.remove(indexes.size()-1);
        }


    }

    public void addPoint(double[] point) {
        removeLastPoint();
        addPointToList(point);
        addPointToList(parentCoordinates.get(0));
        //coordinatesSize = coordinates.size();

    }

    public void addFirstPoint(double[] point) {
        addPointToList(point);
        addPointToList(point);
        //coordinatesSize = coordinates.size();
    }

    public GLUtilities.RenderedElementTypes getType() {
        return GLUtilities.RenderedElementTypes.LINE;
    }

    public void offsetCoordinates() {
        super.offsetCoordinates();
        normalsA = new float[0];
        //coordinatesSize = coordinates.size();
        updateBuffers();
    }

    public float getLineWidth() {
        return lineWidth;
    }

    public List<double[]> getParentCoordinates() {
        return parentCoordinates;
    }



    public void refreshCoordinates(List<double[]> parentCoordinates) {
        super.refreshCoordinates();
        coordinates.clear();
        normals.clear();
        indexes.clear();
        this.parentCoordinates = parentCoordinates;
        calculateCoordinates();
        //coordinatesSize = coordinates.size();
        updateBuffers();
    }



}
