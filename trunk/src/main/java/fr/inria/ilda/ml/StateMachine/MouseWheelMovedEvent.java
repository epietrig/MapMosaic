/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.StateMachine;

import java.awt.*;
import java.awt.event.MouseWheelEvent;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by mjlobo on 22/05/15.
 */
public class MouseWheelMovedEvent extends Event{
    java.awt.Component target;
    int x;
    int y;
    int wheelRotation;

    public MouseWheelMovedEvent(int x, int y, int wheelRotation) {
        super();
        this.x = x;
        this.y = y;
        this.wheelRotation = wheelRotation;
    }

    public MouseWheelMovedEvent(java.awt.Component target, int x, int y, int wheelRotation) {
        super();
        source = this.target = target;
        this.x = x;
        this.y = y;
        this.wheelRotation = wheelRotation;
    }

    private static MouseWheelEventListener listener = new MouseWheelEventListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.attachTo( stateMachine, (java.awt.Component)target );
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.detachFrom( stateMachine, (java.awt.Component)target );
        }
    }

    public java.awt.Component getTarget() {
        return target;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWheelRotation () {
        return wheelRotation;
    }
}

class MouseWheelEventListener extends java.awt.event.MouseAdapter {

    private HashMap<Component, HashSet<StateMachine>> listeners;

    public MouseWheelEventListener() {
        listeners = new HashMap<Component, HashSet<StateMachine>>();
    }

    public void attachTo( StateMachine stateMachine, java.awt.Component target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            // Target was not found, start listening to it
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            target.addMouseWheelListener( this );
            //System.out.println("Added MouseWheelListener");
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, java.awt.Component target ) {
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                target.removeMouseListener( this );
                //System.out.println("Removed MouseWheelListener");
            }
        }
    }


    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        java.awt.Component target = (java.awt.Component)e.getSource();
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            // Create custom event
            MouseWheelMovedEvent customEvent = new MouseWheelMovedEvent(target, e.getX(), e.getY(), e.getWheelRotation());

            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
        } else {
            java.awt.Component parent = target.getParent();
            if (parent != null) {
                parent.dispatchEvent(e);
            }
        }
    }


}

