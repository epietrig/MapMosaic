/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import org.lwjgl.Sys;

import java.util.ArrayList;

/**
 * Created by mjlobo on 16/02/15.
 */
public class DrawnLayer extends Layer {

    ArrayList<GeoElement> elements;

    public DrawnLayer(boolean isVisible) {

        super(isVisible);
        elements = new ArrayList<GeoElement>();
    }
    @Override
    public ArrayList<GeoElement> getElements() {
        return elements;
    }

    public void addElement(GeoElement geoElement) {
        if(!isVisible) isVisible = true;
        elements.add(geoElement);
    }

    public void removeElement(GeoElement geoElement){
        elements.remove(geoElement);
    }
}
