/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;


import fr.inria.ilda.ml.StateMachine.ModeButtonPressedEvent;
import fr.inria.ilda.ml.StateMachine.StateMachine;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 30/07/15.
 */
public class ModeToolBar extends JToolBar{

    protected static Font FONT = new Font(null, Font.PLAIN, 16);
    List<String> modes;

    HashMap<String, JButton> modeButtons = new HashMap<>();
    public ModeToolBar(List<String> modes) {
        //super(new BorderLayout());
        setBorder(new EmptyBorder(2,0,2,0));
        this.modes = modes;
        addButtons();
        //setOpaque(false);
    }





    public void addButtons() {
        for (String mode : modes) {
            if (mode.equals(MapLayerViewer.SEPARATOR)) {
                addSeparator();
            }
            else {
                JButton button = null;
                File image = new File("icons/" + mode + ".png");
                if (image.exists()) {
                    button = new JButton(new ImageIcon(new ImageIcon("icons/" + mode + ".png").getImage().getScaledInstance(16, 16, Image.SCALE_SMOOTH)));
                    //button.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
                    button.setMargin(new Insets(3, 5, 3, 5));
                } else {
                    button = new JButton(mode);
                }
                button.setActionCommand(mode);
                button.setFont(FONT);
                add(button);
                modeButtons.put(mode, button);
                //addSeparator();
            }

        }


    }

    public void setButtonSelected (String selectedMode) {
        for (String mode: modeButtons.keySet()) {
            if (mode.equals(selectedMode)) {
                modeButtons.get(mode).setSelected(true);
            }
            else {
                modeButtons.get(mode).setSelected(false);
            }
        }
    }

    public void addListeners(final StateMachine stateMachine) {
        for (final JButton button: modeButtons.values()) {
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    stateMachine.handleEvent(new ModeButtonPressedEvent(button, button.getActionCommand(),ModeToolBar.this));
                }
            });
        }


    }

    public void disableAllButtonsButOne(String modeToKeep) {
        for (String mode: modeButtons.keySet()) {
            if (mode.equals(modeToKeep)) {
                modeButtons.get(mode).setEnabled(true);
            }
            else {
                modeButtons.get(mode).setEnabled(false);
            }
        }
    }

    public void enableAll() {
        for (String mode: modeButtons.keySet()) {
            modeButtons.get(mode).setEnabled(true);
        }
    }

}
