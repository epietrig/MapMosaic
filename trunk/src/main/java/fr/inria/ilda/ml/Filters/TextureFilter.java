/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.Filters;

import fr.inria.ilda.ml.FrameBufferObject;
import fr.inria.ilda.ml.gl.PolygonGL;
import fr.inria.ilda.ml.gl.ShaderProgram;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 22/07/15.
 */
public class TextureFilter {
    PolygonGL quad;
    private int vaoID;
    //    Texture texture;
//    FrameBufferObject in;
    FrameBufferObject frameBufferObject;
    ShaderProgram drawShader;
    //ShaderProgram outShader;

    public TextureFilter (int vaoID, FrameBufferObject frameBufferObject, ShaderProgram drawShader) {
        this.frameBufferObject = frameBufferObject;
        this.vaoID = vaoID;
        this.drawShader = drawShader;
        //this.outShader = outShader;
    }

    public void calculateQuad (double[] lowerCorner, double width, double height) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{lowerCorner[0] + width, lowerCorner[1] + height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]+height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]});
        coordinates.add(new double[]{lowerCorner[0]+width, lowerCorner[1]});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad = new PolygonGL(coordinates, indexes, frameBufferObject.getTexture());


    }

    public void refreshQuadCoordinates (double[] lowerCorner, double width, double height) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{lowerCorner[0] + width, lowerCorner[1] + height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]+height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]});
        coordinates.add(new double[]{lowerCorner[0]+width, lowerCorner[1]});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad.setCoordinates(coordinates);
        quad.setIndexes(indexes);
        quad.refreshCoordinates();
        quad.updateBuffers();

//        FloatBuffer texBuff = quad.getTextureCoords();
//        System.out.println("************************************************************");
//        for (int i =0; i<texBuff.limit(); i++) {
//            System.out.println("Tex coord: "+texBuff.get(i));
//        }
//        quad.printCoordinates();


    }

    public PolygonGL getQuad() {
        return quad;
    }

    public FrameBufferObject getFrameBufferObject() {
        return frameBufferObject;
    }

    public int getVaoID() {
        return vaoID;
    }
}
