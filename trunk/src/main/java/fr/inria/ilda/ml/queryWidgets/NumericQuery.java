/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.queryWidgets;


import fr.inria.ilda.ml.StateMachine.FreeQueryEvent;
import fr.inria.ilda.ml.StateMachine.NumericQueryEvent;
import fr.inria.ilda.ml.StateMachine.StateMachine;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

public class NumericQuery extends QueryWidget {
	
	protected static Font FONT = new Font("Courier", Font.PLAIN, 12);
	protected static Font QUERY_FONT = new Font("Courier", Font.PLAIN, 10);
	
	protected AdaptiveSpinner spinnerLowerBound;
	protected AdaptiveSpinner spinnerUpperBound;
	protected JTextField textFieldFreeQuery;
	protected String typeName;
	
	protected JLabel queryLabel;

	protected double lowerBound;
	protected double upperBound;

	String nameforStateMachine = "numericQuery";
	int componentsToAttach = 2;
	static int currentIndex =0;
	
	public NumericQuery(String typeName, LinkedList<Double> values, int sizeLabel) {
		super();
		this.typeName = typeName;
		int extraSpaces = sizeLabel - this.typeName.length();
		String extraSpace = "";
		for (int i = 0; i < extraSpaces; i++) {
			extraSpace += " ";
		}
		
		AdaptiveSpinnerModel spinnerModelLowerBound = new AdaptiveSpinnerModel(values);
		this.spinnerLowerBound = new AdaptiveSpinner(spinnerModelLowerBound);
		this.spinnerLowerBound.setValue(spinnerModelLowerBound.getMinValue());
		AdaptiveSpinnerModel spinnerModelUpperBound = new AdaptiveSpinnerModel(values);
		this.spinnerUpperBound = new AdaptiveSpinner(spinnerModelUpperBound);
		this.spinnerUpperBound.setValue(spinnerModelLowerBound.getMaxValue());
		this.textFieldFreeQuery = new JTextField("                            ");
		GridBagConstraints gbc = new GridBagConstraints();
		setLayout(new GridBagLayout());
		gbc.fill=GridBagConstraints.NONE;
		gbc.anchor=GridBagConstraints.WEST;
		JLabel label = new JLabel(this.typeName+" >= "+extraSpace);
		label.setFont(FONT);
		label.setVerticalAlignment(JLabel.CENTER);
		Utils.buildConstraints(gbc, 0, 0, 1, 1, 0, 0);
		add(label, gbc);
		Utils.buildConstraints(gbc, 1, 0, 1, 1, 0, 0);
		add(this.spinnerLowerBound, gbc);
		label = new JLabel(" AND "+this.typeName+" <= "+extraSpace);
		label.setFont(FONT);
		label.setVerticalAlignment(JLabel.CENTER);
		Utils.buildConstraints(gbc, 2, 0, 1, 1, 0, 0);
		add(label, gbc);
		Utils.buildConstraints(gbc, 3, 0, 1, 1, 0, 0);
		add(this.spinnerUpperBound, gbc);
		label = new JLabel(" AND ");
		label.setFont(FONT);
		label.setVerticalAlignment(JLabel.CENTER);
		Utils.buildConstraints(gbc, 4, 0, 1, 1, 0, 0);
		add(label, gbc);
		gbc.fill=GridBagConstraints.HORIZONTAL;
		Utils.buildConstraints(gbc, 5, 0, 1, 1, 1, 0);
		add(this.textFieldFreeQuery, gbc);
		
		this.queryLabel = new JLabel(getQuery());
		this.queryLabel.setForeground(Color.DARK_GRAY);
		this.queryLabel.setFont(QUERY_FONT);
		Utils.buildConstraints(gbc, 0, 1, 6, 1, 1, 0);
		add(this.queryLabel, gbc);
		

		this.textFieldFreeQuery.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				NumericQuery.this.queryLabel.setText(getQuery());
			}
			public void insertUpdate(DocumentEvent e) {
				NumericQuery.this.queryLabel.setText(getQuery());
				System.out.println("insert update!");
			}
			public void changedUpdate(DocumentEvent e) {
				NumericQuery.this.queryLabel.setText(getQuery());
			}
		});

	}
	
	public String getQuery() {
		return this.typeName+" >= "+this.spinnerLowerBound.getValue()
				+" AND "+this.typeName+" <= "+this.spinnerUpperBound.getValue()
				+(this.textFieldFreeQuery.getText().trim().length() > 0 ? " AND ("+this.textFieldFreeQuery.getText().trim()+")": "");
	}
	
//	public static void main(String[] args) {
//		LinkedList<Double> values = new LinkedList<Double>();
//		values.add(0.234);
//		values.add(0.237);
//		values.add(4.234);
//		values.add(2.010);
////		AdaptiveSpinnerModel spinnerModel = new AdaptiveSpinnerModel(values);
////		AdaptiveSpinner adaptiveSpinner = new AdaptiveSpinner(spinnerModel);
//		NumericQuery nq = new NumericQuery("LARGEUR", values, 15);
//		JFrame frame = new JFrame();
////		frame.getContentPane().add(adaptiveSpinner);
//		frame.getContentPane().add(nq);
//		frame.pack();
//		frame.setVisible(true);
//		System.out.println(0.237-0.234);
//	}

	public void addListeners (final StateMachine stateMachine) {
		this.spinnerLowerBound.getModel().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				//NumericQuery.this.queryLabel.setText(getQuery());
				NumericQuery.this.queryLabel.setText(getQuery());
				stateMachine.handleEvent(new NumericQueryEvent(spinnerLowerBound.getValue(), spinnerUpperBound.getValue(),typeName));
				//System.out.println("lower bound changeeeed"+stateMachine.getStateName());

			}
		});
		this.spinnerUpperBound.getModel().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				//NumericQuery.this.queryLabel.setText(getQuery());
				//System.out.println("lower bound changeeeed");
				NumericQuery.this.queryLabel.setText(getQuery());
				stateMachine.handleEvent(new NumericQueryEvent(spinnerLowerBound.getValue(), spinnerUpperBound.getValue(),typeName));
			}
		});
		this.textFieldFreeQuery.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stateMachine.handleEvent(new FreeQueryEvent(textFieldFreeQuery.getText(),typeName));
			}
		});
	}

	public String getNameforStateMachine() {
		String name = nameforStateMachine+currentIndex;
		currentIndex++;
		return name;

	}

	public int getComponentsToAttach() {
		return componentsToAttach;
	}
	public void attachToStateMachine (StateMachine stateMachine) {
		stateMachine.setSource(getNameforStateMachine(),spinnerLowerBound);
		stateMachine.setSource(getNameforStateMachine(), spinnerUpperBound);
	}

	public AdaptiveSpinner getSpinnerLowerBound() {
		return spinnerLowerBound;
	}

	public AdaptiveSpinner getSpinnerUpperBound() {
		return spinnerUpperBound;

	}

	public String getTypeName() {
		return typeName;
	}

	public void setValues(double lowerBound, double upperBound) {
		setEnabled(true);
		this.spinnerLowerBound.setValue(lowerBound);
		this.spinnerUpperBound.setValue(upperBound);
	}

	public void setValues (double value) {
		setValues(value, value);
		setEnabled(false);
	}

	public void resetValues() {
		//System.out.println("min value in reset values "+spinnerLowerBound.getModel().getMinValue());
		//System.out.println("max value in reset values "+spinnerLowerBound.getModel().getMaxValue());
		this.spinnerLowerBound.setValue(spinnerLowerBound.getModel().getMinValue());
		this.spinnerUpperBound.setValue(spinnerUpperBound.getModel().getMaxValue());
		this.queryLabel.setText("");
	}

	public void setEnabled(boolean enabled) {
		spinnerLowerBound.setEnabled(enabled);
		spinnerUpperBound.setEnabled(enabled);
	}
 }
