/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;


import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.operation.buffer.BufferOp;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;
import fr.inria.ilda.ml.gl.LineStripGL;
import fr.inria.ilda.ml.gl.PolygonGL;

import javax.vecmath.Vector3f;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 13/02/15.
 */
public class VectorGeoElement extends GeoElement {

    float z=0.0f;


    List<PolygonGL> polygons;
    List<LineStripGL> lines;
    List<LineStripGL> selectionLines;
    List<LineStripGL> selectionLinesBis;
    List<double[]> coordinatesOffseted;
    Geometry offsetedGeometry;
    List<PolygonGL> polygonsOffseted;
    List<PolygonGL> originalPolygons;
    List<PolygonGL> inPolygons;
    protected boolean isPoint;
    double alphaRadius;
    PolygonGL selectedPolygon;
    boolean selected;




    public VectorGeoElement(Coordinate[] coords, Texture texture) {
        shapes = new ArrayList<>();
        polygonsOffseted = new ArrayList<>();
        originalPolygons = new ArrayList<>();
        lines = new ArrayList<>();
        selectionLines = new ArrayList<>();
        selectionLinesBis = new ArrayList<>();
        inPolygons = new ArrayList<>();

        generateOriginalPolygons(coords, texture);
        coordinatesOffseted = new ArrayList<double[]>();
        isDocked = true;

        calculateGeometry();
        for (LineStripGL line : lines) {
            line.setVisible(false);
            shapes.add(line);
        }
        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setVisible(false);
            shapes.add(selectionLine);
        }
        renderingType = GLUtilities.RenderedElementTypes.TEXTURED;
        fillColor = new Vector3f(MapLayerViewer.defaultFillColor.x, MapLayerViewer.defaultFillColor.y, MapLayerViewer.defaultFillColor.z);
        outlineColor = new Vector3f(MapLayerViewer.defaultOutlineLine.x, MapLayerViewer.defaultOutlineLine.y, MapLayerViewer.defaultOutlineLine.z);

    }

    public VectorGeoElement(Geometry geometry, Texture texture) {
        shapes = new ArrayList<>();
        polygonsOffseted = new ArrayList<>();
        originalPolygons = new ArrayList<>();
        inPolygons = new ArrayList<>();
        lines = new ArrayList<>();
        selectionLines = new ArrayList<>();
        selectionLinesBis = new ArrayList<>();
        generateOriginalPolygons(geometry.getCoordinates(), texture);
        coordinatesOffseted = new ArrayList<double[]>();
        this.geometry = geometry;
        for (LineStripGL line : lines) {
            line.setVisible(false);
            shapes.add(line);
        }
        for (LineStripGL selectionLineBis : selectionLinesBis) {
            selectionLineBis.setVisible(false);
            shapes.add(selectionLineBis);
        }
        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setVisible(false);
            shapes.add(selectionLine);
        }
        renderingType = GLUtilities.RenderedElementTypes.TEXTURED;
        fillColor = new Vector3f(MapLayerViewer.defaultFillColor.x, MapLayerViewer.defaultFillColor.y, MapLayerViewer.defaultFillColor.z);
        outlineColor = new Vector3f(MapLayerViewer.defaultOutlineLine.x, MapLayerViewer.defaultOutlineLine.y, MapLayerViewer.defaultOutlineLine.z);

    }

    public VectorGeoElement(Geometry geometry, Texture texture, double alphaRadius) {
        this(geometry, texture);
        this.alphaRadius = alphaRadius;
        calculatePolygonOffseted(alphaRadius, texture);
    }

    public VectorGeoElement (Coordinate[] coords, Texture texture, double alphaRadius) {
        this(coords, texture);
        this.alphaRadius = alphaRadius;
        calculatePolygonOffseted(alphaRadius, texture);

    }

    public VectorGeoElement(Geometry geometry, List<Geometry> holes, Texture texture, double alphaRadius) {
        this(geometry, texture);
        this.alphaRadius = alphaRadius;
        List<double[]> holeCoordinates = new ArrayList<>();
        calculatePolygonOffseted(alphaRadius, texture);
        for (Geometry hole: holes) {
            Pair<List<PolygonGL>, List<double[]>> holesPolygons = generatePolygons(hole.getCoordinates(), texture, true);
            for (PolygonGL holePolygon: holesPolygons.getValue1()) {
                polygons.add(holePolygon);
                originalPolygons.add(holePolygon);
                holePolygon.setOffseted(false);
                shapes.add(holePolygon);
                holePolygon.setIsHole(true);
            }
        }
        fillColor = new Vector3f(MapLayerViewer.defaultFillColor.x, MapLayerViewer.defaultFillColor.y, MapLayerViewer.defaultFillColor.z);
        outlineColor = new Vector3f(MapLayerViewer.defaultOutlineLine.x, MapLayerViewer.defaultOutlineLine.y, MapLayerViewer.defaultOutlineLine.z);

    }

    public VectorGeoElement (VectorGeoElement vectorGeoElement) {
        shapes = new ArrayList<>();
        polygonsOffseted = new ArrayList<>();
        originalPolygons = new ArrayList<>();
        lines = new ArrayList<>();
        selectionLines = new ArrayList<>();
        selectionLinesBis = new ArrayList<>();
        inPolygons = new ArrayList<>();
        generateOriginalPolygons(vectorGeoElement.getGeometry().getCoordinates(), vectorGeoElement.getTexture());
        coordinatesOffseted = new ArrayList<double[]>();
        this.geometry = (Geometry)vectorGeoElement.getGeometry().clone();
        for (LineStripGL line : lines) {
            line.setVisible(false);
            shapes.add(line);
        }
        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setVisible(false);
            shapes.add(selectionLine);
        }
        renderingType = GLUtilities.RenderedElementTypes.TEXTURED;
        fillColor = vectorGeoElement.getFillColor();
        //calculatePolygonOffseted(vectorGeoElement.alphaRadius, vectorGeoElement.getTexture());
    }


    public void setTexture(Texture texture) {
        for (PolygonGL original : originalPolygons) {
            original.setTexture(texture);
        }
        for (PolygonGL offseted: polygonsOffseted) {
            offseted.setTexture(texture);
        }
    }

    public Texture getTexture() {
        return polygons.get(0).getTexture();
    }

    public List<LineStripGL> getLines() {
        return lines;
    }

    public void select() {
        super.select();
        selected = true;
        setVisible(true);
        isIn = false;
        //line.setVisible(true);
        for (PolygonGL polygonGL: polygons) {
            polygonGL.setVisible(false);
        }
        for (PolygonGL polygonGL: inPolygons) {
            polygonGL.setVisible(false);
        }

        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setColor(MapLayerViewer.selectionColorIn);
            selectionLine.setVisible(true);
        }
        for (LineStripGL selectionLineBis : selectionLinesBis) {
            selectionLineBis.setColor(MapLayerViewer.selectionColorOut);
            selectionLineBis.setVisible(true);
        }


    }

    public void select(double x, double y) {
        super.select();
        setVisible(true);
        //line.setVisible(true);
        PolygonGL closerPolygon = getPolygonAt(x, y);
        int indexOfCloserPolygon = polygons.indexOf(closerPolygon);
        for (PolygonGL polygonGL: polygons) {
            polygonGL.setVisible(false);
        }
        for (PolygonGL polygonGL: inPolygons) {
            polygonGL.setVisible(false);
        }

        selectionLines.get(indexOfCloserPolygon).setColor(MapLayerViewer.selectionColor);
        selectionLines.get(indexOfCloserPolygon).setVisible(true);


    }

    public void deselect() {
        selected = false;
        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setColor(MapLayerViewer.inColorVector);
            selectionLine.setVisible(false);
        }
        for (LineStripGL selectionLineBis : selectionLinesBis) {
            selectionLineBis.setVisible(false);
        }
    }

    public void inElement() {
        isIn = true;
        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setVisible(true);
        }
    }

    public void outElement() {
        isIn = false;
        if (!selected) {
            for (LineStripGL selectionLine : selectionLines) {
                selectionLine.setVisible(false);
            }
        }
    }


    void generateOriginalPolygons(Coordinate[] coords, Texture texture) {
        Pair<List<PolygonGL>, List<double[]>> pair = generatePolygons(coords, texture, true);
        polygons = pair.getValue1();
        coordinates = pair.getValue2();
        PolygonGL inPolygon;
        for (PolygonGL polygon: polygons) {
            inPolygon = new PolygonGL(polygon);
            inPolygons.add(inPolygon);
            shapes.add(polygon);
            shapes.add(inPolygon);
            inPolygon.setVisible(false);
            polygon.setOnlyInFirst(true);
            originalPolygons.add(polygon);
        }

    }

    Pair<List<PolygonGL>, List<double[]>> generatePolygons (Coordinate[] coords, Texture texture, boolean addLines) {
        List<double[]> coordinates = new ArrayList<>();
        List<PolygonGL> polygons = new ArrayList<>();
        CoordinateList newCoordinates = new CoordinateList(coords,false);
        int indexContains;
        int startPolygon = 0;
        for (int k=0; k<newCoordinates.size();k++) {
            indexContains =
                    coordinatesContains(new double[]{newCoordinates.getCoordinate(k).x,newCoordinates.getCoordinate(k).y}, coordinates);
            if(indexContains==-1) {

                coordinates.add(new double[]{newCoordinates.getCoordinate(k).x, newCoordinates.getCoordinate(k).y});
            }
            else if (indexContains==startPolygon) {
                LineStripGL line = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()),MapLayerViewer.outlineLineWidth,outlineColor);
                LineStripGL selectionLine = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), MapLayerViewer.selectionLineWidth, MapLayerViewer.inColorVector);
                LineStripGL selectionLineBis = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), MapLayerViewer.selectionOutLineWidth, MapLayerViewer.selectionColorOut);
                if (addLines) {
                    selectionLinesBis.add(selectionLineBis);
                    lines.add(line);
                    selectionLines.add(selectionLine);
                }
                PolygonGL polygon = new PolygonGL(coordinates.subList(startPolygon,coordinates.size()), texture,z);
                polygon.calculateGeometry();
                polygons.add(polygon);
                startPolygon = coordinates.size();
            }

        }
        return new Pair(polygons, coordinates);
    }



    public void bufferPolygon (double bufferOffset) {
        buffer = bufferOffset;
        setBlurredTexture(null);
        super.bufferCoordinates(bufferOffset);
        Texture texture = polygons.get(0).getTexture();
        updatePolygons(geometry.getCoordinates(), texture);

        offsetedGeometry=bufferCoordinates(bufferOffset, offsetedGeometry, coordinatesOffseted);
        Pair<List<PolygonGL>, List<double[]>> pair = updatePolygons(offsetedGeometry.getCoordinates(), texture, polygonsOffseted);
        polygonsOffseted = pair.getValue1();
        coordinatesOffseted = pair.getValue2();
        for (PolygonGL polygonOffseted: polygonsOffseted) {
            polygonOffseted.setOffseted(true);
            if (!shapes.contains(polygonOffseted)) {
                shapes.add(polygonOffseted);
            }
        }

    }

    public void bufferOnlyBasePolygon(double bufferOffset) {
        super.bufferCoordinates(bufferOffset);
        Texture texture = polygons.get(0).getTexture();
        updatePolygons(geometry.getCoordinates(), texture);

    }

    void updatePolygons(Coordinate[] coords, Texture texture) {
        List<double[]> coordinates = new ArrayList<>();
        List<PolygonGL> newPolygons = new ArrayList<>();
        List<LineStripGL> newLines = new ArrayList<>();
        List<LineStripGL> newSelectionLines = new ArrayList<>();
        List<LineStripGL> newSelectionLinesBis = new ArrayList<>();

        CoordinateList newCoordinates = new CoordinateList(coords,false);
        int indexContains;
        int startPolygon = 0;
        int polygonCount = 0;

        for (int k=0; k<newCoordinates.size();k++) {
            indexContains =
                    coordinatesContains(new double[]{newCoordinates.getCoordinate(k).x,newCoordinates.getCoordinate(k).y}, coordinates);
            if(indexContains==-1) {
                coordinates.add(new double[]{newCoordinates.getCoordinate(k).x, newCoordinates.getCoordinate(k).y});
            }
            else if (indexContains==startPolygon) {
                if (polygonCount < polygons.size()) {
                    PolygonGL current = polygons.get(polygonCount);
                    LineStripGL currentLine = lines.get(polygonCount);
                    LineStripGL currentSelectinLine = selectionLines.get(polygonCount);
                    LineStripGL currentSelectionLineBis = selectionLinesBis.get(polygonCount);
                    current.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    currentLine.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    currentSelectinLine.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    currentSelectionLineBis.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    newPolygons.add(current);
                    newLines.add(currentLine);
                    newSelectionLines.add(currentSelectinLine);
                    polygonCount++;
                }
                else {
                    PolygonGL polygon = new PolygonGL(coordinates.subList(startPolygon, coordinates.size()), texture, z);
                    LineStripGL line = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), MapLayerViewer.outlineLineWidth, outlineColor);
                    LineStripGL selectionLine = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()),MapLayerViewer.selectionLineWidth);
                    LineStripGL selectionLineBis = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), MapLayerViewer.selectionOutLineWidth, MapLayerViewer.selectionColorOut);
                    lines.add(line);
                    selectionLines.add(selectionLine);
                    selectionLinesBis.add(selectionLineBis);
                    polygons.add(polygon);
                    newPolygons.add(polygon);
                    newLines.add(line);
                    lines.add(line);
                    selectionLines.add(selectionLine);
                    newSelectionLines.add(selectionLine);
                    newSelectionLinesBis.add(selectionLineBis);
                    selectionLinesBis.add(selectionLineBis);
                    polygonCount++;

                }
                startPolygon = coordinates.size();
            }

        }
        polygons = newPolygons;
        selectionLines = newSelectionLines;
        lines = newLines;
    }

    Pair<List<PolygonGL>, List<double[]>> updatePolygons (Coordinate[] coords, Texture texture, List<PolygonGL> polygons) {
        List<double[]> coordinates = new ArrayList<>();
        List<PolygonGL> newPolygons = new ArrayList<>();
        CoordinateList newCoordinates = new CoordinateList(coords,false);
        int indexContains;
        int startPolygon = 0;
        int polygonCount = 0;
        for (int k=0; k<newCoordinates.size();k++) {
            indexContains =
                    coordinatesContains(new double[]{newCoordinates.getCoordinate(k).x,newCoordinates.getCoordinate(k).y}, coordinates);
            if(indexContains==-1) {
                coordinates.add(new double[]{newCoordinates.getCoordinate(k).x, newCoordinates.getCoordinate(k).y});
            }
            else if (indexContains==startPolygon) {
                if (polygonCount < polygons.size()) {
                    polygons.get(polygonCount).refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    newPolygons.add(polygons.get(polygonCount));
                    polygonCount++;
                }
                else {
                    PolygonGL polygon = new PolygonGL(coordinates.subList(startPolygon, coordinates.size()), texture, z);
                    polygons.add(polygon);

                }
                startPolygon = coordinates.size();
            }

        }
        return new Pair(newPolygons, coordinates);
    }


    public void transformCoordinates(AffineTransform affineTransform) {
        super.transformCoordinates(affineTransform);
        Texture texture = polygons.get(0).getTexture();
        updatePolygons(geometry.getCoordinates(), texture);

    }

    public void calculatePolygonOffseted(double bufferOffset, Texture texture) {
        coordinatesOffseted.clear();
        BufferOp bufferOp = new BufferOp(geometry);
        Geometry newGeometry = bufferOp.getResultGeometry(bufferOffset);
        newGeometry = DouglasPeuckerSimplifier.simplify(newGeometry, bufferOffset / 100);
        offsetedGeometry = newGeometry;
        Pair<List<PolygonGL>, List<double[]>> pair =  generatePolygons(newGeometry.getCoordinates(), texture, false);
        polygonsOffseted= pair.getValue1();
        coordinatesOffseted = pair.getValue2();
        for (PolygonGL polygonOffseted: polygonsOffseted) {
            polygonOffseted.setOffseted(true);
            shapes.add(polygonOffseted);
        }

    }

    public List<PolygonGL> getPolygonsOffseted() {
        return polygonsOffseted;
    }

    public List <PolygonGL> getOriginalPolygons() {
        return originalPolygons;
    }

    public void setTranslucentBorder(boolean translucent) {
        super.setTranslucentBorder(translucent);
        for (PolygonGL polygonGL: originalPolygons) {
            polygonGL.setOnlyInFirst(translucent);
        }
        for (PolygonGL polygonGL:polygonsOffseted) {
            polygonGL.setVisible(translucent);
        }
    }



    public void setColor(Vector3f color) {
        setRenderingType(GLUtilities.RenderedElementTypes.TEXTURED);
        Color fillColorJava = new Color(color.x, color.y, color.z);
        float hsbVals[] = Color.RGBtoHSB( fillColorJava.getRed(),
                fillColorJava.getGreen(),
                fillColorJava.getBlue(), null );
        Color shadow = Color.getHSBColor( hsbVals[0], hsbVals[1], 0.5f * hsbVals[2] );
        outlineColor.set(shadow.getRed()/255.0f, shadow.getGreen()/255.0f, shadow.getBlue()/255.0f);
        for (PolygonGL polygon: originalPolygons) {
            polygon.setColor(color);
        }
        for (LineStripGL line: lines) {
            line.setColor(outlineColor);
        }
    }

    public void setOutlineColor(Vector3f outlineColor) {
        this.outlineColor = outlineColor;

        for (LineStripGL line: lines) {
            line.setColor(outlineColor);
        }
        }

    public List<LineStripGL> getSelectionLines() {
        return selectionLines;
    }

    public void toggleOutlineVisible() {
        //line.setVisible(!line.getVisible());
        for (LineStripGL line: lines) {
            line.setVisible(!line.getVisible());
        }
    }

    public void setIsPoint(boolean isPoint) {

        this.isPoint = isPoint;
        Color fillColorJava = new Color(fillColor.x, fillColor.y, fillColor.z);
        float hsbVals[] = Color.RGBtoHSB( fillColorJava.getRed(),
                fillColorJava.getGreen(),
                fillColorJava.getBlue(), null );
        Color highlight = Color.getHSBColor( hsbVals[0], hsbVals[1], 0.5f * ( 1f + hsbVals[2] ));
        outlineColor.set(highlight.getRed()/255.0f, highlight.getGreen()/255.0f, highlight.getBlue()/255.0f);
        for (LineStripGL line: lines) {
            line.setColor(outlineColor);
            //line.setVisible(true);
        }


    }

    public boolean getIsPoint() {
        return isPoint;
    }

    public PolygonGL getPolygonAt(double x, double y) {
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(new Coordinate[]{new Coordinate(x,y)});
        Point point = new Point(coordinateArraySequence, geometryFactory);
        PolygonGL closerPolygon = null;
        double closerDistance = Double.MAX_VALUE;
        for (PolygonGL polygonGL: polygons) {
            if (polygonGL.getGeometry().contains(point)) {
                if (polygonGL.getGeometry().getCentroid().distance(point)<closerDistance) {
                    closerPolygon = polygonGL;
                }
            }
        }
        return closerPolygon;
    }

    public PolygonGL getSelectedPolygon() {
        return selectedPolygon;
    }

    public void setSelectedPolygon(PolygonGL polygon) {
        this.selectedPolygon = polygon;
    }

    public List<double[]> getCoordinatesOffseted() {
        return coordinatesOffseted;
    }

    public List<LineStripGL> getSelectionLinesBis() {return selectionLinesBis;}

    public List<PolygonGL> getInPolygons() {return inPolygons;}













}

