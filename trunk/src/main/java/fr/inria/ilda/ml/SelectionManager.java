/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import com.vividsolutions.jts.geom.Polygon;
import fr.inria.ilda.ml.canvasEvents.CanvasDrawnEvent;
import fr.inria.ilda.ml.canvasEvents.CanvasEvent;
import fr.inria.ilda.ml.canvasEvents.CanvasVectorSelectionsEvent;
import org.geotools.geometry.jts.GeometryBuilder;


import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mjlobo on 24/06/15.
 */
public class SelectionManager {
    static List<VectorSelection> vectorSelections = new ArrayList<>();
    List<VectorSelection> geometricSelections = new ArrayList<>();
    HashMap<Layer,VectorSelection> backgroundSelections = new HashMap<>();
    List<VectorSelection> backgroundSelectionsList = new ArrayList<>();
    MapLayerViewer mapLayerViewer;
    protected HashMap<DrawnGeoElement, List<VectorSelection>> drawnSelections;
    protected HashMap<DrawnGeoElement, Texture> backgroundTextures;
    protected HashMap<DrawnGeoElement, Vector3f> backgroundColors;
    protected HashMap<DrawnGeoElement, Float> backgroundAlphas;
    protected HashMap<DrawnGeoElement, GLUtilities.RenderedElementTypes> drawnSelectionType;
    DrawnGeoElement backgroundSelectionDrawn;
    Layer backgroundSelectionLayer;


    public SelectionManager(MapLayerViewer mapLayerViewer) {
        this.mapLayerViewer = mapLayerViewer;
        vectorSelections = new ArrayList<>();
        drawnSelections = new HashMap<>();
        backgroundTextures = new HashMap<>();
        backgroundColors = new HashMap<>();
        drawnSelectionType = new HashMap<>();
        backgroundAlphas = new HashMap<>();

        //Background Selection Objects
        backgroundSelectionLayer = new DrawnLayer(false);
        backgroundSelectionLayer.setSelectable(false);
        backgroundSelectionsList = new ArrayList<>();

    }

    public VectorSelection getVectorSelection (DrawnGeoElement ge, VectorLayer vectorLayer) {
        for (VectorSelection vectorSelection:geometricSelections) {
                if (vectorSelection.getGeometricFilter().getDrawnGeoElement() == ge &&  vectorSelection.getGeometricFilter().getLayer()== vectorLayer) {
                    return vectorSelection;
                }

        }
        return null;
    }

    public java.util.List<VectorSelection> getVectorSelectionsDrawn (DrawnGeoElement ge) {
        if (drawnSelections.get(ge)!=null) {
            return drawnSelections.get(ge);
        }
        else {
            return new ArrayList<>();
        }
    }

    public List<VectorSelection> getVectorSelectionsVisibleDrawn(DrawnGeoElement ge) {
        List<VectorSelection> visibleSelections = new ArrayList<>();
        if (drawnSelections.get(ge)!=null) {
            for (VectorSelection vectorSelection: drawnSelections.get(ge)) {
                if (vectorSelection.getIsVisible()) {
                    visibleSelections.add(vectorSelection);
                }
            }
        }
        return visibleSelections;
    }

    public java.util.List<VectorSelection> getVectorSelectionLayerAttribute (Layer layer, ShapeFileAttribute attribute) {
        java.util.List<VectorSelection> result = new ArrayList<VectorSelection>();
        for (VectorSelection vectorSelection:vectorSelections) {
            for (QueryFilter filter : vectorSelection.getFilters()) {
                if (filter.getLayer() == layer) {
                    if (filter instanceof NumericQueryFilter) {
                        if (((NumericQueryFilter) filter).getAttribute() == attribute) {
                            result.add(vectorSelection);
                        }
                    }
                }
            }
        }
        return result;
    }

    public List<VectorSelection> getVectorSelectionsBackground() {
        List <VectorSelection> result = new ArrayList<>();
        for (VectorSelection vectorSelection:vectorSelections) {
            if (vectorSelection.getGeometricFilter() == null) {
                result.add(vectorSelection);
            }
        }
        return result;
    }

    public VectorSelection getVectorSelectionLayerComplete (Layer layer) {
        for (VectorSelection vectorSelection: vectorSelections) {
            if (vectorSelection.getLayer()==layer && vectorSelection.getGeometricFilter()==null) {
                return vectorSelection;
            }
        }
        return null;
    }

    public List<VectorSelection> getVectorSelectionLayerGeometric (Layer layer) {
        List <VectorSelection> result = new ArrayList<>();
        for (VectorSelection vectorSelection: vectorSelections) {
            if (vectorSelection.getLayer()==layer && vectorSelection.getGeometricFilter()!=null) {
                result.add(vectorSelection);
            }
        }
        return result;
    }

    VectorSelection filterByNumericAttribute(VectorLayer vectorLayer, String shapeFileAttributeName, double lowerValue, double maxValue, VectorSelection vectorSelection) {
        //System.out.println("filterByNumericAttribute");

        if (!vectorLayer.getVisibility()) {
            vectorLayer.setVisibility(true);
        }
        //System.out.println("Vector layer "+vectorLayer.getName());
        ShapeFileNumericAttribute numericAttribute = vectorLayer.getShapeFileNumericAttribute(shapeFileAttributeName);
        if (vectorSelection == null) {
            ArrayList <QueryFilter> filters = new ArrayList<>();
            vectorSelection = new VectorSelection(filters, mapLayerViewer.baseTexture, vectorLayer);
            vectorSelections.add(vectorSelection);
        }
        NumericQueryFilter filter = ((NumericQueryFilter) vectorSelection.getNumericFilterByLayerAndAttribute(vectorLayer, numericAttribute));
        if (filter==null) {
            filter = new NumericQueryFilter(numericAttribute, lowerValue, maxValue, vectorLayer);
            vectorSelection.addFilter(filter);
        }

        filter.setMinValue(lowerValue);
        filter.setMaxValue(maxValue);
        vectorSelection.update();
        return vectorSelection;
    }

    VectorSelection filterByFreeTextQuery(VectorLayer vectorLayer, String query, VectorSelection vectorSelection) {
        if (!vectorLayer.getVisibility()) {
            vectorLayer.setVisibility(true);
        }
        if (vectorSelection == null) {
            ArrayList <QueryFilter> filters = new ArrayList<>();
            vectorSelection = new VectorSelection(filters, mapLayerViewer.baseTexture, vectorLayer);
            vectorSelections.add(vectorSelection);
        }
        FreeQueryFilter filter = (FreeQueryFilter)vectorSelection.getFreeQueryFilter(vectorLayer);
        if (filter == null) {
            filter = new FreeQueryFilter(query, vectorLayer);
            vectorSelection.addFilter(filter);
        }
        vectorSelection.update();


        return vectorSelection;
    }

    public void filterByFreeTextQuery(VectorLayer vectorLayer, String query) {
        filterByFreeTextQuery(vectorLayer, query, getVectorSelectionLayerComplete(vectorLayer));
        mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, backgroundSelectionsList,backgroundSelectionDrawn,mapLayerViewer.getBackgroundTexture(),mapLayerViewer.defaultFillColor, GLUtilities.RenderedElementTypes.TEXTURED,0));
    }

    public void filterByFreeTextQuery(VectorLayer vectorLayer, String query, DrawnGeoElement ge) {
        filterByFreeTextQuery(vectorLayer, query, getVectorSelection(ge, vectorLayer));
        pushCanvasVectorSelectionEvent(drawnSelections.get(ge), ge);
    }

    public void filterByNumericAttribute(VectorLayer vectorLayer, String shapeFileAttributeName, double lowerValue, double maxValue) {
       // List<VectorSelection> layerGeometricSelections = getVectorSelectionLayerGeometric(vectorLayer);
        VectorSelection vectorSelection = filterByNumericAttribute(vectorLayer, shapeFileAttributeName, lowerValue, maxValue, getVectorSelectionLayerComplete(vectorLayer));
        //System.out.println("Vector selection yeeeeo "+vectorSelection);
        //System.out.println("Layer geometric selections " + layerGeometricSelections.size());
        //System.out.println("Layer geometric selections " + layerGeometricSelections.size());
//        for (VectorSelection geometricSelection : layerGeometricSelections) {
//            vectorSelection.removeElements(vectorLayer.getElementsIntersectingGeometry(((GeometryQueryFilter)geometricSelection.getGeometricFilter()).getGeometry()));
//            //System.out.println("Elements to remove "+ vectorLayer.getElementsIntersectingGeometry(((GeometryQueryFilter)geometricSelection.getGeometricFilter()).getGeometry()));
//        }
//        vectorSelection.setElementsVisibility(true);

    }
    public void filterByNumericAttribute(VectorLayer vectorLayer, String shapeFileAttributeName, double lowerValue, double maxValue, DrawnGeoElement ge) {
        //System.out.println("do filter numeric attribute in element "+ge);
        VectorSelection vectorSelection = filterByNumericAttribute(vectorLayer, shapeFileAttributeName, lowerValue, maxValue, getVectorSelection(ge, vectorLayer));
        //System.out.println("vector selection in filterbynumericattribute "+vectorSelection);

        //mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, drawnSelections.get(ge), ge, backgroundTextures.get(ge)));
        pushCanvasVectorSelectionEvent(drawnSelections.get(ge), ge);
    }

    VectorSelection filterByStringAttribute(VectorLayer vectorLayer, String shapeFileAttributeName, LinkedList<String> strings, VectorSelection vectorSelection) {
        if (!vectorLayer.getVisibility()) {
            vectorLayer.setVisibility(true);
        }

        ShapeFileStringAttribute stringAttribute = vectorLayer.getShapeFileStringAttribute(shapeFileAttributeName);
        //VectorSelection vectorSelection = getVectorSelectionLayerComplete(vectorLayer);
        if (vectorSelection == null) {
            //System.out.println("Vector Selection null");
            ArrayList <QueryFilter> filters = new ArrayList<>();
            vectorSelection = new VectorSelection(filters, mapLayerViewer.baseTexture, vectorLayer);
            vectorSelections.add(vectorSelection);
        }

        StringQueryFilter filter = ((StringQueryFilter)vectorSelection.getStringFilterByLayerAndAttribute(vectorLayer, stringAttribute));
        if (filter==null) {
            filter = new StringQueryFilter(stringAttribute, strings, vectorLayer);
            vectorSelection.addFilter(filter);
        }
        filter.setStrings(strings);
        vectorSelection.update();
        //System.out.println("after update number of elements "+vectorSelection.getSelectedElements().size());
        //vectorSelection.setElementsVisibility(true);
        mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, backgroundSelectionsList,backgroundSelectionDrawn,mapLayerViewer.getBackgroundTexture(),mapLayerViewer.defaultFillColor, GLUtilities.RenderedElementTypes.TEXTURED,0));
        return vectorSelection;
    }

    public void filterByStringAttribute(VectorLayer vectorLayer, String shapeFileAttributeName, LinkedList<String> strings) {
        filterByStringAttribute(vectorLayer, shapeFileAttributeName, strings, getVectorSelectionLayerComplete(vectorLayer));
    }

    public void filterByStringAttribute(VectorLayer vectorLayer, String shapeFileAttributeName, LinkedList<String> strings, DrawnGeoElement drawnGeoElement) {
        filterByStringAttribute(vectorLayer, shapeFileAttributeName, strings, getVectorSelection(drawnGeoElement, vectorLayer));
        //mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, drawnSelections.get(drawnGeoElement), drawnGeoElement,  backgroundTextures.get(drawnGeoElement)));
        pushCanvasVectorSelectionEvent(drawnSelections.get(drawnGeoElement),drawnGeoElement);
    }





    public void toggleElementsInRegionVisibility (DrawnGeoElement ge, String layerName) {
        VectorLayer vectorLayer = (VectorLayer)(mapLayerViewer.getLayerbyName(layerName));

        VectorSelection vectorSelection = getVectorSelection(ge, vectorLayer);
        if (vectorSelection == null) {
            Texture textureToApply= null;
            GLUtilities.RenderedElementTypes typeToApply = null;
            if (drawnSelections.get(ge)==null) {
                drawnSelections.put(ge, new ArrayList<VectorSelection>());
                textureToApply = ge.getTexture();
                typeToApply = ge.getRenderingType();
                backgroundTextures.put(ge, mapLayerViewer.getBackgroundTexture());

            }
            else {
                textureToApply = drawnSelections.get(ge).get(drawnSelections.get(ge).size() - 1).getTexture();
                typeToApply = drawnSelections.get(ge).get(drawnSelections.get(ge).size() - 1).getSelectedType();
            }
            //System.out.println("Vector selection null!");
            GeometryQueryFilter geometryQueryFilter = new GeometryQueryFilter(ge,vectorLayer);
            if (typeToApply == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT || typeToApply== GLUtilities.RenderedElementTypes.FILLED || vectorLayer.getIsPointLayer()) {
                if (vectorLayer.getIsPointLayer()) {
                    vectorSelection = new VectorSelection(geometryQueryFilter, vectorLayer.getColor(), vectorLayer);
                }
                else {
                    vectorSelection = new VectorSelection(geometryQueryFilter, ge.getFillColor(), vectorLayer);
                }
                vectorSelection.setTexture(ge.getTexture());
            }
            else {
                vectorSelection = new VectorSelection(geometryQueryFilter, textureToApply, vectorLayer);
            }
            //System.out.println("Texture Id in toggle elements visibility " + mapLayerViewer.getCanvas().getSimpleFilterTexture().getID());
            vectorSelections.add(vectorSelection);
            geometricSelections.add(vectorSelection);

            drawnSelections.get(ge).add(vectorSelection);
            reOrderVectorSelections(ge);
            //mapLayerViewer.getDrawnLayer().getTranslucentElements().remove(ge);
        }

        //
        vectorSelection.toggleVisibility();
        //ge.setTexture(mapLayerViewer.getBackgroundTexture());
        //System.out.println("Vector selection texture in toggleelementsinregionvisibility "+vectorSelection.getTexture().getName());
        if (!backgroundColors.containsKey(ge)) {
            backgroundColors.put(ge, ge.getFillColor());

        }
        if (!backgroundTextures.keySet().contains(ge)) {
            backgroundTextures.put(ge, mapLayerViewer.getBackgroundTexture());
        }
        if (!backgroundAlphas.keySet().contains(ge)) {
            backgroundAlphas.put(ge, ge.getAlpha());
            ge.setAlpha(1.0f);
        }

//        if (ge.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT || ge.getRenderingType()== GLUtilities.RenderedElementTypes.FILLED) {
//            System.out.println("ge.getrenderingtype = drawn");

//            //drawnSelectionType.put(ge, GLUtilities.RenderedElementTypes.FILLED);
//            pushCanvasVectorSelectionEvent(drawnSelections.get(ge), ge);
//            //mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, drawnSelections.get(ge), ge, ge.getFillColor()));
//        }
//        else {
//
//            drawnSelectionType.put(ge, GLUtilities.RenderedElementTypes.TEXTURED);
//            pushCanvasVectorSelectionEvent(drawnSelections.get(ge), ge);
//            //mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, drawnSelections.get(ge), ge, backgroundTextures.get(ge)));
//        }
        if (getVectorSelectionsVisibleDrawn(ge).size()>0) {
            if (!drawnSelectionType.containsKey(ge)) {
                drawnSelectionType.put(ge, GLUtilities.RenderedElementTypes.TEXTURED);
            }
            pushCanvasVectorSelectionEvent(drawnSelections.get(ge), ge);
        }

        if (ge.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT || ge.getRenderingType() == GLUtilities.RenderedElementTypes.DRAWN) {
            ge.setRenderingType(GLUtilities.RenderedElementTypes.DRAWN);
        }
        else {
            //System.out.println("set textured");
            ge.setRenderingType(GLUtilities.RenderedElementTypes.TEXTURED);
        }

        if (getVectorSelectionsVisibleDrawn(ge).size()==0) {
            //System.out.println("getVectorSelectionsVisibleDrawn(ge).size()==0");
            if (ge.getRenderingType() == GLUtilities.RenderedElementTypes.DRAWN) {
                if (drawnSelectionType.get(ge) == GLUtilities.RenderedElementTypes.FILLED) {
                    ge.setRenderingType(GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT);
                }
            }
            else {
                ge.setRenderingType(drawnSelectionType.get(ge));
            }
            pushCanvasVectorSelectionEvent(drawnSelections.get(ge), ge);
        }


        //ge.getPolygon().setVisible(false);
//        for (ShapeGL shape : ge.getShapes()) {
//            if (shape instanceof PolygonGL) {
//                ((PolygonGL) shape).setAlpha(0.0f);
//            }
//        }


    }

    public void copyVectorSelectionsDrawn(DrawnGeoElement target, DrawnGeoElement source) {
        drawnSelections.put(target, new ArrayList<VectorSelection>());
        Texture textureToApply = backgroundTextures.get(source);
        GLUtilities.RenderedElementTypes typeToApply = source.getRenderingType();
        backgroundTextures.put(target, textureToApply);
        for (VectorSelection vectorSelection : getVectorSelectionsDrawn(source)) {

            GeometryQueryFilter geometryQueryFilter = new GeometryQueryFilter(target,vectorSelection.getLayer());
            VectorSelection copiedVectorSelection = new VectorSelection(geometryQueryFilter, vectorSelection);
//            if (typeToApply == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT || typeToApply== GLUtilities.RenderedElementTypes.FILLED || vectorSelection.getLayer().getIsPointLayer()) {
//                if (vectorSelection.getLayer().getIsPointLayer()) {
//                    copiedVectorSelection = new VectorSelection(geometryQueryFilter, vectorSelection.getLayer().getColor(), vectorSelection.getLayer());
//                }
//                else {
//                    copiedVectorSelection = new VectorSelection(geometryQueryFilter, source.getFillColor(), vectorSelection.getLayer());
//                }
//                copiedVectorSelection.setTexture(vectorSelection.getTexture());
//            }
//            else {
//                copiedVectorSelection = new VectorSelection(geometryQueryFilter, textureToApply, vectorSelection.getLayer());
//            }
            vectorSelections.add(copiedVectorSelection);
            geometricSelections.add(copiedVectorSelection);

            drawnSelections.get(target).add(copiedVectorSelection);
            reOrderVectorSelections(target);
            copiedVectorSelection.toggleVisibility();
        }

        backgroundColors.put(target, source.getFillColor());
        backgroundAlphas.put(target, source.getAlpha());
        target.setAlpha(1.0f);
        drawnSelectionType.put(target, drawnSelectionType.get(source));
        pushCanvasVectorSelectionEvent(drawnSelections.get(target), target);
        target.setRenderingType(source.getRenderingType());


    }





    GeometryBuilder geometryBuilder = new GeometryBuilder();
    public void toggleElementsInLayerVisibility(VectorLayer vectorLayer) {
        //System.out.println("elements in layer "+vectorLayer.getElements().size());

        if (!vectorLayer.getVisibility()) {
            vectorLayer.setVisibility(true);
        }

        if (backgroundSelectionDrawn == null) {
            Polygon backgroundBox = geometryBuilder.box(mapLayerViewer.getCanvas().getCamera().getCameraSpace().getMinX(), mapLayerViewer.getCanvas().getCamera().getCameraSpace().getMinY(), mapLayerViewer.getCanvas().getCamera().getCameraSpace().getMaxX(), mapLayerViewer.getCanvas().getCamera().getCameraSpace().getMaxY());
            backgroundSelectionDrawn = new DrawnGeoElement(backgroundBox);
            backgroundSelectionDrawn.drawCompletePolygon(mapLayerViewer.maxAlphaRadius, mapLayerViewer.getBackgroundTexture());
            backgroundSelectionLayer.addElement(backgroundSelectionDrawn);
            mapLayerViewer.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, backgroundSelectionDrawn, backgroundSelectionLayer));
            //mapLayerViewer.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_FINISHED, backgroundSelectionDrawn, backgroundSelectionLayer));
            backgroundSelectionDrawn.setVisible(true);
            backgroundSelectionLayer.setVisibility(false);
            //System.out.println("after creating background drawn "+System.currentTimeMillis());
        }

        VectorSelection vectorSelection = getVectorSelectionLayerComplete(vectorLayer);
        //System.out.println("vector selection only layer "+vectorSelection);
        if (vectorSelection == null) {
            ArrayList <QueryFilter> filters = new ArrayList<>();
            vectorSelection = new VectorSelection(filters, mapLayerViewer.baseTexture, vectorLayer);
            vectorSelection.update();
            vectorSelections.add(vectorSelection);
            backgroundSelectionsList.add(vectorSelection);
            //drawnSelections.put(backgroundSelectionDrawn,new ArrayList<VectorSelection>());
            //drawnSelections.get(backgroundSelectionDrawn).add(vectorSelection);
            //vectorSelection.setBufferedLayer(vectorLayer);
            backgroundSelections.put(vectorLayer, vectorSelection);
            if (vectorLayer.getColor()!=null) {
                vectorSelection.setColor(vectorLayer.getColor());
                vectorSelection.setSelectedType(GLUtilities.RenderedElementTypes.FILLED);
            }
        }
        mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, backgroundSelectionsList, backgroundSelectionDrawn, mapLayerViewer.getBackgroundTexture(),
                mapLayerViewer.defaultFillColor, GLUtilities.RenderedElementTypes.TEXTURED, 0));
        vectorSelection.toggleVisibility();
        //System.out.println("end toggleelementsinlayer "+System.currentTimeMillis());
    }

    public void updateBackgroundSelectionDrawn() {
        Polygon backgroundBox = geometryBuilder.box(mapLayerViewer.getCanvas().getCamera().getCameraSpace().getMinX(), mapLayerViewer.getCanvas().getCamera().getCameraSpace().getMinY(), mapLayerViewer.getCanvas().getCamera().getCameraSpace().getMaxX(), mapLayerViewer.getCanvas().getCamera().getCameraSpace().getMaxY());
        backgroundSelectionDrawn.refreshCoordinates(backgroundBox);
        mapLayerViewer.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.ELEMENT_MODIFIED,backgroundSelectionDrawn, backgroundSelectionLayer));
    }

    public static boolean isElementInActiveSelection (GeoElement element, VectorSelection current) {
        for (int i =0; i<vectorSelections.size();i++) {
            VectorSelection vectorSelection = vectorSelections.get(i);
            if (vectorSelection != current) {
                if (vectorSelection.getRemovedElements().contains(element)) {
                    vectorSelection.getSelectedElements().add(element);
                    vectorSelection.getRemovedElements().remove(element);
                    return true;
                }
                else if (vectorSelection.getSelectedElements().contains(element)) {
                    return true;
                }
            }
        }
        return false;

    }



    void pushCanvasVectorSelectionEvent(List<VectorSelection> drawnSelections, DrawnGeoElement ge) {
//        if (drawnSelectionType.get(ge) == GLUtilities.RenderedElementTypes.FILLED) {
            mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, drawnSelections, ge, backgroundTextures.get(ge), backgroundColors.get(ge), drawnSelectionType.get(ge), backgroundAlphas.get(ge)));
//        }
//        else {
//            mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, drawnSelections, ge, backgroundTextures.get(ge)));
//        }
    }

    void pushCanvasVectorSelectionEvent(DrawnGeoElement ge) {
//        if (drawnSelectionType.get(ge) == GLUtilities.RenderedElementTypes.FILLED) {
        mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, getVectorSelectionsDrawn(ge), ge, backgroundTextures.get(ge), backgroundColors.get(ge), drawnSelectionType.get(ge), backgroundAlphas.get(ge)));
//        }
//        else {
//            mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, drawnSelections, ge, backgroundTextures.get(ge)));
//        }
    }

    public void deleteGeoElement(DrawnGeoElement ge) {
        List<VectorSelection> thisDrawnSelections = getVectorSelectionsDrawn(ge);
        if (thisDrawnSelections!= null) {
            for (VectorSelection vectorSelection: thisDrawnSelections) {
                vectorSelections.remove(vectorSelection);

            }
            thisDrawnSelections.remove(ge);
        }
        if (backgroundTextures.containsKey(ge)) {
            backgroundTextures.remove(ge);
        }
        if (backgroundColors.containsKey(ge)) {
            backgroundColors.remove(ge);
        }



    }

    public void changeColorInVectorSelection(DrawnGeoElement ge, Layer layer, Vector3f color) {
        VectorSelection selectedVectorSelection = getVectorSelection(ge, (VectorLayer) layer);
        //selected.setFillColor(color);
        List<VectorSelection>selectionsDrawn = getVectorSelectionsDrawn(ge);
        selectedVectorSelection.setColor(color);
        selectedVectorSelection.setSelectedType(GLUtilities.RenderedElementTypes.FILLED);
        pushCanvasVectorSelectionEvent(selectionsDrawn,ge);

    }

    public void changeTextureInVectorSelection(DrawnGeoElement ge, Layer layer, Texture texture) {
        VectorSelection selectedVectorSelection = getVectorSelection(ge, (VectorLayer) layer);
        List<VectorSelection>selectionsDrawn = getVectorSelectionsDrawn(ge);
        selectedVectorSelection.setTexture(texture);
        selectedVectorSelection.setSelectedType(GLUtilities.RenderedElementTypes.TEXTURED);
        pushCanvasVectorSelectionEvent(selectionsDrawn,ge);
    }

    public void bufferVectorSelection(VectorSelection vectorSelection,double bufferOffset, boolean isBackground) {
        //boolean isFirst = vectorSelection.bufferSelectedElements(bufferOffset);

        BufferSelectionWorker bufferSelectionWorker = new BufferSelectionWorker(vectorSelection, (bufferOffset- vectorSelection.getBuffer())/mapLayerViewer.getCanvas().getCamera().getPixels_per_unit()[0], mapLayerViewer.getCanvas(), vectorSelection.getLayer(),bufferOffset,this, isBackground);
        //vectorSelection.setBuffer(bufferOffset);
        bufferSelectionWorker.startWorker();


    }

    public void vectorSelectionBuffered(VectorSelection vectorSelection, double bufferOffset, boolean isBackground) {
        //vectorSelection.setBuffer(bufferOffset);
        if (!isBackground) {
            List<VectorSelection> drawnSelections = getVectorSelectionsDrawn(vectorSelection.getGeometricFilter().getDrawnGeoElement());
            pushCanvasVectorSelectionEvent(drawnSelections, vectorSelection.getGeometricFilter().getDrawnGeoElement());
        }
        else {
            mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, backgroundSelectionsList,backgroundSelectionDrawn,mapLayerViewer.getBackgroundTexture(),mapLayerViewer.defaultFillColor, GLUtilities.RenderedElementTypes.TEXTURED,0));
        }
        vectorSelection.setBuffer(bufferOffset);
    }

    public void setOutlineVectorSelectionVisible (VectorSelection vectorSelection, Vector3f color, boolean visible) {
        vectorSelection.setOutlineVisible(visible);
        vectorSelection.setOutlineColor(color);
        List <VectorSelection> drawnSelections = getVectorSelectionsDrawn(vectorSelection.getGeometricFilter().getDrawnGeoElement());
        pushCanvasVectorSelectionEvent(drawnSelections, vectorSelection.getGeometricFilter().getDrawnGeoElement());
    }

    public void changeColorInVectorSelectionBackground(Layer layer, Vector3f color) {
        VectorSelection selectedVectorSelection = backgroundSelections.get(layer);
        selectedVectorSelection.setSelectedType(GLUtilities.RenderedElementTypes.FILLED);
        selectedVectorSelection.setColor(color);
        mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, backgroundSelectionsList,backgroundSelectionDrawn,mapLayerViewer.getBackgroundTexture(),mapLayerViewer.defaultFillColor, GLUtilities.RenderedElementTypes.TEXTURED,0));
    }

    public void changeTextureInVectorSelectionBackground(Layer layer, Texture texture) {
        VectorSelection selectedVectorSelection = backgroundSelections.get(layer);
        selectedVectorSelection.setSelectedType(GLUtilities.RenderedElementTypes.TEXTURED);
        selectedVectorSelection.setTexture(texture);
        mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, backgroundSelectionsList,backgroundSelectionDrawn,mapLayerViewer.getBackgroundTexture(),mapLayerViewer.defaultFillColor, GLUtilities.RenderedElementTypes.TEXTURED,0));
    }

    public void setOutlineVectorSelectionBackgroundVisible (VectorSelection vectorSelection, Vector3f color, boolean visible) {
        vectorSelection.setOutlineVisible(visible);
        vectorSelection.setOutlineColor(color);
        mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, backgroundSelectionsList,backgroundSelectionDrawn,mapLayerViewer.getBackgroundTexture(),mapLayerViewer.defaultFillColor, GLUtilities.RenderedElementTypes.TEXTURED,0));
    }

    public void changeAlphaBackgroundVectorSelection(Layer layer, float alpha) {
        VectorSelection selectedVectorSelection = backgroundSelections.get(layer);
        selectedVectorSelection.setAlpha(alpha);
        mapLayerViewer.getCanvas().pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, backgroundSelectionsList,backgroundSelectionDrawn,mapLayerViewer.getBackgroundTexture(),mapLayerViewer.defaultFillColor, GLUtilities.RenderedElementTypes.TEXTURED,0));

    }

    public void changeAlphaVectorSelection(DrawnGeoElement ge, Layer layer, float alpha) {
        VectorSelection selectedVectorSelection = getVectorSelection(ge, (VectorLayer) layer);
        List<VectorSelection>selectionsDrawn = getVectorSelectionsDrawn(ge);
        selectedVectorSelection.setAlpha(alpha);
        pushCanvasVectorSelectionEvent(selectionsDrawn, ge);
    }

    public VectorSelection getSelectionContainingElement (DrawnGeoElement drawn, GeoElement ge) {
        for (VectorSelection vectorSelection: getVectorSelectionsDrawn(drawn)) {
            if (vectorSelection.getSelectedElements().contains(ge)) {
                return vectorSelection;
            }
        }
        return null;
    }

    public void updateAllVectorSelections() {
        for (DrawnGeoElement drawnGeoElement: drawnSelections.keySet()) {
            pushCanvasVectorSelectionEvent(drawnGeoElement);
        }
    }



    public List<VectorSelection> getGeometricSelections() {
        return geometricSelections;
    }

    public HashMap<Layer,VectorSelection> getBackgroundSelections() {
        return backgroundSelections;
    }

    public HashMap<DrawnGeoElement, Texture> getBackgroundTextures() {
        return backgroundTextures;
    }

    public HashMap<DrawnGeoElement, Vector3f> getBackgroundColors() {return backgroundColors;}

    public HashMap<DrawnGeoElement, GLUtilities.RenderedElementTypes> getDrawnSelectionType() {
        return drawnSelectionType;
    }

    public HashMap<DrawnGeoElement, List<VectorSelection>> getDrawnSelections() {
        return drawnSelections;
    }

    public HashMap<DrawnGeoElement, Float> getBackgroundAlphas() {
        return backgroundAlphas;
    }

    public void reOrderVectorSelections (DrawnGeoElement ge) {
        ArrayList<VectorSelection> vectorSelectionsOrdered = new ArrayList<>();
        for (Layer layer: mapLayerViewer.getLayers()) {
            if (layer instanceof VectorLayer) {
                for (VectorSelection vectorSelection: drawnSelections.get(ge)) {
                    if (vectorSelection.getLayer() == layer) {
                        vectorSelectionsOrdered.add(vectorSelection);
                    }
                }
            }
        }
        drawnSelections.put(ge, vectorSelectionsOrdered);
    }

    public Layer getBackgroundSelectionLayer() {
        return backgroundSelectionLayer;
    }

    public DrawnGeoElement getBackgroundSelectionDrawn() {
        return backgroundSelectionDrawn;
    }

    public List <VectorSelection> getBackgroundSelectionsList() {
        return backgroundSelectionsList;
    }


}
