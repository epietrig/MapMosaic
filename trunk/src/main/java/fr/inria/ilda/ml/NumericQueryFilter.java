/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import fr.inria.ilda.ml.queryWidgets.NumericQuery;

import java.util.HashSet;
import java.util.List;

/**
 * Created by mjlobo on 23/06/15.
 */
public class NumericQueryFilter extends QueryFilter{
    ShapeFileNumericAttribute attribute;
    double minValue;
    double maxValue;
    HashSet<GeoElement> elements;
    HashSet<ShapeFileFeature> features;

    public NumericQueryFilter(ShapeFileNumericAttribute attribute, double minValue, double maxValue, VectorLayer layer) {
        this.attribute = attribute;
        this.maxValue = maxValue;
        this.minValue = minValue;
        this.layer = layer;
    }

    public NumericQueryFilter (NumericQueryFilter otherFilter) {
        this.attribute = (ShapeFileNumericAttribute)otherFilter.getAttribute();
        this.maxValue = otherFilter.getMaxValue();
        this.minValue = otherFilter.getMinValue();
        this.layer = otherFilter.getLayer();

    }
    @Override
    public HashSet<GeoElement> getElements() {
        elements = new HashSet<>();
        getFeatures();
        for (ShapeFileFeature feature: features) {
            for (GeoElement ge : feature.getVectorGeoElements()) {
                elements.add(ge);
            }
        }
        //elements = ((VectorLayer)layer).getInNumericAttributeRange(attribute, minValue, maxValue);
        return elements;
    }

    @Override
    public HashSet<GeoElement> getElements(List<GeoElement> elementsToConsider) {
        return null;
    }


    public HashSet <ShapeFileFeature> getFeatures() {
        features = ((VectorLayer)layer).getFeaturesInNumericAttributeRange(attribute, minValue, maxValue);
        System.out.println("Features size in numeric query filter "+features.size());
        return features;
    }

    public HashSet <ShapeFileFeature> getFeatures(HashSet<ShapeFileFeature> featuresToConsider) {
        features = ((VectorLayer)layer).getFeaturesInNumericAttributeRange(attribute, minValue, maxValue, featuresToConsider);
        return features;
    }

    @Override
    public HashSet<GeoElement> update() {
        getFeatures();
        elements.clear();
        for (ShapeFileFeature feature: features) {
            for (GeoElement ge : feature.getVectorGeoElements()) {
                elements.add(ge);
            }
        }
        return elements;
    }

    public HashSet<ShapeFileFeature> updateFeatures(HashSet<ShapeFileFeature> featuresToConsider) {
        System.out.println("updateFeatures with parameters");
        getFeatures(featuresToConsider);
        return features;
    }

    public HashSet<ShapeFileFeature> updateFeatures() {
        return getFeatures();
    }

    public void setMinValue(double minValue) {
        this.minValue = minValue;
    }

    public void setMaxValue (double maxValue) {
        this.maxValue = maxValue;
    }

    public ShapeFileAttribute getAttribute () {
        return attribute;
    }

    public double getMinValue() {return minValue;}

    public double getMaxValue() {return maxValue;}
}
