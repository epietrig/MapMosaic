/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.StateMachine;

/**
 * Created by mjlobo on 22/05/15.
 */
public class LeftMouseUpEvent extends MouseUpEvent {
    public LeftMouseUpEvent(int x, int y) {
        super(x,y);
    }

    public LeftMouseUpEvent(java.awt.Component target, int x, int y) {
        super(target, x, y);
    }
}
