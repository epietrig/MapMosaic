/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import java.awt.*;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceAdapter;

/**
 * Created by mjlobo on 05/10/15.
 */
public class LayerOptionsTransfer extends DragSourceAdapter implements DragGestureListener {
    Inspector inspector;

    public LayerOptionsTransfer (Inspector inspector) {
        this.inspector = inspector;
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent dge) {
        Cursor cursor = DragSource.DefaultCopyDrop;
        if (inspector.getLayerOptionsAt(inspector.getMousePosition()) instanceof LayerOptions) {
            //TransferableLayerOptions transferable = new TransferableLayerOptions((LayerOptions)inspector.getLayerOptionsAt(dge.getDragOrigin()));
            LayerOptions layerOptions = inspector.getLayerOptionsAt(inspector.getMousePosition());
            if (layerOptions!=null) {
                TransferableLayerOptions transferable = new TransferableLayerOptions(layerOptions);
                dge.startDrag(cursor, transferable);
            }
        }
    }
}
