/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.queryWidgets;

import fr.inria.ilda.ml.StateMachine.StateMachine;
import fr.inria.ilda.ml.StateMachine.TextQueryEvent;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.LinkedList;

public class TextQuery extends QueryWidget {

	protected static Font FONT = new Font("Courier", Font.PLAIN, 12);
	protected static Font QUERY_FONT = new Font("Courier", Font.PLAIN, 10);
	
	protected CheckList checkList;
	protected JTextField textFieldFreeQuery;
	protected String typeName;
	
	protected JLabel queryLabel;
	static int currentIndex =0;
	String nameforStateMachine = "TextQuery";
	
	public TextQuery(String typeName, LinkedList<String> values, int sizeLabel) {
		super();
		this.typeName = typeName;
		this.checkList = new CheckList(values);
		this.textFieldFreeQuery = new JTextField("                            ");
		GridBagConstraints gbc = new GridBagConstraints();
		setLayout(new GridBagLayout());
		gbc.fill=GridBagConstraints.NONE;
		gbc.anchor=GridBagConstraints.WEST;
		int extraSpaces = sizeLabel - this.typeName.length();
		String extraSpace = "";
		for (int i = 0; i < extraSpaces; i++) {
			extraSpace += " ";
		}
		JLabel label = new JLabel(this.typeName+" = "+extraSpace);
		label.setFont(FONT);
		label.setVerticalAlignment(JLabel.CENTER);
		Utils.buildConstraints(gbc, 0, 0, 1, 1, 0, 0);
		add(label, gbc);
		Utils.buildConstraints(gbc, 1, 0, 1, 1, 0, 0);
		add(this.checkList, gbc);
		label = new JLabel(" AND ");
		label.setFont(FONT);
		Utils.buildConstraints(gbc, 2, 0, 1, 1, 0, 0);
		add(label, gbc);
		gbc.fill=GridBagConstraints.HORIZONTAL;
		Utils.buildConstraints(gbc, 3, 0, 1, 1, 1, 0);
		add(this.textFieldFreeQuery, gbc);
		
		this.queryLabel = new JLabel(" ");
		this.queryLabel.setForeground(Color.DARK_GRAY);
		this.queryLabel.setFont(QUERY_FONT);
		Utils.buildConstraints(gbc, 0, 1, 4, 1, 1, 0);
		add(this.queryLabel, gbc);
		
//		this.checkList.addChangeListener(new ChangeListener() {
//			public void stateChanged(ChangeEvent e) {
//				TextQuery.this.queryLabel.setText(getQuery());
//			}
//		});
		this.textFieldFreeQuery.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				TextQuery.this.queryLabel.setText(getQuery());
			}
			public void insertUpdate(DocumentEvent e) {
				TextQuery.this.queryLabel.setText(getQuery());
			}
			public void changedUpdate(DocumentEvent e) {
				TextQuery.this.queryLabel.setText(getQuery());
			}
		});

		this.textFieldFreeQuery.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				System.out.println("key pressed!");
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					System.out.println("enter!");
				}
				super.keyPressed(e);
			}
		});
	}
	
	public String getQuery() {
		
		String query = "";
		if(checkList.getSelectedValues().size() == checkList.getValues().size() && this.textFieldFreeQuery.getText().trim().length() == 0) {
			return query;
		}
		query += "(";
		for (Iterator<String> iterator = checkList.getSelectedValues().iterator(); iterator.hasNext();) {
			String val = iterator.next();
			query += this.typeName+" = '"+val+"'";
			if(iterator.hasNext()) {
				query += " OR ";
			}
		}
		query += ") ";
		query += (this.textFieldFreeQuery.getText().trim().length() > 0 ? " AND ("+this.textFieldFreeQuery.getText().trim()+")": "");
		return query;
	}

	@Override
	public void attachToStateMachine(StateMachine stateMachine) {

	}

	@Override
	public void addListeners(final StateMachine stateMachine) {
		checkList.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				//System.out.println("State changed in checkbosx List");
				TextQuery.this.queryLabel.setText(getQuery());
				stateMachine.handleEvent(new TextQueryEvent(TextQuery.this,checkList.getSelectedValues(), typeName));
			}
		});
	}

	@Override
	public void resetValues() {
		checkList.setAllCheckBoxItem(true);
		queryLabel.setText("");

	}

	public String getNameForStateMachine() {
		String name = nameforStateMachine + currentIndex;
		currentIndex++;
		return name;
	}

	public String getTypeName() {
		return typeName;
	}

	public CheckList getCheckList() {
		return checkList;
	}

	public void setValues(LinkedList<String> selectedValues) {
		checkList.setSelectedValues(selectedValues);
	}

	public void setValues (String value) {
		LinkedList<String> valueList = new LinkedList<>();
		valueList.add(value);
		checkList.setSelectedValues(valueList);
		setEnabled(false);

	}

	public void setEnabled(boolean enabled) {
		checkList.setEnabled(enabled);
	}
	
	public static void main(String args[]) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		LinkedList<String> items = new LinkedList<String>();
		items.add("apple");
		items.add("orange");
		items.add("mango");
		items.add("pineapple");
		items.add("banana");
		items.add("lemon");
		TextQuery tq = new TextQuery("NOM_VOIE_G", items, 15);
		frame.getContentPane().add(tq);
		frame.pack();
		frame.setVisible(true);
	}


}
