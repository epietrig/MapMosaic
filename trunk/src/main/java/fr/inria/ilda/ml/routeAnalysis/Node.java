/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.routeAnalysis;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * Created by mjlobo on 17/06/15.
 */
public class Node {
    Coordinate coordinate;

    public Node (Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Coordinate getCoordinate(){
        return coordinate;
    }
 }
