/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.StateMachine;

import java.awt.*;

/**
 * Created by mjlobo on 08/08/15.
 */
public class TextFieldEvent extends Event {

    Component target;
    String text;

    public TextFieldEvent(Component target, String text) {
        this.target = target;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public Component getTarget() {
        return target;
    }
}
