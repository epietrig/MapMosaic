/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.operation.buffer.BufferParameters;
import com.vividsolutions.jts.operation.buffer.OffsetCurveBuilder;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.io.GridCoverage2DReader;
import org.geotools.data.DataSourceException;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.feature.type.AttributeTypeImpl;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.gce.arcgrid.ArcGridReader;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.geotools.geometry.jts.GeometryBuilder;
import org.geotools.graph.build.feature.FeatureGraphGenerator;
import org.geotools.graph.build.line.LineStringGraphGenerator;
import org.geotools.resources.coverage.IntersectUtils;
import org.lwjgl.BufferUtils;
import org.opengis.coverage.grid.GridCoverageReader;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.filter.Filter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashSet;

//import java.awt.*;


/**
 * Created by mjlobo on 21/01/15.
 */
public class GeoToolsAdapter {

    private GridCoverage2DReader reader;
    public GeoToolsAdapter () {

    }

    // Code adapted from http://relativity.net.au/gaming/java/Textures.html#loadTexture2


    public RasterGeoElement getRasterPixelData(String tiffName) {
        File geoTiffImage = new File (tiffName);
        GeoTiffReader reader;
        GridCoverage2D coverage;
        RenderedImage image = null;
        RasterGeoElement rasterGeoElement = null;
        try {
            reader = new GeoTiffReader(geoTiffImage);
            coverage = (GridCoverage2D) reader.read(null);
            if (! (coverage.getRenderedImage().getData().getDataBuffer() instanceof DataBufferByte) ) {
                rasterGeoElement = new MNTRasterGeoElement(coverage, reader.getMetadata().getNoData());
            }
            else {
                rasterGeoElement = new RGBRasterGeoElement(coverage);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rasterGeoElement;
    }

    public RasterGeoElement getRasterMNT (String mntName) {
        File mntFile = new File (mntName);
        GridCoverageReader reader;
        RasterGeoElement result=null;
        GridCoverage2D gc;
        try {
            reader = new ArcGridReader(mntFile);
            try {
                gc = (GridCoverage2D) reader.read(null);
                result = new MNTRasterGeoElement(gc);
                //System.out.println("Grid coverage MNT "+gc.getEnvelope2D().getLowerCorner());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (DataSourceException e) {
            e.printStackTrace();
        }

        return result;

    }

    public SimpleFeatureSource getFeatureSourcePath(String shpPath, String query) {
        SimpleFeatureSource featureSource = null;
        FileDataStore fileDataStore = null;
        File shpFile = new File(shpPath);

        try {
            fileDataStore = FileDataStoreFinder.getDataStore(shpFile);
            featureSource = fileDataStore.getFeatureSource();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //System.out.println("feature source in geotoolsadapter "+featureSource);
        return  featureSource;
    }

    public HashSet<ShapeFileFeature> getPolygons(String shpPath, Texture texture, double alphaRadius){
        //System.out.println("in get Polygons");
        File shpFile = new File (shpPath);
        FileDataStore fileDataStore=null;
        SimpleFeatureSource featureSource=null;
        Geometry geometry;
        SimpleFeature feature;
        HashSet<VectorGeoElement> polygons = new HashSet<VectorGeoElement>();
        ArrayList<LineString> lineStrings = new ArrayList<>();
        HashSet<ShapeFileFeature> shapeFileFeatures = new HashSet<>();
        ArrayList<ShapeFileAttribute> shapeFileAttributes = new ArrayList<>();
        LineStringGraphGenerator lineStringGraphGenerator = new LineStringGraphGenerator();
        FeatureGraphGenerator featureGen = new FeatureGraphGenerator( lineStringGraphGenerator );
        GeometryFactory geometryFactory = new GeometryFactory();
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(new Coordinate[]{new Coordinate(MapLayerViewer.LOWER_CORNER_X,MapLayerViewer.LOWER_CORNER_Y), new Coordinate(MapLayerViewer.LOWER_CORNER_X, MapLayerViewer.LOWER_CORNER_Y+MapLayerViewer.DIMENSION_Y),
                                                                                        new Coordinate(MapLayerViewer.LOWER_CORNER_X+MapLayerViewer.DIMENSION_X,MapLayerViewer.LOWER_CORNER_Y+MapLayerViewer.DIMENSION_Y),new Coordinate(MapLayerViewer.LOWER_CORNER_X+MapLayerViewer.DIMENSION_X, MapLayerViewer.LOWER_CORNER_Y),
                                                                                        new Coordinate(MapLayerViewer.LOWER_CORNER_X,MapLayerViewer.LOWER_CORNER_Y)});
        LinearRing linearRing = new LinearRing(coordinateArraySequence,geometryFactory);
        Polygon bbox = new Polygon(linearRing, new LinearRing[0],geometryFactory);
        //LinearRing linearRing = new LinearRing()
        try {
            fileDataStore = FileDataStoreFinder.getDataStore(shpFile);

            featureSource = fileDataStore.getFeatureSource();
            String filterS = "BBOX(the_geom, "+MapLayerViewer.LOWER_CORNER_X+","+ (MapLayerViewer.LOWER_CORNER_Y+MapLayerViewer.DIMENSION_Y)+","+ (MapLayerViewer.LOWER_CORNER_X+MapLayerViewer.DIMENSION_X)+"," +MapLayerViewer.LOWER_CORNER_Y+")";
            Filter filter = CQL.toFilter(filterS);
            SimpleFeatureCollection features = featureSource.getFeatures(filter);
            SimpleFeatureIterator simpleFeatureIterator = features.features();
            int i =0;
            Geometry polygon;
            Geometry unionPolygon = null;
            Geometry simplifiedPolygon;
            PrecisionModel precisionModel = new PrecisionModel();
            OffsetCurveBuilder offsetCurveBuilder;
            BufferParameters bufferParameters = new BufferParameters();



            try {
                while( simpleFeatureIterator.hasNext()){
                    feature = simpleFeatureIterator.next();
                    ShapeFileFeature shapeFileFeature = new ShapeFileFeature(feature);
                    shapeFileFeatures.add(shapeFileFeature);
                    geometry= (Geometry)feature.getDefaultGeometry();
                    if(geometry instanceof com.vividsolutions.jts.geom.Polygon) {
                        //System.out.println("Yuhu I am a Polygon");
                    }
                    if (geometry instanceof MultiPolygon) {
                        Geometry [] geometries = new Geometry[geometry.getNumGeometries()];
                        java.util.List geometryList = new ArrayList<Geometry>();
                        MultiPolygon multiPolygon = (MultiPolygon)geometry;
                        for (int j=0; j<multiPolygon.getNumGeometries(); j++) {
                            GeometryBuilder geometryBuilder = new GeometryBuilder(geometryFactory);
                            polygon = multiPolygon.getGeometryN(j);
                            Polygon polygonCasted = ((Polygon)polygon);
                            Polygon exteriorPolygon = null;
                            ArrayList<Geometry> holes = new ArrayList<>();
                            if (polygonCasted.getNumInteriorRing()>0) {
                                CoordinateArraySequence coordinateArraySequencePolygon = new CoordinateArraySequence(polygonCasted.getExteriorRing().getCoordinates());
                                LinearRing exteriorRing = new LinearRing(coordinateArraySequencePolygon, geometryFactory);
                                exteriorPolygon= geometryBuilder.polygon(exteriorRing);
                                for (int k=0; k<polygonCasted.getNumInteriorRing(); k++) {
                                    coordinateArraySequencePolygon = new CoordinateArraySequence(polygonCasted.getInteriorRingN(k).getCoordinates());
                                    exteriorRing = new LinearRing(coordinateArraySequencePolygon, geometryFactory);
                                    Polygon holePolygon = geometryBuilder.polygon(exteriorRing);
                                    if (bbox.intersects(holePolygon)) {
                                        if (!bbox.contains(holePolygon)) {
                                            holes.add(IntersectUtils.intersection(holePolygon, bbox));
                                        }
                                        else {
                                            holes.add(holePolygon);
                                        }
                                    }
                                    //holes.add(geometryBuilder.polygon(exteriorRing));
                                }
                            }


                            //System.out.println("polygon class num interior ring"+((Polygon)polygon).getNumInteriorRing());
                            if (bbox.intersects(polygon)) {
                                if (!bbox.contains(polygon)) {
                                    polygon = IntersectUtils.intersection(polygon, bbox);
                                    if (exteriorPolygon != null) {
                                        polygon = IntersectUtils.intersection(exteriorPolygon, bbox);
                                    }

                                }

                                VectorGeoElement newVectorGeoElement;
                                newVectorGeoElement = new VectorGeoElement(polygon, holes, texture, alphaRadius);
                                polygons.add(newVectorGeoElement);
                                shapeFileFeature.addVectorGeoElement(newVectorGeoElement);
                            }
                            if(polygon instanceof MultiPolygon) {
                                //System.out.println("Multi multi polygon!");
                            }


//
                        }

                    }

                    if(geometry instanceof MultiLineString) {
                        geometry= (Geometry)feature.getDefaultGeometry();
                        featureGen.add(feature);
                        if (geometry instanceof MultiLineString) {
                            MultiLineString multiLineString = (MultiLineString) geometry;
                            for(int j=0; j<multiLineString.getNumGeometries(); j++){


                                polygon = multiLineString.getGeometryN(j);
                                offsetCurveBuilder = new OffsetCurveBuilder(precisionModel, bufferParameters);
                                Coordinate[] coordinatesOffseted = offsetCurveBuilder.getLineCurve(polygon.getCoordinates(),5);
                                VectorGeoElement newVectorGeoElement = new VectorGeoElement(coordinatesOffseted, texture, alphaRadius);
                                polygons.add(newVectorGeoElement);
                                shapeFileFeature.addVectorGeoElement(newVectorGeoElement);
                                lineStrings.add((LineString)polygon);

                            }


                        }
                    }
                    if (geometry instanceof Point) {
                        GeometryBuilder geometryBuilder = new GeometryBuilder();
                        polygon = geometryBuilder.circle(((Point) geometry).getX(), ((Point) geometry).getY(),20, 10);
                        VectorGeoElement newVectorGeoElement = new VectorGeoElement(polygon,texture);
                        polygons.add(newVectorGeoElement);
                        newVectorGeoElement.setIsPoint(true);
                        shapeFileFeature.addVectorGeoElement(newVectorGeoElement);
                    }

                    i++;
                }
            }
            finally {
                simpleFeatureIterator.close();
            }
            //System.out.println("number of features: "+features.getSchema().getCoordinateReferenceSystem());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CQLException e) {
            e.printStackTrace();
        }

        return shapeFileFeatures;

    }

    public ArrayList <ShapeFileAttribute> getShapeFileAttributes (String shpPath) {
        File shpFile = new File (shpPath);
        FileDataStore fileDataStore=null;
        ArrayList<ShapeFileAttribute> shapeFileAttributes = new ArrayList<>();
        try {
            fileDataStore = FileDataStoreFinder.getDataStore(shpFile);

            String[] typeNames = fileDataStore.getTypeNames();
            for(int k = 0; k < typeNames.length; k++) {
                String typeName = typeNames[k];
                SimpleFeatureType sft = fileDataStore.getSchema(typeName);
                String longestLocalName = "";
                for (AttributeDescriptor attDesc : sft.getAttributeDescriptors()) {
                    if (attDesc.getType() instanceof AttributeTypeImpl) {
                        Class cl = ((AttributeTypeImpl) attDesc.getType()).getBinding();
                        //System.out.println(attDesc.getLocalName() + ", " + cl);
                        if (attDesc.getLocalName().length() > longestLocalName.length()) {
                            longestLocalName = attDesc.getLocalName();
                        }
                        if (cl.equals(java.lang.String.class)) {
                            //System.out.println(" String");
                            shapeFileAttributes.add(new ShapeFileStringAttribute(attDesc.getName(), attDesc.getLocalName()));
                        } else if (cl.equals(java.lang.Float.class)
                                || cl.equals(java.lang.Double.class)
                                || cl.equals(java.lang.Long.class)
                                || cl.equals(java.lang.Integer.class)) {
                            //System.out.println(" Integer");
                            shapeFileAttributes.add(new ShapeFileNumericAttribute(attDesc.getName(), attDesc.getLocalName()));
                        } else {
                            System.out.println("--> not registered.");
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return shapeFileAttributes;
    }

    public void generateVectorGeoElements(Polygon polygon) {

    }
}
