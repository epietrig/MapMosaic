/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import fr.inria.ilda.ml.DrawnGeoElement;
import fr.inria.ilda.ml.MapLayerViewer;
import fr.inria.ilda.ml.StateMachine.CheckBoxEvent;
import fr.inria.ilda.ml.StateMachine.LayerOrderChangedEvent;
import fr.inria.ilda.ml.StateMachine.StateMachine;
import fr.inria.ilda.ml.VectorGeoElement;
import fr.inria.ilda.ml.VectorSelection;

import javax.swing.*;
import javax.vecmath.Vector3f;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mjlobo on 06/08/15.
 */
public class Inspector extends JPanel {

    java.util.List<String> rasterLayers;
    java.util.List<String> vectorLayers;
    StateMachine stateMachine;
    JPanel optionsPanel;
    List<LayerOptions> layerPanels;
    JCheckBox enablerCheckbox;
    JCheckBox translucentBorderCheckbox;
    JCheckBox dockedCheckbox;
    JCheckBox traceCheckbox;
    MarginTextField alphaTextField;
    ColorCheckbox outlineColorCheckbox;
    StringColorComboPanel targetLayerSelection;
    MarginTextField marginTextField;
    JCheckBox filterCheckbox;
    JLabel selectionDescription;
    JFrame parentJFrame;
    OverlayOptions overlayOptions;
    MarginTextField magicWandTolerance;
    InterpolationOptions interpolationOptions;
    LayerOptions clickedLayerOptions;
    int layerOptionsIndexInConstraints=-1;
    GridBagConstraints gbc;

    public Inspector(java.util.List<String> rasterLayers, java.util.List<String> vectorLayers, JFrame parentJFrame) {
        this.rasterLayers = rasterLayers;
        this.vectorLayers = vectorLayers;
        this.stateMachine = stateMachine;
        //setPreferredSize(new Dimension(300,520));
        this.parentJFrame = parentJFrame;
        layerPanels = new ArrayList<LayerOptions>();
        createLayerPanels();
        gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());


        setBorder(BorderFactory.createEmptyBorder(50,2,2,2));

//        gbc.gridx = 0;
//        gbc.gridy = 0;
//        gbc.gridwidth = 1;
//        gbc.gridheight = 1;
//        gbc.weightx = 0.5;
//        gbc.weighty =0;
//        gbc.fill = GridBagConstraints.HORIZONTAL;
//        gbc.anchor = GridBagConstraints.PAGE_START;
//        add(new Label("Composite Settings", SwingConstants.CENTER),gbc);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.weightx = 0.5;
        gbc.weighty =0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.PAGE_START;
        add(addEnablerCheckbox(),gbc);
        //selectionDescription = new JLabel("Background selected");
        //add(selectionDescription, gbc);
        //selectionDescription.setVisible(false);

        gbc.gridy = 1;
        gbc.weighty=0;
        gbc.anchor = GridBagConstraints.LINE_START;
        add(addCompositeOptions(),gbc);
        //add(Box.createGlue(),gbc);



//        layerPanels.add(lala);
//        gbc.gridy=2;
//        gbc.weighty=1;
//        add(roads,gbc);
//        add(Box.createRigidArea(new Dimension(getWidth(),5)));
//        gbc.gridy =3;
//        //gbc.anchor = GridBagConstraints.PAGE_END;
//        add(buildings,gbc);
//        gbc.gridy=4;
//        add(lala, gbc);
        layerOptionsIndexInConstraints = gbc.gridy +1;
        InspectorDropTargetListener dndListener = new InspectorDropTargetListener(this, gbc, DataFlavor.stringFlavor);
        DragSource ds = new DragSource();
        for (LayerOptions layerOptions: layerPanels ) {
            gbc.gridy +=1;
            gbc.weighty=0;
            add(layerOptions,gbc);
            layerOptions.setIndexInParent(gbc.gridy);
            layerOptions.setVisible(false);
            ds.createDefaultDragGestureRecognizer(layerOptions, DnDConstants.ACTION_COPY, new LayerOptionsTransfer(this));
        }

        gbc.gridy +=1;
        overlayOptions = new OverlayOptions(rasterLayers.subList(0, rasterLayers.size()-1));
        add(overlayOptions, gbc);
        overlayOptions.setVisible(false);

        gbc.gridy+=1;
        magicWandTolerance = new MarginTextField("Magic Wand Tolerance");
        magicWandTolerance.setName(MapLayerViewer.WAND_TOOLERANCE);
        add(magicWandTolerance, gbc);
        magicWandTolerance.setVisible(false);

        gbc.gridy +=1;
        interpolationOptions = new InterpolationOptions(Color.GREEN, Color.RED);
        add(interpolationOptions, gbc);
        interpolationOptions.setVisible(false);


        JPanel emptyPanel = new JPanel();
        gbc.gridy = gbc.gridy +1;
        gbc.weighty = 1;
        add(emptyPanel, gbc);

//        Box verticalBox = Box.createVerticalBox();
//        verticalBox.setAlignmentX(Component.LEFT_ALIGNMENT);
//        add(verticalBox);
//        JCheckBox oli = new JCheckBox("holi");
//        oli.setMaximumSize(new Dimension(300,30));
//        oli.setAlignmentX(Component.LEFT_ALIGNMENT);
//        verticalBox.add(oli);
        //verticalBox.add(new JCheckBox("chau"));





    }



    JPanel addEnablerCheckbox() {
        JPanel enablerPanel = new JPanel();
        enablerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        //enablerPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        enablerCheckbox = new JCheckBox("On/Off");
        enablerCheckbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        enablerCheckbox.setActionCommand(MapLayerViewer.TOGGLE_COMPOSITE);
        enablerPanel.add(enablerCheckbox);
        return enablerPanel;

    }
    JPanel addCompositeOptions() {
        optionsPanel = new JPanel();
        optionsPanel.setAlignmentY(TOP_ALIGNMENT);
        //optionsPanel.setMinimumSize(new Dimension(300,200));
        //optionsPanel.setMaximumSize(new Dimension(300,100));
        //optionsPanel.setPreferredSize(new Dimension(300,100));
//        optionsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        GridBagConstraints gbc = new GridBagConstraints();
        optionsPanel.setLayout(new GridBagLayout());
        //gbc.fill=GridBagConstraints.VERTICAL;

        //gbc.anchor=GridBagConstraints.LINE_START;

        //optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.Y_AXIS));
        //optionsPanel.setLayout(new BorderLayout());
        //optionsPanel.setAlignmentY(Component.TOP_ALIGNMENT);
        //JCheckBox translucentBorderCheckbox = addCheckbox("Translucent Border");
        //translucentBorderCheckbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        //translucentBorderCheckbox.setAlignmentY(Component.TOP_ALIGNMENT);
        //optionsPanel.add(translucentBorderCheckbox, BorderLayout.PAGE_START);
        gbc.gridx = gbc.gridy = 0;
        gbc.gridwidth = GridBagConstraints.REMAINDER;

        gbc.anchor = GridBagConstraints.LINE_START;

        gbc.gridheight = 1;
        gbc.weightx = 0.5;
//        //gbc.weighty = 0.5;
//        JPanel translucentBorderPanel = new JPanel();
//        translucentBorderPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
//
        translucentBorderCheckbox = new JCheckBox("Translucent Border");
        translucentBorderCheckbox.setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 2));
        translucentBorderCheckbox.setActionCommand(MapLayerViewer.TRANSLUCENT_BORDER);
        optionsPanel.add(translucentBorderCheckbox, gbc);
        //optionsPanel.add(addCheckbox("Translucent Border"), gbc);
        //Utils.buildConstraints(gbc, 0,1,1,1,0,0);
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.gridheight = 1;
        gbc.weightx = 0.5;
        gbc.weighty =0;
        gbc.anchor = GridBagConstraints.LINE_START;
        //gbc.fill = GridBagConstraints.BOTH;
        //optionsPanel.add(addCheckbox("waaaaa"), gbc);

        Color defaultOutlineColor = new Color (MapLayerViewer.defaultOutlineLine.x, MapLayerViewer.defaultOutlineLine.y, MapLayerViewer.defaultOutlineLine.z);
        outlineColorCheckbox = new ColorCheckbox("Outline",defaultOutlineColor);
        optionsPanel.add(outlineColorCheckbox,gbc);
        //optionsPanel.add(translucentBorderCheckbox);
        //optionsPanel.setVisible(false);
        //optionsPanel.add(new ColorCheckbox("Outline", Color.CYAN), BorderLayout.PAGE_START);
        // optionsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        //add(optionsPanel);
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.gridheight = 1;
        gbc.weightx = 0.5;
        gbc.weighty =0;
        gbc.anchor = GridBagConstraints.LINE_START;
        marginTextField = new MarginTextField("Margin");
        marginTextField.setName(MapLayerViewer.MARGIN);
        optionsPanel.add(marginTextField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.gridheight = 1;
        gbc.weightx = 0.5;
        gbc.weighty =0;
        gbc.anchor = GridBagConstraints.LINE_START;
        rasterLayers.add(MapLayerViewer.FILL_COLOR);
        Color defaultFillColor = new Color (MapLayerViewer.defaultFillColor.x, MapLayerViewer.defaultFillColor.y, MapLayerViewer.defaultFillColor.z);
        targetLayerSelection = new StringColorComboPanel("Target Layer: ", rasterLayers, defaultFillColor);
        optionsPanel.add(targetLayerSelection, gbc);

        gbc.gridy = 4;
        alphaTextField = new MarginTextField("Opacity: ");
        alphaTextField.setName(MapLayerViewer.ALPHA);
        optionsPanel.add(alphaTextField, gbc);

        gbc.gridy=5;
        dockedCheckbox = new JCheckBox("Docked");
        dockedCheckbox.setActionCommand(MapLayerViewer.DOCKED);
        dockedCheckbox.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 2));
        optionsPanel.add(dockedCheckbox, gbc);


        gbc.gridy = 6;
        filterCheckbox = new JCheckBox("Apply Filter");
        filterCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JCheckBox checkBox = (JCheckBox)e.getSource();
                for (LayerOptions layerOptions: layerPanels) {
                    layerOptions.reset(targetLayerSelection.getStringSelectedItem(), targetLayerSelection.getColor());
                }
                if (checkBox.isSelected()) {
                    for (LayerOptions layerOptions : layerPanels) {
                        layerOptions.setVisible(true);
                    }
                }
                else {
                    for (LayerOptions layerOptions : layerPanels) {
                        layerOptions.setVisible(false);
                    }
                }
                //parentJFrame.pack();
            }
        });
        filterCheckbox.setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 2));
        //optionsPanel.add(filterCheckbox, gbc);

        gbc.gridy=6;
        traceCheckbox = new JCheckBox("Leave trace");
        traceCheckbox.setActionCommand(MapLayerViewer.LEAVE_TRACE);
        traceCheckbox.setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 2));
        optionsPanel.add(traceCheckbox, gbc);




        optionsPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        //optionsPanel.setVisible(false);
        optionsPanel.setVisible(true);
        optionsPanel.setEnabled(false);
        return optionsPanel;
    }



    JPanel addCheckbox (String text) {
        JPanel checkboxPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JCheckBox checkBox = new JCheckBox(text);
        checkboxPanel.add(checkBox);
        //checkBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        //checkBox.setBorder(new EmptyBorder(2,0,2,0));
        //add(checkBox);
        return checkboxPanel;
    }


    void createLayerPanels() {
        for (String vectorLayer: vectorLayers) {
            final LayerOptions layerOptions = new LayerOptions(vectorLayer, rasterLayers);
            layerOptions.setName(vectorLayer);
            layerPanels.add(layerOptions);

        }
    }


    public void update(DrawnGeoElement drawnGeoElement, String fill, List<VectorSelection> selectionsDrawn, float alpha) {
        overlayOptions.setVisible(false);
        magicWandTolerance.setVisible(false);
        interpolationOptions.setVisible(false);
        showOptionsPanelDrawn(true);
        setLayerPanelsVisible(true);
        enablerCheckbox.setVisible(true);
        traceCheckbox.setVisible(true);
        resetOptions();
        Color outlineColor = new Color(drawnGeoElement.getOutlineColor().x, drawnGeoElement.getOutlineColor().y, drawnGeoElement.getOutlineColor().z);
        Color fillColor = new Color(drawnGeoElement.getFillColor().x, drawnGeoElement.getFillColor().y, drawnGeoElement.getFillColor().z);
        //System.out.println("fill color "+fillColor);
        enablerCheckbox.setSelected(drawnGeoElement.getIsComposite());

        optionsPanel.setVisible(true);
        setLayerPanelsVisible(true);
        for (LayerOptions layerOptions: layerPanels) {
            //layerOptions.reset(drawnGeoElement);
            layerOptions.reset(fill, fillColor);
        }
        if (!drawnGeoElement.getIsComposite()) {
           // optionsPanel.setVisible(false);
            enableOptionsPanel(false);
            setTitleCheckboxesEnabled(false);
            //setLayerPanelsVisible(false);
        }
        else {
            //optionsPanel.setVisible(true);
            enableOptionsPanel(true);
            translucentBorderCheckbox.setVisible(true);
            translucentBorderCheckbox.setSelected(drawnGeoElement.getTranslucentBorder());
//            Color outlineColor = new Color(drawnGeoElement.getOutlineColor().x, drawnGeoElement.getOutlineColor().y, drawnGeoElement.getOutlineColor().z);
//            Color fillColor = new Color(drawnGeoElement.getFillColor().x, drawnGeoElement.getFillColor().y, drawnGeoElement.getFillColor().z);
            outlineColorCheckbox.setVisible(true);
//            outlineColorCheckbox.update(drawnGeoElement.getOutlineVisible(), outlineColor);
//            targetLayerSelection.update(fill, fillColor);
            marginTextField.setVisible(true);
            dockedCheckbox.setVisible(true);
            alphaTextField.setEnabled(true);
            dockedCheckbox.setSelected(drawnGeoElement.getIsDocked());
            traceCheckbox.setSelected(drawnGeoElement.getLeavingTrace());


//            marginTextField.updateText(""+drawnGeoElement.getBuffer());
            if (selectionsDrawn!=null && selectionsDrawn.size()>0) {

                for (VectorSelection vectorSelection: selectionsDrawn) {
                    LayerOptions layerOptions = getLayerOptionsbyLayerName(vectorSelection.getLayer().getName());
                    //System.out.println("layerOptions "+ layerOptions);
                    //layerOptions.setVisible(true);
                    setLayerPanelsVisible(true);
                    layerOptions.update(vectorSelection);
                    //getLayerOptionsbyLayerName(vectorSelection.getLayer().getName()).update(vectorSelection);
                }
            }
            else {
                if (drawnGeoElement.getIsComposite()) {
                    for (LayerOptions layerOptions : layerPanels) {
                        layerOptions.setEnabledTitle(true);
                    }
                }
                else {
                    for (LayerOptions layerOptions : layerPanels) {
                        layerOptions.setEnabledTitle(false);
                    }
                }
                //setLayerPanelsVisible(false);
                filterCheckbox.setSelected(false);
                //setTitleCheckboxesEnabled(false);
            }
        }
        //Color outlineColor = new Color(drawnGeoElement.getOutlineColor().x, drawnGeoElement.getOutlineColor().y, drawnGeoElement.getOutlineColor().z);
        //Color fillColor = new Color(drawnGeoElement.getFillColor().x, drawnGeoElement.getFillColor().y, drawnGeoElement.getFillColor().z);
        //System.out.println("updating outline color "+outlineColor);
        outlineColorCheckbox.update(drawnGeoElement.getOutlineVisible(), outlineColor);
        //outlineColorCheckbox.update(drawnGeoElement.getOutlineVisible(), outlineColor);
        targetLayerSelection.update(fill, fillColor);
        marginTextField.updateText("" + drawnGeoElement.getBuffer());
        alphaTextField.setVisible(true);
        alphaTextField.updateText(""+alpha);
        //parentJFrame.pack();
        revalidate();


    }

    public void updateVectorGeoElement(VectorGeoElement selectedVectorGeoElement) {
        overlayOptions.setVisible(false);
        enablerCheckbox.setVisible(true);
        enablerCheckbox.setSelected(false);
        magicWandTolerance.setVisible(false);
        interpolationOptions.setVisible(false);
//       optionsPanel.setVisible(false);
        enableOptionsPanel(false);
        showOptionsPanelDrawn(true);
        setLayerPanelsVisible(true);
        setTitleCheckboxesEnabled(false);
        resetOptions();
        for (LayerOptions layerOptions: layerPanels) {
            //layerOptions.reset(drawnGeoElement);
            Color fillColor = new Color(selectedVectorGeoElement.getFillColor().x, selectedVectorGeoElement.getFillColor().y, selectedVectorGeoElement.getFillColor().z);
            layerOptions.reset(selectedVectorGeoElement.getTexture().getName(), fillColor);
        }

//        translucentBorderCheckbox.setVisible(false);
//        marginTextField.setVisible(false);
//        filterCheckbox.setVisible(false);
//        targetLayerSelection.setEnabled(true);
//        outlineColorCheckbox.setVisible(true);
//        outlineColorCheckbox.setEnabled(true);
//        Color fillColor = new Color(selectedVectorGeoElement.getFillColor().x, selectedVectorGeoElement.getFillColor().y, selectedVectorGeoElement.getFillColor().z);
//        if (selectedVectorGeoElement.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT || selectedVectorGeoElement.getRenderingType()== GLUtilities.RenderedElementTypes.FILLED) {
//            targetLayerSelection.update(MapLayerViewer.FILL_COLOR, fillColor);
//        }
//        else {
//            targetLayerSelection.update(selectedVectorGeoElement.getTexture().getName(), fillColor);
//        }
//
//        setLayerPanelsVisible(false);
        //parentJFrame.pack();

    }

    public LayerOptions getLayerOptionsbyLayerName(String layerName) {
        for (LayerOptions layerOptions: layerPanels) {
            if (layerOptions.getLayerName().equals(layerName)) {
                //System.out.println("Layer Options name "+layerOptions.getName());
                return layerOptions;
            }
        }
        return null;
    }

    public void backgroundSelected(List<VectorSelection> backgroundSelections, String fill, Vector3f color) {
        overlayOptions.setVisible(false);
        magicWandTolerance.setVisible(false);
        interpolationOptions.setVisible(false);
        //overlayOptions.setEnabled(true);
        enableOptionsPanel(true);
        enablerCheckbox.setSelected(false);
        enablerCheckbox.setVisible(false);
        optionsPanel.setVisible(true);
        translucentBorderCheckbox.setVisible(false);
        outlineColorCheckbox.setVisible(false);
        marginTextField.setVisible(false);
        dockedCheckbox.setVisible(false);
        traceCheckbox.setVisible(false);
        alphaTextField.setVisible(false);
        alphaTextField.setEnabled(false);
        setLayerPanelsVisible(true);
        setTitleCheckboxesEnabled(true);
        resetOptions();

        //Color outlineColor = new Color(drawnGeoElement.getOutlineColor().x, drawnGeoElement.getOutlineColor().y, drawnGeoElement.getOutlineColor().z);
        Color fillColor = new Color(color.x, color.y, color.z);
        targetLayerSelection.update(fill, fillColor);

        for (LayerOptions layerOptions: layerPanels) {
            //layerOptions.reset(drawnGeoElement);
            layerOptions.reset(fill, fillColor);
        }

        if (backgroundSelections!=null && backgroundSelections.size()>0) {

            for (VectorSelection vectorSelection: backgroundSelections) {
                LayerOptions layerOptions = getLayerOptionsbyLayerName(vectorSelection.getLayer().getName());
                //System.out.println("layerOptions "+ layerOptions);
                //layerOptions.setVisible(true);
                setLayerPanelsVisible(true);
                layerOptions.update(vectorSelection);
                //getLayerOptionsbyLayerName(vectorSelection.getLayer().getName()).update(vectorSelection);
            }
        }
        else {
            //setLayerPanelsVisible(false);
            //filterCheckbox.setSelected(false);
        }
        //selectionDescription.setVisible(true);

        //setLayerPanelsVisible(true);
        revalidate();

    }

    public void notShow() {
        magicWandTolerance.setVisible(false);
        overlayOptions.setVisible(false);
        enablerCheckbox.setVisible(false);
        optionsPanel.setVisible(false);
        interpolationOptions.setVisible(false);
        setLayerPanelsVisible(false);
        //parentJFrame.pack();
    }

    public void resetOptions() {
        enablerCheckbox.setSelected(false);
        alphaTextField.updateText("" + 1.0);
        translucentBorderCheckbox.setSelected(false);
        traceCheckbox.setSelected(false);
        Color defaultOutline = new Color(MapLayerViewer.defaultOutlineLine.x, MapLayerViewer.defaultOutlineLine.y, MapLayerViewer.defaultOutlineLine.z);
        Color defaultFill = new Color(MapLayerViewer.defaultFillColor.x, MapLayerViewer.defaultFillColor.y, MapLayerViewer.defaultFillColor.z);
        outlineColorCheckbox.update(false, defaultOutline);
        dockedCheckbox.setSelected(false);
        targetLayerSelection.update(rasterLayers.get(0), defaultFill);
        marginTextField.updateText(""+0.0);



    }


    public void addListeners(final StateMachine stateMachine) {
        enablerCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JCheckBox source = (JCheckBox)e.getSource();
                if (source.isSelected()) {
                    //optionsPanel.setEnabled(true);
                    enableOptionsPanel(true);
                    setTitleCheckboxesEnabled(true);
                }
                else {
                    enableOptionsPanel(false);
                    setTitleCheckboxesEnabled(false);
                    //optionsPanel.setEnabled(false);
                }
                stateMachine.handleEvent(new CheckBoxEvent(enablerCheckbox.getText(), enablerCheckbox));
            }
        });
        translucentBorderCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new CheckBoxEvent(translucentBorderCheckbox.getText(), translucentBorderCheckbox));
            }
        });
        outlineColorCheckbox.getCheckBox().setActionCommand(MapLayerViewer.OUTLINE);
        outlineColorCheckbox.addListeners(stateMachine);
        targetLayerSelection.addListeners(stateMachine, parentJFrame);
        marginTextField.addListeners(stateMachine);
        alphaTextField.addListeners(stateMachine);
        for (LayerOptions layerOption: layerPanels) {
            layerOption.addListeners(stateMachine, parentJFrame);
        }
        dockedCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new CheckBoxEvent(dockedCheckbox.getText(), dockedCheckbox));
            }
        });

        traceCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new CheckBoxEvent(traceCheckbox.getText(), traceCheckbox));
            }
        });

        overlayOptions.addListeners(stateMachine);
        magicWandTolerance.addListeners(stateMachine);
        interpolationOptions.addListeners(stateMachine);

    }

    public void updateOverlay(HashMap<String, Double> alphasOverlay) {
        magicWandTolerance.setVisible(false);
        interpolationOptions.setVisible(false);
        overlayOptions.setVisible(true);
        setLayerPanelsVisible(false);
        optionsPanel.setVisible(false);
        enablerCheckbox.setVisible(false);
        overlayOptions.update(alphasOverlay, parentJFrame);
    }

    public void updateMagicWand(float tolerance) {
        setLayerPanelsVisible(false);
        interpolationOptions.setVisible(false);
        optionsPanel.setVisible(false);
        enablerCheckbox.setVisible(false);
        magicWandTolerance.setVisible(true);
        magicWandTolerance.updateText(""+tolerance);
    }

    public void updateInterpolation(Color startColor, Color endColor, String interpolationText) {
        magicWandTolerance.setVisible(false);
        overlayOptions.setVisible(false);
        setLayerPanelsVisible(false);
        optionsPanel.setVisible(false);
        enablerCheckbox.setVisible(false);
        interpolationOptions.setVisible(true);
        interpolationOptions.update(startColor, endColor, interpolationText);
    }

    public void enableOptionsPanel (boolean enabled) {
        translucentBorderCheckbox.setEnabled(enabled);
        outlineColorCheckbox.setEnabled(enabled);
        marginTextField.setEnabled(enabled);
        targetLayerSelection.setEnabled(enabled);
        alphaTextField.setEnabled(enabled);
        dockedCheckbox.setEnabled(enabled);
        filterCheckbox.setEnabled(enabled);
        traceCheckbox.setEnabled(enabled);

    }

    public void showOptionsPanelDrawn(boolean visible) {
        optionsPanel.setVisible(true);
        translucentBorderCheckbox.setVisible(visible);
        outlineColorCheckbox.setVisible(visible);
        marginTextField.setVisible(visible);
        targetLayerSelection.setVisible(visible);
        alphaTextField.setVisible(visible);
        dockedCheckbox.setVisible(visible);
        filterCheckbox.setVisible(visible);
    }

    void setLayerPanelsVisible(boolean visible) {
        for (LayerOptions layerOptions : layerPanels) {
            layerOptions.setVisible(visible);
        }
    }

    void setTitleCheckboxesEnabled(boolean enabled) {
        for (LayerOptions layerOptions : layerPanels) {
            layerOptions.setTitleCheckboxEnabled(enabled);
        }
    }

    public InterpolationOptions getInterpolationOptions() {
        return interpolationOptions;
    }




    public LayerOptions getLayerOptionsAt(Point p) {
        for (LayerOptions layerOptions : layerPanels) {
            if (layerOptions.getBounds().contains(p)) {
                return layerOptions;
            }
        }
        return null;
    }


    public void swapLayerOptions(LayerOptions movingLayerOptions, LayerOptions destionationLayerOptions) {
        for (LayerOptions layerOptions : layerPanels) {
            layerOptions.remove(layerOptions);
        }
        int index = layerPanels.indexOf(destionationLayerOptions);
        layerPanels.remove(movingLayerOptions);
        layerPanels.add(index, movingLayerOptions);
        int indexInGbc = layerOptionsIndexInConstraints;
        gbc.gridy = indexInGbc;
        for (LayerOptions layerOptions : layerPanels) {
            add(layerOptions, gbc);
            gbc.gridy +=1;
        }
        stateMachine.handleEvent(new LayerOrderChangedEvent(movingLayerOptions.getLayerName(), destionationLayerOptions.getLayerName()));

    }

    public void setStateMachine (StateMachine stateMachine) {
        this.stateMachine = stateMachine;
    }







}
