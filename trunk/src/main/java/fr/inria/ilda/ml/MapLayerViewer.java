/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.FPSAnimator;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;
import fr.inria.ilda.ml.StateMachine.*;
import fr.inria.ilda.ml.StateMachine.Event;
import fr.inria.ilda.ml.canvasEvents.CanvasDrawnEvent;
import fr.inria.ilda.ml.canvasEvents.CanvasEvent;
import fr.inria.ilda.ml.canvasEvents.CanvasMNTEvent;
import fr.inria.ilda.ml.canvasEvents.CanvasVectorSelectionsEvent;
import fr.inria.ilda.ml.gl.PointGL;
import fr.inria.ilda.ml.inspector.Inspector;
import fr.inria.ilda.ml.magicWand.MagicWand;
import fr.inria.ilda.ml.magicWand.Pixel;
import fr.inria.ilda.ml.queryWidgets.ShapeFileTableSummary;
import org.apache.commons.io.FilenameUtils;
import org.geotools.geometry.jts.GeometryBuilder;


import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.awt.*;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import java.util.List;

/**
 * Created by mjlobo on 26/11/14.
 */
public class MapLayerViewer {

    GeoToolsAdapter geoToolsAdapter;
    static int SCREEN_WIDTH =  1280;
    static int SCREEN_HEIGHT =  720;
    static double LOWER_CORNER_X = 710000;
    static double LOWER_CORNER_Y = 1615000;
    static double DIMENSION_X = 10000;
    static double DIMENSION_Y = 10000; //how many km in one raster quad
    static int SCENE_PIXELS_WIDTH;
    static int SCENE_PIXELS_HEIGHT;

    //Colors
    static Vector3f selectionColor = new Vector3f (0.79f,0f,0.125f);
    static Vector3f inColorDrawn = new Vector3f(1.0f,0.5f, 0f);
    static Vector3f inColorVector = new Vector3f(0.60f, 0.30f, 0.64f);
    static Vector3f areaBaseBoderColor = new Vector3f(0.1f,0.1f,0.1f);
    public static Vector3f defaultOutlineLine = new Vector3f(0f,1.0f,1.0f);
    public static Vector3f defaultFillColor = new Vector3f(1.0f,1.0f,1.0f);
    public static Vector3f selectionColorOut = new Vector3f(1.0f,1.0f,1.0f);
    public static Vector3f selectionColorIn = new Vector3f(0.0f,0.0f,0.0f);
    public static Vector3f traceColor = new Vector3f(1.0f,1.0f,0.2f);
    public static Vector3f inAreaColor = new Vector3f(1.0f,1.0f,1.0f);
    public static float inAreaAlpha=0.3f;


    static float areaBaseLineWidth = 1.0f;
    static float selectionLineWidth = 2.0f;
    static float selectionOutLineWidth = 5.0f;
    static float outlineLineWidth = 4.0f;
//    static float PIXELS_PER_UNIT = 0.4f;
    static float NTILES = 1;
    MapGLCanvas canvas;
    ArrayList <Layer> layers ;
    HashMap<RasterGeoElement, Texture> textures;
    HashMap<Texture, RasterGeoElement> rasters;
    ArrayList<String> rasterNames;
    Vector3d eyeInit;
    Vector3d centerInit;
    private Layer backgroundLayer;
    public final JFrame frame = new JFrame();

    DrawnGeoElement currentDrawingGeoElement;
    GeoElement currentMovingGeoElement;
    DrawnLayer drawnLayer;
    boolean drawnLayerChanged;
    static boolean isRetina;

    GeoElement selected = null;
    Layer selectedLayer = null;
    Layer querySelectedLayer = null;

    StateMachine stateMachine;
    RasterGeoElement reference = null;

    static Texture baseTexture;

    double startPoint[] = new double[2];
    double drawnOrigin[];
    int [] startPointPx = new int[2];

    double [] sceneBounds;
    double scene_pixels_per_unit=-1;
    ArrayList <VectorSelection> vectorSelections = new ArrayList<>();

    double maxAlphaRadius = 20;
    double maxBorderOffset;

    PointGL[] alignPoints = new PointGL[6];
    int alignIndex = 0;
    Layer layerToAlign;
    JTabbedPane jtp;
    ArrayList <ShapeFileTableSummary> shapeFileTableSummaries;
    JFrame shapeFileQuery;
    ModeToolBar modeToolBar;
    ModeToolBar rasterToolBar;
    fr.inria.ilda.ml.inspector.Inspector inspector;

    SelectionManager selectionManager;
    HashMap<String, JCheckBox> layerCheckboxs;

    AlignementManager alignementManager;
    PopUpMenu popUpMenu;

    Texture backgroundTexture;
    int[] popUpMenuPos = new int[2];
    VectorSelection selectedVectorSelection;

    java.util.List<VectorSelection> selectionsDrawn;

    GeoElement inElement;
    VectorSelection inSelection;
    Layer inLayer;
    java.util.List<GeoElement> selectedElements;

    double initialRadius = 5;
    int nbSlicesCircle = 40;

    float traceAlpha=0.5f;

    protected java.util.List<String> actions;

    protected HashMap<String, Double> rasterOverlayAlphas;
    protected boolean overlay = false;

    protected Cursor drawCursor;
    protected Cursor circleCursor;
    protected Cursor navigationCursor;
    protected Cursor squareCursor;
    protected Cursor magicWandCursor;

    public static String LINEAR_INTERPOLATION = "Linear";
    public static String SIGMOID_INTERPOLATION = "Sigmoid";

    //Modes
    static String SELECT_MODE = "select";
    static String DRAW_MODE = "lasso";
    static String NAVIGATION_MODE = "navigate";
    static String CREATE_CIRCLE = "circle";
    static String CREATE_SQUARE = "square";
    static String MAGIC_WAND ="magicwand";
    static String COMPOSITE = "Composite";
    static String OVERLAY = "Overlay";
    static String SEPARATOR = "Separator";
    //Actions
    static String BUFFER_PLUS = "Positive Buffer (B)";
    static String BUFFER_MINUS = "Negative Buffer (V)";
    public static String TRANSLUCENT_BORDER = "Toggle Translucent Border";
    public static String OUTLINE = "Toggle outline";
    public static String FILL_COLOR = "Color fill";
    public static String TOGGLE_COMPOSITE = "Toggle Composite";
    public static String DOCKED = "Docked";
    public static String ALPHA = "alpha";
    public static String MARGIN = "margin";
    public static String WAND_TOOLERANCE="wandTolerance";
    public static String LEAVE_TRACE = "leaveTrace";
    public static String INTERPOLATION_START = "interpolationStart";
    public static String INTERPOLATION_END = "interpolationEnd";
    public static String COPY_STYLE = "Copy style";
    public static String PASTE_STYLE = "Paste style";
    ArrayList<String> pointActions;

    float magicWandTolerance = 0.15f;

    static String getScreenQuery() {
        return "BBOX(the_geom, "+MapLayerViewer.LOWER_CORNER_X+","+ (MapLayerViewer.LOWER_CORNER_Y+MapLayerViewer.DIMENSION_Y)+","+ (MapLayerViewer.LOWER_CORNER_X+MapLayerViewer.DIMENSION_X)+"," +MapLayerViewer.LOWER_CORNER_Y+")";
    }



    protected ArrayList<String> modes;
    Texture previewingTexture = null;
    JFrame inspectorFrame;
    JPanel canvasPanel;

    RenderingFeatures copiedRenderingFeatures;
    GeoElement copiedElement;

    public MapLayerViewer(MLOptions options) {
        isRetina = options.isRetina;
        geoToolsAdapter = new GeoToolsAdapter();
        selectionManager = new SelectionManager(this);
        alignementManager = new AlignementManager(this);
        layers = new ArrayList<Layer>();
        textures = new HashMap<>();
        rasters = new HashMap<>();
        selectedElements = new ArrayList<>();
        modes = new ArrayList<>();
        rasterOverlayAlphas = new HashMap<>();
        rasterNames = new ArrayList<>();
        modes.add(SELECT_MODE);
        modes.add(NAVIGATION_MODE);
        modes.add(SEPARATOR);
        modes.add(DRAW_MODE);
        modes.add(CREATE_CIRCLE);
        modes.add(CREATE_SQUARE);
        modes.add(MAGIC_WAND);


        Layer scan;
        if (options.martinique) {
            if (options.difference.equals("")) {
                scan = initRasterLayers("maps/diffbat/Scan_25_Little3.tif", true, "Scan");
                initRasterLayers("maps/diffbat//Ortho_Little2.tif", false, "Ortho");
                initVectorLayers(false, "maps/diffbat/mapcompshp/originals/ROUTE_PRIMAIRE.SHP", "Roads");
                initVectorLayers(false, "maps/diffbat/mapcompshp/originals/BATI_INDUSTRIEL.SHP", "Buildings");
                initVectorLayers(false, "maps/diffbat/mapcompshp/Shape_BuildingsAlongARoad/Shape_BuildingsAlongARoad.shp", "Buildings and roads" );
            }
            else {
                if (options.difference.equals("missing_road_2_shp")) {
                    scan = initRasterLayers("maps/diffbat/Scan_25_missing_road_2.tif", true, "Scan");
                    initRasterLayers("maps/diffbat//Ortho_Little2.tif", false, "Ortho");
                    initVectorLayers(false, "maps/diffbat/mapcompshp/originals/ROUTE_PRIMAIRE.SHP", "Primary Roads");
                    initVectorLayers(false, "maps/diffbat/mapcompshp/originals/route_secondaires_importantes.SHP", "Secondary Roads");

                } else {
                    scan = initRasterLayers("maps/diffbat/Scan_25_" + options.difference + ".tif", true, "Scan");
                    initRasterLayers("maps/diffbat//Ortho_Little2.tif", false, "Ortho");
                    String entity = "";
                    if (options.difference.contains("road")) {
                        entity = "Roads";
                    } else if (options.difference.contains("building")) {
                        entity = "Buildings";
                    }
                    initVectorLayers(false, "maps/diffbat/mapcompshp/originals/ROUTE_PRIMAIRE.SHP", "Primary Roads");
                    initVectorLayers(false, "maps/diffbat/mapcompshp/" + options.difference + "/" + options.difference + ".shp", "Secondary roads");
                }
            }


        }
        else if (options.martinique_complete) {
            scan = initRasterLayers("maps/diffbat/Scan_25_Little3.tif", true, "Scan");
            initRasterLayers("maps/diffbat//Ortho_Little2.tif", false, "Ortho");
            initVectorLayers(false, "maps/diffbat/mapcompshp/originals/ROUTE_PRIMAIRE.SHP", "Roads");
            initVectorLayers(false, "maps/diffbat/mapcompshp/originals/BATI_INDUSTRIEL.SHP", "Buildings");
            initVectorLayers(false, "maps/diffbat/mapcompshp/originals/SURFACE_EAU.SHP", "Water");
            initVectorLayers(false, "maps/diffbat/mapcompshp/originals/ZONE_VEGETATION.SHP", "Vegetation");
        }
        else if (options.trekking) {

            scan = initRasterLayers("maps/trekking/scan_trekking2.tif", true, "Scan");
            initRasterLayers("maps/trekking/ortho_trekking3.tif", false, "Ortho");
            initRasterLayers("maps/trekking/MNT_trekking2.tif", false, "Altitude");
            initVectorLayers(false, "maps/trekking/shp/ROUTE.SHP", "Roads");
            initVectorLayers(false, "maps/trekking/shp/CHEMIN.SHP", "Chemin");
            initVectorLayers(false, "maps/trekking/shp/Commune_Basse_Mer.SHP", "Commune");

        }
        else if (options.tsunami) {
            scan = initRasterLayers("maps/tsunami/scan_tsunami-2.tif", true, "Scan");
            initRasterLayers("maps/tsunami/ortho_tsunami3.tif", false, "Ortho");
            initRasterLayers("maps/tsunami/mnt_1m_sta-2.tif", false, "Altitude");
            initRasterLayers("maps/tsunami/zi_surcote-2.tif", false, "Simulation", new Vector3f(0.48f,0.93f,0.92f), new Vector3f(0.14f,0.18f,0.49f));

            initRasterLayers("maps/tsunami/popNuit.tif", false, "Population", new Vector3f(0.99f,0.94f,0.85f), new Vector3f(0.7f,0.0f,0.0f));

            initVectorLayers(false,"maps/tsunami/3_Zonage_Evacuation/zone_evacuer_sainteAnne-2.shp", "Evacuation zone");

            initVectorLayers (false, "maps/tsunami/shp/BATI_INDIFFERENCIE.SHP", "Buildings");
            initVectorLayers(false,"maps/tsunami/2_Infastructures_Critiques/Etablissements_Scolaires.shp", "Critical Infrastructures", new Vector3f(1.0f,0.2f,0.0f));
            initVectorLayers(false, "maps/tsunami/shp/ROUTE.SHP", "Roads");


        }
        else if(options.scan) {
            scan = initRasterLayers("maps/scan/SCEXP25_ST_0340_6260_L93.tif", true, "Legend 1");
            initRasterLayers("maps/scan/SC25_TOUR_0340_6260_L93.tif", false, "Legend 2");
            initRasterLayers("maps/scan/SCEXP25_CL_0340_6260_L93.tif", false, "Legend 3");


        }
        else if (!options.folder.equals("")) {
            File folder = new File(options.folder);
            File[] listOfFiles = folder.listFiles();
            List<File> tifFiles = new ArrayList<>();
            List<File> shpFiles = new ArrayList<>();
            scan = null;
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    //System.out.println("File " + listOfFiles[i].getName());
                    String ext = FilenameUtils.getExtension(listOfFiles[i].getAbsolutePath());
                    if (ext.equals("tif") || (ext.equals("TIF") || (ext.equals("tiff") || (ext.equals("TIFF"))))) {
                        tifFiles.add(listOfFiles[i]);

                    }
                    if (ext.equals("shp") || ext.equals("SHP")) {
                        shpFiles.add(listOfFiles[i]);
                    }
                }
            }
            if (tifFiles.size() > 0) {
                for (File file : tifFiles) {
                    if (scan == null) {
                        scan = initRasterLayers(file.getAbsolutePath(), true, file.getName());
                    } else {
                        initRasterLayers(file.getAbsolutePath(), true, file.getName());
                    }
                }
                for (File file : shpFiles) {
                    initVectorLayers(false, file.getAbsolutePath(), file.getName());
                }

            }
        }

        else {
            scan = null;
        }

        if (scan !=null ) {
            setBackgroundLayer(scan);
            drawnLayer = new DrawnLayer(false);
            layers.add(selectionManager.getBackgroundSelectionLayer());
            layers.add(drawnLayer);
            initGL();
            initInspector();
            createCursors();
            initUI();
            initStateMachine();
        }



    }


    void initGL() {
        final GLCapabilities glCapabilities = new GLCapabilities(GLProfile.get(GLProfile.GL4));
        glCapabilities.setStencilBits(8);
        canvas = new MapGLCanvas(this,glCapabilities);
        initCameraParameters();

        // Create a animator that drives canvas' display() at the specified FPS.
        final FPSAnimator animator = new FPSAnimator(canvas, 120, true);

        // Create the top-level container
        // Swing's JFrame or AWT's Frame
        canvasPanel = new JPanel(new BorderLayout());
        frame.setLayout(new BorderLayout());

        canvasPanel.add(canvas);
        frame.add(canvasPanel);
        frame.setPreferredSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                // Use a dedicate thread to run the stop() to ensure that the
                // animator stops before program exits.
                new Thread() {
                    @Override
                    public void run() {
                        if (animator.isStarted()) animator.stop();
                        System.exit(0);
                    }
                }.start();
            }
        });
        frame.setTitle("MapMosaic");
        modeToolBar = new ModeToolBar(modes);
        canvasPanel.add(modeToolBar, BorderLayout.PAGE_START);
        ArrayList<String> rasterNames = new ArrayList<>();
        rasterNames.add(COMPOSITE);
        for (Texture texture: textures.values()) {
            rasterNames.add(texture.getName());
        }
        rasterNames.add(OVERLAY);
        rasterToolBar = new ModeToolBar(rasterNames);
        canvasPanel.add(rasterToolBar, BorderLayout.PAGE_END);
        frame.pack();
        frame.setVisible(true);

        animator.start();
    }

    void initUI() {
        shapeFileTableSummaries = new ArrayList<>();
        layerCheckboxs = new HashMap<>();
        shapeFileQuery = new JFrame();
        shapeFileQuery.setTitle("Vector shapes");
        jtp = new JTabbedPane();
        shapeFileQuery.getContentPane().add(jtp);
        JPanel all = new JPanel();
        all.setName("all");
        all.setLayout(new BoxLayout(all, BoxLayout.Y_AXIS));
        for(Layer layer: layers) {
            if (layer instanceof VectorLayer) {
                final JCheckBox jCheckBox = new JCheckBox(((VectorLayer) layer).getName());
                layerCheckboxs.put(((VectorLayer) layer).getName(),jCheckBox);
                jCheckBox.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        stateMachine.handleEvent(new CheckBoxEvent(jCheckBox.isSelected(), jCheckBox.getText()));
                    }
                });
                all.add(jCheckBox);
                ShapeFileTableSummary shapeFileTableSummary = new ShapeFileTableSummary((VectorLayer)layer);
                shapeFileTableSummaries.add(shapeFileTableSummary);
                JScrollPane scrollPane = new JScrollPane(shapeFileTableSummary);
                scrollPane.setName(shapeFileTableSummary.getName());
                jtp.add(((VectorLayer) layer).getName(), scrollPane);


            }
        }


        shapeFileQuery.getRootPane().putClientProperty("Window.style", "small");
        shapeFileQuery.setAlwaysOnTop(true);
        shapeFileQuery.pack();
        shapeFileQuery.setVisible(false);

        actions = new ArrayList<>();
        actions.add(COPY_STYLE);
        actions.add(PASTE_STYLE);
        popUpMenu = new PopUpMenu(actions);
        JPopupMenu jPopupMenu = new JPopupMenu();
        jPopupMenu.add(new JMenuItem(COPY_STYLE));
        jPopupMenu.add(new JMenuItem(PASTE_STYLE));



    }

    public void initInspector() {
        java.util.List<String> rasterElements = new ArrayList<>();
        java.util.List<String> vectorLayers = new ArrayList<>();
        for (String rasterName: rasterNames) {
            rasterElements.add(rasterName);
            rasterOverlayAlphas.put(rasterName, 1.0);
        }
        for (Layer layer: layers) {
            if (layer instanceof VectorLayer) {
                vectorLayers.add(0,((VectorLayer) layer).getName());
            }
        }
        inspector = new fr.inria.ilda.ml.inspector.Inspector (rasterElements, vectorLayers, frame);
        Border emptyBorder = BorderFactory.createEmptyBorder(70,0,0,0);
        TitledBorder titledBorder = BorderFactory.createTitledBorder(emptyBorder, "Composite Settings", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION);
        inspector.setBorder(titledBorder);
        JScrollPane scrollPane = new JScrollPane(inspector);
        scrollPane.createVerticalScrollBar();
        inspector.notShow();
        scrollPane.setPreferredSize(new Dimension(300, SCREEN_HEIGHT));
        frame.add(scrollPane, BorderLayout.LINE_END);
        frame.pack();
    }

    public void createCursors () {
        navigationCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon("icons/cursor/navigate_cursor.png").getImage(),
                new Point(0,0),"navigation cursor");
        drawCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon("icons/cursor/draw_cursor.png").getImage(),
                new Point(0,0),"draw cursor");
        circleCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon("icons/cursor/circle_cursor.png").getImage(),
                new Point(0,0),"circle cursor");
        squareCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon("icons/cursor/square_cursor.png").getImage(),
                new Point(0,0),"square cursor");
        magicWandCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon("icons/cursor/magicwand_cursor.png").getImage(),
                new Point(0,0),"magic wand cursor");

    }

    public void setCursor(String mode) {
        if (mode.equals(NAVIGATION_MODE)) {
            canvas.setCursor(navigationCursor);
        }
        else if(mode.equals(CREATE_CIRCLE)) {
            canvas.setCursor(circleCursor);
        }
        else if (mode.equals(DRAW_MODE)) {
            canvas.setCursor(drawCursor);
        }
        else if (mode.equals(CREATE_SQUARE)) {
            canvas.setCursor(squareCursor);
        }
        else if (mode.equals(MAGIC_WAND)) {
            canvas.setCursor(magicWandCursor);
        }
    }
    public void setCursor (Cursor cursor) {
        canvas.setCursor(cursor);
    }

    void resetLayerCheckboxs() {
        for (JCheckBox checkBox : layerCheckboxs.values()) {
            checkBox.setSelected(false);
            checkBox.setEnabled(true);
        }
    }

    void enableLayerCheckboxs() {
        for (JCheckBox checkBox : layerCheckboxs.values()) {
            checkBox.setEnabled(true);
        }
    }

    void updateShapeFileTableSummary(VectorSelection vectorSelection) {
       for (ShapeFileTableSummary shapeFileTableSummary: shapeFileTableSummaries) {
           shapeFileTableSummary.setEnabled(true);
            if (shapeFileTableSummary.getLayer() == vectorSelection.getLayer()) {
                shapeFileTableSummary.update(vectorSelection);
            }
           else {
                shapeFileTableSummary.reset();
            }
       }
    }

    void updateShapeFileTableSummary (VectorGeoElement vectorGeoElement) {
        for (ShapeFileTableSummary shapeFileTableSummary: shapeFileTableSummaries) {
            if (shapeFileTableSummary.getLayer() == selectedLayer) {
                shapeFileTableSummary.update(((VectorLayer)selectedLayer).getFeatureForVectorGeoELement(vectorGeoElement));
            }
            else {
                shapeFileTableSummary.reset();
            }
        }
    }

    void updateQueryPanel (VectorGeoElement vectorGeoElement, VectorLayer layer) {
        updateShapeFileTableSummary(vectorGeoElement);
        for (String checkboxName: layerCheckboxs.keySet()) {
            layerCheckboxs.get(checkboxName).setEnabled(false);
            if (checkboxName.equals(layer.getName())) {
                layerCheckboxs.get(checkboxName).setSelected(true);
            }
            else {
                layerCheckboxs.get(checkboxName).setSelected(false);
            }
        }
    }

    void resetShapeFileTableSummaries() {
        for (ShapeFileTableSummary shapeFileTableSummary: shapeFileTableSummaries) {
           shapeFileTableSummary.reset();
            //shapeFileTableSummary.setEnabled(true);
        }
    }

    public void updateShapeFileTableSummaryBackground() {
        resetLayerCheckboxs();
        resetShapeFileTableSummaries();
        for (VectorSelection vectorSelection: selectionManager.getVectorSelectionsBackground()) {
            layerCheckboxs.get(vectorSelection.getLayer().getName()).setSelected(true);
            updateShapeFileTableSummary(vectorSelection);
        }
    }

    public void updateShapeFileTableSummary() {
        resetShapeFileTableSummaries();
        if (selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected)!=null || selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected).size()>0) {
            for (VectorSelection vs : selectionManager.getVectorSelectionsDrawn((DrawnGeoElement)selected)) {
                updateShapeFileTableSummary(vs);
            }

        }


    }



    private Layer initRasterLayers (String tiffName, boolean isVisible, String name) {
        RasterLayer rasterLayer = new RasterLayer(isVisible, name);
        RasterGeoElement rasterGeoElement = geoToolsAdapter.getRasterPixelData(tiffName);
        rasterGeoElement.setName(name);
        rasterLayer.addElement(rasterGeoElement);
        layers.add(rasterLayer);
        textures.put(rasterGeoElement, rasterGeoElement.getTexture());
        rasters.put(rasterGeoElement.getTexture(), rasterGeoElement);
        rasterNames.add(name);
        rasterGeoElement.getTexture().setName(name);
        if (isVisible) {
            backgroundTexture = rasterGeoElement.getTexture();
        }
        if (reference == null) {
            reference = rasterGeoElement;
            LOWER_CORNER_X = reference.getLowerCorner()[0];
            LOWER_CORNER_Y = reference.getLowerCorner()[1];
            //PIXELS_PER_UNIT = reference.getPixels_per_unit();
            DIMENSION_Y = reference.getRealHeight();
            DIMENSION_X = reference.getRealWidth();
            SCENE_PIXELS_HEIGHT = reference.getImageHeight();
            SCENE_PIXELS_WIDTH = reference.getImageWidth();
        }
        else if (baseTexture==null) {
            baseTexture = rasterGeoElement.getTexture();
        }

        if (sceneBounds == null) {
            sceneBounds = new double[4];
            sceneBounds[0] = rasterGeoElement.getLowerCorner()[0];
            sceneBounds[1] = rasterGeoElement.getLowerCorner()[1];
            sceneBounds[2]= rasterGeoElement.getLowerCorner()[0]+rasterGeoElement.getRealWidth();
            sceneBounds[3] = rasterGeoElement.getLowerCorner()[1] + rasterGeoElement.getRealHeight();
        }
        else {
            if (sceneBounds[0]>rasterGeoElement.getLowerCorner()[0]) {
                sceneBounds[0] = rasterGeoElement.getLowerCorner()[0];
            }
            if (sceneBounds[1] > rasterGeoElement.getLowerCorner()[1]){
                sceneBounds[1] = rasterGeoElement.getLowerCorner()[1];
            }
            if (sceneBounds[2] < rasterGeoElement.getLowerCorner()[0]+rasterGeoElement.getRealWidth()){
                sceneBounds[2]= rasterGeoElement.getLowerCorner()[0]+rasterGeoElement.getRealWidth();
            }
            if (sceneBounds[3] < rasterGeoElement.getLowerCorner()[1] + rasterGeoElement.getRealHeight()){
                sceneBounds[3] = rasterGeoElement.getLowerCorner()[1] + rasterGeoElement.getRealHeight();
            }
        }

        if (scene_pixels_per_unit == -1) {
            scene_pixels_per_unit = rasterGeoElement.getPixels_per_unit();
        }
        else if (scene_pixels_per_unit < rasterGeoElement.getPixels_per_unit()) {
            scene_pixels_per_unit = rasterGeoElement.getPixels_per_unit();
        }

        maxBorderOffset= maxAlphaRadius/scene_pixels_per_unit;

        return rasterLayer;
    }

    private Layer initRasterLayers (String tiffName, boolean isVisible, String name, Vector3f minColor, Vector3f maxColor) {
        RasterLayer rasterLayer = new RasterLayer(isVisible, name);
        RasterGeoElement rasterGeoElement = geoToolsAdapter.getRasterPixelData(tiffName);
        rasterGeoElement.setName(name);
        rasterLayer.addElement(rasterGeoElement);
        if (rasterGeoElement instanceof MNTRasterGeoElement) {
            ((MNTRasterGeoElement) rasterGeoElement).setMinColor(minColor);
            ((MNTRasterGeoElement) rasterGeoElement).setMaxColor(maxColor);
        }
        layers.add(rasterLayer);
        textures.put(rasterGeoElement, rasterGeoElement.getTexture());
        rasters.put(rasterGeoElement.getTexture(), rasterGeoElement);
        rasterNames.add(name);
        rasterGeoElement.getTexture().setName(name);
        if (isVisible) {
            backgroundTexture = rasterGeoElement.getTexture();
        }

        return rasterLayer;
    }


    private Layer initVectorLayers(boolean isVisible, String path, String name) {
        VectorLayer vectorLayer = new VectorLayer(isVisible, name);

        //ArrayList<VectorGeoElement> polygons = geoToolsAdapter.getPolygons(path, baseTexture);
        ArrayList<ShapeFileAttribute> attributes = geoToolsAdapter.getShapeFileAttributes(path);
        vectorLayer.setAttributes(attributes);
        HashSet<ShapeFileFeature> shapeFileFeatures = geoToolsAdapter.getPolygons(path, baseTexture, maxBorderOffset);
        vectorLayer.setFeatures(shapeFileFeatures);
        for (ShapeFileFeature shapeFileFeature: shapeFileFeatures) {
            for (ShapeFileAttribute attribute: attributes) {
                shapeFileFeature.addAttribute(attribute);
                attribute.update(shapeFileFeature.getFeature().getAttribute(attribute.getTypeName()));
            }
            for (VectorGeoElement v : shapeFileFeature.getVectorGeoElements()) {
                if (v.getIsPoint() && !vectorLayer.getIsPointLayer()) {
                    vectorLayer.setIsPointLayer(true);
                }

                vectorLayer.addElement(v, isVisible);
                v.setTexture(baseTexture);
            }
        }
        vectorLayer.setFeatureSource(geoToolsAdapter.getFeatureSourcePath(path, getScreenQuery()));
        layers.add(vectorLayer);
        return vectorLayer;
    }

    private Layer initVectorLayers (boolean isVisible, String path, String name, Vector3f color) {
        VectorLayer vectorLayer = new VectorLayer(isVisible, name);
        ArrayList<ShapeFileAttribute> attributes = geoToolsAdapter.getShapeFileAttributes(path);
        vectorLayer.setAttributes(attributes);
        HashSet<ShapeFileFeature> shapeFileFeatures = geoToolsAdapter.getPolygons(path, baseTexture, maxBorderOffset);
        vectorLayer.setFeatures(shapeFileFeatures);
        for (ShapeFileFeature shapeFileFeature: shapeFileFeatures) {
            for (ShapeFileAttribute attribute: attributes) {
                shapeFileFeature.addAttribute(attribute);
                attribute.update(shapeFileFeature.getFeature().getAttribute(attribute.getTypeName()));
            }
            for (VectorGeoElement v : shapeFileFeature.getVectorGeoElements()) {
                if (v.getIsPoint() && !vectorLayer.getIsPointLayer()) {
                    vectorLayer.setIsPointLayer(true);
                }
                vectorLayer.addElement(v, isVisible);
                v.setColor(color);
                v.setRenderingType(GLUtilities.RenderedElementTypes.FILLED);
            }
        }
        vectorLayer.setColor(color);
        layers.add(vectorLayer);
        return vectorLayer;
    }

    public void initCameraParameters () {
        eyeInit = new Vector3d(layers.get(0).getElements().get(0).getX()-reference.getRealWidth()/2, layers.get(0).getElements().get(0).getY()-reference.getRealHeight()/2, 5.0);
        centerInit = new Vector3d(layers.get(0).getElements().get(0).getX()-reference.getRealWidth()/2, layers.get(0).getElements().get(0).getY()-reference.getRealHeight()/2, 0.0);
        canvas.getCamera().setEye(eyeInit);
        canvas.getCamera().setCenter(centerInit);
        canvas.getCamera().setCamera_pixels_per_unit((float)baseTexture.getPixels_per_unit());
        canvas.getCamera().setPixels_per_unit(baseTexture.getPixels_per_unit_array());
    }

    public void setBackgroundLayer(Layer layer) {
        this.backgroundLayer = layer;
    }

    public MapGLCanvas getCanvas () {
        return canvas;
    }


    public synchronized void addPointDrawnGeoElement(int x, int y) {
        if (currentDrawingGeoElement != null) {
            double[] point = canvas.getCamera().screenToWorld(x, y);
            getCurrentDrawingGeoElement().addPoint(point);
            canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.POINT_ADDED,getCurrentDrawingGeoElement(), drawnLayer));
        }
    }

    public void finishDrawnGeoElement() {

        if (getCurrentDrawingGeoElement().isDrawable()) {
            try {
                getCurrentDrawingGeoElement().drawCompletePolygon(maxBorderOffset, baseTexture);
                getCurrentDrawingGeoElement().setTexture(baseTexture);
                selectionManager.getBackgroundTextures().put(getCurrentDrawingGeoElement(), baseTexture);
                canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_FINISHED, getCurrentDrawingGeoElement(), drawnLayer));
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
        else {
            drawnLayer.removeElement(getCurrentDrawingGeoElement());
        }

    }

    public void newDrawnGeoElement(int x, int y) {
        if(!drawnLayer.isVisible) {
            drawnLayer.setVisibility(true);
        }
        setCurrentDrawingGeoElement(new DrawnGeoElement());
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED,getCurrentDrawingGeoElement(), drawnLayer));
        addPointDrawnGeoElement(x, y);
        drawnLayer.addElement(currentDrawingGeoElement);
        drawnLayer.addTranslucentElement(currentDrawingGeoElement);
    }
    GeometryBuilder geometryBuilder = new GeometryBuilder();

    public void newCircleDrawnGeoElement(int x, int y) {
        double [] worldCoord = canvas.getCamera().screenToWorld(x,y);
        startPoint = worldCoord;
        if(!drawnLayer.isVisible) {
            drawnLayer.setVisibility(true);
        }

        Polygon circle = geometryBuilder.circle(worldCoord[0], worldCoord[1],initialRadius, nbSlicesCircle);
        DrawnGeoElement circleDrawn = new DrawnGeoElement(circle);
        setCurrentDrawingGeoElement(circleDrawn);
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED,getCurrentDrawingGeoElement(), drawnLayer));
        canvas.getTraceFilter().refreshQuadCoordinates(canvas.getCamera());
        drawnLayer.addElement(currentDrawingGeoElement);
        drawnLayer.addTranslucentElement(currentDrawingGeoElement);

    }

    public void newSquareDrawnGeoElement(int x, int y) {
        double [] worldCoord = canvas.getCamera().screenToWorld(x,y);
        startPoint = worldCoord;
        if(!drawnLayer.isVisible) {
            drawnLayer.setVisibility(true);
        }
        Polygon square = geometryBuilder.box(worldCoord[0], worldCoord[1], worldCoord[0] + initialRadius, worldCoord[1] + initialRadius);
        DrawnGeoElement boxDrawn = new DrawnGeoElement(square);
        setCurrentDrawingGeoElement(boxDrawn);
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED,getCurrentDrawingGeoElement(), drawnLayer));
        drawnLayer.addElement(currentDrawingGeoElement);
        drawnLayer.addTranslucentElement(currentDrawingGeoElement);
    }

    public void drawCircleDrawnGeoElement(int x, int y) {
        double[] worldCoord = canvas.getCamera().screenToWorld(x,y);
        double distToCenter = Math.sqrt((worldCoord[0]-startPoint[0])*(worldCoord[0]-startPoint[0])+ (worldCoord[1]-startPoint[1])*(worldCoord[1]-startPoint[1]));
        Polygon circle = geometryBuilder.circle(startPoint[0], startPoint[1],initialRadius+distToCenter, nbSlicesCircle);
        currentDrawingGeoElement.refreshCoordinatesContour(circle);
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.POINT_ADDED,getCurrentDrawingGeoElement(), drawnLayer));

    }

    public void drawSquareDrawnGeoElement(int x, int y) {
        double[] worldCoord = canvas.getCamera().screenToWorld(x,y);
        double lowerX;
        double lowerY;
        double upperX;
        double upperY;
        if (worldCoord[0] > startPoint[0]) {
            upperX = worldCoord[0];
            lowerX = startPoint[0];
        }
        else {
            upperX = startPoint[0];
            lowerX = worldCoord[0];
        }
        if (worldCoord[1]>startPoint[1]) {
            lowerY = startPoint[1];
            upperY = worldCoord[1];
        }
        else {
            lowerY = worldCoord[1];
            upperY = startPoint[1];
        }
        Polygon box = geometryBuilder.box(lowerX, lowerY, upperX, upperY);
        currentDrawingGeoElement.refreshCoordinatesContour(box);
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.POINT_ADDED,getCurrentDrawingGeoElement(), drawnLayer));

    }

    public ArrayList<Layer> getLayers () {
        return layers;
    }

    public synchronized DrawnGeoElement getCurrentDrawingGeoElement () {
        return currentDrawingGeoElement;
    }

    public synchronized void setCurrentDrawingGeoElement(DrawnGeoElement ge) {
        this.currentDrawingGeoElement = ge;
    }

    public boolean isTextureInSameAreaThatCamera (Texture texture1) {
        Camera camera = canvas.getCamera();
        if (texture1.getWidth()!=camera.getScreenRealWidth()) {
            return false;
        }
        else if (texture1.getHeight() != camera.getScreenRealHeight()) {
            return false;
        }
        else if (texture1.getLowerCorner()[0] != camera.getLowerCorner()[0]) {
            return false;
        }
        else if (texture1.getLowerCorner()[1] != camera.getLowerCorner()[1]) {
            return false;
        }
        else {
            return true;
        }
    }


    public GeoElement getVectorGeoElementAt(int x, int y)
    {
        double [] point = canvas.getCamera().screenToWorld(x, y);
        for (Layer layer : layers) {
            if (layer instanceof VectorLayer) {
                for (GeoElement ge : layer.getElements()) {
                    if (ge.containsPoint(point[0],point[1])) {
                        return ge;

                    }
                }
            }
        }
        return null;
    }


    public Pair<GeoElement, Layer> getVectorGeoElementandLayerAt(double[] point) {
        for (Layer layer : layers) {
            if (layer instanceof VectorLayer) {
                for (int i = layer.getElements().size() - 1; i >= 0; i--) {
                    GeoElement ge = layer.getElements().get(i);
                    if (ge.containsPoint(point[0], point[1])) {
                        return new Pair<>(ge, layer);

                    }
                }
            }
        }
        return null;
    }



    public Pair <GeoElement, Layer> getDrawnGeoElementAndLayerAt(double[] point) {
        for (Layer layer : layers) {
            if (layer instanceof DrawnLayer) {
                for (int i = layer.getElements().size() - 1; i >= 0; i--) {
                    GeoElement ge = layer.getElements().get(i);
                    //for (GeoElement ge : layer.getElements()) {
                    if (ge.containsPoint(point[0], point[1])) {
                        return new Pair<>(ge, layer);

                    }
                }
            }

        }
        return null;
    }



    public Pair<GeoElement, Layer> getGeoElementAndLayerAt(double[] point) {
        Pair drawnPair = getDrawnGeoElementAndLayerAt(point);
        if (drawnPair != null) {
            return drawnPair;
        }
        else {
            return getVectorGeoElementandLayerAt(point);
        }
    }

    public Pair<GeoElement, Layer> getGeoElementAndLayerByBBox (double[] point) {
        double smallestArea = Double.MAX_VALUE;
        GeoElement smallestGeoElement = null;
        Layer layerContainingSmallestGeoElement = null;
        for (Layer layer: layers) {
            if (layer.getSelectable() && (layer instanceof VectorLayer || layer instanceof DrawnLayer)) {
                for (GeoElement ge : layer.getElements()) {
                    if (ge.containsPoint(point[0], point[1])) {
                        if (ge.getGeometry().getEnvelope().getArea() <= smallestArea) {
                            smallestGeoElement = ge;
                            layerContainingSmallestGeoElement = layer;
                            smallestArea = ge.getGeometry().getEnvelope().getArea();
                        }
                    }
                }
            }
        }
        if (smallestGeoElement!= null && layerContainingSmallestGeoElement!=null) {
            return new Pair<GeoElement, Layer>(smallestGeoElement, layerContainingSmallestGeoElement);
        }
        else {
            return null;
        }
    }

    public Pair <GeoElement, Layer> getNextHierarchicalObjectAndLayerAt(double[] point, GeoElement selectedGe) {
        double smallestArea = Double.MAX_VALUE;
        GeoElement smallestGeoElement = null;
        Layer layerContainingSmallestGeoElement = null;
        for (Layer layer: layers) {
            if (layer.getSelectable() && (layer instanceof VectorLayer || layer instanceof DrawnLayer)){
                for (GeoElement ge : layer.getElements()) {
                    if (ge.containsPoint(point[0], point[1]) && ge.getGeometry().contains(selectedGe.getGeometry()) && ge!=selectedGe) {
                        if (ge.getGeometry().getEnvelope().getArea() <= smallestArea) {
                            smallestGeoElement = ge;
                            layerContainingSmallestGeoElement = layer;
                            smallestArea = ge.getGeometry().getEnvelope().getArea();
                        }
                    }
                }
            }
        }
        if (smallestGeoElement!= null && layerContainingSmallestGeoElement!=null) {
            return new Pair<GeoElement, Layer>(smallestGeoElement, layerContainingSmallestGeoElement);
        }
        else {
            return null;
        }
    }


    public void suggestSelect (int x, int y) {
        double [] point = canvas.getCamera().screenToWorld(x, y);
        Pair<GeoElement, Layer>layerGe = getGeoElementAndLayerByBBox(point);
        if (layerGe == null) {
            if (inElement!=null) {
                inElement.outElement();
                inElement = null;
            }
        }
        else {
            if (inElement !=null && inElement!= layerGe.getValue1() && inElement != selected) {
                inElement.outElement();
            }

            if (!layerGe.getValue2().getVisibility()) {
                layerGe.getValue2().setVisibility(true);
            }
            layerGe.getValue1().inElement();
            inElement = layerGe.getValue1();
            inLayer = layerGe.getValue2();
        }
    }





    public GeoElement selectElementAt(int x, int y) {

        double [] point = canvas.getCamera().screenToWorld(x, y);
        if (selected != null && selected.containsPoint(point[0], point[1]) && inElement==null) {
            Pair<GeoElement, Layer> next = getNextHierarchicalObjectAndLayerAt(point, selected);
            if (next != null) {
                selected.deselect();
                selected = next.getValue1();
                selectedLayer = next.getValue2();
                selected.select();
            }
            else {
                if (selected != null && selected != inElement) {
                    selected.deselect();
                }
                selected = getGeoElementAndLayerByBBox(point).getValue1();
                selectedLayer = getGeoElementAndLayerByBBox(point).getValue2();
                selected.select();
            }
            inElement = null;
            inLayer = null;
            startPoint = canvas.getCamera().screenToWorld(x,y);
            startPoint[0]-=getSelected().getLocation().x;
            startPoint[1]-=getSelected().getLocation().y;

            return selected;
        }
        else if (inElement != null) {

                if (selected != null && selected != inElement) {
                    selected.deselect();
                }
                selected = inElement;
                selectedLayer = inLayer;
                inElement.select();
            inElement = null;
            inLayer = null;
            startPoint = canvas.getCamera().screenToWorld(x,y);
            startPoint[0]-=getSelected().getLocation().x;
            startPoint[1]-=getSelected().getLocation().y;
            return selected;
        }
        else {
            if (selected != null) {
                selected.deselect();
                selected = null;
                selectedLayer = null;
            }
            return null;
        }
    }



    public GeoElement getContextSelection(int x, int y, DrawnGeoElement ge) {
        double [] point = canvas.getCamera().screenToWorld(x, y);
        if (!ge.containsPoint(point[0],point[1])) {
            return null;
        }
        else {
            GeoElement selectedVector = getVectorGeoElementAt(x, y);
            if (selectedVector != null) {
                for (VectorSelection vectorSelection : selectionManager.getVectorSelectionsDrawn(ge)) {
                    if (vectorSelection.getSelectedElements().contains(selectedVector)) {
                        return selectedVector;
                    }
                }
            }
        }
            return ge;
        }


    public VectorSelection getContextVectorSelection() {
            return selectionManager.getVectorSelectionLayerComplete(selectedLayer);

    }




    public void moveDrawnObject(int x, int y) {
        double[] newPoint = canvas.getCamera().screenToWorld(x, y);
        double[] diff = new double[2];
        double [] diffVectorSelection = new double[2];

        diff[0] = newPoint[0]-startPoint[0]- getSelected().getLocation().x;
        diff[1] = newPoint[1]-startPoint[1]- getSelected().getLocation().y;

        diffVectorSelection[0] = newPoint[0]-startPoint[0];
        diffVectorSelection[1] = newPoint[1]-startPoint[1];


        if (selected != null) {
            selected.setOffset(diff);
        }

    }

    public void moveVectorGeoElement(int x, int y) {
        double[] newPoint = canvas.getCamera().screenToWorld(x, y);
        double[] diff = new double[2];
        diff[0] = newPoint[0]-startPoint[0]- getSelected().getLocation().x;
        diff[1] = newPoint[1]-startPoint[1]- getSelected().getLocation().y;
        selected.setOffset(diff);
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.TRANSLUCENT_MOVED, currentMovingGeoElement, selectedLayer));
    }

    public void finishMovingVectorGeoElement() {
        if (selected != null) {
            selected.setOffset(new double[] {0,0});
        }
    }

    public void finishMovingDrawnObject() {
        currentMovingGeoElement = null;
        if (selected != null) {
            if (!((DrawnGeoElement)selected).getIsDocked()) {
                selected.offsetCoordinates();
                canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_MOVED, selected, selectedLayer));
            }
            else {
                selected.setOffset(new double[] {0,0});
            }


            }

    }

    public GeoElement getSelected() {
        return selected;
    }

    public void setSelected(GeoElement selected) {
        this.selected = selected;
    }


    public void bufferSelected(double direction) {
        selected.bufferPolygon(direction*2 / canvas.getCamera().getCamera_pixels_per_unit());
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.ELEMENT_MODIFIED, selected, selectedLayer));
    }

    public void bufferSelectedDrawn(double bufferOffset) {
        if (bufferOffset != selected.getBuffer()) {
            BufferWorker bufferWorker = new BufferWorker(selected, (bufferOffset- selected.getBuffer())/canvas.getCamera().getPixels_per_unit()[0], canvas, selectedLayer);
            selected.setBuffer(bufferOffset);
            bufferWorker.startWorker();
        }

    }



    public void changeTextureInSelection(String texture, int x, int y) {
        Texture newTexture = getTextureByName(texture);
        if (selected instanceof VectorGeoElement) {
            if (selectedVectorSelection != null) {
                selectedVectorSelection.setTexture(newTexture);
                selectedVectorSelection.setSelectedType(GLUtilities.RenderedElementTypes.TEXTURED);
            }
            else {
                selected.setTexture(newTexture);
                selected.setRenderingType(GLUtilities.RenderedElementTypes.TEXTURED);
            }
        }
        else if (selected instanceof DrawnGeoElement) {
            GeoElement contextSelection = getContextSelection(x, y, (DrawnGeoElement)selected);
                if (selectedVectorSelection != null) {
                    selectionsDrawn = selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected);
                    selectedVectorSelection.setTexture(getTextureByName(texture));
                    selectedVectorSelection.setSelectedType(GLUtilities.RenderedElementTypes.TEXTURED);
                    canvas.pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, selectionsDrawn, (DrawnGeoElement) selected, selectionManager.getBackgroundTextures().get(selected), selectionManager.getBackgroundColors().get(selected), selectionManager.getDrawnSelectionType().get((DrawnGeoElement)selected)));

                }
            else if (contextSelection instanceof DrawnGeoElement) {
                java.util.List<VectorSelection> selectionsDrawn = selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected);
                if (selectionsDrawn.size() > 0) {
                    selectionManager.getBackgroundTextures().put((DrawnGeoElement)selected, newTexture);
                    selectionManager.getDrawnSelectionType().put((DrawnGeoElement)selected, GLUtilities.RenderedElementTypes.TEXTURED);
                    canvas.pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, selectionsDrawn, (DrawnGeoElement) selected, newTexture, selectionManager.getBackgroundColors().get(selected),GLUtilities.RenderedElementTypes.TEXTURED));
                }
                else {
                    selected.setTexture(getTextureByName(texture));
                    selected.setRenderingType(GLUtilities.RenderedElementTypes.DRAWN);

                }
            }


        }
        else if (selected == null) {
            changeBackgroundTexture(texture);
        }

    }

    public void changeAlphaInDrawn(double alpha) {
        java.util.List<VectorSelection> selectionsDrawn = selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected);
        if (selectionsDrawn.size()>0) {
            selectionManager.getBackgroundAlphas().put((DrawnGeoElement)selected, (float)alpha);
            canvas.pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, selectionsDrawn, selected, selectionManager.getBackgroundTextures().get(selected), selectionManager.getBackgroundColors().get(selected), selectionManager.getDrawnSelectionType().get(selected),(float)alpha));
        }
        else {
            ((DrawnGeoElement) selected).setAlpha((float)alpha);
        }
    }

    public void changeTextureInDrawn(String texture) {
        Texture newTexture = getTextureByName(texture);
                java.util.List<VectorSelection> selectionsDrawn = selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected);
                selectionManager.getBackgroundTextures().put((DrawnGeoElement)selected, newTexture);
                if (selectionsDrawn.size() > 0) {
                    selectionManager.getDrawnSelectionType().put((DrawnGeoElement)selected, GLUtilities.RenderedElementTypes.TEXTURED);
                    canvas.pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, selectionsDrawn, (DrawnGeoElement) selected, newTexture, selectionManager.getBackgroundColors().get(selected),GLUtilities.RenderedElementTypes.TEXTURED));
                }

                else {
                    if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT) {
                        selected.setRenderingType(GLUtilities.RenderedElementTypes.DRAWN);
                    }
                    else if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED){
                        selected.setRenderingType(GLUtilities.RenderedElementTypes.TEXTURED);

                    }
                    selected.setTexture(getTextureByName(texture));

                }

//        }

    }




    public void changeColorInDrawn(Vector3f color) {
            selected.setFillColor(color);
            java.util.List<VectorSelection> selectionsDrawn = selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected);
            if (selectionsDrawn.size() > 0) {
                selectionManager.getBackgroundColors().put((DrawnGeoElement)selected,color);
                selectionManager.getDrawnSelectionType().put((DrawnGeoElement)selected, GLUtilities.RenderedElementTypes.FILLED);
                canvas.pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, selectionsDrawn, (DrawnGeoElement) selected,selectionManager.getBackgroundTextures().get(selected),color, selectionManager.getDrawnSelectionType().get((DrawnGeoElement)selected)));

            }

            else {
                if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.DRAWN) {
                    selected.setRenderingType(GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT);
                }
                else if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.TEXTURED){
                    selected.setRenderingType(GLUtilities.RenderedElementTypes.FILLED);

                }
                updateInspectorDrawn();
        }

    }

    public void changeColorInVector(Vector3f color) {
        selected.setVisible(true);
        selected.setFillColor(color);
        if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.DRAWN) {
            selected.setRenderingType(GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT);
        }
        else if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.TEXTURED){
            selected.setRenderingType(GLUtilities.RenderedElementTypes.FILLED);

        }
    }

    public void changeColorInDrawn() {
        changeColorInDrawn(selected.getFillColor());
    }



    public void changeColorInSelection(Vector3f color, int x, int y) {
        if (selected instanceof VectorGeoElement) {

            if (selectedVectorSelection != null) {
                selectedVectorSelection.setColor(color);
                selectedVectorSelection.setSelectedType(GLUtilities.RenderedElementTypes.FILLED);
            }
            else {
                ((VectorGeoElement) selected).setFillColor(color);
                selected.setRenderingType(GLUtilities.RenderedElementTypes.FILLED);
            }
        }
        else if (selected instanceof DrawnGeoElement) {
            GeoElement contextSelection = getContextSelection(x, y, (DrawnGeoElement)selected);
            if (contextSelection instanceof  VectorGeoElement) {
                if (selectedVectorSelection != null) {
                    selected.setFillColor(color);
                    selectionsDrawn = selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected);
                    selectedVectorSelection.setColor(color);
                    selectedVectorSelection.setSelectedType(GLUtilities.RenderedElementTypes.FILLED);
                    canvas.pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, selectionsDrawn, (DrawnGeoElement) selected, selectionManager.getBackgroundTextures().get(selected), color, selectionManager.getDrawnSelectionType().get((DrawnGeoElement)selected)));

                }

            }
            else if (contextSelection instanceof DrawnGeoElement) {
                selected.setFillColor(color);
                java.util.List<VectorSelection> selectionsDrawn = selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected);
                if (selectionsDrawn.size() > 0) {
                    selectionManager.getBackgroundColors().put((DrawnGeoElement)selected,color);
                    selectionManager.getDrawnSelectionType().put((DrawnGeoElement)selected, GLUtilities.RenderedElementTypes.FILLED);
                    canvas.pushEvent(new CanvasVectorSelectionsEvent(CanvasEvent.VECTOR_SELECTION_UPDATED, selectionsDrawn, (DrawnGeoElement) selected,selectionManager.getBackgroundTextures().get(selected),color, selectionManager.getDrawnSelectionType().get((DrawnGeoElement)selected)));

                }

                else {
                    selected.setRenderingType(GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT);
                    selected.setFillColor(color);

                }
            }


        }

    }

    public void toggleOutlineLineInSelection(Vector3f color, int x, int y) {
        if (selected instanceof VectorGeoElement) {
            if (selectedVectorSelection != null) {
                selectedVectorSelection.setOutlineColor(color);
                selectedVectorSelection.toggleOutlineVisible();
            }
            else {
                ((VectorGeoElement) selected).setOutlineColor(color);
                //((VectorGeoElement) selected).getLine().setVisible(true);
                ((VectorGeoElement) selected).toggleOutlineVisible();
            }
        }
        else if (selected instanceof DrawnGeoElement) {
            GeoElement contextSelection = getContextSelection(x, y, (DrawnGeoElement)selected);
            if (contextSelection instanceof VectorGeoElement) {
                if (selectedVectorSelection != null) {
                    selectionsDrawn = selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected);
                    selectedVectorSelection.setOutlineColor(color);
                    selectedVectorSelection.toggleOutlineVisible();
                    selectionManager.pushCanvasVectorSelectionEvent(selectionsDrawn, (DrawnGeoElement) selected);
                }
            }
            else if (contextSelection instanceof DrawnGeoElement) {
                selected.setOutlineColor(color);
                ((DrawnGeoElement) selected).toggleOutlineVisible();
            }
        }
    }


    public void changeBackgroundTexture(String texture) {
        rasters.get(backgroundTexture).setRenderingType(GLUtilities.RenderedElementTypes.TEXTURED);
        for (RasterGeoElement rasterGeoElement: textures.keySet()) {
            if (textures.get(rasterGeoElement).getName().equals(texture)) {
                rasterGeoElement.setVisible(true);
                backgroundTexture = rasterGeoElement.getTexture();

            }
            else {
                rasterGeoElement.setVisible(false);
            }
        }

    }

    public void changeBackgroundColor(Vector3f color) {
        rasters.get(backgroundTexture).setRenderingType(GLUtilities.RenderedElementTypes.FILLED);
        rasters.get(backgroundTexture).setFillColor(color);
    }

    public void changeBackgroundColor() {
       changeBackgroundColor(rasters.get(backgroundTexture).getFillColor());
    }


    public void startPanning(int x, int y) {
        startPointPx = new int[] {x,y};
    }

    public void pan (int x, int y) {
        canvas.getCamera().moveCamera(startPointPx[0]-x,y-startPointPx[1]);
        startPointPx[0] = x;
        startPointPx[1] = y;
    }

    public void toggleTranslucentBorder () {
        if (!selectedLayer.getTranslucentElements().contains(selected)) {
            selectedLayer.addTranslucentElement(selected);
            if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.TEXTURED) {
                selected.setRenderingType(GLUtilities.RenderedElementTypes.DRAWN);
            }
            else if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED) {
                selected.setRenderingType(GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT);
            }
                canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.TRANSLUCENT_BORDER_ENABLED, selected, selectedLayer));
        }
        else {
            if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.DRAWN) {
                selected.setRenderingType(GLUtilities.RenderedElementTypes.TEXTURED);
            }
            else if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT) {
                selected.setRenderingType(GLUtilities.RenderedElementTypes.FILLED);
            }
            selectedLayer.getTranslucentElements().remove(selected);
        }

    }

    public void toggleTranslucentBorder (boolean translucentBorder) {
        if (translucentBorder) {

            selected.setTranslucentBorder(true);
            selectedLayer.addTranslucentElement(selected);
            if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.TEXTURED) {
                selected.setRenderingType(GLUtilities.RenderedElementTypes.DRAWN);
            }
            else if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED) {
                selected.setRenderingType(GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT);
            }
            canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.TRANSLUCENT_BORDER_ENABLED, selected, selectedLayer));

        }
        else {
            selected.setTranslucentBorder(false);
            if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.DRAWN) {
                selected.setRenderingType(GLUtilities.RenderedElementTypes.TEXTURED);
            }
            else if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT) {
                selected.setRenderingType(GLUtilities.RenderedElementTypes.FILLED);
            }
            selectedLayer.getTranslucentElements().remove(selected);
        }

    }



    public void deleteDrawnGeoElement(DrawnGeoElement geoElement) {
        selectedLayer.removeElement(geoElement);
        selected = null;
        selectedLayer = null;
        selectedVectorSelection = null;
        selectionManager.deleteGeoElement((DrawnGeoElement)geoElement);
    }

    public void moveLayerUp(Layer layer) {
        int index = layers.indexOf(layer);
        if (index<layers.size()-1) {
            Collections.swap(layers,index, index+1 );
        }
    }

    public void moveLayerDown(Layer layer) {
        int index = layers.indexOf(layer);
        if (index>0) {
            Collections.swap(layers,index, index-1);
        }
    }

    public void layerSwap(String sourceLayerName, String destinationLayerName) {

        Layer sourceLayer = getLayerbyName(sourceLayerName);
        Layer destinationLayer = getLayerbyName(destinationLayerName);
        int index = layers.indexOf(destinationLayer);
        layers.remove(sourceLayer);
        layers.add(index, sourceLayer);

    }

    public void layerSwapInArea(String sourceLayerName, String destinationLayerName) {
        layerSwap(sourceLayerName, destinationLayerName);
        selectionManager.reOrderVectorSelections((DrawnGeoElement)selected);
        selectionManager.pushCanvasVectorSelectionEvent((DrawnGeoElement)selected);
    }

    public void moveLensUp() {
        int index = drawnLayer.getElements().indexOf(selected);
        if (index < drawnLayer.getElements().size()-1) {
            Collections.swap(drawnLayer.getElements(), index, index +1);
        }
    }

    public void moveLensDown() {
        int index = drawnLayer.getElements().indexOf(selected);
        if (index>0) {
            Collections.swap(drawnLayer.getElements(), index, index-1);
        }
    }

    public void colorFill (Vector3f color) {
        changeColorInSelection(color, popUpMenuPos[0], popUpMenuPos[1]);
    }

    public void createDrawnFromSelectedVector() {
        DrawnGeoElement newDrawnGeoElement = new DrawnGeoElement((VectorGeoElement)selected, maxBorderOffset);
        drawnLayer.addElement(newDrawnGeoElement);
        selected.setVisible(false);
        selected.deselect();
        selectedLayer = drawnLayer;

        newDrawnGeoElement.select();
        newDrawnGeoElement.setComposite(true);
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.GEOELEMENT_CREATED,newDrawnGeoElement,drawnLayer));
        if (selectedVectorSelection != null && selectedVectorSelection.getGeometricFilter()!=null) {
            selectedVectorSelection.setSelectedElement(null);
            selectionManager.pushCanvasVectorSelectionEvent(selectedVectorSelection.getGeometricFilter().getDrawnGeoElement());
        }
        selected = newDrawnGeoElement;
    }

    public void deselectAll() {
        if (selected != null) {
            selected.deselect();
            selected = null;
        }
        if (selectedVectorSelection != null) {
            selectedVectorSelection.setSelected(false);
            selectedVectorSelection = null;
        }
    }

    public void toggleCompositeDrawn(boolean checkboxSelected) {
        DrawnGeoElement selectedDrawn = (DrawnGeoElement)selected;
        selectedDrawn.setComposite(checkboxSelected);
    }

    public void fireMenuAction(String action) {
        if (action.equals(BUFFER_PLUS)) {
            bufferSelected(1);
        }
        else if (action.equals(BUFFER_MINUS)) {
            bufferSelected(-1);
        }
        else if (action.equals(TRANSLUCENT_BORDER)) {
            toggleTranslucentBorder();
        }
        else if (action.equals(FILL_COLOR)) {
            colorFill(new Vector3f(1.0f,1.0f,1.0f));
        }
        else if (action.equals(OUTLINE)) {
            toggleOutlineLineInSelection(new Vector3f(1.0f,0.0f,1.0f), popUpMenuPos[0], popUpMenuPos[1]);
        }
    }

    public void fireCheckBoxActionDrawn(String action, boolean checkBoxSelected) {
        if (action.equals(TOGGLE_COMPOSITE)) {
            toggleCompositeDrawn(checkBoxSelected);
            updateInspectorDrawn();
        }
        else if (action.equals(TRANSLUCENT_BORDER)) {
            toggleTranslucentBorder(checkBoxSelected);
        }
        else if (action.equals(OUTLINE)) {
            ((DrawnGeoElement)selected).setLineVisible(checkBoxSelected);
        }
        else if (action.equals(DOCKED)) {
            ((DrawnGeoElement)selected).setIsDocked(checkBoxSelected);
        }
        else if(action.equals(LEAVE_TRACE)) {
            ((DrawnGeoElement)selected).setLeavingTrace(checkBoxSelected);
        }
        else {
            Layer layer = getLayerbyName(action);
            if (layer != null) {
                selectionManager.toggleElementsInRegionVisibility((DrawnGeoElement)selected,action);
                updateInspectorDrawn();
            }
        }

    }

    public void updateInspectorDrawn() {
        if (selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected)==null || selectionManager.getVectorSelectionsDrawn((DrawnGeoElement) selected).size()==0) {
            if (selected.getRenderingType() == GLUtilities.RenderedElementTypes.FILLED || selected.getRenderingType()==GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT) {
                inspector.update((DrawnGeoElement)selected, FILL_COLOR, selectionManager.getVectorSelectionsDrawn((DrawnGeoElement)selected), ((DrawnGeoElement) selected).getAlpha());
            }
            else {
                inspector.update((DrawnGeoElement)selected, ((DrawnGeoElement) selected).getTexture().getName(), selectionManager.getVectorSelectionsDrawn((DrawnGeoElement)selected), ((DrawnGeoElement) selected).getAlpha());
            }
        }
        else {
            if (selectionManager.getDrawnSelectionType().get((DrawnGeoElement)selected) == GLUtilities.RenderedElementTypes.FILLED || selectionManager.getDrawnSelectionType().get((DrawnGeoElement)selected) == GLUtilities.RenderedElementTypes.FILLED_TRANSLUCENT) {
                inspector.update((DrawnGeoElement)selected, FILL_COLOR, selectionManager.getVectorSelectionsDrawn((DrawnGeoElement)selected), selectionManager.getBackgroundAlphas().get(selected));
            }
            else {
                inspector.update((DrawnGeoElement)selected, selectionManager.getBackgroundTextures().get(selected).getName(), selectionManager.getVectorSelectionsDrawn((DrawnGeoElement)selected), selectionManager.getBackgroundAlphas().get(selected));
            }
        }

    }

    public void updateInspectorBackground()
    {
        List<VectorSelection> backgroundSelections = selectionManager.getVectorSelectionsBackground();
        if (rasters.get(backgroundTexture).getRenderingType() == GLUtilities.RenderedElementTypes.FILLED) {
            inspector.backgroundSelected(backgroundSelections, FILL_COLOR,rasters.get(backgroundTexture).getFillColor());
        }
        else {
            inspector.backgroundSelected(backgroundSelections, backgroundTexture.getName(), rasters.get(backgroundTexture).getFillColor());
        }
    }


    public Inspector getInspector() {
        return inspector;
    }

    public void updateVectorSelection(String layerName, Color fillColor, String rasterName, boolean outlineSelected,Color outlineColor, Double margin, Double alpha) {
        Layer layer = getLayerbyName(layerName);
        selectedVectorSelection = selectionManager.getVectorSelection((DrawnGeoElement)selected, (VectorLayer)layer);
        if (rasterName!= null) {
            if (rasterName == FILL_COLOR) {
                if (fillColor != null) {
                    Vector3f fillColorVector = new Vector3f(fillColor.getRed() / 255.0f, fillColor.getGreen() / 255.0f, fillColor.getBlue() / 255.0f);
                    selectionManager.changeColorInVectorSelection((DrawnGeoElement)selected,layer,fillColorVector);
                } else {
                    selectionManager.changeColorInVectorSelection((DrawnGeoElement)selected,layer,selectedVectorSelection.getColor());
                }

            }
            else {
                selectionManager.changeTextureInVectorSelection((DrawnGeoElement)selected, layer, getTextureByName(rasterName));
            }
        }
        if (margin != null && selectedVectorSelection.getBuffer() != margin) {
            selectionManager.bufferVectorSelection(selectedVectorSelection,margin, false);
        }
        if (outlineColor != null) {
            Vector3f outlineColorVector = new Vector3f(outlineColor.getRed() / 255.0f, outlineColor.getGreen() / 255.0f, outlineColor.getBlue() / 255.0f);
            if (outlineColorVector != selectedVectorSelection.getOutlineColor()) {
                selectionManager.setOutlineVectorSelectionVisible(selectedVectorSelection,outlineColorVector, outlineSelected);
            }
        }
        if (alpha != null && selectedVectorSelection.getAlpha()!= alpha) {
            selectionManager.changeAlphaVectorSelection((DrawnGeoElement)selected,layer,alpha.floatValue());
        }

    }

    public void updateVectorSelectionBackground(String layerName, Color fillColor, String rasterName, boolean outlineSelected, Color outlineColor, Double margin, Double alpha) {
        Layer layer = getLayerbyName(layerName);
        selectedVectorSelection = selectionManager.getBackgroundSelections().get(layer);
        if (rasterName != null) {
            if (rasterName == FILL_COLOR) {
                if (fillColor != null) {
                    Vector3f fillColorVector = new Vector3f(fillColor.getRed() / 255.0f, fillColor.getGreen() / 255.0f, fillColor.getBlue() / 255.0f);
                    selectionManager.changeColorInVectorSelectionBackground(layer, fillColorVector);
                } else {
                    selectionManager.changeColorInVectorSelectionBackground(layer, selectedVectorSelection.getColor());
                }
            }
            else {
                selectionManager.changeTextureInVectorSelectionBackground(layer, getTextureByName(rasterName));
            }

        }
        if (margin != null && selectedVectorSelection.getBuffer() != margin) {
            selectionManager.bufferVectorSelection(selectedVectorSelection,margin, true);
        }
        if (outlineColor != null) {
            Vector3f outlineColorVector = new Vector3f(outlineColor.getRed() / 255.0f, outlineColor.getGreen() / 255.0f, outlineColor.getBlue() / 255.0f);
            if (outlineColorVector != selectedVectorSelection.getOutlineColor()) {
                selectionManager.setOutlineVectorSelectionBackgroundVisible(selectedVectorSelection, outlineColorVector, outlineSelected);
            }
        }
        if (alpha!=null) {
            selectionManager.changeAlphaBackgroundVectorSelection(layer,alpha.floatValue());
        }

    }

    public boolean changePreviewingTexture(String mode) {
        rasterToolBar.setButtonSelected(mode);
        if (mode.equals(COMPOSITE)) {
            inspector.getInterpolationOptions().setVisible(false);
            overlay = false;
            previewingTexture = null;
            modeToolBar.enableAll();
            inspector.setVisible(true);
            return false;

        }
        else if (mode.equals(OVERLAY)) {
            overlay = true;
            previewingTexture = null;
            modeToolBar.disableAllButtonsButOne(NAVIGATION_MODE);
            inspector.setVisible(true);
            inspector.updateOverlay(rasterOverlayAlphas);
            return false;

        }
        else {
            overlay = false;
            previewingTexture = getTextureByName(mode);
            if (rasters.get(previewingTexture) instanceof MNTRasterGeoElement) {
                MNTRasterGeoElement mnt = (MNTRasterGeoElement)(rasters.get(previewingTexture));
                Color startColor = new Color (mnt.getMinColor().x, mnt.getMinColor().y, mnt.getMinColor().z);
                Color endColor = new Color (mnt.getMaxColor().x, mnt.getMaxColor().y, mnt.getMaxColor().z);
                inspector.updateInterpolation(startColor, endColor, mnt.getInterpolationMethod());
            }
            else {
                inspector.notShow();
            }
            modeToolBar.disableAllButtonsButOne(NAVIGATION_MODE);
            return true;
            //inspector.setVisible(false);

        }
    }

    public void updateMNT() {
        Layer rasterLayer = getRasterLayer(rasters.get(previewingTexture));
        canvas.pushEvent(new CanvasMNTEvent(CanvasEvent.MNT_UPDATED,(MNTRasterGeoElement)(rasters.get(previewingTexture)), rasterLayer));
    }
    public void changeAlphaInRasterOverlay(String rasterName, double newAlpha) {
        rasterOverlayAlphas.put(rasterName, newAlpha);
    }

    public void doMagicWand(int x, int y) {
        deselectAll();
        double[] worldCoord = canvas.getCamera().screenToWorld(x, y);
        int [] pixelPosition = rasters.get(backgroundTexture).getCoordinateInPx(worldCoord);
        RasterGeoElement backgroundRaster = rasters.get(backgroundTexture);
        ArrayList<double[]> selectionCoords = new ArrayList<>();
        LinkedList<Pixel> pixels = MagicWand.getCoordinatesFromMagicWand(((RGBRasterGeoElement)backgroundRaster).getColors(),pixelPosition[0], pixelPosition[1],backgroundRaster.getImageWidth(), backgroundRaster.getImageHeight(), magicWandTolerance);
        if (pixels.size()>0) {
            GeometryFactory geometryFactory = new GeometryFactory();
            Coordinate[] coordinatesArray = new Coordinate[pixels.size()];
            double[] coord;
            for (int i = 0; i < coordinatesArray.length - 1; i++) {
                coord = backgroundRaster.pxToCoordinate(new int[]{pixels.get(i).getX(), pixels.get(i).getY()});
                coordinatesArray[i] = new Coordinate(coord[0], coord[1]);
            }
            coordinatesArray[coordinatesArray.length - 1] = coordinatesArray[0];
            CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(coordinatesArray);
            LinearRing linearRing = new LinearRing(coordinateArraySequence, geometryFactory);
            linearRing.buffer(0);
            Geometry geometry = new Polygon(linearRing, null, geometryFactory);
            geometry.buffer(0);
            geometry = DouglasPeuckerSimplifier.simplify(geometry, geometry.getArea()/10000000);
            DrawnGeoElement magicDrawn = new DrawnGeoElement(geometry);
            magicDrawn.drawCompletePolygon(maxBorderOffset, baseTexture);


            magicDrawn.setIsDocked(true);
            drawnLayer.addElement(magicDrawn);
            selectedLayer = drawnLayer;
            magicDrawn.select();
            magicDrawn.setComposite(false);
            canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.GEOELEMENT_CREATED, magicDrawn, drawnLayer));
            selected = magicDrawn;
        }

    }

    //Copy and paste style
    public void copySelectedStyle() {
        DrawnGeoElement selectedDrawn = (DrawnGeoElement)selected;
        copiedRenderingFeatures = new RenderingFeatures(selectedDrawn);
        copiedElement = selectedDrawn;
    }

    public void pasteSelectedStyle() {
        DrawnGeoElement selectedDrawn = (DrawnGeoElement)selected;
        toggleCompositeDrawn(copiedRenderingFeatures.getIsComposite());
        selectedDrawn.setLineVisible(copiedRenderingFeatures.getOutlineVisible());
        selectedDrawn.setFillColor(copiedRenderingFeatures.getFillColor());
        selectedDrawn.setOutlineColor(copiedRenderingFeatures.getOutlineColor());
        selectedDrawn.setAlpha(copiedRenderingFeatures.getAlpha());
        if (selectionManager.getVectorSelectionsDrawn((DrawnGeoElement)copiedElement)==null || selectionManager.getVectorSelectionsDrawn((DrawnGeoElement)copiedElement).size()==0) {
            selectedDrawn.setTexture(copiedRenderingFeatures.getTexture());
            selectedDrawn.setRenderingType(copiedRenderingFeatures.getRenderingType());
        } else {
            selectionManager.copyVectorSelectionsDrawn(selectedDrawn, (DrawnGeoElement)copiedElement);
            updateInspectorDrawn();
        }
        if (copiedRenderingFeatures.getTreanslucentBorder()) {
            toggleTranslucentBorder(true);
        }

    }


    public HashMap<Texture, RasterGeoElement> getRasters() {
        return rasters;
    }

    public Layer getRasterLayer (RasterGeoElement rasterGeoElement) {
        for (Layer layer : layers) {
            if (layer instanceof RasterLayer) {
                if (layer.getElements().contains(rasterGeoElement)) {
                    return layer;
                }
            }
        }
        return null;
    }

    public Texture getPreviewingTexture() {
        return previewingTexture;
    }

    public boolean getOverlay() {
        return overlay;
    }

    public void setOverlay(boolean overlay) {
        this.overlay = overlay;
    }

    public Layer getRasterLayerByName(String name) {
        for (Layer layer: layers) {
            if (layer instanceof RasterLayer) {
                if (((RasterLayer) layer).getName().equals(name)) {
                    return layer;
                }
            }
        }
        return null;
    }

    public HashMap<String, Double> getRasterOverlayAlphas () {
        return rasterOverlayAlphas;
    }


    void initStateMachine() {
        StateMachineBuilder stateMachineBuilder = new StateMachineBuilder(this);
        stateMachine = stateMachineBuilder.getExpeStateMachine();
        stateMachine.setSource("canvas", canvas);
        stateMachine.setSource("shapeFileQuery", shapeFileQuery);
        stateMachine.setSource("canvasPanel", canvasPanel);
        stateMachine.setSource("inspector", inspector);
        stateMachine.setSource("tabbedPane", jtp);

        modeToolBar.addListeners(stateMachine);
        inspector.addListeners(stateMachine);
        rasterToolBar.addListeners(stateMachine);
        rasterToolBar.setButtonSelected(COMPOSITE);
        inspector.setStateMachine(stateMachine);

        for (ShapeFileTableSummary shapeFileTableSummary: shapeFileTableSummaries) {
            shapeFileTableSummary.addListeners(stateMachine);
        }

        popUpMenu.addListeners(stateMachine);
        stateMachine.start();
    }

    public void handleEvent(Event event) {
        stateMachine.handleEvent(event);
    }

    public Layer getLayerbyName(String name) {
        for (Layer layer: layers) {
            if (layer instanceof VectorLayer) {
                if (((VectorLayer) layer).getName().equals(name)) {
                    return layer;
                }
            }
        }
        return null;
    }

    public void toggleLayerVisibility(String name) {
        VectorLayer selected = (VectorLayer)getLayerbyName(name);
        selectionManager.toggleElementsInLayerVisibility(selected);
    }

    public HashMap<RasterGeoElement, Texture> getTextures() {
        return textures;
    }


    public double getScene_pixels_per_unit() {
        return scene_pixels_per_unit;
    }

    public Layer getSelectedLayer() {
        return selectedLayer;
    }

    public GeoElement getCurrentMovingGeoElement() {
        return currentMovingGeoElement;
    }

    public SelectionManager getSelectionManager() {
        return  selectionManager;
    }

    public PointGL[] getAlignPoints() {return alignPoints;}

    public void setSelectedLayer(Layer selectedLayer) {
        this.selectedLayer = selectedLayer;
    }

    public AlignementManager getAlignementManager(){
        return alignementManager;
    }

    public Texture getTextureByName(String name) {
        for (RasterGeoElement rasterGeoElement: textures.keySet()) {
            if (rasterGeoElement.getName().equals(name)) {
                return textures.get(rasterGeoElement);
            }
        }
        return null;
    }

    public Texture getBackgroundTexture() {
        return backgroundTexture;
    }

    public ModeToolBar getModeToolBar() {
        return modeToolBar;
    }

    public ModeToolBar getRasterToolBar() {
        return rasterToolBar;
    }

    public Layer getQuerySelectedLayer() {
        return querySelectedLayer;
    }

    public void setQuerySelectedLayer(Layer querySelectedLayer) {
        this.querySelectedLayer = querySelectedLayer;
    }

    public JTabbedPane getJtp() {
        return jtp;
    }

    public VectorSelection getSelectedVectorSelection() {
        return selectedVectorSelection;
    }

    public void setSelectedVectorSelection(VectorSelection selectedVectorSelection) {
        this.selectedVectorSelection = selectedVectorSelection;
    }


    public JFrame getShapeFileQuery() {
        return shapeFileQuery;
    }

    public JFrame getInspectorFrame() {return inspectorFrame;}

    public float getMagicWandTolerance() {
        return magicWandTolerance;
    }

    public void setMagicWandTolerance(float magicWandTolerance) {
        this.magicWandTolerance = magicWandTolerance;
    }

    public float getTraceAlpha() {
        return traceAlpha;
    }

    public GeoElement getInElement() {
        return inElement;
    }

    public void setInElement(GeoElement inElement) {
        this.inElement = inElement;
    }

    public void setPreviewingTexture(Texture texture) {
        this.previewingTexture = texture;
    }

    public Layer getInLayer() {return inLayer;}

    public Texture getBaseTexture() {return baseTexture;}

    public PopUpMenu getPopUpMenu() {return popUpMenu;}







}
