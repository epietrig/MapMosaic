/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import org.geotools.coverage.grid.GridCoverage2D;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.*;

/**
 * Created by mjlobo on 21/07/15.
 */
public class RGBRasterGeoElement extends RasterGeoElement {

    byte[] imageData;
    Color[][] colors;

    public RGBRasterGeoElement(GridCoverage2D coverage) {
        super (coverage);
        imageData = getRGBAPixelData(coverage.getRenderedImage());
    }

    public byte[] getImageData() {
        return  imageData;
    }

    public byte[] getRGBAPixelData(RenderedImage img) {
        int [] pixel = new int[3];
        byte[] imgRGBA;
        int height = img.getHeight();
        int width = img.getWidth();
        ComponentColorModel colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[]{8, 8, 8, 8}, true, false, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);
        WritableRaster raster  = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, width, height, 4, null);
        BufferedImage newImage = new BufferedImage(colorModel, raster, false, null);
        DataBufferByte dataBuf = (DataBufferByte) raster.getDataBuffer();
        java.awt.geom.AffineTransform gt = new java.awt.geom.AffineTransform();
        gt.translate(0, height);
        gt.scale(1,-1d);
        Graphics2D g = newImage.createGraphics();
        //g.transform(gt);
        g.drawRenderedImage(img,gt);
        g.dispose();

        // DataBufferByte dataBuf = (DataBufferByte) raster.getDataBuffer();
        imgRGBA = dataBuf.getData();
        colors = convertTo2DWithoutUsingGetRGB(imgRGBA, newImage);
        //System.out.println("imgRGBA" + dataBuf.getData());
        return imgRGBA;
    }

    private Color[][] convertTo2DWithoutUsingGetRGB(byte[] pixels, BufferedImage image) {

        //final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        final int width = image.getWidth();
        final int height = image.getHeight();
        final boolean hasAlphaChannel = image.getAlphaRaster() != null;



        int[][] result = new int[height][width];
        Color[][] resultColor = new Color[width][height];
        if (hasAlphaChannel) {
            final int pixelLength = 4;
            //System.out.println("buffered image type "+ (image.getType() == BufferedImage.TYPE_CUSTOM));
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;

                int red = ((int) pixels[pixel] & 0xff);
                //System.out.println("red "+red);
                int green = (((int) pixels[pixel+1] & 0xff)); // green
                //System.out.println("green "+green);
                int blue = (((int) pixels[pixel + 2] & 0xff));
                //System.out.println("blue "+blue);
                int alpha = (((int) pixels[pixel] & 0xff) << 24);

                result[row][col] = argb;
                //resultColor[col][row] = new Color (argb);
                resultColor[col][height-row-1] = new Color (red, green, blue);
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        } else {
            final int pixelLength = 3;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += -16777216; // 255 alpha
                argb += ((int) pixels[pixel] & 0xff); // blue
                argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
                argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
                resultColor[col][row] = new Color (argb);
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        }

        return resultColor;
    }

    public Color[][] getColors() {
        return colors;
    }

}
