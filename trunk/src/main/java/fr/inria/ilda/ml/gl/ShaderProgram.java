/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.gl;

import java.util.Hashtable;

/**
 * Created by mjlobo on 27/02/15.
 */
public class ShaderProgram {
    int programId;
    Hashtable<String, Integer> uniforms;
    Hashtable<Integer, String> uniformMethods;
    Hashtable<String, Integer> attributes;
    Hashtable<Integer, String> attributesMethods;


    public ShaderProgram (int programId) {
        this.programId = programId;
        uniforms = new Hashtable<>();
        uniformMethods = new Hashtable<>();
        attributes = new Hashtable<>();
        attributesMethods = new Hashtable<>();
    }

    public void addUniform(int uniformId, String uniformName) {
        uniforms.put(uniformName, uniformId);
        //uniformMethods.put(uniformId, method);
    }

    public void addAttribute(int attributeid, String attributeName) {
        attributes.put(attributeName, attributeid);
        //attributes.put(attributeid, method);
    }

    public Hashtable<Integer, String> getUniformsMethods() {
        return uniformMethods;
    }

    public Hashtable<Integer, String> getAttributesMethods() {
        return attributesMethods;
    }

    public int getProgramId () {
        return programId;
    }

    public int getUniform (String uniformName) {
        return uniforms.get(uniformName);
    }

    public int getAttribute (String attributeName) {
        return attributes.get(attributeName);
    }


}
