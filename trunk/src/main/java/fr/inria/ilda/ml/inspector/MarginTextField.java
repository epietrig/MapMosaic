/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import fr.inria.ilda.ml.StateMachine.LayerOptionChanged;
import fr.inria.ilda.ml.StateMachine.StateMachine;
import fr.inria.ilda.ml.StateMachine.TextFieldEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.NumberFormat;

/**
 * Created by mjlobo on 06/08/15.
 */
public class MarginTextField extends JComponent{

    JFormattedTextField textField;
    JLabel marginLabel;
    String text;

    public MarginTextField(String text) {
        this.text = text;
        this.setBorder(BorderFactory.createEmptyBorder(2,5,2,2));
        initUI();
        setAlignmentX(Component.LEFT_ALIGNMENT);
        setLayout(new FlowLayout(FlowLayout.LEFT));
    }

    void initUI() {
        marginLabel = new JLabel(text);
        marginLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        //DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        //decimalFormat.setGroupingUsed(false);
        textField = new JFormattedTextField(numberFormat);
        textField.setText("0.0");
        textField.setColumns(3);
//        textField = new JTextField(2);
//        Document doc = textField.getDocument();
//        if (doc instanceof AbstractDocument) {
//            ((AbstractDocument) doc).setDocumentFilter(new DocumentNumberFilter(2));
//        }
        //colorLabel.setHorizontalAlignment(Component.LEFT_ALIGNMENT);
        add(marginLabel);
        add(textField);
    }

    public void setEnabled(boolean enabled) {
        textField.setEnabled(enabled);
        marginLabel.setEnabled(enabled);
    }

    public void addListeners(final StateMachine stateMachine) {
        textField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("texfield action listener");
                stateMachine.handleEvent(new TextFieldEvent(textField,textField.getText()));
            }
        });
        textField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                //System.out.println("Focus Lost!");
                stateMachine.handleEvent(new TextFieldEvent(textField,textField.getText()));
            }
        });
    }

    public void addListenersSelection(final String layerName, final StateMachine stateMachine) {
        textField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("texfield action listener");
                try {
                    double d = Double.valueOf(textField.getText());
                    stateMachine.handleEvent(new LayerOptionChanged(d, layerName, textField.getName()));
                }
                catch(NumberFormatException exception) {
                    exception.printStackTrace();
                }

            }
        });
        textField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                try {
                    double d = Double.valueOf(textField.getText());
                    stateMachine.handleEvent(new LayerOptionChanged(d, layerName, textField.getName()));
                }
                catch(NumberFormatException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }

    void updateText(String text) {
        //System.out.println("update!");
        textField.setText(text);
    }

    public void setName(String name) {
        super.setName(name);
        textField.setName(name);
    }




}
