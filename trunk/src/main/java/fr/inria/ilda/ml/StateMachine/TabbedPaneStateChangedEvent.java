/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.StateMachine;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by mjlobo on 23/06/15.
 */
public class TabbedPaneStateChangedEvent extends Event{

    JTabbedPane target;

    public TabbedPaneStateChangedEvent(JTabbedPane target) {
        super();
        source = this.target = target;
    }

    private static TabbedPaneStateChangedEventListener listener = new TabbedPaneStateChangedEventListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof JTabbedPane ) {
            listener.attachTo( stateMachine, (JTabbedPane)target );
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof JTabbedPane) {
            listener.detachFrom( stateMachine, (JTabbedPane)target );
        }
    }

    public JTabbedPane getTarget() {
        return target;
    }


}

class TabbedPaneStateChangedEventListener implements ChangeListener {

    private HashMap<Component, HashSet<StateMachine>> listeners;
    public TabbedPaneStateChangedEventListener() {
        listeners = new HashMap<Component, HashSet<StateMachine>>();
    }

    public void attachTo( StateMachine stateMachine, JTabbedPane target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            // Target was not found, start listening to it
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            target.addChangeListener(this);
            //System.out.println("Added MouseMoveListener");
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, JTabbedPane target ) {
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                target.removeChangeListener(this);
                //System.out.println("Removed MouseMoveListener");
            }
        }
    }


    @Override
    public void stateChanged(ChangeEvent event) {
        JTabbedPane target = (JTabbedPane)event.getSource();
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            // Create custom event
            TabbedPaneStateChangedEvent customEvent = new TabbedPaneStateChangedEvent(target);
            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
        }
    }
}
