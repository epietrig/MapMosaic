/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import fr.inria.ilda.ml.canvasEvents.CanvasDrawnEvent;
import fr.inria.ilda.ml.canvasEvents.CanvasEvent;

import javax.swing.*;

/**
 * Created by mjlobo on 08/08/15.
 */
public class BufferWorker {
    GeoElement selected;
    double bufferOffset;
    Layer selectedLayer;
    MapGLCanvas mapGLCanvas;
    SwingWorker bufferCalculator;

    public BufferWorker(GeoElement selected, double bufferOffset, MapGLCanvas mapGLCanvas, Layer selectedLayer) {
        this.selected = selected;
        this.bufferOffset = bufferOffset;
        this.mapGLCanvas = mapGLCanvas;
        this.selectedLayer = selectedLayer;
        bufferCalculator = new BufferCalculator();
    }


    public void startWorker() {
        bufferCalculator.execute();
    }


    class BufferCalculator extends SwingWorker<GeoElement, String> {

        @Override
        protected GeoElement doInBackground() throws Exception {
            selected.bufferPolygon(bufferOffset);
            return selected;
        }

        protected void done() {
            selected.setBlurredTexture(null);
            mapGLCanvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.ELEMENT_MODIFIED, selected,selectedLayer));
        }

    }

}
