/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.io.IOException;

/**
 * Created by mjlobo on 05/10/15.
 */
public class InspectorDropTargetListener extends DropTargetAdapter {
    private DropTarget dropTarget;
    private Inspector inspector;
    GridBagConstraints constraints;
    DataFlavor layerOptionsDataFlavour;

    public InspectorDropTargetListener(Inspector inspector, GridBagConstraints constraints, DataFlavor layerOptionsDataFlavour) {
        this.inspector = inspector;
        this.constraints = constraints;
        this.layerOptionsDataFlavour = layerOptionsDataFlavour;
        dropTarget = new DropTarget(inspector, DnDConstants.ACTION_COPY, this, true, null);

    }
    @Override
    public void drop(DropTargetDropEvent dtde) {
        Transferable tr = dtde.getTransferable();
        try {
            //LayerOptions layerOptions = (LayerOptions)tr.getTransferData(layerOptionsDataFlavour);
            String layerOptionsName = (String)tr.getTransferData(DataFlavor.stringFlavor);
            LayerOptions layerOptions = inspector.getLayerOptionsbyLayerName(layerOptionsName);
            dtde.acceptDrop(DnDConstants.ACTION_COPY);
            dtde.dropComplete(true);
            Point p = inspector.getMousePosition();
            Component comp = inspector.getLayerOptionsAt(p);

            if (comp != layerOptions && comp instanceof LayerOptions)  // over a child component
            {
                inspector.swapLayerOptions(layerOptions, (LayerOptions)comp);
            }

            inspector.revalidate();
            return;

        }
        catch (UnsupportedFlavorException | IOException e) {
            dtde.rejectDrop();
            e.printStackTrace();
        }
    }



}
