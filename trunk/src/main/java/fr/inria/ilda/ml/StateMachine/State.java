/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.StateMachine;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 22/03/15.
 */
public class State {
    //HashMap<Class, Action> actions;
    HashMap<String,HashMap<Class,Action>> actions;

    public State() {
        actions = new HashMap<String, HashMap<Class,Action>>();
    }

    public State addAction(Class event, Action action) {
        return addAction(event,null,action);
    }

    public State addAction(Class event, String sourceName, Action action) {
        if (!actions.containsKey(sourceName)) {
            actions.put(sourceName, new HashMap<Class, Action>());
        }
        actions.get(sourceName).put(event,action);
        return this;
    }

    public State addActions(Class event, List<String> sourceNames, Action action) {
        for (String s: sourceNames) {
            addAction(event, s, action);
        }
        return this;
    }

    public State removeAction(Class event) {
        return removeAction(event,null);
    }

    public State removeAction(Class event, String sourceName) {
        if (actions.containsKey(sourceName)) {
            actions.get(sourceName).remove(event);
        }
        return this;
    }

    public String handleEvent(StateMachine stateMachine, Event event) {
        Class eventClass = event.getClass();
        Object source = event.getSource();
        String sourceName = stateMachine.getNameOfSource(source);
        HashMap<Class,Action> actionsForSource = actions.get(sourceName);
        if (actionsForSource != null) {
            if (actionsForSource.containsKey(eventClass)) {
                return actionsForSource.get(eventClass).perform(stateMachine, event);
            }
        }

        return null;
    }

    public void start(StateMachine stateMachine) {
        for (String sourceName : actions.keySet()) {
            Object target = stateMachine.getSourceOfName(sourceName);
            if (target != null) {
                for (Class eventClass : actions.get(sourceName).keySet()) {
                    try {
                        eventClass.getMethod("attachTo", new Class[]{StateMachine.class, Object.class}).invoke(null, stateMachine, target);
                    } catch (IllegalAccessException error) {
                        error.printStackTrace();
                    } catch (IllegalArgumentException error) {
                        error.printStackTrace();
                    } catch (InvocationTargetException error) {
                        error.printStackTrace();
                    } catch (NoSuchMethodException exception) {
                        exception.printStackTrace();
                    }
                }
            }
        }
    }

    public void stop (StateMachine stateMachine) {
        for (String sourceName : actions.keySet()) {
            Object target = stateMachine.getSourceOfName(sourceName);
            if (target != null) {
                for (Class eventClass : actions.get(sourceName).keySet()) {
                    try {
                        eventClass.getMethod("detachFrom", new Class[]{StateMachine.class, Object.class}).invoke(null, stateMachine, target);
                    } catch (IllegalAccessException error) {
                        error.printStackTrace();
                    } catch (IllegalArgumentException error) {
                        error.printStackTrace();
                    } catch (InvocationTargetException error) {
                        error.printStackTrace();
                    } catch (NoSuchMethodException exception) {
                        exception.printStackTrace();
                    }
                }
            }
        }
    }

}
