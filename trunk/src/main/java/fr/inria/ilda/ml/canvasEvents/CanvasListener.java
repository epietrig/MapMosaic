/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.canvasEvents;

import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.ml.DrawnGeoElement;
import fr.inria.ilda.ml.MapLayerViewer;
import fr.inria.ilda.ml.StateMachine.LeftMouseButtonReleaseEvent;
import fr.inria.ilda.ml.StateMachine.RightMouseButtonPressEvent;

import java.awt.event.*;

//import javax.media.opengl.GLAutoDrawable;

/**
 * Created by mjlobo on 15/02/15.
 */
public class CanvasListener implements KeyListener, MouseListener, MouseMotionListener {
    MapLayerViewer mapLayerViewer;
    int mode = 0;
    static int DRAWING = 1;
    static int NORMAL = 0;
    boolean dragging = false;
    GLAutoDrawable gl;
    double[] startPoint;
    int[] beginPoint;
    public CanvasListener(MapLayerViewer mapLayerViewer) {

        this.mapLayerViewer = mapLayerViewer;
    }



    //Event handling
    @Override
    public void keyTyped(KeyEvent e) {
        //System.out.println(e.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println(e.getKeyChar());
        if (e.getKeyChar()=='b') {
            //System.out.println("It's V");
            mapLayerViewer.getLayers().get(2).setVisibility(!mapLayerViewer.getLayers().get(2).isVisible);
        }
        if (e.getKeyChar()=='o') {
            //System.out.println("It's V");
            mapLayerViewer.getLayers().get(1).setVisibility(!mapLayerViewer.getLayers().get(1).isVisible);
        }
        if (e.getKeyChar()=='s') {
            //System.out.println("Size"+mapLayerViewer.frame.getBounds().getSize());
        }

        if (e.getKeyChar()=='d') {
            mode = DRAWING;
            //System.out.println("Now in mode drawing");
            //mapLayerViewer.createDrawnGeoElement();
        }
        if (e.getKeyChar()=='n') {
            mode = NORMAL;
            //mapLayerViewer.finishDrawnGeoElement();
        }

        if (e.getKeyChar() == 'z') {
            mapLayerViewer.getCanvas().getCamera().zoomIn(mapLayerViewer.getCanvas().getWidth()/2, mapLayerViewer.getCanvas().getHeight()/2);
        }
        if (e.getKeyChar() == 'x') {
            mapLayerViewer.getCanvas().getCamera().zoomOut(mapLayerViewer.getCanvas().getWidth()/2, mapLayerViewer.getCanvas().getHeight()/2);
        }


//        if (e.getKeyChar() == 'f') {
//            mapLayerViewer.bufferDrawnGeoElement();
//        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println(e.getButton());
        //System.out.println("Mouse Pressed!: "+ e.getX()+": "+e.getY());
        if (e.getButton() == MouseEvent.BUTTON3) {

            //mapLayerViewer.selectDrawnGeolementAt(e.getX(), e.getY());
            mapLayerViewer.handleEvent(new RightMouseButtonPressEvent(e.getX(), e.getY()));
            startPoint = mapLayerViewer.getCanvas().getCamera().screenToWorld(e.getX(),e.getY());
            startPoint[0]-=((DrawnGeoElement)mapLayerViewer.getSelected()).getLocation().x;
            startPoint[1]-=((DrawnGeoElement)mapLayerViewer.getSelected()).getLocation().y;

        }
        else {
            //mapLayerViewer.newDrawnGeoElement(e.getX(), e.getY());
            //mapLayerViewer.handleEvent(new LeftMouseButtonPressEvent(e.getX(), e.getY()));
        }

        //mapLayerViewer.addPointDrawnGeoElement(e.getX(),e.getY());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            //mapLayerViewer.finishDrawnGeoElement();
            mapLayerViewer.handleEvent(new LeftMouseButtonReleaseEvent(e.getX(),e.getY()));
        }

        if (e.getButton() == MouseEvent.BUTTON3) {
            //mapLayerViewer.finishMovingDrawnObject();
            //mapLayerViewer.handleEvent(new RightMouseButtonReleaseEvent(e.getX(),e.getY()));
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        //System.out.println("dragged "+e.getX()+" "+e.getY());
        if (e.getButton() == MouseEvent.BUTTON1) {
            //mapLayerViewer.addPointDrawnGeoElement(e.getX(), e.getY());
            //mapLayerViewer.handleEvent(new MouseMoveEvent(e.getX(),e.getY()));
        }
        else if (e.getButton() == MouseEvent.BUTTON3) {
            double[] newPoint = mapLayerViewer.getCanvas().getCamera().screenToWorld(e.getX(), e.getY());
            double[] diff = new double[2];
            diff[0] = newPoint[0]-startPoint[0]-((DrawnGeoElement)mapLayerViewer.getSelected()).getLocation().x;
            diff[1] = newPoint[1]-startPoint[1]-((DrawnGeoElement)mapLayerViewer.getSelected()).getLocation().y;

            //mapLayerViewer.moveDrawnObject(new float[] {newPoint[0]-startPoint[0], newPoint[1]-startPoint[1]});
            //mapLayerViewer.moveDrawnObject(diff);
            //mapLayerViewer.handleEvent(new MouseMoveEvent(e.getX(),e.getY(), diff));
        }

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

}
