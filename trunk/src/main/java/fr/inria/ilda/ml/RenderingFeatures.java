/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import fr.inria.ilda.ml.DrawnGeoElement;
import fr.inria.ilda.ml.Texture;

import javax.vecmath.Vector3f;

/**
 * Created by mjlobo on 02/06/16.
 */
public class RenderingFeatures {
    boolean visible;
    boolean outlineVisible;
    Vector3f fillColor;
    Vector3f outlineColor;
    boolean translucentBorder;
    boolean isDocked;
    double buffer;
    float alpha;
    Texture texture;
    boolean isComposite;
    GLUtilities.RenderedElementTypes renderingType;

    public RenderingFeatures(DrawnGeoElement geoElement) {
        this.visible = true;
        this.outlineVisible = geoElement.getOutlineVisible();
        this.fillColor = geoElement.getFillColor();
        this.outlineColor = geoElement.getOutlineColor();
        this.translucentBorder = geoElement.getTranslucentBorder();
        this.isDocked = geoElement.getIsDocked();
        this.buffer = geoElement.getBuffer();
        this.alpha = geoElement.getAlpha();
        this.texture = geoElement.getTexture();
        this.isComposite = geoElement.getIsComposite();
        this.renderingType = geoElement.getRenderingType();

    }

    public boolean getVisible() {
        return visible;
    }

    public boolean getOutlineVisible() {
        return outlineVisible;
    }

    public Vector3f getFillColor() {
        return fillColor;
    }

    public Vector3f getOutlineColor() {
        return outlineColor;
    }

    public boolean getTreanslucentBorder() {
        return translucentBorder;
    }

    public boolean getIsDocked() {
        return isDocked;
    }

    public double getBuffer() {
        return buffer;
    }

    public float getAlpha() {
        return alpha;
    }

    public Texture getTexture() {
        return texture;
    }

    public boolean getIsComposite() {
        return isComposite;
    }

    public GLUtilities.RenderedElementTypes getRenderingType() {
        return renderingType;
    }
}
