/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.StateMachine;

/**
 * Created by mjlobo on 20/05/16.
 */
public class FreeQueryEvent extends Event {
    String query;
    String typeName;

    public FreeQueryEvent(String query, String typeName) {
        this.query = query;
        this.typeName = typeName;
    }

    public String getQuery() {
        return query;
    }

    public String getTypeName() {
        return typeName;
    }
}
