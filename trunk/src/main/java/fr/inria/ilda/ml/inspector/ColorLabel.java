/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import fr.inria.ilda.ml.StateMachine.ColorChangedEvent;
import fr.inria.ilda.ml.StateMachine.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by mjlobo on 30/09/15.
 */
public class ColorLabel extends JComponent{
    JLabel colorLabel;
    String text;
    Color color;
    JColorChooser jColorChooser;
    JLabel textLabel;

    public ColorLabel(String text, Color color) {
        this.text = text;
        this.color = color;
        jColorChooser = new JColorChooser();
        setLayout(new FlowLayout(FlowLayout.LEFT));
        initUI();

    }

    void initUI() {
        textLabel = new JLabel(text);
        textLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        textLabel.setHorizontalAlignment(SwingConstants.LEFT);
        colorLabel = new JLabel("");
        colorLabel.setPreferredSize(new Dimension(50,15));
        colorLabel.setBackground(color);
        colorLabel.setOpaque(true);
        colorLabel.setHorizontalAlignment(SwingConstants.LEFT);
        add(textLabel);
        add(colorLabel);
    }

    public void update (Color color) {
        colorLabel.setBackground(color);
    }

    public void addListeners(final StateMachine stateMachine) {
        colorLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                //System.out.println("click on label!");
                Color newColor = JColorChooser.showDialog(ColorLabel.this,"Color picker",color);
                if (newColor!=null) {
                    ColorLabel.this.color = newColor;
                    ColorLabel.this.colorLabel.setBackground(newColor);
                    stateMachine.handleEvent(new ColorChangedEvent(colorLabel, color));
                }
            }
        });
    }

    public void setName(String name) {
        super.setName(name);
        colorLabel.setName(name);
    }

}
