/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import fr.inria.ilda.ml.StateMachine.CheckBoxEvent;
import fr.inria.ilda.ml.StateMachine.ColorChangedEvent;
import fr.inria.ilda.ml.StateMachine.LayerOptionChanged;
import fr.inria.ilda.ml.StateMachine.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by mjlobo on 06/08/15.
 */
public class ColorCheckbox extends JComponent{
    JCheckBox checkBox;
    JLabel colorLabel;
    String text;
    Color color;
    JColorChooser jColorChooser;

    public ColorCheckbox(String text, Color color) {
        this.text = text;
        this.color = color;
        jColorChooser = new JColorChooser();
        setLayout(new FlowLayout(FlowLayout.LEFT));
        initUI();
        //setAlignmentX(Component.LEFT_ALIGNMENT);

    }

    void initUI() {
        checkBox = new JCheckBox(text);
        checkBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        checkBox.setHorizontalAlignment(SwingConstants.LEFT);
        colorLabel = new JLabel("");
        colorLabel.setPreferredSize(new Dimension(50,15));
        //colorLabel.setMinimumSize(new Dimension(50, 15));
        colorLabel.setBackground(color);
        colorLabel.setOpaque(true);
        //colorLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        colorLabel.setHorizontalAlignment(SwingConstants.LEFT);

        add(checkBox);
        add(colorLabel);
    }

    public void setEnabled(boolean enabled) {
        checkBox.setEnabled(enabled);
        colorLabel.setEnabled(enabled);
    }

    public void update(boolean selected, Color color) {
        checkBox.setSelected(selected);
        colorLabel.setBackground(color);
        this.color = color;
    }

    public void addListeners(final StateMachine stateMachine) {
        checkBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new CheckBoxEvent(checkBox.getText(), checkBox));
            }
        });
        colorLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                //System.out.println("click on label!");
                Color newColor = JColorChooser.showDialog(ColorCheckbox.this,"Color picker",color);
                if (newColor!=null) {
                    ColorCheckbox.this.color = newColor;
                    ColorCheckbox.this.colorLabel.setBackground(newColor);
                    stateMachine.handleEvent(new ColorChangedEvent(colorLabel, color));
                }
            }
        });

    }

    public void addListenersSelection(final String layer, final StateMachine stateMachine) {
        checkBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new LayerOptionChanged(checkBox.isSelected(), color,layer));
            }
        });
        colorLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                //System.out.println("click on label!");
                Color newColor = JColorChooser.showDialog(ColorCheckbox.this,"Color picker",color);
                if (newColor!=null) {
                    ColorCheckbox.this.color = newColor;
                    ColorCheckbox.this.colorLabel.setBackground(newColor);
                    stateMachine.handleEvent(new LayerOptionChanged(checkBox.isSelected(), color, layer));
                }
            }
        });
    }

    public JCheckBox getCheckBox() {
        return checkBox;
    }

}
