/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import java.util.ArrayList;

/**
 * Created by mjlobo on 09/02/15.
 */
public class RasterLayer extends Layer {

    protected String name;

    public RasterLayer (boolean isVisible) {
        super(isVisible);
    }

    public RasterLayer(boolean isVisible, String name) {
        super(isVisible);
        this.name = name;
    }
    public ArrayList<GeoElement> getElements() {
        return elements;
    }

    public void addElement (GeoElement geoElement) {
        elements.add(geoElement);
    }

    public String getName() {
        return name;
    }
}
