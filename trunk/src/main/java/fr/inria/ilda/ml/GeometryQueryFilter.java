/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml;

import com.vividsolutions.jts.geom.Geometry;

import java.util.HashSet;
import java.util.List;

/**
 * Created by mjlobo on 11/06/15.
 */
public class GeometryQueryFilter extends QueryFilter {
    Geometry geometry;
    HashSet<GeoElement> elements;
    DrawnGeoElement drawnGeoElement;
    HashSet<ShapeFileFeature> features;

    public GeometryQueryFilter (DrawnGeoElement drawnGeoElement, VectorLayer layer) {
        this.geometry = (Geometry)drawnGeoElement.getGeometry().clone();
        this.drawnGeoElement = drawnGeoElement;
        this.layer = layer;
    }



    void offsetSelectionGeometry(DrawnGeoElement drawnGeoElement) {
        for (int i=0; i<geometry.getCoordinates().length; i++) {
            if (drawnGeoElement.getOffset()!=null) {
                geometry.getCoordinates()[i].x = drawnGeoElement.getGeometry().getCoordinates()[i].x + drawnGeoElement.getOffset()[0];
                geometry.getCoordinates()[i].y = drawnGeoElement.getGeometry().getCoordinates()[i].y + drawnGeoElement.getOffset()[1];
                geometry.geometryChanged();
            }
        }
    }

    public HashSet<GeoElement> getElements() {
        //elements = layer.getElementsContainedInGeometry(geometry);
        elements = layer.getElementsIntersectingGeometry(geometry);
        return elements;
    }

    @Override
    public HashSet<GeoElement> getElements(List<GeoElement> elementsToConsider) {
        elements = layer.getElementsIntersectingGeometry(geometry, elementsToConsider);
        return elements;
    }

    @Override
    public HashSet<ShapeFileFeature> getFeatures() {
        features = ((VectorLayer)layer).getFeaturesIntersectingGeometry(geometry);
        return features;
    }

    @Override
    public HashSet<ShapeFileFeature> getFeatures(HashSet<ShapeFileFeature> featuresToConsider) {
        features = ((VectorLayer)layer).getFeaturesIntersectingGeometry(geometry, featuresToConsider);
        return features;
    }

    public HashSet<GeoElement> update() {
        offsetSelectionGeometry(drawnGeoElement);
        HashSet<GeoElement> newSelectedElements = getElements();
        return newSelectedElements;

    }

    @Override
    public HashSet<ShapeFileFeature> updateFeatures(HashSet<ShapeFileFeature> featuresToConsider) {
        offsetSelectionGeometry(drawnGeoElement);
        return getFeatures(featuresToConsider);
    }

    @Override
    public HashSet<ShapeFileFeature> updateFeatures() {
        offsetSelectionGeometry(drawnGeoElement);
        return getFeatures();
    }

    public DrawnGeoElement getDrawnGeoElement() {
     return drawnGeoElement;
    }

    public Geometry getGeometry() {
        return geometry;
    }
}
