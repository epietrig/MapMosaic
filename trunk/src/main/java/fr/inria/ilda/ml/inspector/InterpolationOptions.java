/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.inspector;

import fr.inria.ilda.ml.MapLayerViewer;
import fr.inria.ilda.ml.StateMachine.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by mjlobo on 30/09/15.
 */
public class InterpolationOptions extends JPanel {
    ColorLabel startColor;
    ColorLabel endColor;
    StringComboPanel interpolation;


    public InterpolationOptions(Color start, Color end) {
        startColor = new ColorLabel("Star Color", start);
        startColor.setName(MapLayerViewer.INTERPOLATION_START);
        endColor = new ColorLabel("End Color", end);
        endColor.setName(MapLayerViewer.INTERPOLATION_END);

        ArrayList<String> interpolationValues = new ArrayList<>();
        interpolationValues.add(MapLayerViewer.LINEAR_INTERPOLATION);
        interpolationValues.add(MapLayerViewer.SIGMOID_INTERPOLATION);
        interpolation = new StringComboPanel("Interpolation",interpolationValues);

        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.gridheight = 1;
        gbc.weightx = 0.5;
        gbc.weighty =0;
        gbc.anchor = GridBagConstraints.LINE_START;

        add(interpolation, gbc);
        gbc.gridy +=1;
        add(startColor, gbc);
        gbc.gridy +=1;
        add(endColor, gbc);
    }

    public void addListeners(StateMachine stateMachine) {
        startColor.addListeners(stateMachine);
        endColor.addListeners(stateMachine);
        interpolation.addListeners(stateMachine);
    }

    public void update (Color start, Color end, String interpolationText) {
        startColor.update(start);
        endColor.update(end);
        interpolation.update(interpolationText);
    }


}
