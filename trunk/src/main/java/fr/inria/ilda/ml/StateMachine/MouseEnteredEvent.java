/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.StateMachine;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by mjlobo on 02/09/15.
 */
public class MouseEnteredEvent  extends Event {
    Component target;
    int x;
    int y;

    public MouseEnteredEvent(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }

    public MouseEnteredEvent(java.awt.Component target, int x, int y) {
        super();
        source = this.target = target;
        this.x = x;
        this.y = y;
    }

    private static MouseEnteredEventListener listener = new MouseEnteredEventListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.attachTo( stateMachine, (java.awt.Component)target );
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.detachFrom( stateMachine, (java.awt.Component)target );
        }
    }

    public java.awt.Component getTarget() {
        return target;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}

class MouseEnteredEventListener implements java.awt.event.MouseListener {

    private HashMap<Component, HashSet<StateMachine>> listeners;

    public MouseEnteredEventListener() {
        listeners = new HashMap<Component, HashSet<StateMachine>>();
    }

    public void attachTo( StateMachine stateMachine, java.awt.Component target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            // Target was not found, start listening to it
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            target.addMouseListener(this);
            //System.out.println("Added MouseMoveListener");
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, java.awt.Component target ) {
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                target.removeMouseListener( this );
                //System.out.println("Removed MouseMoveListener");
            }
        }
    }




    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent event) {
        java.awt.Component target = (java.awt.Component)event.getSource();
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            // Create custom event
            MouseEnteredEvent customEvent = new MouseEnteredEvent( target, event.getX(), event.getY() );
            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
