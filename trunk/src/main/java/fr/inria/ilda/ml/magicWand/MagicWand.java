/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.ml.magicWand;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by mjlobo on 07/09/15.
 */
public class MagicWand {

    public static LinkedList<Pixel> getCoordinatesFromMagicWand(Color[][] colors, int i, int j, int width, int height, double distance) {
        Color targetColor = colors[i][j];
        Integer[][] maskColors = new Integer[width][height];
        LinkedList<Segment> segmentStack = new LinkedList<Segment>();

        for (int k=0; k<width; k++) {
            for (int l=0; l<height; l++) {
                maskColors[k][l]=-1;
            }
        }

        Segment currentSegment = processLine(targetColor, colors, i,j, distance);
        for (Pixel pixel: currentSegment.getPixels()) {
            maskColors[pixel.getX()][pixel.getY()] = 1;
        }

        segmentStack.add(currentSegment);
        Segment addedSegment;
        while (segmentStack.size()>0) {
            currentSegment = segmentStack.pop();
            for (Pixel pixel:currentSegment.getPixels()) {
                if (pixel.getX() + 1 < colors.length) {
                    if (colorsClose(targetColor, colors[pixel.getX() + 1][pixel.getY()], distance) && maskColors[pixel.getX() + 1][pixel.getY()] == -1) {
                        addedSegment = processLine(targetColor, colors, pixel.getX() + 1, pixel.getY(), distance);
                        for (Pixel pixelAdded : addedSegment.getPixels()) {
                            maskColors[pixelAdded.getX()][pixelAdded.getY()] = 1;
                        }
                        segmentStack.push(addedSegment);
                    }
                }
                if (pixel.getX() - 1 > 0) {
                    if (colorsClose(targetColor, colors[pixel.getX() - 1][pixel.getY()], distance) && maskColors[pixel.getX() - 1][pixel.getY()] == -1) {
                        addedSegment = processLine(targetColor, colors, pixel.getX() - 1, pixel.getY(), distance);
                        for (Pixel pixelAdded : addedSegment.getPixels()) {
                            maskColors[pixelAdded.getX()][pixelAdded.getY()] = 1;
                        }
                        segmentStack.push(addedSegment);
                    }
                }
            }
        }




        HashMap<Pixel, LinkedList<Segment>> pathHashMap = getMaskSegments(maskColors, height, width);
        LinkedList<Pixel> pathCoordinates = buildPath(pathHashMap);

        return pathCoordinates;
    }

    static Segment processLine(Color targetColor, Color[][] colors, int i, int j, double distance) {
        //System.out.println("start processLine");
        Segment currentSegment = new Segment();
        int colorCount = 0;
        Color comparedColor = colors[i][j+colorCount];
        LinkedList<Pixel> currentLine = new LinkedList<Pixel>();
        currentLine.add(new Pixel(i,j));
        while(colorsClose(targetColor, comparedColor,distance) && j+colorCount<colors[0].length) {
            comparedColor = colors[i][j+colorCount];
            currentSegment.addPixel(new Pixel(i, j + colorCount));
            colorCount ++;

        }

        colorCount = 0;
        comparedColor = colors[i][j-colorCount];
        while(colorsClose(targetColor, comparedColor, distance) && colorCount<=j && j+colorCount<colors[0].length && j>=0) {
            comparedColor = colors[i][j-colorCount];
            //System.out.println("j "+j);
            //System.out.println("colorCount "+colorCount);
            currentSegment.getPixels().addFirst(new Pixel(i, j - colorCount));
            colorCount ++;

        }
        //System.out.println("end processLine");
        return currentSegment;

    }

    static boolean colorsClose(Color c1, Color c2, double distance) {
        double maxDist = 0;
        float[] colorComponentsC1 = new float[3];
        float[] colorComponentsC2 = new float[3];
        c1.getColorComponents(c1.getColorSpace(),colorComponentsC1);
        c2.getColorComponents(c2.getColorSpace(), colorComponentsC2);
        float newDist;
        for (int i=0; i<c1.getColorSpace().getNumComponents(); i++) {
            newDist = Math.abs(colorComponentsC1[i]-colorComponentsC2[i]);
            if (maxDist<newDist) {
                maxDist = newDist;
            }
        }

        if (maxDist <distance) {
            return true;
        }
        //return c1.equals(c2);
        return false;
    }

    static HashMap<Pixel, LinkedList<Segment>> getMaskSegments(Integer[][] maskColors, int rows, int columns) {
        HashMap<Pixel, LinkedList<Segment>> pathHashMap = new HashMap<Pixel, LinkedList<Segment>>();
        Pixel[][] pixels = new Pixel[columns+1][rows+1];
        for (int i=0; i<=columns; i++) {
            for (int j=0; j<=rows; j++) {
                pixels[i][j] = new Pixel(i,j);
            }
        }
        Segment currentSegment;
        for (int i=0; i<columns; i++) {
            for (int j=0; j<rows; j++) {
                if (maskColors[i][j] == 1) {
                    if (j>0 && maskColors[i][j-1]!=1 || j==0) {
                        currentSegment = new Segment(pixels[i][j], pixels[i+1][j]);
                        if (!pathHashMap.keySet().contains(pixels[i][j])) {
                            pathHashMap.put(pixels[i][j],new LinkedList<Segment>());
                        }
                        pathHashMap.get(pixels[i][j]).add(currentSegment);
                        if (!pathHashMap.keySet().contains(pixels[i+1][j])) {
                            pathHashMap.put(pixels[i+1][j], new LinkedList<Segment>());
                        }
                        pathHashMap.get(pixels[i+1][j]).add(currentSegment);
                    }
                    if (i<columns && maskColors[i+1][j]!=1 || i==columns-1) {
                        currentSegment = new Segment(pixels[i+1][j], pixels[i+1][j+1]);
                        if (!pathHashMap.keySet().contains(pixels[i+1][j])) {
                            pathHashMap.put(pixels[i+1][j],new LinkedList<Segment>());
                        }
                        pathHashMap.get(pixels[i+1][j]).add(currentSegment);
                        if (!pathHashMap.keySet().contains(pixels[i+1][j+1])) {
                            pathHashMap.put(pixels[i+1][j+1], new LinkedList<Segment>());
                        }
                        pathHashMap.get(pixels[i+1][j+1]).add(currentSegment);
                    }
                    if (i>0 && maskColors[i-1][j]!=1 || i==0) {
                        currentSegment = new Segment(pixels[i][j], pixels[i][j+1]);
                        if (!pathHashMap.keySet().contains(pixels[i][j])) {
                            pathHashMap.put(pixels[i][j],new LinkedList<Segment>());
                        }
                        pathHashMap.get(pixels[i][j]).add(currentSegment);
                        if (!pathHashMap.keySet().contains(pixels[i][j+1])) {
                            pathHashMap.put(pixels[i][j+1], new LinkedList<Segment>());
                        }
                        pathHashMap.get(pixels[i][j+1]).add(currentSegment);
                    }
                    if (j+1<rows && i+1<columns && maskColors[i][j+1]!=1 || j==rows-1) {
                        currentSegment = new Segment(pixels[i][j+1], pixels[i+1][j+1]);
                        if (!pathHashMap.keySet().contains(pixels[i][j+1])) {
                            pathHashMap.put(pixels[i][j+1],new LinkedList<Segment>());
                        }
                        pathHashMap.get(pixels[i][j+1]).add(currentSegment);
                        if (!pathHashMap.keySet().contains(pixels[i+1][j+1])) {
                            pathHashMap.put(pixels[i+1][j+1], new LinkedList<Segment>());
                        }
                        pathHashMap.get(pixels[i+1][j+1]).add(currentSegment);
                    }
                }
            }
        }
        return pathHashMap;

    }

    public static LinkedList<Pixel> buildPath(HashMap<Pixel, LinkedList<Segment>> pathHashMap) {
        LinkedList<Pixel> pathCoordinates = new LinkedList<Pixel>();
        if (pathHashMap.size()>0) {
            Map.Entry<Pixel, LinkedList<Segment>> entry = pathHashMap.entrySet().iterator().next();
            Pixel currentPixel = entry.getKey();
            pathCoordinates.add(currentPixel);
            LinkedList<Segment> segments = entry.getValue();
            Segment currentSegment;
            while (segments.size() > 0) {
                currentSegment = segments.getFirst();
                if (currentSegment.getP1() == currentPixel) {
                    currentPixel = currentSegment.getP2();
                    pathCoordinates.add(currentPixel);
                    segments.remove(currentSegment);
                    pathHashMap.get(currentPixel).remove(currentSegment);

                } else if (currentSegment.getP2() == currentPixel) {
                    currentPixel = currentSegment.getP1();
                    pathCoordinates.add(currentPixel);
                    segments.remove(currentSegment);
                    pathHashMap.get(currentPixel).remove(currentSegment);
                }

                segments = pathHashMap.get(currentPixel);
            }
        }


        return pathCoordinates;
    }

}
