/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410 core


out vec4 fragColor;

uniform sampler2D blurred;
uniform float alpha;
in vec2 texCoordFBO;
uniform vec3 color;

void main()
{


    fragColor = texture(blurred, texCoordFBO);
    fragColor.a = fragColor.r*alpha;
    fragColor.rgb = color;


}
