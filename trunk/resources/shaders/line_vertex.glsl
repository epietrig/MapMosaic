/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 330 core

in vec4 position;
in vec2 normal;
uniform mat4 mv_matrix;
uniform mat4 proj_matrix;
uniform float u_linewidth;
uniform vec2 offset;
float linewidth = 5.0f;

void main(void)
{

    vec4 newpos = vec4(position.x + offset.x, position.y +offset.y, position.z,1.0);
    vec4 delta = vec4(normalize(normal) * u_linewidth, 0, 0);
    delta = vec4(normalize(normal) * linewidth, 0, 0);
    vec4 pos = mv_matrix * vec4(newpos.xy,0,1);
    gl_Position = proj_matrix * mv_matrix * position;


}