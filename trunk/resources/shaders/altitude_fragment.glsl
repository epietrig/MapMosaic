/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

in vec2 tc;
out vec4 fragColor;
uniform sampler2D s;
in vec4 color;
uniform vec3 startColorRGB;
uniform vec3 endColorRGB;
vec3 HSVColor;

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 interpolateRGB(vec3 color) {
     vec3 newColor;
     newColor.r = (1-color.r)*startColorRGB.r+color.r*endColorRGB.r;
     newColor.g = (1-color.r)*startColorRGB.g+color.r*endColorRGB.g;
     newColor.b = (1-color.r)*startColorRGB.b+color.r*endColorRGB.b;
     return newColor;
}

void main()
{
     vec3 startColorHSV = rgb2hsv(startColorRGB);
     vec3 endColorHSV = rgb2hsv(endColorRGB);
     vec3 textureColor = texture(s,tc).rgb;
     HSVColor.x = textureColor.r/(endColorHSV.x - startColorHSV.x)+startColorHSV.x;
     HSVColor.y = textureColor.g/(endColorHSV.y - startColorHSV.y)+startColorHSV.y;
     HSVColor.z = textureColor.b/(endColorHSV.z - startColorHSV.z)+startColorHSV.z;
     HSVColor.y = endColorHSV.y;
     HSVColor.z = endColorHSV.z;
     fragColor.rgb = hsv2rgb(HSVColor);
     fragColor.rgb = interpolateRGB(textureColor);
     fragColor.a = 1.0;



}

