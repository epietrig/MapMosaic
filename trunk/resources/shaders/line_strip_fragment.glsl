/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410 core

uniform vec3 color;
uniform float alpha;
out vec4 fragColor;

void main()
{
    fragColor =  vec4(color.rgb,alpha);

}